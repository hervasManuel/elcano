#include <cstdio>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <MapSerializer.h>
#include <Map.h>
#include <KeyFrame.h>
#include <MapPoint.h>

using namespace std;
using namespace PTAMM;

/* This is how PTAMM's levels work:
   0 - High Resolution: 640x480
   1 -
   2 -
   3 - Low Resolution: 80x

At the moment, we only use high resolution since
we just load those images (remember, we use
readImage of OpenCV) :)

 */

/* Headers */
void checkArguments(int argc, char** argv, std::string* mapId);
void init(std::vector<Map*>* maps, MapSerializer* mapSerializer, std::string& mapId,std::vector<IplImage*>* keyFrames,std::vector<cv::Mat*>* matKeyFrames);
void showKeyFrame(int keyFrameId,IplImage* keyFrame, std::vector<Map*>* maps);
int showMenu();
void saveMap(MapSerializer* mapSerializer,std::vector<Map*>* maps,std::string mapId);
void setDistance(int keyFrameId,int k1, int k2, float distance, std::vector<Map*>*maps);
float getDistance(int keyFrameId,int k1, int k2, std::vector<Map*>*maps);
void quit();

int main(int argc, char** argv){
  std::string mapId;
  /*Reading arguments*/
  /* TODO que funcionen los nombres de mapas */
  checkArguments(argc, argv, &mapId);
  std::cout<<"Map Id is "<<mapId<<std::endl;

  setlocale (LC_NUMERIC, "C");

  /* Declaring variables */
  std::vector<Map*> maps;
  MapSerializer* mapSerializer=NULL;
  int keyFrameId=0;
  std::vector<IplImage*> keyFrames;
  std::vector<cv::Mat*> matKeyFrames;

  /* Initializing structures */
  init(&maps,mapSerializer,mapId,&keyFrames,&matKeyFrames);

  showKeyFrame(keyFrameId,keyFrames.at(keyFrameId),&maps);


  /* Main loop */
  int exit = 0;

  do{
    int option = showMenu();

    switch(option){
    case 0: exit=1; break;
    case 1: keyFrameId=(keyFrameId+1)%maps.at(0)->vpKeyFrames.size();
      std::cout<<"Actual KeyFrame: "<<keyFrameId<<std::endl;
      showKeyFrame(keyFrameId,keyFrames.at(keyFrameId),&maps); break;
    case 2:
      (--keyFrameId==-1)?keyFrameId+=maps.at(0)->vpKeyFrames.size():keyFrameId=keyFrameId%maps.at(0)->vpKeyFrames.size();
      std::cout<<"Actual KeyFrame: "<<keyFrameId<<std::endl;
      showKeyFrame(keyFrameId,keyFrames.at(keyFrameId),&maps); break;
    case 3:{
      int k1, k2;
      float distance;
      std::cout<<"First KeyPoint> "<<std::endl;
      std::cin>>k1;
      std::cout<<"Second KeyPoint> "<<std::endl;
      std::cin>>k2;
      std::cout<<"Actual distance in metres> "<<std::endl;
      std::cin>>distance;
      setDistance(keyFrameId,k1,k2,distance,&maps);
      break;
    }
    case 4:{
      int k1, k2;
      std::cout<<"First KeyPoint> "<<std::endl;
      std::cin>>k1;
      std::cout<<"Second KeyPoint> "<<std::endl;
      std::cin>>k2;
      getDistance(keyFrameId,k1,k2,&maps);
      break;
    }
    case 5:
      saveMap(mapSerializer,&maps,mapId); break;
    default: break;
    }

  }while(!exit);


  return 0;
}

/* Functions implementation */
void checkArguments(int argc, char** argv, std::string* mapId){
  if(argc!=2){
    cout<<"Number of arguments is incorrect"<<endl;
    cout<<"Usage:"<<endl;
    cout<<"AspectRatio #mapName"<<endl<<endl;

    exit(0);
  }else{
    *mapId = std::string(argv[1]);
    if((*mapId)[mapId->size()-1]=='/'){
      mapId->erase(mapId->size()-1,mapId->size());
    }
  }
}

void init(std::vector<Map*>* maps, MapSerializer* mapSerializer, std::string& mapId,std::vector<IplImage*>* keyFrames,std::vector<cv::Mat*>* matKeyFrames){
  /* Initializing maps */
  maps->push_back(new Map());

  /* MapSerializer */
  mapSerializer = new MapSerializer(*maps);
  if( mapSerializer->Init("LoadMap", mapId, *maps->at(0)) ) {
    mapSerializer->start();
  }
  mapSerializer->join();
  delete mapSerializer;

  if(maps->at(0)->IsGood()){
    std::cout<<"Map loaded successfully!"<<std::endl;
  }else{
    std::cout<<"ERROR: Loading map 0!"<<std::endl;
    std::cout<<"Does it exist or is it corrupt?"<<std::endl;
    exit(0);
  }

  /* Sorry, but I read my own Images, is more confy! :) */
  for(unsigned int i=0;i<maps->at(0)->vpKeyFrames.size();i++){
    IplImage* ipl;
    std::stringstream ss;
    ss<<i;
    std::string number=ss.str();
    
    while(number.size()<6) number="0"+number;
    std::string path=std::string("./")+mapId+("/KeyFrames/")+number+std::string(".png");
    std::cout<<"Loading a KeyFrame in path "<<path<<std::endl;
    ipl=cvLoadImage(path.c_str());
    keyFrames->push_back(ipl);
    matKeyFrames->push_back(new cv::Mat(ipl));
    
    /* Draw Points & numbers */
    std::vector<MapPoint*>& points=maps->at(0)->vpPoints;
    int nPoint = 0;
    KeyFrame* currentKeyFrame = maps->at(0)->vpKeyFrames.at(i);
    for(unsigned int j=0;j<points.size();j++){
      MapPoint* p=points.at(j);
      KeyFrame* pointKeyFrame = p->pPatchSourceKF;
      //If is level 0 and the owner KeyFrame is the current KeyFrame */
      if(p->nSourceLevel==0 && currentKeyFrame == pointKeyFrame){
	nPoint++;
	cv::Mat* currentMatKF = matKeyFrames->at(i);

	/* Dibujar punto*/
	cv::Point pt = cv::Point(p->irCenter.x,p->irCenter.y);
	cv::Scalar color = cv::Scalar(0,0,255);
	cv::circle(*currentMatKF, pt, 2, color, 1);

	/* Dibujar número de punto j */
	std::stringstream ss2;
	ss2<<j;
	cv::putText(*currentMatKF,ss2.str(),pt,cv::FONT_HERSHEY_SIMPLEX,.3,cv::Scalar(255,0,0));
      }
    }

  }

  std::cout<<"Read "<<maps->at(0)->vpKeyFrames.size()<<" KeyFrames."<<std::endl;


  /* OpenCV */
  cvNamedWindow("Window",1);
  cvMoveWindow("Window", 100, 100);

}



void saveMap(MapSerializer* mapSerializer, std::vector<Map*>* maps,std::string mapId){
  mapSerializer = new MapSerializer(*maps);
  if(mapSerializer->Init("SaveMap", mapId+std::string("new"), *maps->at(0))) {
    mapSerializer->start();
  }else{
    std::cout<<"ERROR: Saving map 0"<<std::endl;
  }

  mapSerializer->join();
  std::cout<<"Map saved successfully!! :)"<<std::endl;
  delete mapSerializer;
}

void quit(){
  cvDestroyWindow("Window");
}

void showKeyFrame(int keyFrameId,IplImage* keyFrame, std::vector<Map*>* maps){
  //Four maximum levels
  cvWaitKey(50);
  cvShowImage("Window",keyFrame);
  cvWaitKey(100);
}

int showMenu(){
  int option = -1;

  while(option==-1 || option>5){
    std::cout<<endl;
    std::cout<<"---Menu---"<<std::endl;
    std::cout<<"1. Next KeyFrame"<<std::endl;
    std::cout<<"2. Previous KeyFrame"<<std::endl;
    std::cout<<"3. Set distance between points"<<std::endl;
    std::cout<<"4. Get distance between points"<<std::endl;
    std::cout<<"5. Save configuration to map"<<std::endl;
    std::cout<<std::endl;
    std::cout<<"0. Exit"<<std::endl;
    std::cout<<"> ";

    std::cin>>option;

    std::cout<<"Option "<<option<<" chosen."<<std::endl;
  }

  return option;
}


void setDistance(int keyFrameId, int k1, int k2, float distance, std::vector<Map*>*maps){
  std::cout<<"Chosen KeyPoints: "<<k1<<" and "<<k2<<"."<<std::endl;
  std::cout<<"Actual distance in metres: "<<distance<<"."<<std::endl;
  std::cout<<"Performing rescale..."<<std::endl;

  float ptammDistance = getDistance(keyFrameId,k1,k2,maps);
  float ratio = distance/ptammDistance;
  
  for(unsigned int i=0;i<maps->at(0)->vpKeyFrames.size();i++){
    maps->at(0)
      ->vpKeyFrames[i]->se3CfromW.get_translation()*=ratio;
  }

  std::vector<MapPoint*>& points=maps->at(0)->vpPoints;
  for(unsigned int j=0;j<points.size();j++){
    MapPoint* p=points.at(j);
    p->v3WorldPos[0]*=ratio;
    p->v3WorldPos[1]*=ratio;
    p->v3WorldPos[2]*=ratio;

    p->v3PixelRight_W[0] *= ratio;
    p->v3PixelRight_W[1] *= ratio;
    p->v3PixelRight_W[2] *= ratio;

    p->v3PixelDown_W[0] *= ratio;
    p->v3PixelDown_W[1] *= ratio;
    p->v3PixelDown_W[2] *= ratio;

    p->RefreshPixelVectors();
  }

  std::cout<<"Done! Don't forget saving the configuration! :)"<<std::endl;
}

float getDistance(int keyFrameId,int k1, int k2, std::vector<Map*>*maps){
  MapPoint* kp1 = maps->at(0)->vpPoints[k1];
  MapPoint* kp2 = maps->at(0)->vpPoints[k2];

  float distance = sqrt(pow(kp1->v3WorldPos[0]-kp2->v3WorldPos[0],2)+
			pow(kp1->v3WorldPos[1]-kp2->v3WorldPos[1],2)+
			pow(kp1->v3WorldPos[2]-kp2->v3WorldPos[2],2));

  std::cout<<"The PTAMM distance is, more or less, "<<distance<<" metres."<<std::endl;

  return distance;
}
