#include <stdio.h>
#include <iostream>
#include <Ogre.h>
#include <Fine.h>
#include <core/Logger.h>
#include <core/video/VideoSource.h>
#include <core/video/VideoCaptureFactory.h>
#include <core/video/VideoEnumerate.h>
#include <core/tracking/TrackingMethodFactory.h>


using namespace std;
using namespace mars;

class OgreFrameListener;


Ogre::RenderWindow* window=NULL;
Ogre::Camera* cam=NULL;
Fine* fine = NULL;
mars::TrackingController* tC=NULL;
Ogre::SceneManager* sceneManager=NULL;
Ogre::Root* root=NULL;
VideoSource* vD_user=NULL;
int mapId=-1;


void initOgre();
void postInit();
void createBackground();
void loadResources();
void createScene();


int main(int argc, char* argv[]){
 /* ---- Getting the Map Id ------ */
  if(argc!=2){
    std::cout<<"Usage: ./build/PTAMMLocator #mapId"<<std::endl;
    return 0;
  }

  mapId = atoi(argv[1]);
  if(mapId<0){
    std::cout<<"Invalid Map Id :( FUCK  YOU!"<<std::endl;
    return 0;
  }

  /*----------------------------*/
  /* Initializing Video Devices */
  /*----------------------------*/
  
  if(Configuration::getInstance()->useVideoInput()!=""){
    Logger::getInstance()->note("Using video file input.");
    mars::VideoCapture* vC_opencv = VideoCaptureFactory::getFactory()->createVideoCapture(OPENCV);
    
    //vC_opencv->addNewVideoSource("video/video.mpeg", "user");
    vC_opencv->addNewVideoSource(Configuration::getInstance()->useVideoInput(), "user");
    vD_user = vC_opencv->getVideoFile("user");

  }else if(Configuration::getInstance()->useUEye()){
    /* UEYE */
    Logger::getInstance()->note("Using UEye device.");
    mars::VideoCapture* vC = VideoCaptureFactory::getFactory()->createVideoCapture(UEYE);
    if (vC ==NULL) {
      exit(-1);
    }
    
    vD_user = vC->getVideoDevice(0);
  
  }else{
    /* OPENCV */
    Logger::getInstance()->note("Using OpenCV device.");
    mars::VideoCapture* vC_opencv = VideoCaptureFactory::getFactory()->createVideoCapture(OPENCV);;
    vector<string> deviceNames = getDevVideoCaptureNames();
    if (deviceNames.size() < 1){
      Logger::getInstance()->error("Se necesita al menos una cámara para esta demo");
      return 0;
    } else {
      
      unsigned firstVideoDevice = atoi(& deviceNames.at(0)[getDevVideoCaptureNames().at(0).size()-1]);
      
      /*OpenCv Camera*/
      vC_opencv->addNewVideoSource(firstVideoDevice, "user"); // Let's add a videosource
      vD_user = vC_opencv->getVideoDevice("user");
    }    
  }
  
  if (vD_user == NULL) {
    Logger::getLogger()->error("No device or video loaded. HALTING.");
    return 0;
  }else{
    initOgre();
  
    Ogre::Matrix4 mat=vD_user->getOgreProjectionMatrix();

    /* Hack */
    mat[2][3]*=-1;
    cam->setCustomProjectionMatrix(true,mat);
  }


  /*-------------------------------*/
  /* Initializing Tracking Methods */
  /*-------------------------------*/
  tC=mars::TrackingController::getController();

  if(Configuration::getInstance()->usePTAMM()){
    TrackingMethodFactory::getInstance()->createTrackingMethod("PTAMM",mars::PTAMM,vD_user);
  }

  if(Configuration::getInstance()->useARTK()){
    TrackingMethodFactory::getInstance()->createTrackingMethod("ARTK",mars::ARTK,vD_user);
  }

  /* Final initializations */
  postInit();
   
  bool running=true;
  while(running)
    {
      Ogre::WindowEventUtilities::messagePump();
      running = root->renderOneFrame();
    }

  return 0;
}



void postInit(){
  Ogre::Viewport* viewport = window->addViewport(cam);
  viewport->setBackgroundColour(Ogre::ColourValue(0.0,0.0,0.0));
  double width = viewport->getActualWidth();
  double height = viewport->getActualHeight();
  cam->setAspectRatio(width / height);
  
  tC->setCamera(cam);
  
  fine = new Fine(window, root, tC,vD_user, mapId);
  root->addFrameListener(fine);
  
  loadResources();
  createBackground();
  fine->createScene();

}

void createBackground(){
  mars::Logger::getInstance()->note("Waiting for the first frame of the camera...");
  cv::Mat* frame;
  do{
    frame =  vD_user->getLastFrame();
  }while(frame->rows==0);


  /* Ogre code */
  Ogre::TexturePtr texture=Ogre::TextureManager::getSingleton().createManual(
            "BackgroundTex",// name
	    Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
            Ogre::TEX_TYPE_2D,// texture type
	    frame->cols,
	    frame->rows,
            0,// number of mipmaps
    	    Ogre::PF_BYTE_BGRA,
	    Ogre::HardwareBuffer::HBU_DYNAMIC_WRITE_ONLY_DISCARDABLE
            );

  delete frame;

  Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().create("Background", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME); 
  material->getTechnique(0)->getPass(0)->createTextureUnitState(); 
  material->getTechnique(0)->getPass(0)->setSceneBlending(Ogre::SBT_TRANSPARENT_ALPHA);
  material->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false); 
  material->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false); 
  material->getTechnique(0)->getPass(0)->setLightingEnabled(false);   
  material->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setTextureName("BackgroundTex");

  // Create background rectangle covering the whole screen 
  Ogre::Rectangle2D* rect = new Ogre::Rectangle2D(true); 
  rect->setCorners(-1.0, 1.0, 1.0, -1.0); 
  rect->setMaterial("Background"); 
  
  // Render the background before everything else 
  rect->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);   
  
  // Hacky, but we need to set the bounding box to something big 
  // NOTE: If you are using Eihort (v1.4), please see the note below on setting the bounding box 
  rect->setBoundingBox(Ogre::AxisAlignedBox(-100000.0*Ogre::Vector3::UNIT_SCALE, 100000.0*Ogre::Vector3::UNIT_SCALE));   
  
  // Attach background to the scene 
  Ogre::SceneNode* node = sceneManager->getRootSceneNode()->createChildSceneNode("BackgroundNode"); 
  node->attachObject(rect);   
  
 }

void loadResources(){
  Ogre::ConfigFile cf;
  cf.load("config/resources.cfg");
  
  Ogre::ConfigFile::SectionIterator sI = cf.getSectionIterator();
  Ogre::String sectionstr, typestr, datastr;
  while (sI.hasMoreElements()) {
    sectionstr = sI.peekNextKey();
    Ogre::ConfigFile::SettingsMultiMap *settings = sI.getNext();
    Ogre::ConfigFile::SettingsMultiMap::iterator i;
    for (i = settings->begin(); i != settings->end(); ++i) {
      typestr = i->first;    datastr = i->second;
      Ogre::ResourceGroupManager::getSingleton().addResourceLocation
            (datastr, typestr, sectionstr);	
    }
  }
  Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}


void initOgre(){

  root = new Ogre::Root("config/plugins.cfg","config/ogre.cfg","Ogre.log");

#ifdef NDEBUG
  //Deactivating the Ogre Log Console
  Ogre::LogManager::getSingleton().getDefaultLog()->setDebugOutputEnabled(false);
#endif

  
  if(!root->restoreConfig()) {
    root->showConfigDialog();
    root->saveConfig();
  }
  
  window = root->initialise(true,"ElCano's FineTunning");
  sceneManager = root->createSceneManager(Ogre::ST_GENERIC,"SceneManager");

  cam = sceneManager->createCamera("MainCamera");
  cam->setPosition(Ogre::Vector3(15,10,15));
  cam->lookAt(Ogre::Vector3(0,0,0));
  cam->setNearClipDistance(0.01);
  cam->setFarClipDistance(1000);
}
