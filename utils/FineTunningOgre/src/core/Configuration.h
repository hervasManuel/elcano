/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * Configuration.h
 *
 */

#ifndef CONFIGURATION_H_
#define CONFIGURATION_H_

#define CONFIG_FILE "config/mars.cfg"

#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <cctype>
#include <cstdio>
#include <sstream>
#include <vector>

#include <core/Logger.h>
#include <core/tracking/ARTKMark.h>
#include <core/Singleton.h>
#include <core/Matrix16.h>

namespace mars {

/**
 * Structure that holds a Stream URL config
 */
struct urlStreamConfig{
  std::string name;
  std::string URL;
  std::string description;
};

/**
 *
 * The configuration class. This will read the config file and act consequently.
 *
 */
class Configuration : public Singleton<Configuration> {
public:

	static Configuration* getConfiguration();



	/**
	 * Returns the image configured as the default frame returned by a FramesBuffer
	 * @see FramesBuffer
	 */
	const std::string defaultImage(){ return _defaultImage; }

	/**
	 * Returns the default texture of a RenderableVideoPlane
	 *
	 * @see RenderableVideoPlane
	 */
	const std::string defaultVideoTexture(){ return _defaultVideoTexture; }

	/**
	 * Returns the path where mars will try to locate the textures.
	 *
	 * @see TextureFactory
	 */
	const std::string defaultTexturePath(){ return _defaultTexturePath; }

	/**
	 * Returns the path where mars will try to locate the videos.
	 */
	const std::string defaultVideoPath(){ return _defaultVideoPath; }


	/**
	 * ARTK RELATED
	 */
	const std::string ARTKDataPath(){ return _ARTKDataPath;}

	/**
	 *  Parses the ARTK Marks file
	 */
	void parseARTKPatternsFile(std::vector<ARTKMark>& marks);

	/**
	 * ARTK RELATED
	 */
	const std::string ARTKPatternsFile(){ return _ARTKPatternsFile;}

	/**
	 * Returns the path where mars will try to locate the sounds.
	 */
	const std::string defaultSoundPath(){ return _defaultSoundPath;}

	/**
	 * Configured as a full screen application?
	 */
	const bool openGLFullScreen(){ return _openGLFullScreen;}

	/**
	 * X resolution
	 */
	const int  openGLScreenWidth(){ return _openGLScreenWidth;}

	/**
	 * Y resolution
	 */
	const int  openGLScreenHeight(){ return _openGLScreenHeight;}

	/**
	 * Returns the depth of the video mode.
	 */
	const int  openGLScreenDepth(){ return _openGLScreenDepth;}


	/* New configuration */
	const bool useUEye(){ return __useUEye;}
	const bool usePTAMM(){ return __usePTAMM;}
	const bool useARTK(){ return __useARTK;}
	const bool usePhotoPos(){ return __usePhotoPos;}
	const bool useCommunications(){ return __useCommunications;}
	//const bool useVideoInput(){ return __useVideoInput;}
	const std::string& useVideoInput(){ return __useVideoInput;}
	const bool useVideoStreaming(){ return __useVideoStreaming;}
	const int numPTAMMMaps(){return __numPTAMMMaps;}



private:

	Configuration();
	virtual~ Configuration();

	void parseConfig(); ///< Parses the config file
	void addproperty(std::string property, std::string value, int nLine); ///< Parses the result of a line
	void addStreamURL(std::string csv, int nLine); ///< Adds a new Stream URL


	std::string _defaultImage;
	std::string _defaultVideoTexture;
	std::string _defaultTexturePath;
	std::string _defaultVideoPath;
	std::string _ARTKDataPath;
	std::string _ARTKPatternsFile;
	std::string _defaultSoundPath;
	bool _openGLFullScreen;
	int  _openGLScreenWidth;
	int  _openGLScreenHeight;
	int  _openGLScreenDepth;
	
	/* New config */
	bool __useUEye;
	bool __usePTAMM;
	bool __useARTK;
	bool __usePhotoPos;
	bool __useCommunications;
	//bool __useVideoInput;
	std::string __useVideoInput;
	bool __useVideoStreaming;
	int __numPTAMMMaps;

        std::ifstream __configFile; ///< input stream for the config file
        std::ifstream __patternsFile; ///< input stream for the patterns file

        std::vector<urlStreamConfig*> __streamsURL; ///< Vector of streams.

	friend class Singleton<Configuration>;
	friend class WidgetFunction;
	friend class ScriptingLUA;

};


}

#endif /* CONFIGURATION_H_ */
