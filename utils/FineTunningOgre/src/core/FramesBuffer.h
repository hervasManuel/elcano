/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/* This class holds the last frames from a VideoSource */

/*
 * FramesBuffer.h
 *
 */

#ifndef FRAMESBUFFER_H_
#define FRAMESBUFFER_H_

#include <iostream>
#include <vector>
#include <opencv/cv.h>
#include <pthread.h>

#include <sstream>



using namespace cv;

namespace mars {
/**
 *  This class holds the last frames from a VideoSource.
 *  It's used for storing the frames and assuring the access
 *  under mutual exclusion (mutex).
 *
 */
class FramesBuffer {
public:
	/**
	 * Constructor that will create a 5 frames vector.
	 */
	FramesBuffer();


	/**
	 * Alternative constructor.
	 * This constructor will create a FramesBuffer with the number of
	 * frames given by the argument.
	 * @param maxSavedFrames the number of frames that it will hold.
	 *
	 */
	FramesBuffer(const unsigned& maxSavedFrames);


	/**
	 *  A member that adds a new frame.
	 *  This member takes a cv::Mat as an argument and stores it for
	 *  its further use.
	 *  @param newFrame the cv::Mat representing an image.
	 */
	void addFrame(Mat* newFrame);


	/**
	 * Returns a frame.
	 * Returns the frame at the position of the given argument.
	 *
	 * @param nFrame the number of frame to be returned.
	 */
	cv::Mat* getFrame(const unsigned& nFrame);

	/**
	 * Returns the last frame.
	 */
	cv::Mat* getLastFrame();

	unsigned size();

	/**
	 * Returns true when there's no frames at all.
	 */
	bool isEmpty();

	/**
	 * Destructor
	 */
	virtual ~FramesBuffer();


private:

	vector<cv::Mat*> __frames; ///< This is the vector of frames.
	unsigned __maxSavedFrames; ///< The number of last frames saved.
	pthread_mutex_t mutex_frame; ///< The mutex variable.

#ifndef NDEBUG
	string mutex_frame_str; ///< Mutex internal number (only for extra DEBUG builds).
#endif

	/**
	 * Locks the access to the frames vector.
	 */
	void lock();

	/**
	 * Unlocks the access to the frames vector.
	 */
	void unlock();



};

}

#endif /* FRAMESBUFFER_H_ */
