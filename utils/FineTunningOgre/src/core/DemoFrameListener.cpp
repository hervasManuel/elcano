#include "DemoFrameListener.h" 


DemoFrameListener::DemoFrameListener(Ogre::RenderWindow* win,Ogre::Root* r,mars::TrackingController* tC, mars::VideoSource* v) {
  unsigned int windowHandle;  
  std::ostringstream wHandleStr;
  OIS::ParamList param;  

  _root = r;
  _forceExit=false;

  _fps = .0f;
  _vD_user = v;
  _tC = tC;
  _win = win;
  win->getCustomAttribute("WINDOW", &windowHandle);
  Ogre::WindowEventUtilities::addWindowEventListener(win, this);
  wHandleStr << windowHandle;
  param.insert(std::make_pair("WINDOW", wHandleStr.str()));    

  _sceneManager = r->getSceneManager("SceneManager");
  _overlayManager = Ogre::OverlayManager::getSingletonPtr();
  _camera = _tC->getCamera();


  _inputManager = OIS::InputManager::createInputSystem(param);
  _keyboard = static_cast<OIS::Keyboard*>
    (_inputManager->createInputObject(OIS::OISKeyboard, false));
    
  /* Tracking methods */
  _tPTAMM=NULL;
  _tARTK=NULL;
     
  if(mars::Configuration::getInstance()->usePTAMM()){
    _tPTAMM=(mars::TrackingMethodOAbsolutePTAMM*) mars::TrackingMethodFactory::getInstance()->__tMethods.find("PTAMM")->second;
  }

  if(mars::Configuration::getInstance()->useARTK()){
    _tARTK=(mars::TrackingMethodOAbsoluteArtkBz*) mars::TrackingMethodFactory::getInstance()->__tMethods.find("ARTK")->second;
  }

  
}

void DemoFrameListener::refreshBackground(){
  cv::Mat* frame;
  frame =  _vD_user->getLastFrame();
  if(frame->rows==0) return;
    
  
  Ogre::TexturePtr texture = Ogre::TextureManager::getSingleton().getByName("BackgroundTex",Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);

    
  Ogre::HardwarePixelBufferSharedPtr pixelBuffer = texture->getBuffer();
    
  pixelBuffer->lock(Ogre::HardwareBuffer::HBL_DISCARD);
  const Ogre::PixelBox& pixelBox = pixelBuffer->getCurrentLock();
    
  Ogre::uint8* pDest = static_cast<Ogre::uint8*>(pixelBox.data);
    
  for(int j=0;j<frame->rows;j++) {
    for(int i=0;i<frame->cols;i++) {
      int idx = ((j) * pixelBox.rowPitch + i )*4; //Use pixelBox.rowPitch instead of "width"
      pDest[idx] = frame->data[(j*frame->cols + i)*3];
      pDest[idx + 1] = frame->data[(j*frame->cols + i)*3 + 1];
      pDest[idx + 2] = frame->data[(j*frame->cols + i)*3 + 2];
      pDest[idx + 3] = 255;
    }
  }
    
  pixelBuffer->unlock();
    

  Ogre::Rectangle2D* rect = static_cast<Ogre::Rectangle2D*>(_sceneManager->getSceneNode("BackgroundNode")->getAttachedObject(0));
  Ogre::MaterialPtr material = rect->getMaterial();

  delete frame;
}

void DemoFrameListener::refreshInput()
{
  _keyboard->capture();

  /* Unbuffered :S*/
  keyPressed(OIS::KeyEvent(NULL,0,0));

}



bool DemoFrameListener::frameStarted(const Ogre::FrameEvent& evt) {
  if(_forceExit) return false;

  _tC->computeAllMethods();
  refreshBackground();
  refreshInput();

  _fps = 1.0 / evt.timeSinceLastFrame;

  return demoFrameStarted(evt);
} 


//Keyboard events
bool DemoFrameListener::keyPressed(const OIS::KeyEvent &arg)
{
  demoKeyPressed(arg);
  return true;

}

bool DemoFrameListener::keyReleased(const OIS::KeyEvent &arg){}

void DemoFrameListener::windowClosed(Ogre::RenderWindow* win)
{
  _forceExit=true;
}

DemoFrameListener::~DemoFrameListener() {
  delete _root;
}
