#ifndef RENDERABLEOGREMAP_H_
#define RENDERABLEOGREMAP_H_

#include <iostream>
#include <vector>
#include <map>
#include <Ogre.h>
#include <core/Logger.h>
#include <core/Configuration.h>
#include <core/Logger.h>

class RenderableOgreMap{
  // <Method name, Pointer name>
  std::map<std::string,Ogre::Overlay*> _methods;

  Ogre::Overlay* _overlay;
  Ogre::OverlayContainer* _container;

  const float _scale;
  int _startX, _startY;
  int _height, _width;
  int _left, _top;
  float _lineWidth;

  /* Path variables */
  Ogre::ManualObject* _path;


  void initPath();

 public:
  RenderableOgreMap(std::string mapName, int startX, int startY, float scale);
  ~RenderableOgreMap();

  /* Tracking methods methods */
  void addMethod(std::string trackingName, std::string pointerName);
  void setMethodPosition(std::string name, const Ogre::Vector3 &pos);
  void setMethodAngle(std::string name, float angle);

  void show();
  void hide();


  /* Path methods */
  void resetPath();
  void setPoints(const std::vector<Ogre::Vector3> &v);
  void setPathLineWidth(const float& w);
  
  
};

#endif /* RENDERABLEOGREMAP_H_ */
