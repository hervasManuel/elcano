/*
 * Matrix16.h
 *
 */

#ifndef MATRIX16_H_
#define MATRIX16_H_

#include <GL/gl.h>
#include <iostream>
#include <iomanip>
#include <opencv/cv.h>
#include <core/Logger.h>

#include <core/TUserL.h>

namespace mars {

struct Matrix16 {
public:
	/**
	 *
	 * Default constructor
	 *
	 */
	Matrix16();

	/**
	 * Constructor that takes an openCV matrix and creates a Matrix16 based on that.
	 *
	 * @param m openCV Mat
	 */
	Matrix16(cv::Mat& m);

	/**
	 * Constructor accepting a point to a float array (16 positions)
	 *
	 * @param m 16 floats :)
	 */
	Matrix16(GLfloat* m);

        /**
	 * Constructor accepting a tUserL
	 *
	 * @param m 16 MARS tUserL
	 */
	Matrix16(tUserL* t);


	/**
	 * Desctructor
	 */
	virtual ~Matrix16();

	/**
	* Returns the associated translation (points to a 3 float array)
	*/
	GLfloat* getTranslation();

	/**
	 * Returns a pointer to the data of the matrix, i.e. the values
	 */
	GLfloat* getData();

	/**
	 * Sets the value of the Matrix
	 *
	 * @param m Pointer to 16 GLfloats
	 */
	void setData(GLfloat* m);

	/**
	 * Used to print a matrix.
	 *
	 * E.g:
	 *   Matrix16 m;
	 *
	 *   std::cout << m << endl;
	 *
	 */
	friend std::ostream& operator<<(std::ostream& os, const Matrix16& m);


	/**
	 * Static method that prints the value of the current OpenGL ModelView Matrix
	 */
	static void printModelView();


private:
	GLfloat data[16]; ///< The data itself



};

}

#endif /* MATRIX16_H_ */
