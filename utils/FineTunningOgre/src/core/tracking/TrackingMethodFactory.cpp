/*
 * TrackingMethodFactory.cpp
 *
 */

#include "TrackingMethodFactory.h"

namespace mars {

TrackingMethod* TrackingMethodFactory::createTrackingMethod(const std::string& name,
                                                            trackingMethodType type,
                                                            VideoSource* vS1){
	tmIt it;
	it = __tMethods.find(name);

	if (it != __tMethods.end()){
		Logger::getInstance()->warning("TrackingMethodFactory:: Tracking Method "
				"name already exists. Returning it.");
		return it->second;
	}

	TrackingController* tC = TrackingController::getInstance();

	switch (type){
		case ARTK:
		{
			TrackingMethodOAbsoluteArtkBz* temp;
			temp = new TrackingMethodOAbsoluteArtkBz(vS1);
			__tMethods[name] = temp;
			tC->publishMethodAbsolute(temp);
			return temp;
		}
		// case PIXELFLOW:
		// {
		// 	TrackingMethodORelativePixelFlow* temp;
		// 	temp = new TrackingMethodORelativePixelFlow(vS1);
		// 	__tMethods[name] = temp;
		// 	tC->publishMethodRelative(temp);
		// 	return temp;
		// }

	        case PTAMM:{
		  TrackingMethodOAbsolutePTAMM* temp;
		  temp = new TrackingMethodOAbsolutePTAMM(vS1);
		  __tMethods[name] = temp;
		  tC->publishMethodAbsolute(temp);
		  return temp;
		}

	        // case PHOTOPOS:
		// {
		//         TrackingMethodOAbsolutePhotoPos* temp;
		// 	temp = new TrackingMethodOAbsolutePhotoPos(vS1);
		// 	__tMethods[name] = temp;
		// 	tC->publishMethodAbsolute(temp);
		// 	return temp;

		// }
	}

	return NULL;

}


TrackingMethodFactory::TrackingMethodFactory() { }

TrackingMethodFactory::~TrackingMethodFactory() { }

}
