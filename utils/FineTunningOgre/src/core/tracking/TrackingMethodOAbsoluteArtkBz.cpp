/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * TrackingMethodOAbsoluteArtkBz.cpp
 *
 */

#include "TrackingMethodOAbsoluteArtkBz.h"

namespace mars {

/* vS_0 is supposed to be the main camera */
TrackingMethodOAbsoluteArtkBz::TrackingMethodOAbsoluteArtkBz(VideoSource* vS_0){
	__methodName="ARTK-BZ ABSOLUTE TRACKING METHOD";


	if(vS_0==NULL){
		Logger::getLogger()->error("TrackingMethodOAbsoluteArtkBz: Error in ARTK tracking method: video source 0 is NULL");
		exit(0);
	}

	addVideoSource(vS_0);
	__uCam=vS_0;

	/*Initialize*/
	__userMarks=new vector<ARTKMark>;
	__eval=0;
	__globalTrans=new cv::Mat(4,4,CV_32F);
	__actuationMarksFound=0;

	__artkDetector=new PatternsDetectorArtk();
	Logger::getInstance()->note("TrackingMethodOAbsoluteArtkBz: Reading ARTK patterns configuration.");
	mars::Configuration::getConfiguration()->parseARTKPatternsFile(*__userMarks);
 }


void TrackingMethodOAbsoluteArtkBz::loopThread(){
  /*Main loop*/

  /*Reseting the matrix*/
  for(unsigned int i=0;i<4;i++)
    for(unsigned int j=0;j<4;j++)
      __globalTrans->at<float>(i,j)=0.;

  //Trying to find location.
  //
  if(locateCamera(__globalTrans,&__eval)){
    __detecting=true;

    float* d=Mat2Gl(__globalTrans);
    tUserL t;
    Matrix16 m(d);
    delete d;

    t=Mat2TUserL(m,__eval);

    addRecord(t);
  }else{
    __detecting = false;
  }
}

/*Try to locate the camera just using the frames captured*/
/*Global evaluation is the max of the local evaluations*/
bool TrackingMethodOAbsoluteArtkBz::locateCamera(cv::Mat* globalTrans,float* globalEval){
    cv::Mat* frame=NULL;
      int marksFound=0; /*Marks for positioning (position and user marks)*/
      int actuationMarksFound = 0;
    float sumEval=0;

    *globalEval=0;
   
    /*-------------------------------------------PROCESS USER CAM-----------------------------------------*/
    __artkDetector->changeCamera(__uCam);
    //__artkDetector->resetMarks();

    //Grab the current frame
     if (!__done && __uCam!=NULL)
       frame=__uCam->getLastFrame();

     if(frame->rows==0){
       return false;
     }

     
     /* HIDE THE RENDERABLES! :S */
     
     
    // /*Detecting ARTK patterns for the user cam*/
     if(__artkDetector->detect(frame,__userMarks,&__userHistory));

    for(unsigned int percep=0;percep<__userMarks->size();percep++){
      
      if(__userMarks->at(percep).isDetected()){ //Pattern detected
       	/*Actuation mark*/
	if(__userMarks->at(percep).isActuationMark()){
    	  processActuationPattern(&(__userMarks->at(percep))); /*-------Logic of actuacion marks----------*/
	  actuationMarksFound++;
    	}

	/*Position mark*/
    	if(__userMarks->at(percep).isPositionMark()){
    	  marksFound+=1;
    	  /*Acumulate eval factors*/
    	  float localEval=__userMarks->at(percep).getEval(10,10);
   	  *globalEval=*globalEval<localEval?localEval:*globalEval;
    	  sumEval+=localEval;
    	}
      }else{
	
      }
    }

    /* Hiding renderables! */
    // if(actuationMarksFound==0){
    //   if(RenderableConstructor::getInstance()->getVideoPlane("VIDEO")!=NULL){
    // 	RenderableConstructor::getInstance()->getVideoPlane("VIDEO")->hide();
    // 	//RenderableConstructor::getInstance()->getVideoPlane("VIDEO")->stop();
    //   }
    // }
    __actuationMarksFound = actuationMarksFound;

    delete frame;

    /*----------------------------------------------PROCESS EXTERNAL CAMS-------------------------------------*/

    /*for(unsigned int ncam=0;ncam<__externCameras.size();ncam++){
      VideoSource* extCam=__externCameras.at(ncam);
      vector<ARTKMark>* extCamMarks=__extCamsMarks.at(ncam);

      __artkDetector->changeCamera(extCam);

      //Grab the current frame
      if (!__done && extCam!=NULL)
    	frame=extCam->getLastFrame();

      if(frame->rows==0){
	return false;
      }
    */

      /*Detecting ARTK patterns for the user cam*/
      /*if(__artkDetector->detect(frame,extCamMarks,&(__extCamsHistory.at(ncam))));

      for(unsigned int percep=0;percep<extCamMarks->size();percep++){
    	if(extCamMarks->at(percep).isUserMark() && extCamMarks->at(percep).isDetected()){ //User pattern detected
	marksFound+=1;*/
	    /*User mark*/
    	    /*Acumulate eval factors*/
    	    /*float localEval=extCamMarks->at(percep).getEval(10,10);
    	    *globalEval=*globalEval<localEval?localEval:*globalEval;
    	    sumEval+=localEval;

    	}
      }

      delete frame;
      }*//*End for external cams*/



    /*--------------------------------------Creating the particle of the user location---------------------------------*/

    if(marksFound>0){
      /*--------------Process user camera marks-----*/
      for(unsigned int percep=0;percep<__userMarks->size();percep++){
	if(__userMarks->at(percep).isDetected() && __userMarks->at(percep).isPositionMark()){
	  /*---------Position mark (from User Camera)--------------*/
	  cv::Mat* markGlobalTrans;
	  markGlobalTrans = new cv::Mat(__userMarks->at(percep).getRelativeMatrix()->inv() * *(__userMarks->at(percep).getPosMatrix()));
	  //markGlobalTrans=new cv::Mat(__userMarks->at(percep).getRelativeMatrix()->inv() * *(__userMarks->at(percep).getPosMatrix()));

	  float markEval=__userMarks->at(percep).getEval(10,10);
	  //*globalTrans=(*globalTrans)+ *markGlobalTrans * markEval/sumEval;
	  *globalTrans=(*globalTrans)+ *markGlobalTrans * markEval/sumEval;

	  //delete markGlobalTrans;
	}
      }


      /*--------------Process external camera marks-----*/
	  /*---------User mark (from External Cameras--------------*/
      /*for(unsigned int ncam=0;ncam<__externCameras.size();ncam++){
	vector<ARTKMark>* extCamMarks=__extCamsMarks.at(ncam);
	VideoSource* extCam=__externCameras.at(ncam);

	for(unsigned int percep=0;percep<extCamMarks->size();percep++){
	   if(extCamMarks->at(percep).isUserMark() && extCamMarks->at(percep).isDetected()){
	     //cv::Mat* markGlobalTrans;
	     cv::Mat posMatrixHC = cv::Mat(4,4,CV_32F);
    posMatrixHC.at<float>(0,0)=0; posMatrixHC.at<float>(0,1)=0; posMatrixHC.at<float>(0,2)=-1; posMatrixHC.at<float>(0,3)=0;
    posMatrixHC.at<float>(1,0)=0; posMatrixHC.at<float>(1,1)=1; posMatrixHC.at<float>(1,2)=0; posMatrixHC.at<float>(1,3)=0;
    posMatrixHC.at<float>(2,0)=1; posMatrixHC.at<float>(2,1)=0; posMatrixHC.at<float>(2,2)=0; posMatrixHC.at<float>(2,3)=0;
    posMatrixHC.at<float>(3,0)=0.87; posMatrixHC.at<float>(3,1)=1.90; posMatrixHC.at<float>(3,2)=16.3093; posMatrixHC.at<float>(3,3)=1;

    cv::Mat markGlobalTrans(*extCamMarks->at(percep).getRelativeMatrix() * posMatrixHC);
	     //	     cv::Mat markGlobalTrans(*extCamMarks->at(percep).getRelativeMatrix() * (*extCam->getPosMatrix()));
	     //markGlobalTrans=new cv::Mat(*extCamMarks->at(percep).getRelativeMatrix() * (*extCam->getPosMatrix()));
	     float markEval=extCamMarks->at(percep).getEval(10,10);
	     globalTrans=(*globalTrans)+ *markGlobalTrans * markEval/sumEval;
	     *globalTrans=(*globalTrans)+ markGlobalTrans * markEval/sumEval;
	     //delete markGlobalTrans;
	  }
	}
	}*/



      return true;
    }
    return false;
  }

  /*Logic of actuation marks*/
bool TrackingMethodOAbsoluteArtkBz::processActuationPattern(ARTKMark* mark){
  //std::cout<<"Detectando marca: "<<mark->getId()<<std::endl;
  Matrix16 m;
  cv::Mat mat(*(mark->getRelativeMatrix()));
  float* d = Mat2Gl(&mat);
  m.setData(d);
  delete d;

  //APLICAMOS UNA ROTACIÓN DE 180º SOBRE EL EJE Y (EN LA CÁMARA, EL SENTIDO DEL EJE Z ES AL CONTRARIO!.. cosas del gluLookAt)
  m.getData()[0]*=-1;
  m.getData()[2]*=-1;
  m.getData()[4]*=-1;
  m.getData()[6]*=-1;
  m.getData()[8]*=-1;
  m.getData()[10]*=-1;
  m.getData()[12]*=-1;
  m.getData()[14]*=-1;
  //empty->setMatrix(m);ç

  // switch(mark->getId()){
    
  // case 0:{ 
  //   if(RenderableConstructor::getInstance()->getVideoPlane("VIDEO")==NULL) return true;
  //   RenderableConstructor::getInstance()->getVideoPlane("VIDEO")->show();
  //   //RenderableConstructor::getInstance()->getVideoPlane("VIDEO")->play();
  //   RenderableConstructor::getInstance()->getVideoPlane("VIDEO")->setMatrix(m);
  //   break;
  // }
  
  // }
  
  return true;
}

void TrackingMethodOAbsoluteArtkBz::addExternalCamera(VideoSource* vS){
  /*Adding the marks vector*/
  vector<ARTKMark>* marksVector=new vector<ARTKMark>;
  for(unsigned int i=0;i<__userMarks->size();i++){
    ARTKMark* mark=&__userMarks->at(i);
    Matrix16 m((GLfloat*)mark->getPosMatrix()->data);
    marksVector->push_back(*(new ARTKMark(m,mark->getFile(),mark->getSize(),mark->getType() )));
   }

  __extCamsMarks.push_back(marksVector); //Adding the marks vector

  __extCamsHistory.push_back(ARTKHistory()); //Adding history

  __externCameras.push_back(vS);//Adding the videosource

  Logger::getInstance()->note("TrackingMethodOAbsoluteArtkBz: Added a external video source");
}

vector<ARTKMark>* TrackingMethodOAbsoluteArtkBz::getUserMarks(){
  return __userMarks;
}


float* TrackingMethodOAbsoluteArtkBz::Mat2Gl(cv::Mat* mat){
	float *gl;

	gl=new float[16];
	for(uint i=0;i<4;i++){
		for(uint j=0;j<4;j++){
			gl[i*4+j]=mat->at<float>(i,j);
		}
	}
	return  gl;
}

Mat* TrackingMethodOAbsoluteArtkBz::Gl2Mat(float *gl){
  cv::Mat *mat;

  mat=new cv::Mat(4,4,CV_32F);
	for(uint i=0;i<4;i++){
		for(uint j=0;j<4;j++){
			mat->at<float>(i,j)=gl[i*4+j];
		}
	}
	return mat;
}

  void TrackingMethodOAbsoluteArtkBz::incrementThreshold(){
    int t=__artkDetector->getThreshold();
    t+=10;
    if(t<256) __artkDetector->setThreshold(t);
    else __artkDetector->setThreshold(255);
  }
  void TrackingMethodOAbsoluteArtkBz::decrementThreshold(){
    int t=__artkDetector->getThreshold();
    t-=10;
    if(t>0) __artkDetector->setThreshold(t);
    else __artkDetector->setThreshold(0);

  }



TrackingMethodOAbsoluteArtkBz::~TrackingMethodOAbsoluteArtkBz(){
  delete __globalTrans;
  delete __userMarks;
  delete __artkDetector;
  for(unsigned int i=0;i<__extCamsMarks.size();i++)
    delete __extCamsMarks.at(i);


  Logger::getLogger()->note("TrackingMethodOAbsoluteArtkBz: deleting tracking method. Bye!");
}


int TrackingMethodOAbsoluteArtkBz::actuationMarksFound(){
  return __actuationMarksFound;
}


}


