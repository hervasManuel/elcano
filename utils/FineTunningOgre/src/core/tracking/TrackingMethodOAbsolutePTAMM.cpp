/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * TrackingMethodOAbsolutePTAMM.cpp
 *
 */

#include "TrackingMethodOAbsolutePTAMM.h"

namespace mars {

  using namespace PTAMM;
  using namespace CVD;
  using namespace std;
  using namespace GVars3;

  /* vS_0 is supposed to be the main camera */
  //: mGLWindow( CVD::ImageRef(vS_0->getWidth(),vS_0->getHeight()), "PTAMM")
  TrackingMethodOAbsolutePTAMM::TrackingMethodOAbsolutePTAMM(VideoSource* vS_0)
  {
    
    // GUI.LoadFile("settings.cfg");  
    // GUI.StartParserThread(); // Start parsing of the console input
    // atexit(GUI.StopParserThread); 


    __methodName="PTAMM ABSOLUTE TRACKING METHOD";

    if(vS_0==NULL){
      Logger::getLogger()->error("TrackingMethodOAbsolutePTAMM: Error in PTAMM tracking method: video source 0 is NULL");
      exit(0);
    }

    addVideoSource(vS_0);

    mVideoSource = vS_0;



  //PTAMM commands
  GUI.RegisterCommand("SwitchMap", GUICommandCallBack, this);
  GUI.RegisterCommand("NewMap", GUICommandCallBack, this);
  GUI.RegisterCommand("DeleteMap", GUICommandCallBack, this);
  GUI.RegisterCommand("ResetAll", GUICommandCallBack, this);

  GUI.RegisterCommand("LoadMap", GUICommandCallBack, this);
  GUI.RegisterCommand("SaveMap", GUICommandCallBack, this);
  GUI.RegisterCommand("SaveMaps", GUICommandCallBack, this);
  
  GV2.Register(mgvnLockMap, "LockMap", 0, SILENT);
   
   GUI.RegisterCommand("KeyPress", GUICommandCallBack, this);
  
  
  
  mimFrameBW.resize(CVD::ImageRef(mVideoSource->getWidth(),mVideoSource->getHeight()));
 
    int frameWidth = mVideoSource->getWidth();
    int frameHeight = mVideoSource->getHeight();

    double p[3][4];
    mVideoSource->getPerspectiveMatrix(p);
 
    // double k[5];
    //mVideoSource->getDistortionMatrix(k);


    TooN::Vector<NUMTRACKERCAMPARAMETERS>  cameraParams = TooN::makeVector(p[0][0]/frameWidth,
    								 p[1][1]/frameHeight,
    								 p[0][2]/frameWidth,
    								 p[1][2]/frameHeight,
    								 0);

    // TooN::Vector<5> vTest;
  
    // vTest = GV3::get<TooN::Vector<5> >("Camera.Parameters", ATANCamera::mvDefaultParams, HIDDEN);
    // mpCamera = new ATANCamera("Camera");
    // //mpCamera->SetImageSize(mVideoSource.Size());
    // mpCamera->SetImageSize(CVD::ImageRef(mVideoSource->getWidth(),mVideoSource->getHeight()));
  
    // if(vTest == ATANCamera::mvDefaultParams)
    //   {
    // 	exit(1);
    //   }


    mpCamera = new ATANCamera("Camera",cameraParams);

    mpCamera->SetImageSize(CVD::ImageRef(mVideoSource->getWidth(),mVideoSource->getHeight()));
  
    __numPTAMMMaps = Configuration::getInstance()->numPTAMMMaps();

    //create the first map
    mpMap = new Map();
    mvpMaps.push_back( mpMap );
    mpMap->mapLockManager.Register(this);

    mpMapMaker = new MapMaker( mvpMaps, mpMap );	        
    mpTracker = new Tracker(CVD::ImageRef(mVideoSource->getWidth(),mVideoSource->getHeight()), *mpCamera, mvpMaps, mpMap, *mpMapMaker);

    mpMapSerializer = new MapSerializer( mvpMaps );


    // //These commands have to be registered here as they call the classes created above
    // GUI.RegisterCommand("NextMap", GUICommandCallBack, mpMapViewer);
    // GUI.RegisterCommand("PrevMap", GUICommandCallBack, mpMapViewer);
    // GUI.RegisterCommand("CurrentMap", GUICommandCallBack, mpMapViewer);
  
    // GUI.RegisterCommand("Mouse.Click", GUICommandCallBack, mpARDriver);

    
  //create the menus
  GUI.ParseLine("GLWindow.AddMenu Menu Menu");
  GUI.ParseLine("Menu.ShowMenu Root");
  GUI.ParseLine("Menu.AddMenuButton Root \"Reset All\" ResetAll Root");
  GUI.ParseLine("Menu.AddMenuButton Root Reset Reset Root");
  GUI.ParseLine("Menu.AddMenuButton Root Spacebar PokeTracker Root");
  GUI.ParseLine("Menu.AddMenuButton Root Demos \"\" Demos");
  GUI.ParseLine("DrawAR=0");
  GUI.ParseLine("DrawMap=0");
  GUI.ParseLine("Menu.AddMenuToggle Root \"Draw AR\" DrawAR Root");

  GUI.ParseLine("GLWindow.AddMenu MapsMenu Maps");
  GUI.ParseLine("MapsMenu.AddMenuButton Root \"New Map\" NewMap Root");
  GUI.ParseLine("MapsMenu.AddMenuButton Root \"Serialize\" \"\" Serial");
  GUI.ParseLine("MapsMenu.AddMenuButton Serial \"Save Maps\" SaveMaps Root");
  GUI.ParseLine("MapsMenu.AddMenuButton Serial \"Save Map\" SaveMap Root");
  GUI.ParseLine("MapsMenu.AddMenuButton Serial \"Load Map\" LoadMap Root");

  GUI.ParseLine("MapsMenu.AddMenuToggle Serial \"Save Video\" SaveFIFO Serial");
  GUI.ParseLine("MapsMenu.AddMenuSlider Serial Bitrate Bitrate 100 20000 Serial");

  GUI.ParseLine("LockMap=0");
  GUI.ParseLine("MapsMenu.AddMenuToggle Root \"Lock Map\" LockMap Root");
  GUI.ParseLine("MapsMenu.AddMenuButton Root \"Delete Map\" DeleteMap Root");
  GUI.ParseLine("MapInfo=0");
  GUI.ParseLine("MapsMenu.AddMenuToggle Root \"Map Info\" MapInfo Root");

  GUI.ParseLine("GLWindow.AddMenu MapViewerMenu Viewer");
  GUI.ParseLine("MapViewerMenu.AddMenuToggle Root \"View Map\" DrawMap Root");
  GUI.ParseLine("MapViewerMenu.AddMenuButton Root Next NextMap Root");
  GUI.ParseLine("MapViewerMenu.AddMenuButton Root Previous PrevMap Root");
  GUI.ParseLine("MapViewerMenu.AddMenuButton Root Current CurrentMap Root");



    /////////////////////////////////////////////////////
    //Loading the maps! :)------------------------------
    //Loading just the first one! :)
    if( mpMapSerializer->Init( "LoadMap", "", *mpMap) ) {
      mpMapSerializer->start();
      sleep(1);
    }
	
    //Loading the others!
    for(int i=0;i<__numPTAMMMaps-1;i++){
      NewMap();
      if( mpMapSerializer->Init( "LoadMap", "", *mpMap) ) {
	mpMapSerializer->start();
	sleep(1);
      }
    }

  }


  void TrackingMethodOAbsolutePTAMM::loopThread(){

    cv::Mat* frame = mVideoSource->getLastFrame();
  
    if(frame->rows>0){
      //Check if the map has been locked by another thread, and wait for release.
      bool bWasLocked = mpMap->mapLockManager.CheckLockAndWait( this, 0 );
    
      mpMap->bEditLocked = *mgvnLockMap; //sync up the maps edit lock with the gvar bool.
    
      GetAndFillFrameBW(frame);  

      // static bool bFirstFrame = true;
      // if(bFirstFrame)
      // {
      // 	//        mpARDriver->Init();
      //   bFirstFrame = false;
      // }
      
      //      mGLWindow.SetupViewport();
      //mGLWindow.SetupVideoOrtho();
      //mGLWindow.SetupVideoRasterPosAndZoom();
      
      // if(!mpMap->IsGood()) {
      // 	//	mpARDriver->Reset();
      // }

      if(bWasLocked)  {
        mpTracker->ForceRecovery();
      }
      
      // static gvar3<int> gvnDrawMap("DrawMap", 0, HIDDEN|SILENT);
      // static gvar3<int> gvnDrawAR("DrawAR", 0, HIDDEN|SILENT);
      
      // bool bDrawMap = mpMap->IsGood() && *gvnDrawMap;
      // bool bDrawAR = mpMap->IsGood() && *gvnDrawAR;

    
      mpTracker->TrackFrame(mimFrameBW,false);
  


      // if(bDrawMap) {
      // 	//	mpMapViewer->DrawMap(mpTracker->GetCurrentPose());
      // }
      // else if(bDrawAR) {
      //   if( !mpTracker->IsLost() ) {
      //   }
      // 	//	mpARDriver->Render(mimFrameRGB, mpTracker->GetCurrentPose(), mpTracker->IsLost() );
      // }

      // if(*mgvnDrawMapInfo) {
      //   DrawMapInfo();
      // }

      // string sCaption;
      // if(bDrawMap) {
      // 	//	sCaption = mpMapViewer->GetMessageForUser();
      // }
      // else {
      // 	sCaption = mpTracker->GetMessageForUser();
      // }
      // //      mGLWindow.DrawCaption(sCaption);
      // //mGLWindow.DrawMenus();

      // if( *mgvnSaveFIFO )
      // {
      //   SaveFIFO();
      // }
      
      //mGLWindow.swap_buffers();
      //mGLWindow.HandlePendingEvents();
    


    /* OU  YEAH - ELCANO THINGS */

      //if(!mpTracker->IsLost()){
      __detecting = true;

      int currentMap = mpTracker->getCurrentMap()->MapID();
        
      TooN::Vector<3> tr = mpTracker->GetCurrentPose().get_translation();
      TooN::Matrix<3> rt = mpTracker->GetCurrentPose().get_rotation().get_matrix();

      float d[16];

      //This is the trans to change the camera viewing from +z to -z
      // d[0]=rt[0][0];     d[4]=rt[0][1];     d[8]=rt[0][2];      d[12]=tr[0];
      // d[1]=-rt[1][0];    d[5]=-rt[1][1];    d[9]=-rt[1][2];     d[13]=-tr[1];
      // d[2]=-rt[2][0];    d[6]=-rt[2][1];    d[10]=-rt[2][2];    d[14]=-tr[2];
      // d[3]=0.f;          d[7]=0.f;          d[11]=0.f;          d[15]=1.f;

      //std::cout<<"Applying aspect ratio!: "<<__mapsAspectRatios[currentMap]<<std::endl;
      d[0]=rt[0][0];     d[4]=rt[0][1];     d[8]=rt[0][2];      d[12]=tr[0];
      d[1]=-rt[1][0];    d[5]=-rt[1][1];    d[9]=-rt[1][2];     d[13]=-tr[1];
      d[2]=rt[2][0];     d[6]=rt[2][1];     d[10]=rt[2][2];     d[14]=tr[2];
      d[3]=0.f;          d[7]=0.f;          d[11]=0.f;          d[15]=1.f;

      cv::Mat mat (4,4,CV_32F,d);
      cv::Mat mat2 = mat.inv();

      //      cv::Mat mat3 = mat2 * *(__mapsOffsets[currentMap]);

      //Matrix16 m((float*)mat3.data);      
      Matrix16 m((float*)mat2.data);      

      tUserL t;
      t=Mat2TUserL(m,1.0);

      addRecord(t); 
      //    }
    
    }
  
    delete frame;
  

  }
    


  TrackingMethodOAbsolutePTAMM::~TrackingMethodOAbsolutePTAMM(){

    if( mpMap != NULL )  {
      mpMap->mapLockManager.UnRegister( this );
    }
  
    Logger::getLogger()->note("TrackingMethodOAbsolutePTAMM: deleting tracking method. Bye!");
  }

  void TrackingMethodOAbsolutePTAMM::GUICommandCallBack(void *ptr, string sCommand, string sParams)
  {

  if(sCommand=="quit" || sCommand == "exit") {
    //    static_cast<TrackingMethodOAbsolutePTAMM*>(ptr)->mbDone = true;
  }
  else if( sCommand == "SwitchMap")
    {
      int nMapNum = -1;
      if( static_cast<TrackingMethodOAbsolutePTAMM*>(ptr)->GetSingleParam(nMapNum, sCommand, sParams) ) {
	static_cast<TrackingMethodOAbsolutePTAMM*>(ptr)->SwitchMap( nMapNum );
      }
    }
  else if(sCommand == "ResetAll")
    {
      static_cast<TrackingMethodOAbsolutePTAMM*>(ptr)->ResetAll();
      return;
    }
  else if( sCommand == "NewMap")
    {
      static_cast<TrackingMethodOAbsolutePTAMM*>(ptr)->NewMap();
    }
  else if( sCommand == "DeleteMap")
    {
      int nMapNum = -1;
      if( sParams.empty() ) {
        static_cast<TrackingMethodOAbsolutePTAMM*>(ptr)->DeleteMap( static_cast<TrackingMethodOAbsolutePTAMM*>(ptr)->mpMap->MapID() );
      }
      else if( static_cast<TrackingMethodOAbsolutePTAMM*>(ptr)->GetSingleParam(nMapNum, sCommand, sParams) ) {
	static_cast<TrackingMethodOAbsolutePTAMM*>(ptr)->DeleteMap( nMapNum );
      }
    }
  // else if( sCommand == "NextMap")  {
  //   static_cast<MapViewer*>(ptr)->ViewNextMap();
  // }
  // else if( sCommand == "PrevMap")  {
  //   static_cast<MapViewer*>(ptr)->ViewPrevMap();
  // }
  // else if( sCommand == "CurrentMap")  {
  //   static_cast<MapViewer*>(ptr)->ViewCurrentMap();
  // }
  else if( sCommand == "SaveMap" || sCommand == "SaveMaps" || sCommand == "LoadMap")  {
    static_cast<TrackingMethodOAbsolutePTAMM*>(ptr)->StartMapSerialization( sCommand, sParams );
  }
  else if( sCommand == "Mouse.Click" ) {
    vector<string> vs = ChopAndUnquoteString(sParams);
    
    if( vs.size() != 3 ) {
      return;
    }

    istringstream is(sParams);
    int nButton;
    ImageRef irWin;
    is >> nButton >> irWin.x >> irWin.y;
    //    static_cast<ARDriver*>(ptr)->HandleClick( nButton, irWin );
    
  }
  else if( sCommand == "KeyPress" )
    {
      if(sParams == "q" || sParams == "Escape")
	{
	  GUI.ParseLine("quit");
	  return;
	}

      bool bUsed = static_cast<TrackingMethodOAbsolutePTAMM*>(ptr)->mpTracker->HandleKeyPress( sParams );

      if( !bUsed ) {
	//	static_cast<TrackingMethodOAbsolutePTAMM*>(ptr)->mpARDriver->HandleKeyPress( sParams );
      }
    }



  }


  /**
   * Parse and allocate a single integer variable from a string parameter
   * @param nAnswer the result
   * @param sCommand the command (used to display usage info)
   * @param sParams  the parameters to parse
   * @return success or failure.
   */
  bool TrackingMethodOAbsolutePTAMM::GetSingleParam(int &nAnswer, string sCommand, string sParams)
  {

  vector<string> vs = ChopAndUnquoteString(sParams);
    
  if(vs.size() == 1)
    {
      //is param a number?
      bool bIsNum = true;
      for( size_t i = 0; i < vs[0].size(); i++ ) {
	bIsNum = isdigit( vs[0][i] ) && bIsNum;
      }
      
      if( !bIsNum )
	{
	  return false;
	}

      int *pN = ParseAndAllocate<int>(vs[0]);
      if( pN )
	{
	  nAnswer = *pN;
	  delete pN;
	  return true;
	}
    }



  return false;


  }


  /**
   * Switch to the map with ID nMapNum
   * @param  nMapNum Map ID
   * @param bForce This is only used by DeleteMap and ResetAll, and is
   * to ensure that MapViewer is looking at a safe map.
   */
  bool TrackingMethodOAbsolutePTAMM::SwitchMap( int nMapNum, bool bForce )
  {
 //same map, do nothing. This should not actually occur
  if(mpMap->MapID() == nMapNum) {
    return true;
  }

  if( (nMapNum < 0) )
    {
      cerr << "Invalid map number: " << nMapNum << ". Not changing." << endl;
      return false;
    }

  
  for( size_t ii = 0; ii < mvpMaps.size(); ii++ )
    {
      Map * pcMap = mvpMaps[ ii ];
      if( pcMap->MapID() == nMapNum ) {
	mpMap->mapLockManager.UnRegister( this );
	mpMap = pcMap;
	mpMap->mapLockManager.Register( this );
      }
    }

  if(mpMap->MapID() != nMapNum)
    {
      cerr << "Failed to switch to " << nMapNum << ". Does not exist." << endl;
      return false;
    }
  
  /*  Map was found and switched to for system.
      Now update the rest of the system.
      Order is important. Do not want keyframes added or
      points deleted from the wrong map.
  
      MapMaker is in its own thread.
      TrackingMethodOAbsolutePTAMM,Tracker, and MapViewer are all in this thread.
  */

  *mgvnLockMap = mpMap->bEditLocked;

 
  //update the map maker thread
  if( !mpMapMaker->RequestSwitch( mpMap ) ) {
    return false;
  }
  
  while( !mpMapMaker->SwitchDone() ) {
#ifdef WIN32
    Sleep(1);
#else
    usleep(10);
#endif
  }

  //update the map viewer object
  //  mpMapViewer->SwitchMap(mpMap, bForce);
      
  //update the tracker object
  //   mpARDriver->Reset();
  //  mpARDriver->SetCurrentMap( *mpMap );
  
  if( !mpTracker->SwitchMap( mpMap ) ) {
    return false;
  }

  return true;




  }



  /**
   * Create a new map and switch all
   * threads and objects to it.
   */
  void TrackingMethodOAbsolutePTAMM::NewMap()
  {

  *mgvnLockMap = false;
  mpMap->mapLockManager.UnRegister( this );
  mpMap = new Map();
  mpMap->mapLockManager.Register( this );
  mvpMaps.push_back( mpMap );
  
  //update the map maker thread
  mpMapMaker->RequestReInit( mpMap );
  while( !mpMapMaker->ReInitDone() ) {
#ifdef WIN32
    Sleep(1);
#else
    usleep(10);
#endif
  }
    
  //update the map viewer object
  //  mpMapViewer->SwitchMap(mpMap);

  //update the tracker object
  //  mpARDriver->SetCurrentMap( *mpMap);
  //mpARDriver->Reset();
  mpTracker->SetNewMap( mpMap );
    
      
  }


  /**
   * Moves all objects and threads to the first map, and resets it.
   * Then deletes the rest of the maps, placing PTAMM in its
   * original state.
   * This reset ignores the edit lock status on all maps
   */
  void TrackingMethodOAbsolutePTAMM::ResetAll()
  {


  //move all maps to first map.
  if( mpMap != mvpMaps.front() )
    {
      if( !SwitchMap( mvpMaps.front()->MapID(), true ) ) {
	cerr << "Reset All: Failed to switch to first map" << endl;
      }
    }
  mpMap->bEditLocked = false;

  //reset map.
  mpTracker->Reset();
  
  //lock and delete all remaining maps
  while( mvpMaps.size() > 1 )
    {
      DeleteMap( mvpMaps.back()->MapID() );
    }
  
  
  }



/**
 * Delete a specified map.
 * @param nMapNum map to delete
 */
bool TrackingMethodOAbsolutePTAMM::DeleteMap( int nMapNum )
{
  if( mvpMaps.size() <= 1 )
    {

      return false;
    }


  //if the specified map is the current map, move threads to another map
  if( nMapNum == mpMap->MapID() )
    {
      int nNewMap = -1;
    
      if( mpMap == mvpMaps.front() ) {
	nNewMap = mvpMaps.back()->MapID();
      }
      else {
	nNewMap = mvpMaps.front()->MapID();
      }
    
      // move the current map users elsewhere
      if( !SwitchMap( nNewMap, true ) ) {
	cerr << "Delete Map: Failed to move threads to another map." << endl;
	return false;
      }
    }

  
 
  // find and delete the map
  for( size_t ii = 0; ii < mvpMaps.size(); ii++ )
    {
      Map * pDelMap = mvpMaps[ ii ];
      if( pDelMap->MapID() == nMapNum ) {

	pDelMap->mapLockManager.Register( this );
	pDelMap->mapLockManager.LockMap( this );
	delete pDelMap;
	mvpMaps.erase( mvpMaps.begin() + ii );

	///@TODO Possible bug. If another thread (eg serialization) was using this
	/// and waiting for unlock, would become stuck or seg fault.
      }
    }
  
  return true;
}


  /**
   * Set up the map serialization thread for saving/loading and the start the thread
   * @param sCommand the function that was called (eg. SaveMap)
   * @param sParams the params string, which may contain a filename and/or a map number
   */
  void TrackingMethodOAbsolutePTAMM::StartMapSerialization(std::string sCommand, std::string sParams)
  {
    if( mpMapSerializer->Init( sCommand, sParams, *mpMap) ) {
      mpMapSerializer->start();
    }
  }





  void TrackingMethodOAbsolutePTAMM::GetAndFillFrameBW(cv::Mat* frame){
    Mat_<Vec3b>& frame_p = (Mat_<Vec3b>&)*frame;
    //  std::cout<<"Height: "<<mVideoSource->getHeight()<<" Width: "<<mVideoSource->getWidth()<<std::endl;

    for (int i = 0; i < mVideoSource->getHeight(); i++){
      for (int j = 0; j < mVideoSource->getWidth(); j++){
	//      std::cout<<"i: "<<i<<" j: "<<j<<std::endl;
	mimFrameBW[i][j]  = (frame_p(i,j)[0] + frame_p(i,j)[1] + frame_p(i,j)[2]) / 3;
	// mimFrameRGB[i][j].red = frame_p(i,j)[2];
	// mimFrameRGB[i][j].green = frame_p(i,j)[1];
	// mimFrameRGB[i][j].blue = frame_p(i,j)[0];
      }
    }
  
  
  }





/**
 * Draw a box with information about the maps.
 */
void TrackingMethodOAbsolutePTAMM::DrawMapInfo()
{
  int nLines = static_cast<int>(mvpMaps.size()) + 2;
  int x = 5, y = 120, w = 160, nBorder = 5;
  
  //mGLWindow.DrawBox( x, y, w, nLines, 0.7f );

  y += 17;

  glColor3f(1,1,1);
  std::ostringstream os;
  os << "Maps " << mvpMaps.size();
  //  mGLWindow.PrintString( ImageRef(x + nBorder,y + nBorder), os.str() );
  os.str("");
      
  for( size_t i = 0; i < mvpMaps.size(); i++ )
    {
      Map * pMap = mvpMaps[i];
      if( pMap == mpMap ) {
	glColor3f(1,1,0);
      }
      else if( pMap->bEditLocked ) {
	glColor3f(1,0,0);
      }
      else {
	glColor3f(1,1,1);
      }
    
      os << "M: " << pMap->MapID() << "  P: " << pMap->vpPoints.size() << "  K: " << pMap->vpKeyFrames.size();
      //mGLWindow.PrintString( ImageRef( x + nBorder , y + nBorder + (i+1)*17), os.str() );
      os.str("");
    }
}


/**
 * Save the current frame to a FIFO.
 * This function is called on each frame to create a video.
 * The GVar SaveFIFO starts and stops the saving, and the GVar
 * Bitrate sets the quality.
 * Bitrate can only be set before the first call of SaveFIFO.
 */
void TrackingMethodOAbsolutePTAMM::SaveFIFO()
{
// #ifdef _LINUX
//   //Some static variables
//   static CVD::byte* pcImage = NULL;
//   static int fd = 0;
//   static bool bFIFOInitDone = false;
//   static ImageRef irWindowSize;

//   if( !bFIFOInitDone )
//     {
//       //      irWindowSize = mGLWindow.size();

//       ostringstream os;
//       os << /*"/bin/bash\n" <<*/
//         "file=\"`date '+%Y-%m-%d_%H-%M-%S'`.avi\"; " <<
//         "if [ ! -e FIFO ]; then mkfifo FIFO; echo Made FIFO...; fi; " <<
//         "echo Mencoding to $file....; " <<
//         "cat FIFO |nice mencoder -flip -demuxer rawvideo -rawvideo fps=30:w=" <<
// 	//        irWindowSize.x << ":h=" << irWindowSize.y <<
//         ":format=rgb24 -o $file -ovc lavc -lavcopts vcodec=mpeg4:vbitrate=" << *mgvnBitrate <<
//         ":keyint=45 -ofps 30 -ffourcc DIVX - &";

//       cout << "::" << os.str()<< "::" << endl;
//       int i = system( os.str().c_str() );
//       if( i != 0 ) {
// 	cerr << "ERROR: could not set up the FIFO!" << endl;
// 	return;
//       }

// /      posix_memalign((void**)(&pcImage), 16, irWindowSize.x*irWindowSize.y*3);
//       string s = "FIFO";
//       fd = open(s.c_str(), O_RDWR | O_ASYNC);

//       bFIFOInitDone = true;
//     }
  
//   if( irWindowSize != mGLWindow.size() )
//     {
//       cerr << "ERROR: Aborting FIFO as window size has changed!!" << endl;
//       *mgvnSaveFIFO = 0;
//       return;
//     }

//   glReadBuffer(GL_BACK);
//   glReadPixels(0,0,irWindowSize.x,irWindowSize.y,GL_RGB, GL_UNSIGNED_BYTE, pcImage);
//   write(fd, (char*) pcImage, irWindowSize.x*irWindowSize.y*3);

// #else
//   cout << "Video Saving using FIFOs is only available under Linux" << endl;
// #endif

}


  // void TrackingMethodOAbsolutePTAMM::readMapsOffsets(){

  //   std::ifstream file; 
  //   file.open("config/PTAMMOffset.mat",std::ios::in);
  //   if(!file.is_open()){
  //     std::cout<<"Error opening PTAMMOffset.mat"<<std::endl;
  //     std::cout<<"The format must be:"<<std::endl;
  //     std::cout<<"[#MapID] #offsetMatrix"<<std::endl;
  //     exit(-1);
  //   }

  //   string line;
  //   while(!file.eof()){
  //     getline(file,line);
  //     if(line.size()==0) continue;
  //     if(line[0]!='#'){
  // 	setlocale (LC_NUMERIC, "C"); // ... so we have an uniform config file...
      
  // 	unsigned int mapId;
  // 	sscanf(line.c_str(),"[%d]",&mapId);
  // 	if(mapId>=mvpMaps.size()) continue;
  // 	sscanf(line.c_str()+4,"%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f",
  // 	       //	     &(__mapsAspectRatios[mapId]),
  // 	       &(__mapsOffsets[mapId]->at<float>(0,0)), &(__mapsOffsets[mapId]->at<float>(0,1)),&(__mapsOffsets[mapId]->at<float>(0,2)),&(__mapsOffsets[mapId]->at<float>(0,3)),
  // 	       &(__mapsOffsets[mapId]->at<float>(1,0)), &(__mapsOffsets[mapId]->at<float>(1,1)),&(__mapsOffsets[mapId]->at<float>(1,2)),&(__mapsOffsets[mapId]->at<float>(1,3)),
  // 	       &(__mapsOffsets[mapId]->at<float>(2,0)), &(__mapsOffsets[mapId]->at<float>(2,1)),&(__mapsOffsets[mapId]->at<float>(2,2)),&(__mapsOffsets[mapId]->at<float>(2,3)),
  // 	       &(__mapsOffsets[mapId]->at<float>(3,0)), &(__mapsOffsets[mapId]->at<float>(3,1)),&(__mapsOffsets[mapId]->at<float>(3,2)),&(__mapsOffsets[mapId]->at<float>(3,3)));

  // 	/*Applying Aspect Ratio */
  // 	// __mapsOffsets[mapId]->at<float>(3,0)*=__mapsAspectRatios[mapId];
  // 	//__mapsOffsets[mapId]->at<float>(3,1)*=__mapsAspectRatios[mapId];
  // 	//__mapsOffsets[mapId]->at<float>(3,2)*=__mapsAspectRatios[mapId];
    

  //     }

  //   }
  // }

  int TrackingMethodOAbsolutePTAMM::getTrackingQuality(){
    return mpTracker->getTrackingQuality();
  }

  void TrackingMethodOAbsolutePTAMM::printMapsPoints(){
    for(unsigned int i=0;i<mvpMaps.size();i++){
      Map* m = mvpMaps.at(i);
      std::cout<<"\tMap #"<<i<<": "<<m->vpPoints.size()<<" points."<<std::endl;
    }
  }

  Map* TrackingMethodOAbsolutePTAMM::getCurrentMap(){
    return mpMap;

  }

  
  int TrackingMethodOAbsolutePTAMM::getCurrentMapId(){
    return getCurrentMap()->MapID();
  }
  
  
  
  void TrackingMethodOAbsolutePTAMM::getCurrent3DPoints(std::list<TooN::Vector<3> >& dest)
  {
  
    if(mpMap)
      {
	for(std::vector<MapPoint*>::iterator it = mpMap->vpPoints.begin(); it!=mpMap->vpPoints.end(); ++it)
	  {
	    dest.push_back((*it)->v3WorldPos);
	  }
      }else
      {
	std::cout<<"Error, no hay mapa actual"<<std::endl;
      }
  }



}

