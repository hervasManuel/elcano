/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * TrackingMethodOAbsolutePTAMM.h
 *
 */

#ifndef ABSOLUTE_PTAMM_H_
#define ABSOLUTE_PTAMM_H_

#include <iostream>
#include <vector>
#include <core/tracking/TrackingMethodOAbsolute.h>
#include <core/TUserL.h>
#include <gvars3/GStringUtil.h>
#include <stdlib.h>
#include <core/tracking/PTAMM/Tracker.h>
#include <core/tracking/PTAMM/MapSerializer.h>
#include <core/tracking/PTAMM/ATANCamera.h>
#include <core/tracking/PTAMM/MapMaker.h>
#include <gvars3/instances.h>
#include <cvd/image.h>
#include <cvd/rgb.h>
#include <cvd/byte.h>



#include <fcntl.h>





namespace mars{

  using namespace PTAMM;

/* This class implements an absolute tracking method based on ARToolKit perceptions*/

class TrackingMethodOAbsolutePTAMM : public TrackingMethodOAbsolute {
public:
	/* Public methods */
	TrackingMethodOAbsolutePTAMM(VideoSource* vS_0);
	virtual ~TrackingMethodOAbsolutePTAMM();	
	
	void loopThread();
	int getTrackingQuality();
	void printMapsPoints();

	int getCurrentMapId();
	Map* getCurrentMap();
	void getCurrent3DPoints(std::list<TooN::Vector<3> >& dest);

	
private:
	static void GUICommandCallBack(void* ptr, std::string sCommand, std::string sParams);  //process a console command
	bool GetSingleParam(int &nAnswer, std::string sCommand, std::string sParams);          //Extract an int param from a command param
	bool SwitchMap( int nMapNum, bool bForce = false );                                    // Switch to a particular map.
	void NewMap();                                  // Create a new map and move all elements to it
	void ResetAll();                                // Wipes out ALL maps, returning system to initial state
	void StartMapSerialization(std::string sCommand, std::string sParams);   //(de)serialize a map
	void GetAndFillFrameBW(cv::Mat* frame);
	//	void readMapsOffsets();
	void DrawMapInfo();                             // draw a little info box about the maps
	void SaveFIFO();                                // save the video out to a FIFO (save to disk)
	bool DeleteMap( int nMapNum );                  // Delete a specified map
    

	mars::VideoSource* mVideoSource;                // The video image source
	//	GLWindow2 mGLWindow;                            // The OpenGL window
	//CVD::Image<CVD::Rgb<CVD::byte> > mimFrameRGB;   // The RGB image used for AR
	CVD::Image<CVD::byte> mimFrameBW;               // The Black and white image for tracking/mapping
	std::vector<Map*> mvpMaps;                      // The set of maps
	Map *mpMap;                                     // The current map
	Tracker *mpTracker;                             // The tracker
	MapMaker *mpMapMaker;                           // The map maker
	ATANCamera *mpCamera;                           // The camera model
	MapSerializer *mpMapSerializer;                 // The map serializer for saving and loading maps
	GVars3::gvar3<int> mgvnLockMap;                 // Stop a map being edited - i.e. keyframes added, points updated
	//	GVars3::gvar3<int> mgvnDrawMapInfo;             // Draw map info on the screen
	int __numPTAMMMaps;

	
	

		
	//	cv::Mat** __mapsOffsets;
	//float __mapsAspectRatios[NUM_MAPS];
};

}

#endif /* ABSOLUTE_PTAMM_H_*/

