/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * TrackingMethodOAbsoluteARTK.h
 *
 */

#ifndef ABSOLUTE_ARTK_H_
#define ABSOLUTE_ARTK_H_

#include <iostream>
#include <vector>
#include <core/FramesBuffer.h>
#include <core/tracking/TrackingMethodOAbsolute.h>
#include <core/video/VideoDevice.h>
#include <core/tracking/ARTKMark.h>
#include "PatternsDetectorArtk.h"
#include <core/tracking/ARTKHistory.h>
#include <core/TUserL.h>

namespace mars{

/* This class implements an absolute tracking method based on ARToolKit perceptions*/

class TrackingMethodOAbsoluteArtkBz : public TrackingMethodOAbsolute {
public:
	/* Public methods */
	TrackingMethodOAbsoluteArtkBz(VideoSource* vS_0);
	TrackingMethodOAbsoluteArtkBz(VideoSource* vS_0,vector<VideoSource*>* extCams);
	virtual ~TrackingMethodOAbsoluteArtkBz();	
	
	void loopThread();
	vector<ARTKMark>* getUserMarks();
	void addExternalCamera(VideoSource* vS);
	void incrementThreshold();
	void decrementThreshold();
	void saveMarksConf();
	ARTKMark* getCurrentMark();

	/*Util - TO DEPRECATE*/
	float* Mat2Gl(cv::Mat* mat);
	cv::Mat* Gl2Mat(float *gl);
	void printMat(cv::Mat* mat);
	void printGlMat(float* mat[16]);
	int actuationMarksFound();

private:
	/* Private variables */
	VideoSource* __uCam;
	PatternsDetectorArtk* __artkDetector;
	vector<VideoSource*> __externCameras;
	vector<ARTKMark>* __userMarks;
	vector<vector<ARTKMark>*> __extCamsMarks;
	cv::Mat* __globalTrans;
	float __eval;
	ARTKMark* _currentMark;
	int __actuationMarksFound;

	/*For the historic!*/
	void initHistory(ARTKHistory* hist);
	ARTKHistory __userHistory;
	vector<ARTKHistory> __extCamsHistory;
		
	/* Private methods */
	
	bool locateCamera(cv::Mat* globalTrans,float* globalEval);
	bool processActuationPattern(ARTKMark* mark);

};

}

#endif /* ABSOLUTE_ARTK_H_*/

