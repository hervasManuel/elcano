/*
 * ParticleFilter.h
 *
 */

#ifndef PARTICLEFILTER_H_
#define PARTICLEFILTER_H_

#include <vector>
#include <core/TUserL.h>

namespace mars{

  /*This class implements
   *a minimal particle filter
   */

class ParticleFilter{
 public:
  ParticleFilter();
  ~ParticleFilter();

  void filterAbsolute(std::vector<tUserL*>* historicalRecord,tUserL* posResult);


 private:



};
}

#endif /*PARTICLEFILTER_H_*/
