/*
 * TrackingMethod.cpp
 *
 */

#include "TrackingMethod.h"




namespace mars {



  TrackingMethod::TrackingMethod() {
    __maxHistoricalRecords=20; // 20 by default.
    __historicalRecordOfPerceptions.reserve(20); // Reserve 20 slots.

    __done = false;

    __ready = false;
    __slowMode = false;

    __threadError = -1;

    __detecting=false;
#ifndef NDEBUG
    std::cout << "[DEBUG]: Creating tracking method: " ;
#endif

    pthread_mutex_init(&mutex_tracking, 0);

#ifndef NDEBUG
    std::cout << "TrackingMethod:: Creating thread... ";
#endif


  }

  TrackingMethod::TrackingMethod(const unsigned int& nRecords) {
    __maxHistoricalRecords= nRecords;
    __historicalRecordOfPerceptions.reserve(nRecords); // Reserve nRecords slots.
  }

  vector<tUserL*>* TrackingMethod::getHistoricalRecord(){
    return &__historicalRecordOfPerceptions;
  }

  void* TrackingMethod::exec(void* thr){

#ifndef NDEBUG
    std::cout << "Waiting for a valid Tracking Source." << std::endl;
#endif

    while(!((TrackingMethod*) thr)->isReady()){}

    if(!((TrackingMethod*) thr)->isDone())
      ((TrackingMethod*) thr)->Thread();
    return NULL;
  }


  bool TrackingMethod::isReady(){
    return __ready;
  }

  void TrackingMethod::startMethod(){
    __threadError = pthread_create( &__thread_id, NULL, &TrackingMethod::exec, this );

    Logger::getInstance()->note("TrackingMethod " + __methodName + " :: starting...");

    if (!__threadError){
#ifndef NDEBUG
      std::cout << "[DEBUG] : ... created! (" << __thread_id << ")" << std::endl;
#endif
    }
    else{
      std::stringstream msg;
      msg << "TrackingMethod:: creating Thread - " << __threadError;
      Logger::getLogger()->error(msg.str());
    }
  }

  string TrackingMethod::getMethodName(){
    return __methodName;
  }

  bool TrackingMethod::isDone(){
    return __done;
  }




  void TrackingMethod::addRecord(const tUserL& userL){
    //	lock();
    __userL = userL;
#ifndef NDEBUG
    std::cout << "----" << std::endl;
    std::cout << "Adding an User Location/perception" <<std::endl;
    std::cout << "\t· Eye: " << __userL.__Eye <<std::endl;
    std::cout << "\t· LookTo: " << __userL.__LookTo <<std::endl;
    std::cout << "\t· Up: " << __userL.__Up <<std::endl;
    std::cout << "----" <<std::endl;

#endif

    if (__historicalRecordOfPerceptions.size()==__maxHistoricalRecords){

      delete __historicalRecordOfPerceptions.at(0);
      __historicalRecordOfPerceptions.erase( __historicalRecordOfPerceptions.begin() );
    } // If it's full, we get rid of the first record


    //Copy constructor ...
    __historicalRecordOfPerceptions.push_back(new tUserL(__userL));

    //	unlock();

  }

  tUserL* TrackingMethod::getLastUserL(){
    return &__userL;
  }

  void TrackingMethod::lock(){
    pthread_mutex_lock(&mutex_tracking);
  }

  void TrackingMethod::unlock(){
    pthread_mutex_unlock(&mutex_tracking);
  }

  const tUserL TrackingMethod::Mat2TUserL(Matrix16& m, const float& weight){
    tUserL temp;
    GLfloat* data = m.getData();
    temp.__Eye.setComponets(data[12], data[13], data[14]);
    temp.__LookTo.setComponets(data[8]+data[12], data[9]+data[13], data[10] + data[14]);
    temp.__Up.setComponets(data[4], data[5], data[6]);
    temp.__Weight = weight;
    return temp;
  }


  void TrackingMethod::stopMethod(){
    __done = true;
    lock();
  }


  bool TrackingMethod::isDetecting(){
    return __detecting;
  }

  TrackingMethod::~TrackingMethod() {

    if (!__threadError){
#ifndef NDEBUG
      std::cout << "[DEBUG]: TrackingMethod:: " << __methodName << ":: Waiting for the thread (" << __thread_id
		<< ") to finish and join..." <<std::endl;
#endif

      pthread_join(__thread_id, NULL);
    }

#ifndef NDEBUG
    std::cout << "done!" << std::endl;
#endif

    pthread_mutex_destroy(&mutex_tracking);


  }

  void TrackingMethod::pause(){
    __paused = true;
  }
  void TrackingMethod::resume(){
    __paused = false;
  }

  void TrackingMethod::slowMode(){
    __slowMode = true;
    
  }
  void TrackingMethod::fastMode(){
    __slowMode = false;
  }


  Ogre::Vector3 TrackingMethod::getLastPos()
  {
    mars::Vector3D pos = getLastUserL()->__Eye;
    return Ogre::Vector3(pos.x,pos.y,pos.z);
  }
  
  float TrackingMethod::getLastRot()
  {
    
    mars::tUserL* t = getLastUserL();
    float ang=0;
    
    if(t!=NULL){
      /*Getting angle!*/
      double x,z;
      x=t->__LookTo.x-t->__Eye.x;
      z=t->__LookTo.x-t->__Eye.x;
      ang=acos(z);
      
      /*Convertimos a grados*/
      ang=ang*180/3.14159;
      
      if(x>0){
	ang=360-ang;
      }
    }
    return ang;
    
  }



}
