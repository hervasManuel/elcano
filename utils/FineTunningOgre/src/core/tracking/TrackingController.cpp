/*
 * TrackingController.cpp
 *
 */

#include "TrackingController.h"
#include "TrackingMethodOAbsoluteArtkBz.h"
#include <pthread.h>
#include <iostream>

namespace mars {



  TrackingController::TrackingController(){
    pthread_mutex_init(&mutex_userl, 0);
    __filter=new ParticleFilter();

    //    __ARTKWait = 0;
    //__failsARTK = 0;


    //FineTunning does not need Maps Offsets :)

    //_numPTAMMMaps = Configuration::getInstance()->numPTAMMMaps();

    //   _mapsPositions = static_cast<Ogre::Vector3*>(malloc(_numPTAMMMaps*sizeof(Ogre::Vector3)));
    //_mapsOrientations = static_cast<Ogre::Quaternion*>(malloc(_numPTAMMMaps*sizeof(Ogre::Quaternion)));
    
    // if(mars::Configuration::getInstance()->usePTAMM()){
    //   //Initialitings offset matrices
    //   for(int i=0;i<__numPTAMMMaps;i++){
    // 	__mapsOffsets[i] = new cv::Mat(4,4,CV_32F);
    // 	_
    //   }

    ///readMapsOffsets();
    //}

  }

  TrackingController::~TrackingController(){
    pthread_mutex_destroy(&mutex_userl);

    Logger::getLogger()->note("Ending Tracking Controller");
    for (unsigned i = 0; i < __methodsAbsolute.size(); ++i)
      {
	__methodsAbsolute.at(i)->stopMethod();
	delete __methodsAbsolute.at(i);
      }


    delete __filter;

	
  }

  void TrackingController::lock(){
    pthread_mutex_lock(&mutex_userl);
  }

  void TrackingController::unlock(){
    pthread_mutex_unlock(&mutex_userl);
  }

  void TrackingController::setCamera(Ogre::Camera* camera){
    __camera = camera;
  }
  
  Ogre::Camera* TrackingController::getCamera()
  {
    return __camera;
  }

  TrackingController* TrackingController::getController(){
    return getInstance();
  }

  void TrackingController::publishMethodAbsolute(TrackingMethodOAbsolute* tMA){
    __methodsAbsolute.push_back(tMA);
    tMA->startMethod();
  }

  tUserL TrackingController::getComputedUserL() const{
    return __computedUserL;
  }

  void TrackingController::setOgreCamera(const tUserL& t){
  
    __camera->setPosition(Ogre::Real(t.__Eye.x),
    			  Ogre::Real(t.__Eye.y),
    			  Ogre::Real(t.__Eye.z));
    
    __camera->lookAt(Ogre::Real(t.__LookTo.x),
    		     Ogre::Real(t.__LookTo.y),
    		     Ogre::Real(t.__LookTo.z));
    

   
    Ogre::Vector3 zv(Ogre::Real(t.__LookTo.x-t.__Eye.x),
    		     Ogre::Real(t.__LookTo.y-t.__Eye.y),
    		     Ogre::Real(t.__LookTo.z-t.__Eye.z));

    
    Ogre::Vector3 up = __camera->getUp();

     
    __camera->setFixedYawAxis(true,Ogre::Vector3(t.__Up.x,
						 t.__Up.y,
						 t.__Up.z)
                              );

  }

  void TrackingController::computeAllMethods(){
    if(!mars::Configuration::getInstance()->usePTAMM()){
      return;
    }

    if(__methodsAbsolute.at(0)->getHistoricalRecord()->size()==0)
      return;
    
    //__filter->filterAbsolute(__methodsAbsolute.at(0)->getHistoricalRecord(),&__computedUserL);
    __computedUserL = *__methodsAbsolute.at(0)->getLastUserL();

    setOgreCamera(__computedUserL);

  }



  
}
