/*
 * TrackingController.h
 *
 */

#ifndef TRACKINGCONTROLLER_H_
#define TRACKINGCONTROLLER_H_

#include <vector>

#include <core/tracking/TrackingMethodO.h>
#include <core/tracking/TrackingMethodOAbsolute.h>
#include <core/tracking/ParticleFilter.h>
#include <core/Singleton.h>
#include <Ogre.h>

namespace mars {

/**
 * The controller of the tracking methods. It collects the tracking methods data, process it and updates
 * the camera.
 */
class TrackingController : public Singleton<TrackingController>{
public:


	/**
	 * Publishes an Optical Absolute Tracking Method
	 *
	 * @param tMA Tracking Optical Absolute Method
	 */
	void publishMethodAbsolute(TrackingMethodOAbsolute* tMA);

	/**
	 * Sets the camera3D (eventually an openGL one) that will be modified by the results of the tracking
	 */
	void setCamera(Ogre::Camera* camera);

	Ogre::Camera* getCamera();

	/**
	 * Returns the controller itself (it's a singleton)
	 */
	static TrackingController* getController();

	/**
	 * Returns the last computed tUserL
	 */
	tUserL getComputedUserL() const;

	

	void computeAllMethods(); ///< Computes All methods

private:

//	static TrackingController* __instance; ///< Instance, it's a sngleton

	TrackingController();
	virtual ~TrackingController();
	
	void setOgreCamera(const tUserL& t);
	
	void lock(); ///< Blocks the access to the data
	void unlock(); ///< Free the access to the data


	vector<TrackingMethodOAbsolute*> __methodsAbsolute; ///< Vector of absolute tracking methods

	tUserL __computedUserL; ///< Last computed tUserL (the user location and lookto ... and up)

	Ogre::Camera* __camera; ///< The current 3D camera

	pthread_mutex_t mutex_userl; ///< Internal mutex

	ParticleFilter* __filter;
	//Filtro_Particulas* __filter;

	friend class Singleton<TrackingController>;

	//We apply PTAMM offsets
	/* void readMapsOffsets(); */
	/* Ogre::Vector3* _mapsPositions; */
	/* Ogre::Quaternion* _mapsOrientations; */

	//	 int _numPTAMMMaps;
	//Demos variables
	//int _ARTKWait;
	//int _failsARTK;
};

}

#endif /* TRACKINGCONTROLLER_H_ */
