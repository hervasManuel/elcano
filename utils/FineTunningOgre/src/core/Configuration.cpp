/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * Configuration.cpp
 *
 */

#include "Configuration.h"


namespace mars {


Configuration::Configuration(){
  _defaultImage =  "./images/defaultImage.png";
  _defaultVideoTexture =  "./textures/defaultVideoTexture.png";
  _defaultTexturePath = "./textures/";
  _defaultVideoPath = "./videos/";
  _defaultSoundPath = "./sounds/";
  _openGLFullScreen = false;
  _openGLScreenWidth = 640;
  _openGLScreenHeight = 480;
  _openGLScreenDepth = 32;
  _ARTKDataPath = "./ARTKData";
  _ARTKPatternsFile = "patterns.artk";
  
  /* New configuration */
  __useUEye=false;
  __usePTAMM=false;
  __useARTK=false;
  __usePhotoPos=false;
  __useCommunications=false;
  //__useVideoInput=false;
  __useVideoInput="";
  __useVideoStreaming=false;
  __numPTAMMMaps=0;

  
  
  try{
    __configFile.open(CONFIG_FILE, std::ios::in);
    if (__configFile.is_open()){
      parseConfig();
    }else{
      Logger::getLogger()->warning("Config:: No config file: "  CONFIG_FILE);
    }
  } catch (std::exception e){
    Logger::getLogger()->error("Config:: Processig "  CONFIG_FILE );
  }
  
}
  
  void Configuration::parseConfig(){
    std::string line;
    std::string tempP;
    std::string tempV;
    
    int nLine=0;
    
    if (__configFile.is_open())
      {
	while (!__configFile.eof() )
	  {
	    nLine++;
	    getline(__configFile, line);
	    // Remove comments
            std::string tline;
	    size_t sharpPos = line.find("#");
	    if ( sharpPos!= std::string::npos)
	      tline = line.substr(0, sharpPos);
	    else
	      tline = line;
	    // Look for [ and ]
	    size_t lBPos = tline.find("[");
	    size_t rBPos = tline.find("]");
	    size_t eqPos = tline.find(":=");
	    bool existLB = (lBPos != std::string::npos);
	    bool existRB = (rBPos != std::string::npos);
	    bool existEq = (eqPos != std::string::npos);
	    
	    bool goodLine = (existLB && existRB && existEq && (rBPos > lBPos + 1));
	    
	    if (goodLine){
	      tempP = tline.substr(tline.find("[")+1, tline.find("]")-tline.find("[")-1);
	      tempV = tline.substr(eqPos+3, tline.length()-eqPos);
	      std::transform(tempP.begin(), tempP.end(),
			     tempP.begin(), ::toupper);
	      addproperty(tempP, tempV, nLine);
	    }
	    
	  }
      }
    
  }
  
  void Configuration::addproperty(std::string property, std::string value, int nLine){
    if (property.compare("DEFAULTIMAGE")==0){
      _defaultImage = value;
      
    } 
    else if (property.compare("DEFAULTVIDEOTEXTURE")==0){
      
      _defaultVideoTexture = value;
      
    } 
    else if (property.compare("DEFAULTTEXTUREPATH")==0){
      if (value.at(value.length()-1) != '/')
	value = value.append("/");
      
      _defaultTexturePath = value;
      
    }
    else  if (property.compare("DEFAULTVIDEOPATH")==0){
      if (value.at(value.length()-1) != '/')
	value = value.append("/");
      
      _defaultVideoPath = value;
      
    } 
    else if (property.compare("ARTKDATAPATH")==0){
      
      if (value.at(value.length()-1) != '/')
	value = value.append("/");
      
      _ARTKDataPath = value;
    }
    else if (property.compare("DEFAULTSOUNDPATH")==0){
      
      if (value.at(value.length()-1) != '/')
	value = value.append("/");
      
      _defaultSoundPath = value;
    }
    else if (property.compare("OPENGLFULLSCREEN")==0){
      std::string tValue = value;
      std::transform(tValue.begin(), tValue.end(),
		     tValue.begin(), ::toupper);
      if ( tValue.find("YES") != std::string::npos )
	_openGLFullScreen = true;
      else if (tValue.find("NO") != std::string::npos)
	_openGLFullScreen = false;
      else{
        std::stringstream msg;
	msg << "Bad openGLFullScreen value: " << value;
	Logger::getLogger()->warning(msg.str());
	_openGLFullScreen = false;
	return;
      }
    } 
    else if (property.compare("OPENGLSCREENWIDTH")==0){
      std::stringstream ss(value);
      ss >> _openGLScreenWidth;
    } 
    else if (property.compare("OPENGLSCREENHEIGHT")==0){
      std::stringstream ss(value);
      ss >> _openGLScreenHeight;
    } 
    else if (property.compare("OPENGLSCREENDEPTH")==0){
      std::stringstream ss(value);
      ss >> _openGLScreenDepth;
    } 
    else if (property.compare("ARTKPATTERNSFILE")==0){
      _ARTKPatternsFile = value;
    }
    else if (property.compare("ADDSTREAMURL")==0){
      addStreamURL(value, nLine);
    }

    else if (property.compare("USEUEYE")==0){
      std::string tValue = value;
      std::transform(tValue.begin(), tValue.end(),
		     tValue.begin(), ::toupper);
      if ( tValue.find("YES") != std::string::npos ){
	__useUEye = true;
      }
      else if (tValue.find("NO") != std::string::npos){
	__useUEye = false;
      }
    } 




    else if (property.compare("USEPTAMM")==0){
      std::string tValue = value;
      std::transform(tValue.begin(), tValue.end(),
		     tValue.begin(), ::toupper);
      if ( tValue.find("YES") != std::string::npos )
	__usePTAMM = true;
      else if (tValue.find("NO") != std::string::npos)
	__usePTAMM = false;
    } 

    else if (property.compare("USEARTK")==0){
      std::string tValue = value;
      std::transform(tValue.begin(), tValue.end(),
		     tValue.begin(), ::toupper);
      if ( tValue.find("YES") != std::string::npos )
	__useARTK = true;
      else if (tValue.find("NO") != std::string::npos)
	__useARTK = false;
    } 

    else if (property.compare("USEPHOTOPOS")==0){
      std::string tValue = value;
      std::transform(tValue.begin(), tValue.end(),
		     tValue.begin(), ::toupper);
      if ( tValue.find("YES") != std::string::npos )
	__usePhotoPos = true;
      else if (tValue.find("NO") != std::string::npos)
	__usePhotoPos = false;
    } 

    else if (property.compare("USECOMMUNICATIONS")==0){
      std::string tValue = value;
      std::transform(tValue.begin(), tValue.end(),
		     tValue.begin(), ::toupper);
      if ( tValue.find("YES") != std::string::npos )
	__useCommunications = true;
      else if (tValue.find("NO") != std::string::npos)
	__useCommunications = false;
    } 

    else if (property.compare("USEVIDEOINPUT")==0){
      std::stringstream ss(value);
      ss >> __useVideoInput;
    } 
    
    else if (property.compare("USEVIDEOSTREAMING")==0){
      std::string tValue = value;
      std::transform(tValue.begin(), tValue.end(),
		     tValue.begin(), ::toupper);
      if ( tValue.find("YES") != std::string::npos )
	__useVideoStreaming = true;
      else if (tValue.find("NO") != std::string::npos)
	__useVideoStreaming = false;
    } 
    else if (property.compare("NUMPTAMMMAPS")==0){
      std::stringstream ss(value);
      ss >> __numPTAMMMaps;
    } 
    
  }
  
  void Configuration::addStreamURL(std::string csv, int nLine){
	// This should be a csv : comma separated value
	// 3 values = 2 commas
    std::string orig = csv;
	urlStreamConfig* tempUSC = new urlStreamConfig();
	size_t firstNonSpace = csv.find_first_not_of(" ");

	size_t fComma = csv.find_first_of(",");
	if (fComma == std::string::npos){
          std::cout << std::endl;
		std::stringstream msg;
		msg << "Config::  At [" << nLine  << "] Bad CVS [addStreamURL]" << orig;
		Logger::getLogger()->warning(msg.str());
                std::cout << "  Example: [addStreamURL] :=" <<
                  " http://www.vid.com/file.mp4,name,stream description" << std::endl;
                std::cout << std::endl;
		delete tempUSC;
		return;
	}

	tempUSC->URL = csv.substr( firstNonSpace, fComma);
	csv = csv.substr(fComma+1, csv.length()-fComma-1);


	size_t sComma = csv.find_first_of(",");
	if (sComma == std::string::npos){
          std::cout << std::endl;
		std::stringstream msg;
		msg << "Config::  At [" << nLine  << "] Bad CVS [addStreamURL]" << orig;
		Logger::getLogger()->warning(msg.str());
                std::cout << "**Example: [addStreamURL] :=" <<
                  " http://www.exmaple.com/video.mp4,name,little description" << std::endl;
                std::cout << std::endl;
		delete tempUSC;
		return;
	}

	firstNonSpace = csv.find_first_not_of(" ");
	tempUSC->name = csv.substr( firstNonSpace, sComma);

	csv = csv.substr(sComma+1, csv.length()-sComma-1);
	tempUSC->description = csv;

	__streamsURL.push_back(tempUSC);

}

Configuration* Configuration::getConfiguration(){
	return getInstance();
}



  void Configuration::parseARTKPatternsFile(std::vector<ARTKMark>& marks){
  try{
    _ARTKPatternsFile = _ARTKDataPath + _ARTKPatternsFile;
    __patternsFile.open(_ARTKPatternsFile.c_str() , std::ios::in);
  } catch (std::exception e){
    Logger::getLogger()->error("Config:: Processig "  + _ARTKPatternsFile );
  }
  
  if (!__patternsFile.is_open()){
    Logger::getLogger()->warning("Config:: No config file: " + _ARTKPatternsFile);
  }
  else {
    int nLine=0;
    std::string line;
    char tempName[255];
    float d[16] = {1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1};
    
    while (!__patternsFile.eof() )
      {
	nLine++;
	getline(__patternsFile, line);
	
	if (line.compare("") == 0) continue;
	
	setlocale (LC_NUMERIC, "C"); // ... so we have an uniform config file...
	
	float size = 0;
	int nElem;
	char type[255];
	
	int type_flag = 0;
	
	sscanf(line.c_str(),"%s ",type);
	
	if(type[0]=='a'){
	  /*ACTUATION MARK*/
	  type_flag|=ACTUATION_MARK;
	  nElem = sscanf(line.c_str(), "%s %s %f",
			 type, tempName, &size);
	  
	  if (nElem != 3){
	    std::stringstream msgW;
	    msgW << _ARTKPatternsFile << ":: Line(" << nLine << ") is corrupt. ::: " << line;
	    Logger::getLogger()->warning(msgW.str());
	  }
	}else if(type[0]=='b' || type[0]=='p' || type[0]=='u'){
	 
	  if(type[0]=='b'){/*POSITION AND ACTUATION MARK*/
	    type_flag|=ACTUATION_MARK;
	    type_flag|=POSITION_MARK;
	  }else if(type[0]=='p'){/*POSITION MARK*/
	    type_flag|=POSITION_MARK;
	  }else if(type[0]=='u'){/*USER MARK*/
	    type_flag|=USER_MARK;
	  }

	  nElem = sscanf(line.c_str(), "%s %s %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f",
			 type, tempName, &d[0], &d[1], &d[2], &d[3], &d[4], &d[5], &d[6], &d[7],
			 &d[8], &d[9], &d[10], &d[11], &d[12], &d[13], &d[14], &d[15], &size);
	  
	  if (nElem != 19){
	    std::stringstream msgW;
	    msgW << _ARTKPatternsFile << ":: Line(" << nLine << ") is corrupt. ::: " << line;
	    Logger::getLogger()->warning(msgW.str());
	  }			  
	}else{
	  Logger::getLogger()->warning(":: Corrupt line in patterns.artk. :::");
	  return;
	}
	
	// -------------- To string ;)
	std::stringstream ss;
	ss << "Adding ARTK Mark: " << tempName;
	for (int i=0;i<16;i++) ss << " " << d[i];
	ss << " Size(" <<  size << ")";
	// --------------
	Logger::getLogger()->note(ss.str());
	Matrix16 m((GLfloat*) d);
#ifndef NDEBUG
        std::cout << "[DEBUG]: Adding Mark: " << tempName << std::endl;
        std::cout << m << std::endl;
#endif
	ARTKMark mark(m, std::string(this->_ARTKDataPath + tempName), size, type_flag);
	marks.push_back(mark);
      }
  
  }
}
  

Configuration::~Configuration(){
#ifndef NDEBUG
  std::cout << "[DEBUG]: Configuration:: Cleaning up." << std::endl;
#endif
	for (unsigned i=0; i < __streamsURL.size(); i++){
		delete __streamsURL.at(i);
	}
}

}
