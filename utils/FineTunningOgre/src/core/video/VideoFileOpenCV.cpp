/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * VideoFileOpenCV.cpp
 *
 */

#include "VideoFileOpenCV.h"

namespace mars {

VideoFileOpenCV::VideoFileOpenCV() {
	std::cout << "**Warning: This constructor should not be used. Use " <<
          "VideoFileOpenCV::VideoFileOpenCV(string pathToFile) instead" << std::endl;
}

VideoFileOpenCV::VideoFileOpenCV(const string& pathToFile){
	init(pathToFile);
}

VideoFileOpenCV::VideoFileOpenCV(const string& pathToFile, const std::string& calib_name)
	: VideoFile(calib_name) {
	init(pathToFile);
}

void VideoFileOpenCV::init(const std::string& pathToFile){
	this->path = pathToFile;
	this->description = "Video File: " + path + "";
	try{
		this->cap = new cv::VideoCapture(path);
	} catch (cv::Exception e) {
		std::cout << "Error: " << e.err << std::endl;
		throw e; // Up to you ...
	}
	if (!cap->isOpened()){
		Logger::getLogger()->warning("VideoFileOpenCV:: No VideoCapture Open for this file:" + path );
		__done = true;
		framesCount = 0;
		return;
	}

	framesCount = (int)   cap->get(CV_CAP_PROP_FRAME_COUNT);
	if ( framesCount ==0 )
		Logger::getLogger()->warning("VideoFileOpenCV:: Video File " + path + " seems not to be 100% opencv compatible.");
	fps         = (float) cap->get(CV_CAP_PROP_FPS);
	width       = (int)   cap->get(CV_CAP_PROP_FRAME_WIDTH);
	height      = (int)   cap->get(CV_CAP_PROP_FRAME_HEIGHT);

#ifndef NDEBUG
	std::cout << "[DEBUG]: " << description << " - Numbers of frames: " << framesCount << " - FPS: " << fps << std::endl;
#endif
}

void VideoFileOpenCV::ThreadCaptureFrame(){
	Mat* frame = NULL;

	Uint32 ticks0;
	Uint32 ticks1;
	int evenFrame = 0;
	unsigned nFrame = 0;

	float ticksPerFrame = 1000.0 / fps;

	ticks0 = SDL_GetTicks();
	ticks1 = ticks0;

	while(!__done){


	  frame = new Mat();

		*cap >> *frame;
		nFrame++;

		if(nFrame==framesCount){
		  nFrame=0;
		  cap->set(CV_CAP_PROP_POS_AVI_RATIO,0);
		}


#if 0
		cout << nFrame << " / " << framesCount << endl;
#endif
		//if (nFrame > framesCount) __done=true;

#if 0
		cout << "Ticks per Frame: " << ticksPerFrame << endl;
		cout << "t0/t1: " << ticks0 << " / " << ticks1 << endl;
		cout << "evenframe: " << evenFrame << endl;
#endif

		// Let's control the fps
		while (!__done &&  ( (ticks1 - ticks0) < (ticksPerFrame - evenFrame))  ){
			threadWait(ticksPerFrame*0.10);
			ticks1 = SDL_GetTicks();

#if 0
			cout << ticks0 << "::" << ticks1 << "::" << (ticks1 - ticks0) << "::" << (ticksPerFrame - evenFrame) << endl;
#endif
		}






		if(!__done){
			fBuffer->addFrame(frame);
			ticks0 = ticks1;
			evenFrame = !(evenFrame);
		}

	}



}



VideoFileOpenCV::~VideoFileOpenCV() {}

}
