#ifndef VIDEOPLANE_H
#define VIDEOPLANE_H
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <Ogre.h>

#include <core/Configuration.h>



class VideoPlane{
 private:

  cv::VideoCapture __streamingVC;
  Ogre::SceneManager * __sceneManager;

  std::string _streamingUrl;
  Ogre::SceneNode* _node;
  std::string _name;
  Ogre::TexturePtr _texture;

  int _w;
  int _h;
  float _scale;

 public:
  
  VideoPlane(const std::string& name, const Ogre::Root& r, const std::string& url, Ogre::SceneNode* node, int w, int h, float scale=0.005);
  virtual ~VideoPlane();
  
  void refreshTexture();
  void setPosition();
  
};

#endif
