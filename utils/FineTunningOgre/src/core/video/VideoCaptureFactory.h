/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/* This class provides an abstract factory for the VideoCapture */
// This is a singleton class

/*
 * VideoCaptureFactory.h
 *
 */

#ifndef VIDEOCAPTUREFACTORY_H_
#define VIDEOCAPTUREFACTORY_H_

#include <iostream>
#include <vector>
#include <core/video/VideoCapture.h>
#include <core/video/VideoCaptureOpenCV.h>
#include <core/video/VideoCaptureUEye.h>
#include <uEye.h>


#include <core/Singleton.h>

enum VIDEO_CAPTURE_TYPE {OPENCV, UEYE};

namespace mars {

class VideoCaptureFactory : public Singleton<VideoCaptureFactory>{
public:

	static VideoCaptureFactory* getFactory();
	VideoCapture* createVideoCapture(VIDEO_CAPTURE_TYPE vCaptureType);

	//	VideoCapture* createVideoCapture(VIDEO_CAPTURE_TYPE vCaptureType, unsigned videoCaptureNumber);

private:
//	static VideoCaptureFactory* __instance;
	VideoCaptureFactory();
	virtual ~VideoCaptureFactory();

	std::vector<VideoCapture*> __vCapturers;

	friend class Singleton<VideoCaptureFactory>;

	int __uEyeNext; // Number of ueye device to return;
	int __uEyeNDevices;
	PUEYE_CAMERA_LIST __pCam;
	bool __wInit;


};

}

#endif /* VIDEOCAPTUREFACTORY_H_ */
