/*
 * VLCPlayer.cpp
 *
 */

#include "VLCPlayer.h"

namespace mars {


void OneMedia::update(){
	if (!playing) return;
#if 0
	std::cout << "len :: " << libvlc_media_player_get_length(media) << std::endl;
	std::cout << "tim :: " << libvlc_media_player_get_time(media) << std::endl;
#endif
	if (libvlc_media_player_get_length(media) == -1) return;
	if (libvlc_media_player_get_length(media) == 0) return;
	if (libvlc_media_player_get_time(media) == -1) return;
	if(libvlc_media_player_get_length(media)-1000 < libvlc_media_player_get_time(media)){
		libvlc_media_player_stop(media);
		playing = false;
#ifndef NDEBUG
		std::cout << "Video Finnished" << std::endl;
#endif
    }
}
OneMedia::~OneMedia(){
	if (playing){
          std::cout << "Stop" << std::endl;
		libvlc_media_player_stop(media);
	}
	std::cout << "Releasing Media" << std::endl;
	libvlc_media_player_release(media);

	std::cout << "Destroying.." << std::endl;
	// FIXME Check this over the time
	SDL_DestroyMutex(ctx.mutex);
	SDL_FreeSurface(ctx.surface);
}

VLCPlayer::VLCPlayer() {
	initializeVLC();
}


void VLCPlayer::addMedia(std::string name, std::string fileName, bool isURL){
	std::map<std::string, OneMedia* >::iterator it;

	libvlc_media_t* tempMedia;

	it = __mediaPlayers.find(fileName);

	if (it!=__mediaPlayers.end() && !isURL){
//		Logger::getLogger()->note("VLCPlayer :: Trying to add an already existing Video ( " + it->first + " ) : " + fileName);
		return;
	}


	//if (!isURL){
		// Bring the media file up.
		tempMedia = libvlc_media_new_path(__libvlc, (Configuration::getInstance()->defaultVideoPath() + fileName).c_str());
//	} else {
		tempMedia = libvlc_media_new_path(__libvlc, fileName.c_str());
//	}


	if (tempMedia == NULL){
		Logger::getInstance()->note("VLCPlayer :: libvlc_media_new_path returned NULL : " + name + " : " + fileName);
	}

	OneMedia* structMedia = new OneMedia;
	structMedia->media = libvlc_media_player_new_from_media(tempMedia);

	// Release the tempMedia ...
	libvlc_media_release(tempMedia);


	structMedia->playing = false;

	structMedia->width = 256;
	structMedia->heigh = 256;

	// Let's create the surface

	// Masks ...
	Uint32 rmask = 0x000000ff;
	Uint32 gmask = 0x0000ff00;
	Uint32 bmask = 0x00ff0000;
	Uint32 amask = 0xff000000;
	// 32 bits please.

	structMedia->ctx.surface = SDL_CreateRGBSurface(SDL_SWSURFACE, structMedia->width, structMedia->heigh, 32, rmask, gmask, bmask, amask );


	// Let's create the mutex
	structMedia->ctx.mutex = SDL_CreateMutex();

	// Setup callbacks
	libvlc_video_set_callbacks(structMedia->media, lock, unlock, display, &(structMedia->ctx));

	//Set Format FIXME format
	libvlc_video_set_format(structMedia->media, "RV32", structMedia->width, structMedia->heigh, structMedia->width*4);

	// Add the player ... :)
	__mediaPlayers[fileName] = structMedia;

}

OneMedia* VLCPlayer::getMediaFromName(std::string name){
	std::map<std::string, OneMedia* >::iterator it;

	it = __mediaPlayers.find(name);

	if (it==__mediaPlayers.end()){
			Logger::getLogger()->note("VLCPlayer :: Cannot get value from non added media : " + name);
			return NULL;
		}

	return it->second;
}

void VLCPlayer::playMedia(std::string name){

	std::map<std::string, OneMedia* >::iterator it;

	it = __mediaPlayers.find(name);

	if (it==__mediaPlayers.end()){
		Logger::getLogger()->note("VLCPlayer :: Cannot play a non added media : " + name);
			return;
		}

	it->second->playing = true;


	Logger::getLogger()->note("VLCPlayer :: Playing media : " + name);
	//Please VLC, play it...
	libvlc_media_player_play(it->second->media);

}

void VLCPlayer::stopMedia(std::string name, bool isURL){

	std::map<std::string, OneMedia* >::iterator it;

	it = __mediaPlayers.find(name);

	if (it==__mediaPlayers.end()){
			Logger::getLogger()->note("VLCPlayer :: Cannot stop a non added media : " + name);
			return;
		}

	if (!it->second->playing){
		Logger::getLogger()->note("VLCPlayer :: Cannot stop a non playing media : " + name);
		return;
	}

	Logger::getLogger()->note("VLCPlayer :: Stopping media: " + name);

	if (isURL){
		it->second->playing=false;
		std::cout << "URL" << std::endl;
		return;
	}

	std::cout << "NONURL" << std::endl;
	libvlc_media_player_stop(it->second->media);
	it->second->playing=false;
}

void VLCPlayer::destroyMedia(std::string name){

	std::map<std::string, OneMedia* >::iterator it;

	it = __mediaPlayers.find(name);

	if (it==__mediaPlayers.end()){
			Logger::getLogger()->note("VLCPlayer :: Cannot destroy a non added media : " + name);
			return;
		}

	__mediaPlayers.erase(it);

	delete it->second;

}


void VLCPlayer::initializeVLC(){
	// Initialize VLC and so ...

    char const* args[] = {
        "--no-xlib",
//        "-vvv",
        "--vmem-chroma", "RV32",
        "--no-video-title-show",
        "--ignore-config",
//        "--no-overlay",
    };

    // Process number of args on the preceding variable
    int nArgs = sizeof(args) / sizeof(*args);

    __libvlc = libvlc_new(nArgs, args);



}

void* VLCPlayer::lock(void* data, void** pixels){

	Ctx *ctx = static_cast<Ctx*>(data); // Cast or die

	SDL_LockMutex(ctx->mutex);
	SDL_LockSurface(ctx->surface);

	*pixels = ctx->surface->pixels;

	return NULL;
}

void  VLCPlayer::unlock(void* data, void* id, void* const* pixels){

	Ctx *ctx = static_cast<Ctx*>(data); // Cast or die

	// Stuff needed go here... we need nothing at this moment

	// Unlocks
	SDL_UnlockSurface(ctx->surface);
	SDL_UnlockMutex(ctx->mutex);

	assert(id == NULL);

}

void VLCPlayer::display(void* data, void* id){
	// We display nothing ... this is actually done by a RenderableVideoPlane ;)
	// but the function must be created so it can be called.
	(void) data;
	assert(id ==NULL);

}

VLCPlayer::~VLCPlayer() {
	Logger::getLogger()->note("Ending VLCPlayer");

	std::map<std::string, OneMedia* >::iterator it;

	int i = 0;

	for (it=__mediaPlayers.begin(); it!=__mediaPlayers.end(); ++it){
		std::cout << i++ << std::endl;
//		delete it->second;
	}

	libvlc_release(__libvlc);
}

}
