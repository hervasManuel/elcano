/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * VideoCapture.cpp
 *
 *
 *
 */

#include "VideoCapture.h"


namespace mars{

VideoCapture::VideoCapture(){
}

void VideoCapture::addVideoSource_(VideoFile* vF){
	__videoFileList.push_back(vF); // Add device to the list of devices
	__videoFileMap[vF->getName()] = vF;
	addThread(vF);
}

void VideoCapture::addVideoSource_(VideoDevice* vD){
	__videoDeviceList.push_back(vD); // Add device to the list of devices
	__videoDeviceMap[vD->getName()] = vD;
	addThread(vD);
}


void* VideoCapture::exec(void *thr) {
	reinterpret_cast<VideoSource *> (thr)->ThreadCaptureFrame();
	return NULL;
}

string VideoCapture::getName(){
	return __videoCaptureName;
}


cv::Mat* VideoCapture::getFrame(VideoSource* vS, unsigned nFrame){
	if ((nFrame < 0) || (nFrame >= vS->getFBuffer()->size())){
#ifndef NDEBUG
	  std::cout <<"[DEBUG]: Video device: " << vS->getDescription() << " has no frame #" << nFrame << std::endl;
#endif
		return NULL;
	}
	return vS->getFBuffer()->getFrame(nFrame);

}

cv::Mat* VideoCapture::getLastFrame(VideoSource* vS){
	return vS->getFBuffer()->getLastFrame();
}

VideoDevice* VideoCapture::getVideoDevice(unsigned videoDevicePosition){
	if (videoDevicePosition >= __videoDeviceList.size() ){
#ifndef NDEBUG
	  std::cout <<"[DEBUG]: Bad Video device: " << videoDevicePosition << " is not a member" << std::endl;
#endif
		return NULL;
	}
#ifndef NDEBUG
	std::cout <<"[DEBUG]: Returning: " << __videoDeviceList.at(videoDevicePosition)->getDescription() << " device" << std::endl;
#endif

	return __videoDeviceList.at(videoDevicePosition);

}

VideoFile* VideoCapture::getVideoFile(unsigned videoFilePosition){
	if (videoFilePosition >= __videoFileList.size() ){
#ifndef NDEBUG
	  std::cout <<"[DEBUG]: Bad Video file: " << videoFilePosition << " is not a member" << std::endl;
#endif
		return NULL;
	}
#ifndef NDEBUG
	std::cout <<"[DEBUG]: Returning: " << __videoFileList.at(videoFilePosition)->getDescription() << " device" << std::endl;
#endif

	return __videoFileList.at(videoFilePosition);

}

VideoDevice* VideoCapture::getVideoDevice(std::string name){
  std::map<string, VideoDevice*>::iterator it;

	it = __videoDeviceMap.find(name);
	if (it == __videoDeviceMap.end()){
		// Mandatory to stop ...
		Logger::getLogger()->error("VideoCapture getVideoDevice(string name):: Cannot find a videodevice called " + name);
		std::exit(-3);
	} else {
		return it->second;
	}
}

VideoFile* VideoCapture::getVideoFile(std::string name){
  std::map<string, VideoFile*>::iterator it;

	it = __videoFileMap.find(name);
	if (it == __videoFileMap.end()){
		// Mandatory to stop ...
		Logger::getLogger()->error("VideoCapture getVideoFile(string name) :: Cannot find a videodevice called " + name);
		std::exit(-3);
	} else {
		return it->second;
	}

}

void VideoCapture::addThread(VideoSource* vS){
	__threads.push_back( pthread_create(vS->getThreadId() ,NULL, &VideoCapture::exec,vS));
}

void VideoCapture::setDone(){
	for (unsigned i = 0; i < __videoDeviceList.size(); ++i){
		__videoDeviceList.at(i)->setDone();
	}

	for (unsigned i = 0; i < __videoFileList.size(); ++i){
		__videoFileList.at(i)->setDone();
	}
}


VideoCapture::~VideoCapture(){


	for (unsigned i = 0; i < __videoDeviceList.size(); ++i){
		delete __videoDeviceList.at(i);
	}

	for (unsigned i = 0; i < __videoFileList.size(); ++i){
		delete __videoFileList.at(i);
	}

	//	map<string, VideoDevice*>::iterator itVD;
	//	map<string, VideoFile*>::iterator itVF;
	//
	//	itVD = __videoDeviceMap.begin();
	//	itVF = __videoFileMap.begin();
	//
	//	while(itVD != __videoDeviceMap.end())
	//		delete(itVD->second);
	//
	//	while(itVF != __videoFileMap.end())
	//		delete(itVF->second);


}

}
