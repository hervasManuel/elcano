/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * VideoSource.h
 *
 */

#ifndef VIDEOSOURCE_H_
#define VIDEOSOURCE_H_

#include <string>
#include <vector>
#include <iostream>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <core/FramesBuffer.h>
#include <core/Vector3D.h>
#include <core/Matrix16.h>
#include <core/Logger.h>
#include <core/Configuration.h>
#include <Ogre.h>

#include <sys/time.h>

namespace mars{

class VideoSource{
public:

	VideoSource(std::string name="");

	/**
	 *  Alternative constructor. It'll create a nFrames FramesBuffer for this video source.
	 *
	 *  @param nFrame how many frames has the associated buffer.
	 */
	VideoSource(unsigned nFrames, std::string name="");

	/**
	 *
	 *  Returns the frame at nFrame.
	 *  This can be done via fBuffer too. This is given just for standarization.
	 *
	 *  IT CREATES AN INSTANCE OF A cv:Mat, SO YOU SHOULD FREE THE MEM AFTER ITS USE.
	 *  e.g.:
	 *
	 *  cv::Mat* i = vC->getFrameAt(0);
	 *
	 *  // use i ...
	 *
	 *  delete i;
	 *
	 * @param nFrame number of frame to return.
	 */
	cv::Mat* getFrameAt(unsigned nFrame);



	/**
	 *  Returns the last frame.
	 *  This can be done via fBuffer too. This is given just for standarization.
	 *
	 *  IT CREATES AN INSTANCE OF A cv:Mat, SO YOU SHOULD FREE THE MEM AFTER ITS USE.
	 *  e.g.:
	 *
	 *  cv::Mat* i = vC->getLastFrame();
	 *
	 *  // use i ...
	 *
	 *  delete i;
	 *
	 */
	cv::Mat* getLastFrame();

	/**
	 *  Holds a thread for the given milliseconds
	 *
	 *  @param milliseconds
	 */
	static int threadWait(int milliseconds);

	virtual ~VideoSource();

	/**
	 * Used internally.
	 * It should never be used by the coder.
	 */
	virtual void ThreadCaptureFrame() = 0;


	/**
	 * Returns the videoSource Eye (i.e, its location)
	 */
	Point3D getEye() const;

	/**
	 * Returns the videoSource Up vector.
	 */
	Vector3D getUp() const ;

	/**
	 * Returns the videoSource LookTo vector.
	 */
	Vector3D getLookTo() const;


	/**
	 * Sets VideoSource's Eye point.
	 */
	void setEye(const float& x, const float& y, const float& z);

	/**
	 * Sets Videosources's Up vector
	 */
	void setUp(const float& x, const float& y, const float& z);

	/**
	 * Sets VideoSource's LookTo vector
	 */
	void setLookTo(const float& x, const float& y, const float& z);

	/**
	 * Finishes the capture. This is used automagically and should be used with care.
	 */
	void setDone();

	/**
	 * Returns the VideoSource's name.
	 */
	std::string getName();

	/**
	 * Returns the associated OpenGL camera (as a Camera3D)
	 *
	 * @see Camera3D
	 */
	//Ogre::Camera* getOgreCamera();


	/**
	 * Returns true if the camera has calibration info associated.
	 */
	bool isCalibrated();

	Ogre::Matrix4 getOgreProjectionMatrix();

	/**
	 * Returns the perspective matrix calculated via camera calibration.
	 */
	void getPerspectiveMatrix(double m[3][4]);

	/**
	 * Returns the distortion matrix .
	 */
	void getDistortionMatrix(double k[5]);


	pthread_t* getThreadId(){ return &thread_id; }

	int getWidth(){ return width; }

	int getHeight(){ return height; }

	cv::Mat* getDefaultImage(){ return defaultImage; }

	FramesBuffer* getFBuffer() { return fBuffer; }

	string getDescription() { return description; }

	unsigned int getId(){ return id; }

	string getType() { return type; }

	string getPath() { return path; }

	void setPosMatrix(const cv::Mat& posMatrix);
	cv::Mat* getPosMatrix();


protected:


	/**
	 * Calculates the opengl projection matrix from the previous calculated camera matrix (see calibration)
	 */
	void calcProjectionMatrixFromCameraMatrix();


	/**
	 * Called by every constructor.
	 */
	void init();


	unsigned int id;         ///< This number is used by cv::VideoCapture
	float fps;               ///< The number of frames that captured per second
	int width;               ///< The source's width
	int height;              ///< The source's height
	pthread_t thread_id;     ///< The id of the thread associated.
	FramesBuffer* fBuffer;   ///< Vector of frames.
	string type;             ///< Type
	string path;             ///< Path to the file or device
	string description;      ///< Text description of the video device.
	cv::Mat* defaultImage;   ///< Default image (no frames yet).


	bool __done;

	Vector3D __eye;
	Vector3D __lookTo;
	Vector3D __up;


	std::string __name;

	bool __calibrated;

	cv::Mat __cameraMatrix;
	cv::Mat __distCoeffs;

	//Ogre::Camera* __ogreCamera;

private:
	float __fx;
	float __fy;
	float __cx;
	float __cy;
	Mat* __posMatrix;

};

}

#endif /* VIDEOSOURCE_H_ */
