/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * VideoSource.cpp
 *
 */

#include "VideoSource.h"

namespace mars{

VideoSource::VideoSource(std::string name){
	fBuffer = new FramesBuffer();  // We have to create a default buffer
	__name = name;
	init();

#ifndef NDEBUG
	std::cout << "[DEBUG]: VideoSource init" <<std::endl;
#endif
}

VideoSource::VideoSource(unsigned nFrames, std::string name){
	fBuffer = new FramesBuffer(nFrames);  // ... or with n Frames
	__name = name;
	init();
        std::cout << "VideoSource init" << std::endl;
}

void VideoSource::init(){


	__fx = 0;
	__fy = 0;
	__cx = 0;
	__cy = 0;

	//__ogreCamera = NULL;

	__calibrated = false;
	Configuration* cfg=mars::Configuration::getConfiguration();

	defaultImage = new cv::Mat(cv::imread(cfg->defaultImage()));

	__done = false;
	if (__name.compare("")!=0){
		string cName = "calibs/" + __name + "-calib.xml";
		try {
			cv::FileStorage fs(cName.c_str(), cv::FileStorage::READ);
			if (fs.isOpened()){
				fs["cm"] >> __cameraMatrix;
				fs["dm"] >> __distCoeffs;
				string msg = "calibs/" + __name + "-calib.xml found and loaded!";
				Logger::getLogger()->note(msg);
				__calibrated = true;
				calcProjectionMatrixFromCameraMatrix();
			} else {
				Logger::getLogger()->error(cName + " cannot be open, does it exist? Maybe you should calibrate that videosource");
			}

		} catch (cv::Exception e) {
			string msg = "Problem with calibs/" + __name + "-calib.xml found!";
			Logger::getLogger()->warning(msg);
			Logger::getLogger()->error(e.msg);
		}


	} else {
		Logger::getLogger()->warning("No camera name given, so no calibration file will be used!");
	}


}
cv::Mat* VideoSource::getFrameAt(unsigned nFrame){
	return fBuffer->getFrame(nFrame);
}

cv::Mat* VideoSource::getLastFrame(){
	if (fBuffer->isEmpty()){
		return new cv::Mat(*defaultImage);
	}
	return fBuffer->getLastFrame();
}

VideoSource::~VideoSource(){

	__done = true;
#ifndef NDEBUG
	std::cout << "VideoSource:: Waiting for the thread (" <<  thread_id << ") to finish and join...";
#endif

	pthread_join(thread_id, NULL);

#ifndef NDEBUG
	std::cout << "done!" << std::endl << std::flush;
#endif

	delete fBuffer;        // ...And destroy it on the destructor
	delete defaultImage;

//	if (__openGLCamera!=NULL)
//	delete __ogreCamera;
}

Vector3D VideoSource::getEye() const{
	return __eye;
}

Vector3D VideoSource::getUp() const{
	return __up;
}

Vector3D VideoSource::getLookTo() const{
	return __lookTo;
}

void VideoSource::setEye(const float& x, const float& y, const float& z){
	__eye.x = x;
	__eye.y = y;
	__eye.z = z;
}

void VideoSource::setUp(const float& x, const float& y, const float& z){
	__up.x = x;
	__up.y = y;
	__up.z = z;
}

void VideoSource::setLookTo(const float& x, const float& y, const float& z){
	__lookTo.x = x;
	__lookTo.y = y;
	__lookTo.z = z;
}

void VideoSource::setDone(){
	__done = true;
}

std::string VideoSource::getName(){
  return __name;
}
  
  
  
  Ogre::Matrix4 VideoSource::getOgreProjectionMatrix(){

    float near  = 0.01f;
    float far = 1000.0f;

    double fx = __cameraMatrix.at<double>(0,0);
    double fy = __cameraMatrix.at<double>(1,1);
    double cx = __cameraMatrix.at<double>(0,2);
    double cy = __cameraMatrix.at<double>(1,2);

    float w = (float) (Configuration::getConfiguration()->openGLScreenWidth() );
    float h = (float) (Configuration::getConfiguration()->openGLScreenHeight() );
    
    Ogre::Matrix4 m(2.0*fx/w,  0.f,                          2.0*(cx/w)-1.0,           0.f,
		    0.f,       2.0*fy/h,                     2.0*(cy/h)-1.0,           0.f,
		    0.f,       0.f,-(far+near)/(far-near),   2.0*far*near/(far-near),
		    0.0f,      0.0f,                         -1,                       0.0f);
    
    return m;

  }

  void VideoSource::calcProjectionMatrixFromCameraMatrix(){ //FIXME calcProjectionMatrixFromCameraMatrix
#ifndef NDEBUG
    std::cout << "fx = " << __cameraMatrix.at<double>(0,0) <<std::endl;
    std::cout << "fy = " << __cameraMatrix.at<double>(1,1) <<std::endl;
    std::cout << "cx = " << __cameraMatrix.at<double>(0,2) <<std::endl;
    std::cout << "cy = " << __cameraMatrix.at<double>(1,2) <<std::endl;
#endif

	double fx = __cameraMatrix.at<double>(0,0);
	double fy = __cameraMatrix.at<double>(1,1);
	double cx = __cameraMatrix.at<double>(0,2);
	double cy = __cameraMatrix.at<double>(1,2);

	__fx = fx;
	__fy = fy;
	__cx = cx;
	__cy = cy;

	Matrix16 m;

	GLfloat* data = m.getData();

	float near  = 0.1f;
	float far = 10000.0f;
	//float far = 100.0f;

	float w = (float) (Configuration::getConfiguration()->openGLScreenWidth() );
	float h = (float) (Configuration::getConfiguration()->openGLScreenHeight() );

//	__cx = w / 2;
//	__cy = h / 2;

	data[0]  = 2.0*fx/w;
	data[1]  = 0;
	data[2]  = 0;
	data[3]  = 0;

	data[4]  = 0;
	data[5]  = 2.0*fy/h;
	data[6]  = 0;
	data[7]  = 0;

	data[8]  = 2.0*(cx/w)-1.0;
	data[9]  = 2.0*(cy/h)-1.0;
	data[10] = -(far+near)/(far-near);
	data[11] = -1;

	data[12] = 0;
	data[13] = 0;
	data[14] = -2.0*far*near/(far-near);
	data[15] = 0;

	//
	//Logger::getInstance()->error("Hay que pasar la matriz de proyección a la Cámara de Ogre!");

}

  
void VideoSource::getPerspectiveMatrix(double m[3][4]){

	if (!__calibrated)
		Logger::getInstance()->warning("VideoSource:: Getting Projection Matrix"
				" from a non calibrated source");

	m[0][0] = __fx; m[0][1] = 0;    m[0][2] = __cx; m[0][3] = 0;
	m[1][0] = 0;    m[1][1] = __fy; m[1][2] = __cy; m[1][3] = 0;
	m[2][0] = 0;	m[2][1] = 0;    m[2][2] = 1;    m[2][3] = 0;

}

void VideoSource::getDistortionMatrix(double k[5]){
	k[0] = __distCoeffs.at<double>(0,0);
	k[1] = __distCoeffs.at<double>(0,1);
	k[2] = __distCoeffs.at<double>(0,2);
	k[3] = __distCoeffs.at<double>(0,3);
	k[4] = __distCoeffs.at<double>(0,4);
}

bool VideoSource::isCalibrated(){
	return __calibrated;
}

int VideoSource::threadWait(int milliseconds){
	pthread_mutex_t mutex;
	pthread_cond_t condition;
	struct timespec timewaited;
	struct timeval now;

	if(pthread_mutex_init(&mutex,NULL))
		return -1;

	if(pthread_cond_init(&condition,NULL))
		return -1;

	gettimeofday(&now, NULL);

	timewaited.tv_sec = now.tv_sec + milliseconds / 1000;
	timewaited.tv_nsec = now.tv_usec*1000 + (milliseconds % 1000)*1000000;

	return pthread_cond_timedwait(&condition, &mutex, &timewaited);
}
  void VideoSource::setPosMatrix(const cv::Mat& posMatrix){
    if(__posMatrix!=NULL)
      delete __posMatrix;
    __posMatrix=new cv::Mat(posMatrix);
  }
  cv::Mat* VideoSource::getPosMatrix(){
    return __posMatrix;
  }

}
