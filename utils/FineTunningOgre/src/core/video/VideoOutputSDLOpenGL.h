/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * VideoOutputSDLOpenGL.h
 *
 */

#ifndef VIDEOOUTPUTSDLOPENGL_H_
#define VIDEOOUTPUTSDLOPENGL_H_

#include <iostream>
#include <SDL/SDL.h>
#include <SDL/SDL_opengl.h>
#include <GL/gl.h>
#include <GL/glu.h>


#include <core/Configuration.h>
#include <core/World.h>


namespace mars {

class VideoOutputSDLOpenGL : public Singleton<VideoOutputSDLOpenGL>{
public:

	/**
	 * Sets the window title.
	 *
	 * @param title The title
	 */
	void setWindowTitle(string title);


//	/**
//	 * Use only for testing purposes
//	 *
//	 * Please use step() instead
//	 *
//	 * @see step
//	 */
//	void go();

	/**
	 * Steps once the world.
	 *
	 * It draws and processes the events.
	 *
	 */
	void step();


	/**
	 * Sets the Ortho projection mode.
	 *
	 * After using this you should use setView to restore the view projection.
	 * If you don't, expect undefined behavior.
	 *
	 * @see setView
	 */
	void setOrtho() const;

	/**
	 * Restore the vide mode. This should only be used after a setOrtho() call.
	 *
	 * @see setOrtho
	 */
	void setView() const;


private:
	VideoOutputSDLOpenGL();
	virtual ~VideoOutputSDLOpenGL();

	void resizeWindow(int width, int height);

	void SDLInit();
	void OpenGLInit();

	//static void* exec(void *thr);

	SDL_Surface* __screen;

	int  __screenWidth;
	int  __screenHeigh;
	bool __screenFullScreen;
	int  __screenDepth;
	World * __world;
	pthread_t __thread_id;

	friend class Singleton<VideoOutputSDLOpenGL>;

};

}

#endif /* VIDEOOUTPUTSDLOPENGL_H_ */
