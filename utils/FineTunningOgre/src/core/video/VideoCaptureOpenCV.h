/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * VideoCapture.h
 *
 */

#ifndef VIDEOCAPTUREOPENCV_H_
#define VIDEOCAPTUREOPENCV_H_

#include <iostream>
#include <vector>
#include <string>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include "VideoCapture.h"
#include "VideoDeviceOpenCV.h"

namespace mars {


//
// This class will capture the frames from the given devices.
//
class VideoCaptureOpenCV: public VideoCapture {
private:


public:
	VideoCaptureOpenCV();

	/**
	 * Adds a new video source (CAMERA) to this OPENCV videocapture
	 * @param dev number of device
	 * @param name name of the videosource
	 */
	void addNewVideoSource(unsigned dev, const std::string& name);

	/**
	 * Adds a new video source (FILE) to this OPENCV videocapture
	 * @param path video file path
	 * @param name name of the videosource
	 */
	void addNewVideoSource(const std::string& path, const std::string& name);

	~VideoCaptureOpenCV();

};

}

#endif /* VIDEOCAPTUREOPENCV_H_ */
