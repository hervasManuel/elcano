/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * Test Output based on OpenCV's highgui
 *
 *
 * VideoOutputCV.cpp
 *
 */

#include "VideoOutputCV.h"

namespace mars {

VideoOutputCV::VideoOutputCV() {

}

void VideoOutputCV::createWindow(std::string windowName){
	__windowName = windowName;
	cv::namedWindow(__windowName, CV_WINDOW_AUTOSIZE );
}

std::string VideoOutputCV::getWindowName(){
	return __windowName;
}

void VideoOutputCV::setImage(cv::Mat* image){
#ifndef NDEBUG
	std::cout << "[DEBUG]: Setting image on window " << __windowName << std::endl;
#endif
	try{
			cv::imshow(__windowName, *image);
	}
	catch (cv::Exception){
			std::cout << "[DEBUG] : [IGNORING BAD FRAME]" << std::endl;
	}
}


VideoOutputCV::~VideoOutputCV() {}

}
