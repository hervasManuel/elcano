/*
 * VLCPlayer.h
 *
 */

#ifndef VLCPLAYER_H_
#define VLCPLAYER_H_

#include <SDL/SDL.h>
#include <vlc/vlc.h>
#include <vlc/libvlc_structures.h>
#include <core/Singleton.h>
#include <core/Logger.h>
#include <core/Configuration.h>
#include <iostream>
#include <string>
#include <vector>
#include <map>

#include <cassert>

namespace mars {

struct Ctx{
	SDL_Surface* surface;
	SDL_mutex* mutex;
};

struct OneMedia {
	Ctx ctx;
	libvlc_media_player_t* media;
	unsigned width;
	unsigned heigh;
	bool playing;
	void update();

	~OneMedia();

};

/**
 *
 * This class provides the necessary methods for playing videos inside mars.
 * IMPORTANT NOTE: you need libvlc-sdl-plugin installed ...
 *
 */
class VLCPlayer : public Singleton<VLCPlayer>{
public:

	/**
	 * Adds some media
	 * @param name unique name
	 * @param fileName the path to the media file
	 */
	void addMedia(std::string name, std::string fileName, bool isURL=false);

	/**
	 * Pays the media
	 */
	void playMedia(std::string name);

	/**
	 * Stops it
	 */
	void stopMedia(std::string name, bool isURL=false);

	/**
	 * Destroy a media given its unique name
	 */
	void destroyMedia(std::string name);

	/**
	 * Returns an special VLC struct given a media unique nane
	 */
	OneMedia* getMediaFromName(std::string name);


private:
	VLCPlayer();
	virtual ~VLCPlayer();

	static void* lock(void* data, void** pixels);
	static void  unlock(void* data, void* id, void* const* pixels);
	static void display(void* data, void* id);


	/**
	 * Initializes libVLC
	 */
	void initializeVLC();

	friend class Singleton<VLCPlayer>;

	libvlc_instance_t* __libvlc;


	// ------------------------------------------------------------------------
	// Maps a name with a mediaplayer
	// Afaik we need ONE mediaplayer for EACH video we want to play ...
	// ... libvlc documentation concurs.
	// ------------------------------------------------------------------------
	std::map<std::string, OneMedia* > __mediaPlayers;
	// ------------------------------------------------------------------------


};

}

#endif /* VLCPLAYER_H_ */
