/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/* 
   Lista los dispositivos de captura V4L2
*/

#ifndef VIDEO_ENUMERATE_H
#define VIDEO_ENUMERATE_H

#include <core/Logger.h>

namespace mars {

/**
 * Returns the name of the files associated to video devices
 */
  std::vector<std::string> getDevVideoNames();

/**
 * Prints the V4L2 devices
 */
void printDevices();

/**
 * Returns a vector of record capable video devices
 */
 std::vector<std::string> getDevVideoCaptureNames();

}
#endif
