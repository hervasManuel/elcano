// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#include "VideoPlane.h"
 
VideoPlane::VideoPlane(const std::string& name, const Ogre::Root& r, const std::string& url, Ogre::SceneNode* node, int w, int h, float scale): 
  _streamingUrl(url), _node(node), _name(name), _w(w), _h(h), _scale(scale)
{
    
  __sceneManager = r.getSceneManager("SceneManager");
 
  //std::cout<<"Scale: "<<_scale<<", _w*_scale: "<<_w*_scale<<std::endl;

 
  //std::string streamingUrl = "rtsp://161.67.106.103:8554/android.sdp" ;// mars::Configuration::getInstance()->getUrlStreaming();
  //std::string streamingUrl = "/home/fran/Downloads/02.avi";// rtsp://161.67.106.103:8554/android.sdp" ;// mars::Configuration::getInstance()->getUrlStreaming();
  
  __streamingVC = cv::VideoCapture(_streamingUrl);

  _texture=Ogre::TextureManager::getSingleton().createManual(_name+"Tex",// name
															 Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
															 Ogre::TEX_TYPE_2D,// texture type
															 _w,
															 _h,
															 0,// number of mipmaps
															 Ogre::PF_BYTE_BGRA,
															 Ogre::HardwareBuffer::HBU_DYNAMIC_WRITE_ONLY_DISCARDABLE
															 );

  
  Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().create(_name+"Mat", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME); 
  material->getTechnique(0)->getPass(0)->createTextureUnitState(); 
  material->getTechnique(0)->getPass(0)->setSceneBlending(Ogre::SBT_TRANSPARENT_ALPHA);
  material->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false); 
  material->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false); 
  material->getTechnique(0)->getPass(0)->setLightingEnabled(false);   
  material->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setTextureName(_name+"Tex");



  //Movable plane
  Ogre::MovablePlane* plane = new Ogre::MovablePlane(name+"Plane");
  plane->d=0;
  plane->normal = Ogre::Vector3::UNIT_Z;

  Ogre::MeshManager::getSingleton().createPlane(name+"PlaneMesh", 
  												Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, 
  												*plane, 
												_scale*_w, //1, //Width 
												_scale*_h, //1, //Height
												1, 
												1, 
												true, 
												1, 
												1, 
												1, 
												Ogre::Vector3::UNIT_Y);

  Ogre::Entity* mPlaneEnt = Ogre::Root::getSingleton().getSceneManager("SceneManager")->createEntity(name+"PlaneEntity", name+"PlaneMesh");
  mPlaneEnt->setMaterialName(name+"Mat");
  
  _node->attachObject(mPlaneEnt);
}


VideoPlane::~VideoPlane()
{
  //delete __streamingVC;
}

void
VideoPlane::refreshTexture()
{
  cv::Mat* frame = new cv::Mat();
  
  __streamingVC >> *frame;
  

  if(frame->rows==0) return;

  
    
  // Ogre::TexturePtr texture = Ogre::TextureManager::getSingleton().createManual(name,group_name,Ogre::TEX_TYPE_2D,frame->rows,frame->cols,1,0,Ogre::PF_X8R8G8B8,Ogre::TU_DYNAMIC_WRITE_ONLY);
  //Ogre::TexturePtr texture = Ogre::TextureManager::getSingleton().getByName("StreamingTex",Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
  
  //unsigned char* texData=(unsigned char*) texture->getBuffer()->lock(HardwareBuffer::HBL_DISCARD);
  
  
  
  Ogre::HardwarePixelBufferSharedPtr pixelBuffer = _texture->getBuffer();
    
  pixelBuffer->lock(Ogre::HardwareBuffer::HBL_DISCARD);
  const Ogre::PixelBox& pixelBox = pixelBuffer->getCurrentLock();
    
  Ogre::uint8* pDest = static_cast<Ogre::uint8*>(pixelBox.data);
    
  for(int j=0;j<frame->rows;j++) 
    {
      for(int i=0;i<frame->cols;i++) 
        {
          //int idx = ((frame->rows - 1 - j) * pixelBox.rowPitch + i )*4; //Use pixelBox.rowPitch instead of "width"
          int idx = ((j) * pixelBox.rowPitch + i )*4; //Use pixelBox.rowPitch instead of "width"
          pDest[idx] = frame->data[(j*frame->cols + i)*3];
          pDest[idx + 1] = frame->data[(j*frame->cols + i)*3 + 1];
          pDest[idx + 2] = frame->data[(j*frame->cols + i)*3 + 2];
          pDest[idx + 3] = 255;
        }
    }
  pixelBuffer->unlock();
    

  //Ogre::Rectangle2D* rect = static_cast<Ogre::Rectangle2D*>(_node->getAttachedObject(0));
  //Ogre::MaterialPtr material = rect->getMaterial();
    
  delete frame;
}

void
VideoPlane::setPosition()
{

}

