/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * VideoOutputSDLOpenGL.cpp
 *
 */

#include "VideoOutputSDLOpenGL.h"

namespace mars {



VideoOutputSDLOpenGL::VideoOutputSDLOpenGL() {
#ifndef NDEBUG
	std::cout << "[DEBUG]: Creating video Output SDL/OpenGL." << std::endl;
#endif

	// Get the config and setup SDL/OpenGL
	Configuration* cfg=Configuration::getConfiguration();

	__screenFullScreen = cfg->openGLFullScreen();
	__screenWidth      = cfg->openGLScreenWidth();
	__screenHeigh      = cfg->openGLScreenHeight();
	__screenDepth      = cfg->openGLScreenDepth();


	SDLInit();
	SDL_WM_SetCaption("M.A.R.S.","M.A.R.S.");
	OpenGLInit();
	resizeWindow(__screenWidth, __screenHeigh); // Fit the window/viewport

	__world = World::getInstance();



}

void VideoOutputSDLOpenGL::setWindowTitle(string title){
	SDL_WM_SetCaption(title.c_str(),"M.A.R.S.");
}

void VideoOutputSDLOpenGL::SDLInit(){
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		std::cout << "Unable to initialize SDL: " << SDL_GetError() << std::endl;
		exit(-1);
	}

	int videoFlags  = SDL_OPENGL;
	    videoFlags |= SDL_GL_DOUBLEBUFFER;
	    videoFlags |= SDL_HWPALETTE;
//	    videoFlags |= SDL_RESIZABLE;  // not a good idea

	 if (__screenFullScreen)
		 videoFlags |= SDL_FULLSCREEN;

	const SDL_VideoInfo *videoInfo = SDL_GetVideoInfo();
	if (!videoInfo){
          std::cout << "Error: SDL video info query." << std::endl;

	} else {

		if (videoInfo->hw_available){
			videoFlags |= SDL_HWSURFACE;
		}
		else{
			videoFlags |= SDL_SWSURFACE;
		}

		if (videoInfo->blit_hw){
			videoFlags |= SDL_HWACCEL;
		}
	}

	// FIXME detect support live --------------------
#ifdef AA_SDL_SUPPORT
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
#endif
	//------------------------------------------------

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	__screen = SDL_SetVideoMode( __screenWidth, __screenHeigh, __screenDepth, videoFlags );
}

void VideoOutputSDLOpenGL::OpenGLInit(){

	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	//glEnable(GL_MULTISAMPLE);
}

void VideoOutputSDLOpenGL::resizeWindow(int width, int height){
	// Can't divide by 0
	if (height == 0) height = 1;

	GLfloat ratio = (GLfloat) width / (GLfloat) height;

	// Setup the viewport for using all the screen.
	glViewport( 0, 0, (GLsizei) width, (GLsizei) height);

	glMatrixMode(GL_PROJECTION);

	glLoadIdentity();
	gluPerspective(45.0f, ratio, 0.1f, 100.0f); // 45 is the human vision fovy.

	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();
}

void VideoOutputSDLOpenGL::setOrtho() const{
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho( 0,  __screenWidth ,  0 , __screenHeigh, -1.0, 1.0 );
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	glDisable(GL_DEPTH_TEST);
}

void VideoOutputSDLOpenGL::setView() const{
	glEnable(GL_DEPTH_TEST);
	glMatrixMode( GL_PROJECTION );
	glPopMatrix();
	glMatrixMode( GL_MODELVIEW );
	glPopMatrix();
}


//void* VideoOutputSDLOpenGL::exec(void *thr) {
//	reinterpret_cast<World *> (thr)->worldLoop();
//	return NULL;
//}

void VideoOutputSDLOpenGL::step(){
	__world->step();
}

//void VideoOutputSDLOpenGL::go(){
//
//	cout << "Go!" << endl;
//	pthread_create(&(__thread_id),NULL,&VideoOutputSDLOpenGL::exec, __world);
//
//}

VideoOutputSDLOpenGL::~VideoOutputSDLOpenGL() {
	//
	//
}



}
