/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * VideoDeviceOpenCV.cpp
 *
 */

#include "VideoDeviceOpenCV.h"


namespace mars{

VideoDeviceOpenCV::VideoDeviceOpenCV(int nVideoDevice, std::string name) : VideoDevice(name){


	try{
		cap = new cv::VideoCapture(nVideoDevice);
	} catch (cv::Exception e) {
          std::cout << "ERROR: " << e.err << std::endl;
		std::exit(-1);
	}
	if(!cap->isOpened()){      // check if we succeeded
          std::cout << "ERROR: opening device number " << nVideoDevice << std::endl;
		std::exit(-1);
	}


	id  = nVideoDevice;

	std::stringstream ss;
	ss << id;

	description = "cv Camera ===> /dev/video" + ss.str();

#ifndef NDEBUG
	std::cout << "OpenCV Custom VideoDevice Created (" << cap << ") ===> /dev/video"  << id << std::endl;
#endif

	//cap->set(CV_CAP_PROP_FRAME_WIDTH, 640);
	//cap->set(CV_CAP_PROP_FRAME_HEIGHT, 480);

	fps = 15;
//	fps = (float) cap->get(CV_CAP_PROP_FPS);
	width = Configuration::getConfiguration()->openGLScreenWidth(); //(int) cap->get(CV_CAP_PROP_FRAME_WIDTH);
	height = Configuration::getConfiguration()->openGLScreenHeight(); //(int) cap->get(CV_CAP_PROP_FRAME_HEIGHT);

	if (fps==-1) fps = 15;

#ifndef NDEBUG
	std::cout << "[DEBUG]: " << description << " - FPS: " << fps << std::endl;
#endif


}

VideoDeviceOpenCV::~VideoDeviceOpenCV(){
//	if (cap->isOpened()){
//#ifndef NDEBUG
//		cout << "VideoDeviceOpenCV:: Closing Device" << endl;
//#endif
//		delete cap;
//	}
}

void VideoDeviceOpenCV::ThreadCaptureFrame(){


	Mat* frame;

#if 0
	int ticks0;
	int ticks1;
	int evenFrame = 0;

	float ticksPerFrame = 1000.0 / fps;

	ticks0 = SDL_GetTicks();
	ticks1 = ticks0;
#endif

	while(!__done){

		frame = new Mat();
		*cap >> *frame;

		fBuffer->addFrame(frame);

#if 0
		// Let's control the fps
		while (!__done &&  ( (ticks1 - ticks0) < (ticksPerFrame - evenFrame))  ){
			threadWait(ticksPerFrame*0.10);
			ticks1 = SDL_GetTicks();
		}

		if(!__done){
			fBuffer->addFrame(frame);
			ticks0 = ticks1;
			evenFrame = !(evenFrame);

			// Fake Control ... FIXME
			threadWait(ticksPerFrame*0.5);

		}
#endif

	}
}



}
