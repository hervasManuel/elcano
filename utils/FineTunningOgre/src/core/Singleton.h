#ifndef __SINGLETON_H__
#define __SINGLETON_H__

#include "Uncopyable.h"
#include <memory>
#include <cstdlib>


namespace mars{

template<typename T>
class Singleton: public uncopyable
{
public:
	static T* getInstance(){
		if (__instance == NULL){
			__instance = new T;
			std::atexit(destroy);
		}
		return __instance;
	}

private:

	static void destroy(){
		delete __instance;
	}

	static T* __instance;

};

template<typename T>
T* Singleton<T>::__instance = NULL;


}

#endif
