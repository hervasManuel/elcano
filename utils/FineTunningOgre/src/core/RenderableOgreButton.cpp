#include "RenderableOgreButton.h"


// RenderableOgreButton::RenderableOgreButton(std::string name, void(*fButton)(void*), bool invisible) 
// {
//   RenderableOgreButton(name, "", fButton);
// }

RenderableOgreButton::RenderableOgreButton(std::string name, std::string text, void(*fButton)(void*), bool invisible=false):
  _fButton(fButton), _clicked(false), _label(text), _active(true), _invisible(invisible)
{
  
    _overlay = Ogre::OverlayManager::getSingletonPtr()->getByName(name);
    _normalState = _overlay->getChild(name+"Normal");
    _normalState->setMetricsMode(Ogre::GMM_PIXELS);

    if(!invisible){ 
      _clickedState = _overlay->getChild(name+"Clicked");
      _textArea = Ogre::OverlayManager::getSingletonPtr()->getOverlayElement(name+"TextArea");
      _clickedState->setMetricsMode(Ogre::GMM_PIXELS);
      
    }

  _left = _normalState->getLeft() - 5;
  _top = _normalState->getTop() - 5;
  _width = _normalState->getWidth();
  _height = _normalState->getHeight();

  if(!invisible)
    {
      _overlay->show();
      _normalState->show();
      _clickedState->hide();
    }
  
}

void RenderableOgreButton::setActive(bool a)
{
  if(_invisible) return;

  _active = a;
    if(_active)
      {
	_normalState->show();
	_clickedState->hide();
	_clicked = false;
      }
    else
      {
	_normalState->hide();
	_clickedState->hide();
      }

}

void RenderableOgreButton::updateLabel()
{
  setText(_label);
}


void RenderableOgreButton::setText(std::string text)
{
  if(!_invisible)  _textArea->setCaption(text);
}

RenderableOgreButton::~RenderableOgreButton()
{
}

void RenderableOgreButton::mousePress(int x, int y)
{
  if(!_active) return;

  if(_clicked) return;

  if(x>=(_left) && x<=(_left+_width))
    if(y>=(_top) && y<=(_top+_height)){
      _clicked = true;
      if(!_invisible)
	{
	  _normalState->hide();
	  _clickedState->show();
	}
    }
}

void RenderableOgreButton::mouseRelease()
{
  if(_clicked)
    {
      _clicked=false;
      if(!_invisible)
	{
	  _normalState->show();
	  _clickedState->hide();
	}
      if(_fButton)_fButton(NULL);
    }

}

