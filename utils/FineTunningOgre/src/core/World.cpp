/*
 * World.cpp
 *
 */

#include "World.h"

namespace mars {


World::World() {

	__width = Configuration::getConfiguration()->openGLScreenWidth();
	__height = Configuration::getConfiguration()->openGLScreenHeight();

	__resizeFrames = false;

	__bgSet = false;

	//	__defaultCamera = NULL;

	__done = false;

	//	__selectedCamera = 0;

	__nSteps = 0;
	__t0 = SDL_GetTicks();
	__showFPS = false;

}

void World::setFrameResizing(bool b){
	__resizeFrames = b;
}

void World::setBackgroundSource(VideoSource* vS){
	__backgroundVS = vS;
	if (vS != NULL) __bgSet = true;


}
  
void World::setBackgroundImage(cv::Mat* mat){

  flip(*mat,__backgroundImage,0);
}


void World::drawBackground(){



}

void World::draw(){

	// vector<Renderable*>::iterator it;
	// it = __renderables.begin();
	// while(it!=__renderables.end()){
	// 	if (((Renderable*) *it)->isShown())
	// 		((Renderable*) *it)->draw();
	// 	it++;
	// }

	// vector<RenderableWidget*>::iterator itw;
	// itw = __widgets.begin();
	// while(itw!=__widgets.end()){
	// 	if (((Renderable*) *itw)->isShown())
	// 		((Renderable*) *itw)->draw();
	// 	itw++;
	// }

	// if (__console != NULL)
	// 	if (__console->isShown())
	// 		__console->draw();

}

// Camera* World::getSelectedCamera(){
// 	if (!__cameras.empty())
// 		return __cameras.at(__selectedCamera);
// 	else
// 		return __defaultCamera;
// }

// void World::worldLoop(){
// 	// This is only used for demos... it should no be used for a regular build

// 	cout << "[DEBUG]: Setting World Loop up" <<std::endl;

//     while (!__done){

//     	step();
//     	eventsCall();

//     }// end while loop


// }

bool World::eventsCall(){
	SDL_Event event;


    while (SDL_PollEvent(&event)){
    	if (event.type ==SDL_QUIT){
    		__done=true;
    	}

    	if ( event.type == SDL_KEYDOWN ){

#ifndef NDEBUG
	  std::cout <<  "KeyCode: " << (int) (event.key.keysym.sym) <<std::endl;
#endif
    		switch(event.key.keysym.sym){


    		case SDLK_ESCAPE:
		  //__done = true;
		  SDL_Quit();
		  return false;
		  break;
		  

		default: 
		  break;

    		} // end switch

    	} // end if keydown

    }// end while poll

    return true;
}

bool World::step(){


	__nSteps++;
	if (__nSteps == 1000){
		double t = 1000.0 / ( (SDL_GetTicks() - __t0) / (float) (__nSteps) );
		if (__showFPS){
			Logger::getInstance()->note(toString(t)+ " fps");
		}
		__fps = t;
		__t0 = SDL_GetTicks();
		__nSteps = 0;
	}

	// glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	// if (__bgSet){
	// 	cv::Mat* i = __backgroundVS->getLastFrame();
	// 	setBackgroundImage(i);
	// 	delete i;
	// }


	drawBackground();


	//	glMatrixMode( GL_MODELVIEW );
	//glLoadIdentity();


	// if (!__cameras.empty()){
	// 	__cameras[__selectedCamera]->use();
	// }

	
	this->draw();

	//SDL_GL_SwapBuffers();

	return eventsCall();

}

// void World::addCamera(Camera* camera){
// 	__cameras.push_back(camera);
// }

// void World::addCamera(VideoSource* vs){
// 	Camera3D* c = vs->getOpenGLCamera();
// 	__cameras.push_back(c);
// 	c->setEye(0,0,20);
// 	c->setLookTo(0,0,-6);
// 	selectCamera(__cameras.size()-1);
// }


// void World::selectCamera(unsigned n){
// 	if (n > __cameras.size()-1){
// 		Logger::getLogger()->warning("World:: Camera index out of bounds.");
// 	} else {
// 		__selectedCamera = n;
// 	}
// 	setUpProjection();
// }

//  void World::setUpProjection(){


// // 	// Setup the viewport for using all the screen.
// // 	glViewport( 0, 0, (GLsizei) __width, (GLsizei) __height);

// // 	glMatrixMode(GL_PROJECTION);

// // 	glLoadIdentity();
// // 	glLoadMatrixf(__cameras.at(__selectedCamera)->getProjectionMatrix().getData());
// // 	//gluPerspective(54.0f, ratio, 0.1f, 100.0f); // 45 is the human vision fovy.

// // //	Matrix16 m;
// // //	glGetFloatv(GL_PROJECTION_MATRIX, m.getData());
// // //	std::cout << "--\Projection\n--\n" << m << std::endl;

// // #ifndef NDEBUG
// // 	cout << "[DEBUG]: ------------------------------------- " <<std::endl;
// // 	cout << "[DEBUG]:  custom projection matrix loaded      " <<std::endl;
// // 	cout << "[DEBUG]: ------------------------------------- " <<std::endl;
// // #endif

// // 	glMatrixMode(GL_MODELVIEW);

// // 	glLoadIdentity();
//  }



World::~World() {
	Logger::getLogger()->note("The end of the world :)");
	//	delete __defaultCamera;

}

}

