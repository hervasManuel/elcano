#include <Ogre.h>
#include <OIS/OIS.h>
#include <core/tracking/TrackingController.h>
#include <core/tracking/TrackingMethodFactory.h>
#include <core/tracking/TrackingMethodOAbsoluteArtkBz.h>
#include <core/tracking/TrackingMethodOAbsolutePTAMM.h>
#include <core/video/VideoSource.h>
#include <core/Configuration.h>

class DemoFrameListener : public OIS::KeyListener, public Ogre::FrameListener, public Ogre::WindowEventListener{
protected:
  mars::TrackingController* _tC;
  mars::VideoSource* _vD_user;
  Ogre::Root* _root;
  Ogre::SceneManager* _sceneManager;
  Ogre::OverlayManager* _overlayManager;
  Ogre::Camera* _camera;
  OIS::InputManager* _inputManager;
  OIS::Keyboard* _keyboard;
  Ogre::RenderWindow* _win;

  /* Tracking Methods */
  mars::TrackingMethodOAbsolutePTAMM* _tPTAMM;
  mars::TrackingMethodOAbsoluteArtkBz* _tARTK;

  float _fps;
  bool _forceExit;

  void windowClosed(Ogre::RenderWindow* win);

  void refreshBackground();
  void refreshInput();

  //Keyboard events
  virtual void demoKeyPressed(const OIS::KeyEvent &arg)=0;

public:
  DemoFrameListener(Ogre::RenderWindow* win,Ogre::Root* r,mars::TrackingController* tC, mars::VideoSource* v);

  //Keyboard events
  bool keyPressed(const OIS::KeyEvent &arg);
  bool keyReleased(const OIS::KeyEvent &arg);


  bool frameStarted(const Ogre::FrameEvent& evt); 
  virtual void createScene()=0;
  virtual bool demoFrameStarted(const Ogre::FrameEvent& evt)=0;

  ~DemoFrameListener();
};
