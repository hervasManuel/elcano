#ifndef RENDERABLEOGREPATH_H_
#define RENDERABLEOGREPATH_H_

#include <iostream>
#include <Ogre.h>
#include <vector>
#include <core/Logger.h>

#define ARROWS_DIFF 0.5 /* Distance between arrows */

class RenderableOgrePath
{
  std::string _name;
  //float _alpha;
  float _width;
  float _height;
  //Ogre::Vector3 _color;
  Ogre::ManualObject* _path;

  void drawQuad(const Ogre::Vector3& p1, const Ogre::Vector3& p2, const Ogre::Vector3& p3, const Ogre::Vector3& p4);
  void drawArrow(const Ogre::Vector3& point, const Ogre::Vector3& v, const float& distance);
  void drawFinalArrow(const Ogre::Vector3& point, const Ogre::Vector3& v);

 public:
  RenderableOgrePath(std::string name);
  ~RenderableOgrePath();

  void setPoints(const std::vector<Ogre::Vector3> &v);
  void resetPath();
  //  void setColor(Ogre::Vector3 v);
  //  void setAlpha(float alpha);
  void setWidth(float width);
  void setHeight(float height);
  //  void updateUserPos();

};

#endif /* RENDERABLEOGREPATH_H_ */
