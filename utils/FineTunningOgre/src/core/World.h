/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/


/*
 * World.h
 *
 */

#ifndef WORLD_H_
#define WORLD_H_

#include <Ogre.h>
#include <core/video/VideoSource.h>
#include <core/Logger.h>
#include <core/Singleton.h>
#include <core/Configuration.h>

#include <core/Tools.h>

#include <SDL/SDL.h>
#include <SDL/SDL_opengl.h>

#include <opencv/cv.h>


#include <vector>


namespace mars {

class World : public Singleton<World> {

public:

	/**
	 * Adds a new virtual camera
	 */
    //    void addCamera(Ogre::Camera* camera);

	/**
	 * Adds a real camera
	 */
    //	void addCamera(VideoSource* vs);

	/**
	 * Select a camera by its order of addition
	 */
	//void selectCamera(unsigned n);


	/* /\** */
	/*  * DEPRECATED, functional but should not be used. Use step instead */
	/*  *\/ */
	/* void worldLoop(); */

	/**
	 * Steps the world. Renders one frame
	 */
	bool step();

	/**
	 * Sets the videosource from where the backgournd frames will be taken
	 */
	void setBackgroundSource(VideoSource* vS);

	/**
	 * Returns the selected virtual camera
	 */
	//	Ogre::Camera* getSelectedCamera();


	/*
	 * Should the background frame be resized?
	 */
	void setFrameResizing(bool b);

	// FPS related
	/**
	 * Enable or disabel the FPS calculation
	 */
	void   showFPS(bool b) { __showFPS = b; }

	/**
	 * Returns the current number of frames per second
	 */
	double getFPS() { return __fps; }

private:

	friend class Singleton<World>;

	World();
	virtual ~World();


	/**
	 *
	 * Reserves n Renderable slots so reallocation will not be necessary.
	 *
	 * @param n slots reserved
	 */
	void drawBackground();
	void draw();
	void setBackgroundImage(cv::Mat* image);
	bool eventsCall();


	//void setUpProjection();

	//Ogre::Camera* __defaultCamera;


	bool __done;

	cv::Mat __backgroundImage;

	VideoSource* __backgroundVS;

	//std::vector<Ogre::Camera*> __cameras;

	//unsigned __selectedCamera;

	bool __resizeFrames;

	int __width;
	int __height;

	// FPS ...
	unsigned __nSteps;
	unsigned __t0;
	float __fps;
	bool __showFPS;

	bool __bgSet;

};


}

#endif /* WORLD_H_ */
