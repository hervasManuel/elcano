#include "RenderableOgrePath.h"

RenderableOgrePath::RenderableOgrePath(std::string name):
  _name(name), _width(0.5f), _height(0.3f)
{
 
  /* Creating the material */
  Ogre::MaterialPtr mat = Ogre::MaterialManager::getSingleton().create("3DPathMaterial","General");
  //mat->getTechnique(0)->getPass(0)->createTextureUnitState("pathcolour.png");

  mat->getTechnique(0)->getPass(0)->createTextureUnitState();
  mat->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setColourOperationEx(Ogre::LBX_SOURCE1,
										 Ogre::LBS_MANUAL,
										 Ogre::LBS_CURRENT,
										 Ogre::ColourValue::Red);

  mat->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
  mat->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
  mat->getTechnique(0)->getPass(0)->setLightingEnabled(false);

  _path = Ogre::Root::getSingleton().getSceneManager("SceneManager")->createManualObject(name);
  Ogre::Root::getSingleton().getSceneManager("SceneManager")->getRootSceneNode()->createChildSceneNode()->attachObject(_path);
}


void RenderableOgrePath::setPoints(const std::vector<Ogre::Vector3> &points)
{
  /* Reseting path */
  resetPath();

  Ogre::Vector3 p1, p2, p3, p4;
  Ogre::Vector3 v1, v2, v;
  Ogre::Radian alpha;
  float x;

  /* Setting y-coordinate of points to height :) */
  std::vector<Ogre::Vector3> points2;
  for(unsigned int i=0;i<points.size();i++)
    points2.push_back(Ogre::Vector3(points.at(i).x,_height,points.at(i).z));

  /* Calcular los primeros dos puntos */
  v1 = points2.at(1)-points2.at(0);
  v = v1.crossProduct(Ogre::Vector3(.0f,1.f,0.f));
  v.normalise();
  p1 = points2.at(0)+v*_width/2;
  p2 = points2.at(0)-v*_width/2;

  /* Doing n-2 points */
  for(unsigned int i=1;i<points2.size()-1;i++)
    {
      v1 = (points2.at(i)-points2.at(i-1));
      v1.normalise();
      v2 = (points2.at(i+1)-points2.at(i));
      v2.normalise();
      v = v1-v2;
      
      if(v==Ogre::Vector3(0,0,0)) //They are alligned
	continue;

      alpha = v1.angleBetween(-v2);
      x = _width/Ogre::Math::Sin(alpha);
      v.normalise();

      p3 = points2.at(i)+v* x/2;
      p4 = points2.at(i)-v* x/2;

      Ogre::Vector3 vx = v1.crossProduct(v2);
      if(vx.y>0){
	drawQuad(p2, p1, p4, p3); /* FIXED */
	p1 = p3; /* FIXED */
	p2 = p4; /* FIXED */
      }else{
	drawQuad(p2, p1, p3, p4);
	p1 = p4; /* FIXED */
	p2 = p3; /* FIXED */
      }

      
    }

  /* Calcular los dos últimos puntos */
  v1 = points2.at(points2.size()-1)-points2.at(points2.size()-2);
  v = v1.crossProduct(Ogre::Vector3(.0f,1.f,0.f));
  v.normalise();
  p3 = points2.at(points2.size()-1)+v*_width/2;
  p4 = points2.at(points2.size()-1)-v*_width/2;
  
  drawQuad(p2, p1, p4, p3); /* FIXED */
  drawFinalArrow(points2.at(points2.size()-2),v1);

}

/* Draws a Quad (two triangles) given 4 points.
 */
void RenderableOgrePath::drawQuad(const Ogre::Vector3& p1, const Ogre::Vector3& p2, const Ogre::Vector3& p3, const Ogre::Vector3& p4)
{
  _path->begin("3DPathMaterial",Ogre::RenderOperation::OT_TRIANGLE_STRIP);

  Ogre::Vector3 v1, v2, v;

  /* First triangle */
  _path->position(p1.x, _height, p1.z);
  _path->position(p2.x, _height, p2.z);
  _path->position(p3.x, _height, p3.z);
  _path->position(p4.x, _height, p4.z);

  /* Checking triangles order */
  /* aka Triangles' normals */
  
  v1 = p2-p1;
  v2 = p3-p2;
  v = v1.crossProduct(v2);

  if(v.y < 0){//Need to swap
    _path->index(2);
    _path->index(1);
    _path->index(0);
    
  }else{
    _path->index(0);
    _path->index(1);
    _path->index(2);
     
  }
  _path->index(3);
 
  _path->end();    
}

void RenderableOgrePath::drawArrow(const Ogre::Vector3& point, const Ogre::Vector3& v, const float& distance)
{

  _path->begin("3DPathMaterial",Ogre::RenderOperation::OT_TRIANGLE_STRIP);
  
  /* Calcular los primeros dos puntos de la base del triánglo*/
  Ogre::Vector3 v1 = v.crossProduct(Ogre::Vector3(.0f,1.f,0.f));
  v1.normalise();

  Ogre::Vector3 p;
  Ogre::Vector3 vNorm = v;
  vNorm.normalise();

  p = point+distance*vNorm;
 
  Ogre::Vector3 p1, p2, p3;
  p1 = p+v1*_width;
  p2 = p-v1*_width;

  float h = 2*_width*Ogre::Math::Sin(Ogre::Degree(60));

  p3 = p+h*vNorm;

  _path->position(p1.x,p1.y,p1.z);
  _path->position(p2.x,p2.y,p2.z);
  _path->position(p3.x,p3.y,p3.z);

  _path->index(2);
  _path->index(1);
  _path->index(0);

  _path->end();

}

void RenderableOgrePath::drawFinalArrow(const Ogre::Vector3& point, const Ogre::Vector3& v)
{
  drawArrow(point, v,v.length());
}




void RenderableOgrePath::resetPath()
{
  _path->clear();
}

// void RenderableOgrePath::setColor(Ogre::Vector3 v)
// {
//   _color=v;
// }

// void RenderableOgrePath::setAlpha(float alpha)
// {
//   _alpha=alpha;
// }

void RenderableOgrePath::setHeight(float height)
{
  _height = height;
}

void RenderableOgrePath::setWidth(float width)
{
  _width=width;
}

// void RenderableOgrePath::updateUserPos()
// {
// }


RenderableOgrePath::~RenderableOgrePath()
{
}
