#ifndef RENDERABLEOGREBUTTON_H_
#define RENDERABLEOGREBUTTON_H_

#include <iostream>
#include <Ogre.h>

class RenderableOgreButton
{
  Ogre::Overlay* _overlay;
  Ogre::OverlayContainer* _normalState;
  Ogre::OverlayContainer* _clickedState;
  Ogre::OverlayElement* _textArea;
  std::string _label;

  int _left, _top;
  int _width, _height;

  bool _invisible;
  bool _active;
  bool _clicked;
  void (*_fButton)(void*);


 public:
  RenderableOgreButton(std::string name, std::string text, void(*fButton)(void*), bool invisible);
  //RenderableOgreButton(std::string name, void(*fButton)(void*), bool invisible = false);
  ~RenderableOgreButton();

  void setText(std::string text);
  void mousePress(int x, int y);
  void mouseRelease();
  void updateLabel();  
  void setActive(bool v);
};



#endif /* RENDERABLEOGREBUTTON_h_ */
