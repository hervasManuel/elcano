/*
 * Logger.cpp
 *
 */

#include "Logger.h"

namespace mars {

Logger::Logger() {

	__hasConsole = false;

	gettimeofday(&__initTime, NULL);

	__colors = true;

	RED = "\x1b[31m";
	YELLOW = "\x1b[33m";
	RESET = "\x1b[0m";
	HC = "\x1b[1m";

	try{
	  __logFile.open(LOG_FILENAME, std::ios::trunc);
	} catch (std::exception){
		getInstance()->error("Log file cannot be created.");
	}

	if (__logFile.is_open()){
	  __logFile << "  _ _  ___  ___  ___ " << std::endl;
	  __logFile << " | | )|   )|   )|___ " << std::endl;
	  __logFile << " |  / |__/||     __/ " << std::endl << std::endl;
	  __logFile << " ·········· log File "  << std::endl << std::endl;
	}
}

Logger::~Logger() {}

Logger* Logger::getLogger(){
	return getInstance();
}

timeval* Logger::getTime(){
	gettimeofday(&__now, NULL);

	return &__now;
}

void Logger::setConsoleFunctions(void (*e)(const std::string&),
                                 void (*w)(const std::string&),
                                 void (*n)(const std::string&)){

	if (e==NULL || w ==NULL || n ==NULL)
		return;
	__hasConsole = true;
	gAddError = e;
	gAddWarning = w;
	gAddNote = n;

}


double Logger::currentTime(){
	timeval* t = getTime();
	return (((t->tv_sec - __initTime.tv_sec) * 1000000) + (t->tv_usec) - __initTime.tv_usec) / 1000000.0;
}

void Logger::error(std::string msg, bool writeToFile){
	if (__logFile.is_open() && writeToFile){
	  __logFile << "[ERROR]   [" << std::fixed; __logFile.width(9); __logFile << currentTime() << "] :" <<  msg << std::endl;
	}
	if (__colors) std::cout << HC << RED;
	std::cout << "[ERROR]: " << msg << std::endl << RESET;
	if (__hasConsole)
		gAddError("["+toString(currentTime()) + "] " + msg);
}

  void Logger::warning(std::string msg, bool writeToFile){
	if (__logFile.is_open() && writeToFile){
	  __logFile << "[WARNING] [" << std::fixed; __logFile.width(9); __logFile << currentTime() << "] :" <<  msg << std::endl;
	}
#ifndef NDEBUG
	if (__colors) std::cout << HC << YELLOW;
	std::cout << "[WARNING]: " << msg << std::endl;
#endif
	std::cout << RESET;
	if (__hasConsole)
		gAddWarning("["+toString(currentTime()) + "] " + msg);
}

  void Logger::note(std::string msg, bool writeToFile){
	if (__logFile.is_open() && writeToFile){
	  __logFile << "[NOTE]    [" << std::fixed; __logFile.width(9); __logFile << currentTime() << "] :" <<  msg << std::endl;
	}
#ifndef NDEBUG
	if (__colors) std::cout << HC;
	std::cout << "[NOTE]: " << msg << std::endl;
#endif
	std::cout << RESET;
	if (__hasConsole)
		gAddNote("["+toString(currentTime()) + "] " + msg);
}

void Logger::cleanUp(){
	if(__logFile.is_open())
		__logFile.close();
}

}
