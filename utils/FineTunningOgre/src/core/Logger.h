/*
 * Logger.h
 *
 */


#ifndef LOGGER_H_
#define LOGGER_H_


#define LOG_FILENAME "mars.log"

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <SDL/SDL.h>

#include <sys/time.h>

#include <sstream>
#include <iomanip>

#include <core/Singleton.h>

#include <core/Tools.h>

namespace mars {

/**
 * Logger of MARS
 */
class Logger : public Singleton<Logger> {
public:

	/**
	 * Returns the logger's instance
	 */
	static Logger* getLogger();

	/**
	 * Reports an error to log
	 *
	 * @param msg Error Message
	 */
	void error(std::string msg, bool writeToFile=true);

	/**
	 * Reports a warning
	 *
	 * @param msg Warning Message
	 */
	void warning(std::string msg, bool writeToFile=true);

	/**
	 * Note
	 *
	 * @param msg Note to log
	 */
	void note(std::string msg, bool writeToFile=true);

	/**
	 * Returns the aboslute time of the machine
	 */
	timeval* getTime();

	/**
	 * Cleans the logger up.
	 */
	void cleanUp();

	/**
	 * Returns the elapsed time since the Logger was created in seconds.
	 */
	double currentTime();

	/**
	 * Publishes functions with the prototype void f(std::string s) to be
	 * called on error, warning and notice events triggered by error(), warning()
	 * and note().
	 *
	 * @param e function to called on error() call.
	 * @param w function to called on warning() call.
	 * @param n function to called on note() call.
	 */
	void setConsoleFunctions(void (*e)(const std::string&),
	                         void (*w)(const std::string&),
	                         void (*n)(const std::string&));

private:

	Logger();
	virtual ~Logger();

	std::ofstream __logFile; ///< Output stream for the file

	bool __colors; ///< Do we should use colours... of course we should :)

	std::string HC; ///< ANSI Hightlighted color value
	std::string RED; ///< ANSI Red color value
	std::string YELLOW; ///< ANSI Yellow Color value
	std::string RESET; ///< ANSI Reset value

	struct timeval __initTime; ///< The time it was when the logger was created.
	struct timeval __now; ///< The last time we used getTime();

	bool __hasConsole;

	void (*gAddError) (const std::string&);
	void (*gAddWarning) (const std::string&);
	void (*gAddNote) (const std::string&);

	friend class Singleton<Logger>;

	friend class RenderableWidgetLogger;


};

}

#endif /* LOGGER_H_ */
