/*
 * Tools.h
 *
 */

#include <iostream>
#include <string>
#include <sstream>

#ifndef __TOOLS_H__
#define __TOOLS_H__

namespace mars{

template <typename T>
std::string toString(T t){
	std::stringstream ss;
	ss << t;
	return ss.str();
}


}

#endif
