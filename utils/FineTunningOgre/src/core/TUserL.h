#ifndef TUSERL_H_
#define TUSERL_H_

#include <core/Vector3D.h>


namespace mars{


/**
 * Structure used to store a single user tracking.
 */
typedef struct{
		Vector3D __Eye;    ///< where the user is.
		Vector3D __LookTo; ///< where the user is looking to.
		Vector3D __Up; ///< up Vector
		float    __Weight; ///< The weight assigned to this perception
	} tUserL;


}

#endif
