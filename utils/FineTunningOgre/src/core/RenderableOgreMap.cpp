#include "RenderableOgreMap.h"

RenderableOgreMap::RenderableOgreMap(std::string mapName, int startX, int startY, float scale):
  _scale(scale), _startX(startX), _startY(startY), _lineWidth(.5f)
{
  _overlay = Ogre::OverlayManager::getSingleton().getByName(mapName);
  _container = _overlay->getChild(mapName+"Container");
  _overlay->show();
  _container->setMetricsMode(Ogre::GMM_PIXELS);

  _left = _container->getLeft();
  _top = _container->getTop();
  _width = _container->getWidth();
  _height = _container->getHeight();
  

  /* Creating and configuring the 2D-Path */
  _path = Ogre::Root::getSingleton().getSceneManager("SceneManager")->createManualObject(mapName+"Path");
  initPath();

  hide();
}

RenderableOgreMap::~RenderableOgreMap()
{

}


void RenderableOgreMap::addMethod(std::string trackingName, std::string pointerName)
{
  Ogre::Overlay* overlay = Ogre::OverlayManager::getSingletonPtr()->getByName(pointerName);
  _methods[trackingName]=overlay;

  setMethodPosition(trackingName,Ogre::Vector3(0.f,0.f,0.f));
}

void RenderableOgreMap::setMethodPosition(std::string name, const Ogre::Vector3 &pos)
{
  int pixelX = int ( pos.x  * _scale) + _startX;
  int pixelY = int ( pos.z  * _scale) + _startY;

  if ( (pixelX < 0) || (pixelY < 0) || (pixelX > int(_width-1) ) || (pixelY > int(_height-1) ) ){
    mars::Logger::getInstance()->error("RendrableOgreMap:: Out of bounds.");
    return;
  }

  std::map<std::string,Ogre::Overlay*>::iterator it;

  it = _methods.find(name);

  if (it == _methods.end() ){
    mars::Logger::getInstance()->warning("RenderableWidgetMap:: cannot set the"
				   " position of a non existent method.");
    return;
  }
  else 
    {
      int newX = _left + pixelX;
      int newY = _top  + pixelY;
      
      it->second->getChild(it->second->getName()+"Container")->setLeft(newX);
      it->second->getChild(it->second->getName()+"Container")->setTop(newY);
    }
}

/* In degrees! */
void RenderableOgreMap::setMethodAngle(std::string name, float angle)
{
  std::map<std::string,Ogre::Overlay*>::iterator it;

  it = _methods.find(name);

  if (it == _methods.end() ){
    mars::Logger::getInstance()->warning("RenderableWidgetMap:: cannot set the"
				   " position of a non existent method.");
    return;
  }
  else 
    {
      it->second->getChild(name+"Container")->
	getMaterial()->getTechnique(0)->
	getPass(0)->getTextureUnitState(0)->
	setTextureRotate(Ogre::Degree(angle)+Ogre::Degree(180));
    }

}

void RenderableOgreMap::resetPath()
{
  _path->clear();

}

void RenderableOgreMap::setPoints(const std::vector<Ogre::Vector3> &v)
{
  int screenWidth, screenHeight;

  screenWidth = Ogre::Root::getSingleton().getAutoCreatedWindow()->getWidth();
  screenHeight = Ogre::Root::getSingleton().getAutoCreatedWindow()->getHeight();

  /* The points matrix bounds are ([-20,20],[-20,20]) */
  resetPath();

  /* Magic! ... */
  _path->begin("2DPathMaterial",Ogre::RenderOperation::OT_LINE_STRIP);

  for(unsigned int i=0;i<v.size();i++)
    {
      /* Meters to pixels */
      int px = _left + int ( v.at(i).x  * _scale) + _startX;
      int py = _top + int ( v.at(i).z  * _scale) + _startY;;

      /* Pixels to path units */
      float x = px/(float)screenWidth*2.f-1.f;
      float y = -(py/(float)screenHeight*2.f-1.f);
      
      _path->position(x, y, .0);
      _path->index(i);
    }


  _path->end();
}

void RenderableOgreMap::setPathLineWidth(const float& w)
{
  _lineWidth = w;
}


void RenderableOgreMap::initPath()
{
  /* Creating the material */
  Ogre::MaterialPtr mat = Ogre::MaterialManager::getSingleton().create("2DPathMaterial","General");
  //mat->getTechnique(0)->getPass(0)->createTextureUnitState("pathcolour.png");

  mat->getTechnique(0)->getPass(0)->createTextureUnitState();
  mat->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setColourOperationEx(Ogre::LBX_SOURCE1,
										 Ogre::LBS_MANUAL,
										 Ogre::LBS_CURRENT,
										 Ogre::ColourValue::Red);


  mat->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
  mat->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
  mat->getTechnique(0)->getPass(0)->setLightingEnabled(false);

  /* Creating the path / manual object */
  _path->setUseIdentityProjection(true);
  _path->setUseIdentityView(true);

  /* Using infinite AAB to always stay visible :) */
  Ogre::AxisAlignedBox aabInf;
  aabInf.setInfinite();
  _path->setBoundingBox(aabInf);

  _path->setRenderQueueGroup(Ogre::RENDER_QUEUE_OVERLAY+1);
  Ogre::Root::getSingleton().getSceneManager("SceneManager")->getRootSceneNode()->createChildSceneNode("Path2D")->attachObject(_path);

}


void RenderableOgreMap::show()
{
  _overlay->show();
  if(_path) _path->setVisible(true);
  //  for(unsigned int i = 0; i < _methods.size();i++){
  for(std::map<std::string,Ogre::Overlay*>::iterator it = _methods.begin(); it!=_methods.end();++it)
    {
      (*it).second->show();
    }
}

void RenderableOgreMap::hide()
{
  _overlay->hide();
  if(_path) _path->setVisible(false);
  for(std::map<std::string,Ogre::Overlay*>::iterator it = _methods.begin(); it!=_methods.end();++it)
    {
      (*it).second->hide();
    }

}
