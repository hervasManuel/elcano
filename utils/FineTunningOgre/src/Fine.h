#ifndef _FINE_H_
#define _FINE_H_

#include <core/DemoFrameListener.h>
#include <fstream>

#define FINE_ADD 0.01f

#define FINE_ROT Ogre::Degree(1.f)


#define FINE_ROTATE 0
#define FINE_DISPLACE 1


class Fine: public DemoFrameListener
{

  Ogre::SceneNode* _node;
  Ogre::Entity* _planeR;
  Ogre::Entity* _axisR;

 public:
  Fine(Ogre::RenderWindow* win,Ogre::Root* r,mars::TrackingController* tC, mars::VideoSource* v, int mapId);
  ~Fine();
  void createScene();
  bool demoFrameStarted(const Ogre::FrameEvent& evt);

  void printHelp();
  void readPTAMMOffset();
  void saveConf();

  //Keyboard events
  void demoKeyPressed(const OIS::KeyEvent &arg);
  

  //Fine methods! :
  Ogre::Vector3 _axis;
  int _operation;
  int _mapId;
  float timeSinceLastFrame;

  void increment();
  void decrement();
  void incrementPlus();
  void decrementMinus();
};



#endif /* _FINE_H_ */
