#include "Fine.h"


Fine::Fine(Ogre::RenderWindow* win,Ogre::Root* r,mars::TrackingController* tC, mars::VideoSource* v, int mapId):
  DemoFrameListener(win,r,tC,v), _mapId(mapId)
{
  _axis = Ogre::Vector3(1.f,0.f,0.f);
  _operation = FINE_ROTATE;
  timeSinceLastFrame=0;
  printHelp();
}

void Fine::createScene()
{
  //Entities
  //  _planeR  = Ogre::Root::getSingleton().getSceneManager("SceneManager")->createEntity("Plane","Plane.mesh");
  _axisR  = Ogre::Root::getSingleton().getSceneManager("SceneManager")->createEntity("Axis","Axis.mesh");

  _node = Ogre::Root::getSingleton().getSceneManager("SceneManager")->getRootSceneNode()->createChildSceneNode("Node");

  //_node->attachObject(_planeR);

  _node->attachObject(_axisR);


  readPTAMMOffset();

}

bool Fine::demoFrameStarted(const Ogre::FrameEvent& evt)
{
  timeSinceLastFrame += evt.timeSinceLastFrame;

  return true;
}


//Keyboard events
void Fine::demoKeyPressed(const OIS::KeyEvent &arg)
{

  /* Unbuffered */
  /* Controlling the key frequency */
  if(timeSinceLastFrame<0.1) return;
  timeSinceLastFrame=0;

  _keyboard->capture();
  if(_keyboard->isKeyDown(OIS::KC_X)){
    std::cout<<"Seleted axis X"<<std::endl;
    _axis = Ogre::Vector3(1.f,0.f,0.f);
  }
  else if(_keyboard->isKeyDown(OIS::KC_Y)){
    std::cout<<"Seleted axis Y"<<std::endl;
    _axis = Ogre::Vector3(0.f,1.f,0.f);
  }
  else if(_keyboard->isKeyDown(OIS::KC_Z)){
    std::cout<<"Seleted axis Z"<<std::endl;
    _axis = Ogre::Vector3(0.f,0.f,1.f);
  }
  else if(_keyboard->isKeyDown(OIS::KC_R)){
    std::cout<<"Seleted operation ROTATE"<<std::endl;
    _operation = FINE_ROTATE;
  }
  else if(_keyboard->isKeyDown(OIS::KC_D)){
    std::cout<<"Seleted operation DISPLACE"<<std::endl;
    _operation = FINE_DISPLACE;
  }
  else if(_keyboard->isKeyDown(OIS::KC_UP)){
    increment();
  }
  else if(_keyboard->isKeyDown(OIS::KC_DOWN)){
    decrement();
  }
  else if(_keyboard->isKeyDown(OIS::KC_RIGHT)){
    incrementPlus();
  }
  else if(_keyboard->isKeyDown(OIS::KC_LEFT)){
    decrementMinus();
  }
  else if(_keyboard->isKeyDown(OIS::KC_S)){
    saveConf();
  }
  else if(_keyboard->isKeyDown(OIS::KC_ESCAPE) || _keyboard->isKeyDown(OIS::KC_Q)){
    exit(0);
  }
			     
  
}





void Fine::increment()
{
  switch (_operation){
  case FINE_ROTATE:
    _node->rotate(_axis,FINE_ROT,Ogre::SceneNode::TS_LOCAL);
    break;
  case FINE_DISPLACE:
    _node->translate(FINE_ADD*_axis,Ogre::SceneNode::TS_LOCAL);
    break;
  }
}

void Fine::decrement()
{
  switch (_operation){
  case FINE_ROTATE:
    _node->rotate(_axis,-1*FINE_ROT,Ogre::SceneNode::TS_LOCAL);
    break;
  case FINE_DISPLACE:
    _node->translate(-1*FINE_ADD*_axis,Ogre::SceneNode::TS_LOCAL);
    break;
  }
}



void Fine::incrementPlus()
{
  switch (_operation){
  case FINE_ROTATE:
    _node->rotate(_axis,30*FINE_ROT,Ogre::SceneNode::TS_LOCAL);
    break;
  case FINE_DISPLACE:
    _node->translate(30*FINE_ADD*_axis,Ogre::SceneNode::TS_LOCAL);
    break;
  }
}

void Fine::decrementMinus()
{
  switch (_operation){
  case FINE_ROTATE:
    _node->rotate(_axis,-30*FINE_ROT,Ogre::SceneNode::TS_LOCAL);
    break;
  case FINE_DISPLACE:
    _node->translate(-30*FINE_ADD*_axis,Ogre::SceneNode::TS_LOCAL);
    break;
  }
}

void Fine::printHelp()
{
  std::cout<<"All right, let's do it as quick as possible...\n\n"
    "-- ElCano's Fine Tunning... now is Ogre compliant! WOW! --\n"
    "If you are here, it is probably you have not social life, or friends at all...\n"
    "Well, I will teach you how this crap of software works, altough I really\n"
    "encourage you to go out and meet some people, maybe find a girlfriend...\n\n"
    "-- Key --   -- Usage --\n"
    "   R        Active ROTATE operation\n"
    "   D        Active DISPLACE operation\n"
    "   X        Active X axis\n\n"
    "   Y        Active Y axis\n"
    "   Z        Active Z axis\n\n"
    "   UP       Small increment\n"
    "  DOWN      Small decrement\n"
    "  RIGHT     Big increment\n"
    "  LEFT      Big decrement\n\n"
    "And remember! All the transformations are local to the object's axis!\n\n"
    "Good Luck! You bastard!"<<std::endl;

}

Fine::~Fine(){}

void Fine::saveConf(){

  std::cout<<"Writing Offset Matrix for map "<<_mapId<<"..."<<std::endl;
  std::ofstream file; 
  file.open("config/PTAMMOffset.mat",std::ios::out);

  if(!file.is_open()){
    std::cout<<"Error opening PTAMMOffset.mat file to write"<<std::endl;
    exit(-1);
  }
 
  Ogre::Vector3 p = _node->getPosition();
  Ogre::Quaternion o = _node->getOrientation();

  file<<"# #Ogre::Vector3_Position #Ogre::Quaternion_Orientation"<<std::endl;
  file<<"["<<_mapId<<"] ";
  file<<p[0]<<" "<<p[1]<<" "<<p[2]<<" ";
  file<<o[0]<<" "<<o[1]<<" "<<o[2]<<" "<<o[3];
  file<<std::endl;
 
  std::cout<<"Wrote succesfully!"<<std::endl;
}

void Fine::readPTAMMOffset()
{
  int mi;
  Ogre::Vector3 p;
  Ogre::Quaternion o;

  std::ifstream file; 
  file.open("config/PTAMMOffset.mat",std::ios::in);
  if(file.is_open()){
    std::string line;
    while(!file.eof()){
      getline(file,line);
      if(line.size()==0) continue;
      if(line[0]!='#'){
	setlocale (LC_NUMERIC, "C"); // ... so we have an uniform config file...
	sscanf(line.c_str(),"[%d] %f %f %f %f %f %f %f",
	       &mi,
	       &p[0],&p[1],&p[2],
	       &o[0],&o[1],&o[2],&o[3]);
	
	if(mi==_mapId){
	  _node->setPosition(p);
	  _node->setOrientation(o);
	}
      }
    }    
  }
  else
    {
      std::cout<<"Ok, there is not PTAMMOffset.mat file, it is Ok, do not worry, I will handle it..."<<std::endl;
    }  
}

