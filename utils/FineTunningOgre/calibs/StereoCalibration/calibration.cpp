/**************************************************************************************
 *  Calibration.cpp
 *
 *
 *  on 26/12/2011
 ***************************************************************************************/
#include <stdio.h>
#include <iostream>
#include <vector>
#include <sys/time.h>
#include <fstream>
#include <cv.h>
#include "highgui.h"

void showHelp(){
	std::cout << "******************************************************************\n\n"
			"This is the ARGES calibration tool. This program takes a set of frames with "
			"stereo images and calculates the intrinsic parameters of the camera.\n"
			"Usage: ./calibration\n"
			"	-s <square_size>	#Size of the square side in the chess board in meters."
			"	[-w <board_width>]	#Number of inner corners in the width of the chess board. Is 9 by defect.\n"
			"	[-h <board:height>]	#Number of inner corners in the height of the chess board. Is 6 by defect.\n"
			"	[-i <XML_file>]		#XML file with the names of frames previously stored.\n\n"
			"******************************************************************\n\n"
			"Example command line for calibration from a live feed.\n"
			"   ./calibration  -s 0.03 -w 4 -h 5 \n\n"
			"Example command line for calibration from a list of stored images:\n"
			"  ./calibration -s 0.025 image_list.xml\n"
			" where image_list.xml is the standard OpenCV XML/YAML\n"
			" file consisting of the list of strings, e.g.:\n\n"
			"<?xml version=\"1.0\"?>\n"
			"<opencv_storage>\n"
			"<images>\n"
			"view01L.jpg\n"
			"view01R.jpg\n"
			"view02L.jpg\n"
			"view02R.jpg\n"
			"view03L.jpg\n"
			"view03R.jpg\n"
			"</images>\n"
			"</opencv_storage>\n"<< std::endl;
}

void showLiveCaptureHelp(){
	std::cout << "****************************************************************\n\n "
			"ARGES LIVE VIDEO CAPTURE\n"
			"When the live video from camera is used as input, the following hot-keys may be used:\n"
			"<Esc> - Close the camera capture\n"
			"c - Capture a pair of frames. You have 2 seconds to move the chess board "
			"a few centimeters and the program will capture the second image automatically\n" << endl;
}
/**
 * readStringList(const string& filename, vector<cv::Mat>& list ):
 *
 * reads the xml file where we have the names of the image files
 * and take the information of the image files in a vector of cv::Mat.
 * Returns the number of chessboards readed.
 */

int readStringList( const string& filename, std::vector<cv::Mat>& list ){

	int numBoards = 0;
	int numImages = 0;
    list.resize(0);
    cv::FileStorage fs(filename, cv::FileStorage::READ);

    if( !fs.isOpened() )
        return false;

    cv::FileNode n = fs.getFirstTopLevelNode();

    if( n.type() != cv::FileNode::SEQ )
        return false;

    cv::FileNodeIterator it = n.begin(), it_end = n.end();

    for( ; it != it_end; ++it ){
    	std::cout << (string)*it << endl;
    	cv::Mat _image = cv::imread((string)*it,0);
    	if(_image.data == NULL)
    		std::cout << "Exception reading the image. Is it true the image's path?" << endl;
    	list.push_back(_image);
    	numImages++;
    	if(numImages%2 == 0)
    		numBoards++;
    }

    if(numImages%2 == 0)
    	return numBoards;
    else{
    	std::cout << "Exception: must have a pair number of images" << endl;
    	return 0;
    }
}

int main(int argc, char *argv[]){

	if (argc < 2){
		showHelp();
		return 0;
	}

	bool capture = true;

	const char* inputFileName = "chessboards.xml";

	cv::Size chessBoardSize(9,6);

	double squareSide = 0.0;

	//Reading options...

	for(int i=1; i<argc; i++){
		const char* s = argv[i];
		if(strcmp(s, "-s") == 0){
			if(sscanf(argv[++i], "%lf", &squareSide)!= 1 || squareSide <= 0.0){
				std::cerr << "Invalid square side" << std::endl;
				return 0;
			}
		}
		else if(strcmp(s, "-w") == 0){
			if(sscanf(argv[++i], "%u",&chessBoardSize.width)!= 1 || chessBoardSize.width <= 0)
				std::cerr << "Invalid board width" << std::endl;
			return 0;
		}
		else if(strcmp(s, "-h") == 0){
			if(sscanf(argv[++i], "%u",&chessBoardSize.height)!= 1 || chessBoardSize.width <= 0)
				std::cerr << "Invalid board height" << std::endl;
			return 0;
		}
		else if(strcmp(s, "-i") == 0){
			capture = false;
			inputFileName = argv[++i];
		}
	}

	if(capture){
		cv::VideoCapture vC;
		int numImage = 1;
		if(!vC.open(0)){
			std::cerr << "Can not initialize video capture" << endl;
			return 0;
		}

		cv::FileStorage fs("chessboards.xml", cv::FileStorage::WRITE);
		fs << "images" << "[";

		cv::namedWindow("CamL",1);
		cv::namedWindow("CamR",1);
		cv::namedWindow("Capture view",1);

		cv::Mat frameCaptured;

		vector<int> opcsStoreImages;
		opcsStoreImages.push_back(CV_IMWRITE_JPEG_QUALITY);
		opcsStoreImages.push_back(100);

		showLiveCaptureHelp();
		for(;;){
			cv::Mat frameL, frameR;
			if(vC.isOpened()){
				vC >> frameCaptured;
				cv::imshow("Capture view",frameCaptured);
			}
			int key = 0xff & cv::waitKey(vC.isOpened() ? 50:500);

			//If 'Esc' is pressed, program ends
			if((key & 255) == 27)
				break;

			if((key == 'c')){
				frameCaptured.copyTo(frameL);
				for(int i=0;i<50;i++){
					vC >> frameCaptured;
					cv::imshow("Capture view", frameCaptured);
					cv::waitKey(vC.isOpened() ? 50:500);
				}
				frameCaptured.copyTo(frameR);
				std::ostringstream imageNameStreamL;
				imageNameStreamL << "IMG" << numImage << "L.jpg";
				string imageNameL = imageNameStreamL.str();
				std::ostringstream imageNameStreamR;
				imageNameStreamR << "IMG" << numImage << "R.jpg";
				string imageNameR = imageNameStreamR.str();
				std::cout << "The images will be saved as " << imageNameL << " and " << imageNameR << endl;
				std::cout << "Press any key to continue..." << endl;
				numImage++;
				cv::imshow("CamL",frameL);
				cv::imshow("CamR",frameR);
				cv::waitKey();

				cv::imwrite("./calib_frames/"+imageNameL,frameL,opcsStoreImages);
				cv::imwrite("./calib_frames/"+imageNameR,frameR,opcsStoreImages);

				fs << "./calib_frames/"+imageNameL;
				fs << "./calib_frames/"+imageNameR;
			}

		}
		fs << "]";
	}

	//The name of the images is stored in the XML file.

	std::vector<cv::Mat> chessImages;

	// Opening the xml list

	int numBoards = readStringList(inputFileName, chessImages);

	std::cout << chessImages.size() << endl;

	if(numBoards == 0 || chessImages.empty()){
		cout << "Can not open " << inputFileName<< " or the image list is empty" << endl;
		return 0;
	}

	bool camL = true;

	int NL = 0, NR = 0; //Total of corners for each cam

	// Vector of vectors of feature points in left cam and right cam
	// Keypoints[0] contains the keypoints of left image
	// Keypoints[1] contains the keypoints of right image
	std::vector<std::vector<cv::Point2f> > keypoints[2];

	for(std::vector<cv::Mat>::iterator vect_it = chessImages.begin();
			vect_it != chessImages.end(); vect_it++){

		std::vector<cv::Point2f> corners;
		// To find chess board corners, the image must be in gray scale

		bool patternfound = false;

		patternfound = cv::findChessboardCorners(*vect_it,chessBoardSize,corners,
				CV_CALIB_CB_ADAPTIVE_THRESH + CV_CALIB_CB_NORMALIZE_IMAGE+ CV_CALIB_CB_FAST_CHECK);

		if(patternfound){
			cv::cornerSubPix(*vect_it , corners, cv::Size(11, 11), cv::Size(-1, -1),
				cv::TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));
		}
		cv::drawChessboardCorners(*vect_it, chessBoardSize, cv::Mat(corners), patternfound);

		if(camL){
			camL = false;
			NL += corners.size();
			keypoints[0].push_back(corners);
			cv::imshow("CamL",*vect_it);
		}
		else{
			camL = true;
			NR += corners.size();
			keypoints[1].push_back(corners);
			cv::imshow("CamR",*vect_it);
			cv::waitKey();
		}
	}

		// Calibrating the stereo cameras

		std::cout << "Running stereo calibration..." << endl;

		cv::Size imgSize(chessImages.at(0).cols,chessImages.at(0).rows);

		cv::Mat cameraMatrix[2], distCoeffs[2];
			cameraMatrix[0] = cv::Mat::eye(3, 3, CV_64F);
			cameraMatrix[1] = cv::Mat::eye(3, 3, CV_64F);
		cv::Mat R, T, E, F,R1,R2,P1,P2,Q;
		cv::Rect roi1,roi2;

		std::vector<vector<cv::Point3f> > objectPoints;

		for(int i=0;i<numBoards;i++){
			std::vector<cv::Point3f> points;
			for(int j=0;j<(chessBoardSize.height*chessBoardSize.width);j++)
				// The coordinates must be dot by the measure of the squares of the chessboard
				points.push_back(cv::Point3f((j/chessBoardSize.width)*squareSide, (j%chessBoardSize.width)*squareSide, 0.0f));
			objectPoints.push_back(points);
		}


		double rms = cv::stereoCalibrate(objectPoints,keypoints[0],keypoints[1],cameraMatrix[0]
							,distCoeffs[0],cameraMatrix[1],distCoeffs[1],imgSize, R,T,E,F,
							cv::TermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS,100,1e-5),
							CV_CALIB_FIX_ASPECT_RATIO + CV_CALIB_ZERO_TANGENT_DIST +
							CV_CALIB_SAME_FOCAL_LENGTH + CV_CALIB_RATIONAL_MODEL +
							CV_CALIB_FIX_K3 + CV_CALIB_FIX_K4 + CV_CALIB_FIX_K5);

		std::cout << "Calibration done with RMS error=" << rms << endl;

		//Because the output fundamental matrix implictly
		//includes all the output information,
		//we can check the quality of calibration using the
		//epipolar geometry constraint m2^t*F*m1 = 0

		double err = 0;
		int npoints = 0;
		std::vector<cv::Vec3f> lines[2];
		for( int i = 0; i < numBoards; i++ ){
			int npt = (int)keypoints[0][i].size();
			cv::Mat imgpt[2];
			for( int k = 0; k < 2; k++ ){
				 imgpt[k] = (cv::Mat)keypoints[k][i];
				 cv::undistortPoints(imgpt[k], imgpt[k], cameraMatrix[k], distCoeffs[k], cv::Mat(), cameraMatrix[k]);
				 cv::computeCorrespondEpilines(imgpt[k], k+1, F, lines[k]);
			 }
			 for( int j = 0; j < npt; j++ ){
				 double errij = fabs(keypoints[0][i][j].x*lines[1][j][0] +
									 keypoints[0][i][j].y*lines[1][j][1] + lines[1][j][2]) +
								fabs(keypoints[1][i][j].x*lines[0][j][0] +
									 keypoints[1][i][j].y*lines[0][j][1] + lines[0][j][2]);
				 err += errij;
			 }
			 npoints += npt;
		 }
		 cout << "average reprojection err = " <<  err/npoints << endl;

		// Compute and display rectification
		// I use the Boughet's method to do the rectification

		cv::stereoRectify(cameraMatrix[0],distCoeffs[0],cameraMatrix[1],distCoeffs[1],
				imgSize,R,T,R1,R2,P1,P2,Q,-1,imgSize,&roi1,&roi2);

		std::cout << "Left camera matrix = " << cameraMatrix[0] << "\n\n" <<
				"Left camera distortion coefficients = " << distCoeffs[0] << "\n\n" <<
				"Right camera matrix = " << cameraMatrix[1] << "\n\n" <<
				"Right camera distortion coefficients = " << distCoeffs[1] << "\n" << endl;

		// Storing the intrinsic parameters...

		std::cout << "Storing parameters in camera_parameters.yml...";
		cv::FileStorage fs("camera_parameters.yml", CV_STORAGE_WRITE);
		if( fs.isOpened() ){
			fs << "M1" << cameraMatrix[0] << "D1" << distCoeffs[0] <<
					"M2" << cameraMatrix[1] << "D2" << distCoeffs[1];
			fs << "R" << R << "T" << T << "R1" << R1 << "R2" << R2 << "P1" << P1 << "P2" << P2 << "Q" << Q;
			fs << "SS" << squareSide << "IMW" << imgSize.width << "IMH" << imgSize.height;
			fs.release();
			std::cout << "done." << endl;
		}
		else
			std::cout << "\nError: can not save the intrinsic parameters\n";

	return 0;
}
