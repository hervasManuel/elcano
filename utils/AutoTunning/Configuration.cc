/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * Configuration.cpp
 *
 */

#include "Configuration.h"


namespace mars {


Configuration::Configuration(){
  _openGLScreenWidth = 640;
  _openGLScreenHeight = 480;
  _openGLScreenDepth = 32;
  _ARTKDataPath = "./ARTKData/";
  _ARTKPatternsFile = "patterns.artk";
  
}

Configuration* Configuration::getConfiguration(){
	return getInstance();
}



  void Configuration::parseARTKPatternsFile(std::vector<ARTKMark>& marks){
  try{
    _ARTKPatternsFile = _ARTKDataPath + _ARTKPatternsFile;
    __patternsFile.open(_ARTKPatternsFile.c_str() , std::ios::in);
  } catch (std::exception e){
    std::cout<<"Config:: Processig "  + _ARTKPatternsFile<<std::endl;
  }
  
  if (!__patternsFile.is_open()){
    std::cout<<"Config:: No config file: " + _ARTKPatternsFile<<std::endl;
  }
  else {
    int nLine=0;
    std::string line;
    char tempName[255];
    float d[16] = {1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1};
    
    while (!__patternsFile.eof() )
      {
	nLine++;
	getline(__patternsFile, line);
	
	if (line.compare("") == 0) continue;
	
	setlocale (LC_NUMERIC, "C"); // ... so we have an uniform config file...
	
	float size = 0;
	int nElem;
	char type[255];
	
	int type_flag = 0;
	
	sscanf(line.c_str(),"%s ",type);
	
	if(type[0]=='a'){
	  /*ACTUATION MARK*/
	  type_flag|=ACTUATION_MARK;
	  nElem = sscanf(line.c_str(), "%s %s %f",
			 type, tempName, &size);
	  
	  if (nElem != 3){
	    std::stringstream msgW;
	    msgW << _ARTKPatternsFile << ":: Line(" << nLine << ") is corrupt. ::: " << line;
	    //	    Logger::getLogger()->warning(msgW.str());
	  }
	}else if(type[0]=='b' || type[0]=='p' || type[0]=='u'){
	 
	  if(type[0]=='b'){/*POSITION AND ACTUATION MARK*/
	    type_flag|=ACTUATION_MARK;
	    type_flag|=POSITION_MARK;
	  }else if(type[0]=='p'){/*POSITION MARK*/
	    type_flag|=POSITION_MARK;
	  }else if(type[0]=='u'){/*USER MARK*/
	    type_flag|=USER_MARK;
	  }

	  nElem = sscanf(line.c_str(), "%s %s %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f",
			 type, tempName, &d[0], &d[1], &d[2], &d[3], &d[4], &d[5], &d[6], &d[7],
			 &d[8], &d[9], &d[10], &d[11], &d[12], &d[13], &d[14], &d[15], &size);
	  
	  if (nElem != 19){
	    std::stringstream msgW;
	    msgW << _ARTKPatternsFile << ":: Line(" << nLine << ") is corrupt. ::: " << line;
	    //	    Logger::getLogger()->warning(msgW.str());
	  }			  
	}else{
	  //	  Logger::getLogger()->warning(":: Corrupt line in patterns.artk. :::");
	  return;
	}
	
	// -------------- To string ;)
	std::stringstream ss;
	ss << "Adding ARTK Mark: " << tempName;
	for (int i=0;i<16;i++) ss << " " << d[i];
	ss << " Size(" <<  size << ")";
	// --------------
	//	Logger::getLogger()->note(ss.str());
	Matrix16 m((GLfloat*) d);
        std::cout << "Adding Mark: " << tempName << std::endl;
        std::cout << m << std::endl;

	ARTKMark mark(m, std::string(this->_ARTKDataPath + tempName), size, type_flag);
	marks.push_back(mark);
      }
  
  }
}
  

Configuration::~Configuration(){
#ifndef NDEBUG
  std::cout << "[DEBUG]: Configuration:: Cleaning up." << std::endl;
#endif
}

}
