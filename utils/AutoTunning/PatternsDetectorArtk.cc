/*
 * PatternsDetectorArtk.cpp
 *
 */

#include "PatternsDetectorArtk.h"

namespace mars{
  
PatternsDetectorArtk::PatternsDetectorArtk(){
  markerNum=0;
  markerInfo=NULL;
  
  readConfiguration();

}

  bool PatternsDetectorArtk::detect(cv::Mat* frame,std::vector<ARTKMark>* marks,ARTKHistory* hist){  


  /*AR Detect function*/
  //arDetectMarkerLite(frame->data,100,&markerInfo,&markerNum); /*WITHOUTH HISTORY!!*/
  detectMarkerHist(frame->data,thres,&markerInfo,&markerNum,hist);


  /*Check perceptions visibility*/
  for(unsigned int i=0;i<marks->size();i++){
    marks->at(i).setFoundId(checkPatternVisibility(i));
  }

  float glAuxf[16];//Gl aux transformation matrix
  double glAuxd[16];//Gl aux transformation matrix
  double arRelativeTrans[3][4]; //Relative matrix transformation of the camera in ARTK format.

  for(unsigned int percep=0;percep<marks->size();percep++){
    if(marks->at(percep).getFoundId()!=-1){/*Pattern detected*/  
      
      marks->at(percep).setMarkInfo(&markerInfo[marks->at(percep).getFoundId()]);

      arGetTransMat(&markerInfo[marks->at(percep).getFoundId()],marks->at(percep).getCenter(),marks->at(percep).getSize(),arRelativeTrans);
      
      /*Convert ARTK relative trans to OpenGL relative trans*/
      argConvGlpara(arRelativeTrans,glAuxd);

        
      //CASTING. WOODEN PATCH matrixd -> matrixf
      for(int i=0;i<16;i++){
  	glAuxf[i]=(float)glAuxd[i];
      }
      
      cv::Mat* matTrans=Gl2Mat(glAuxf);
      
      //Rotate 180º the camera ;)
      matTrans->at<float>(0,0)*=-1;
      matTrans->at<float>(1,0)*=-1;
      matTrans->at<float>(2,0)*=-1;
      matTrans->at<float>(3,0)*=-1;
      matTrans->at<float>(0,1)*=-1;
      matTrans->at<float>(1,1)*=-1;
      matTrans->at<float>(2,1)*=-1;
      matTrans->at<float>(3,1)*=-1;
      
      marks->at(percep).setRelativeMatrix(*matTrans);
      marks->at(percep).setDetected(true);

  
      delete matTrans;

    }else{
      marks->at(percep).setDetected(false);
    }

    
  }/*End for*/

  return true;
}

  void PatternsDetectorArtk::setThreshold(int t){
thres=t;
 
#ifndef NDEBUG
 std:: cout<<"Threshold: "<<thres<<std::endl;
#endif
}

  int PatternsDetectorArtk::getThreshold(){return thres;}

void PatternsDetectorArtk::readConfiguration(){
	thres=100;
	cf_min=0.85;
}

int PatternsDetectorArtk::checkPatternVisibility(int perceptionId){
	int k=-1;
	for(int j=0;j<markerNum;j++){
		if(perceptionId==markerInfo[j].id){
			if(k==-1) k=j;
			else if(markerInfo[k].cf<markerInfo[j].cf) k=j;
		}
	}	
	/*Checking min cf*/
	if(markerInfo[k].cf<cf_min)
	  k=-1;
	
	return k;
}

// vector<ARTKMark>* PatternsDetectorArtk::getMarks(){
//   return &marks;
// }

  bool PatternsDetectorArtk::changeCamera(PTAMM::VideoSource* cam0){
  ARParam cparam,wparam; //ARToolKit stuff

	/*Setting camera parameters*/
	if(cam0->isCalibrated()){
	  wparam.dist_factor[0]=cam0->getWidth()/2; //center-x of the dist region
	  wparam.dist_factor[1]=cam0->getHeight()/2; //center-y of the dist region
	  wparam.dist_factor[2]=0; //Distortion factor
	  wparam.dist_factor[3]=1; //Scale factor
	   
	  cam0->getPerspectiveMatrix(wparam.mat);

	  wparam.xsize=cam0->getWidth();
	  wparam.ysize=cam0->getHeight();
	  
	  arParamChangeSize( &wparam, cam0->getWidth(), cam0->getHeight(), &cparam );
	  arInitCparam(&cparam );	

	}else{
	  //	  Logger::getLogger()->error("TrackingMethodOAbsoluteARTK: Video source has not perspective matrix. ARToolKit will not work.");	  
	  return false;
	}

	

	return true;
}

  /*Implement our own version of the ARTK history*/
 int PatternsDetectorArtk::detectMarkerHist( ARUint8 *dataPtr, int thresh, ARMarkerInfo **marker_info, int *marker_num,ARTKHistory* hist){
   ARInt16                *limage;
    int                    label_num;
    int                    *area, *clip, *label_ref;
    double                 *pos;
    double                 rarea, rlen, rlenmin;
    double                 diff, diffmin;
    int                    cid, cdir;
    int                    i, j, k;

    *marker_num = 0;

    limage = arLabeling( dataPtr, thresh,
                         &label_num, &area, &pos, &clip, &label_ref );
    if( limage == 0 )    return -1;

    hist->marker_info2 = arDetectMarker2( limage, label_num, label_ref,
                                    area, pos, clip, AR_AREA_MAX, AR_AREA_MIN,
                                    1.0, &hist->wmarker_num);
    if( hist->marker_info2 == 0 ) return -1;

    hist->wmarker_info = arGetMarkerInfo( dataPtr, hist->marker_info2, &hist->wmarker_num );
    if( hist->wmarker_info == 0 ) return -1;

    for( i = 0; i < hist->prev_num; i++ ) {
        rlenmin = 10.0;
        cid = -1;
        for( j = 0; j < hist->wmarker_num; j++ ) {
            rarea = (double)hist->prev_info[i].marker.area / (double)hist->wmarker_info[j].area;
            if( rarea < 0.7 || rarea > 1.43 ) continue;
            rlen = ( (hist->wmarker_info[j].pos[0] - hist->prev_info[i].marker.pos[0])
                   * (hist->wmarker_info[j].pos[0] - hist->prev_info[i].marker.pos[0])
                   + (hist->wmarker_info[j].pos[1] - hist->prev_info[i].marker.pos[1])
                   * (hist->wmarker_info[j].pos[1] - hist->prev_info[i].marker.pos[1]) ) / hist->wmarker_info[j].area;
            if( rlen < 0.5 && rlen < rlenmin ) {
                rlenmin = rlen;
                cid = j;
            }
        }
        if( cid >= 0 && hist->wmarker_info[cid].cf < hist->prev_info[i].marker.cf ) {
            hist->wmarker_info[cid].cf = hist->prev_info[i].marker.cf;
            hist->wmarker_info[cid].id = hist->prev_info[i].marker.id;
            diffmin = 10000.0 * 10000.0;
            cdir = -1;
            for( j = 0; j < 4; j++ ) {
                diff = 0;
                for( k = 0; k < 4; k++ ) {
                    diff += (hist->prev_info[i].marker.vertex[k][0] - hist->wmarker_info[cid].vertex[(j+k)%4][0])
                          * (hist->prev_info[i].marker.vertex[k][0] - hist->wmarker_info[cid].vertex[(j+k)%4][0])
                          + (hist->prev_info[i].marker.vertex[k][1] - hist->wmarker_info[cid].vertex[(j+k)%4][1])
                          * (hist->prev_info[i].marker.vertex[k][1] - hist->wmarker_info[cid].vertex[(j+k)%4][1]);
                }
                if( diff < diffmin ) {
                    diffmin = diff;
                    cdir = (hist->prev_info[i].marker.dir - j + 4) % 4;
                }
            }
            hist->wmarker_info[cid].dir = cdir;
        }
    }

    for( i = 0; i < hist->wmarker_num; i++ ) {
/*
	printf("cf = %g\n", hist->wmarker_info[i].cf);
*/
        if( hist->wmarker_info[i].cf < 0.5 ) hist->wmarker_info[i].id = -1;
   }


/*------------------------------------------------------------*/

    for( i = j = 0; i < hist->prev_num; i++ ) {
        hist->prev_info[i].count++;
        if( hist->prev_info[i].count < 4 ) {
            hist->prev_info[j] = hist->prev_info[i];
            j++;
        }
    }
    hist->prev_num = j;

    for( i = 0; i < hist->wmarker_num; i++ ) {
        if( hist->wmarker_info[i].id < 0 ) continue;

        for( j = 0; j < hist->prev_num; j++ ) {
            if( hist->prev_info[j].marker.id == hist->wmarker_info[i].id ) break;
        }
        hist->prev_info[j].marker = hist->wmarker_info[i];
        hist->prev_info[j].count  = 1;
        if( j == hist->prev_num ) hist->prev_num++;
    }

    for( i = 0; i < hist->prev_num; i++ ) {
        for( j = 0; j < hist->wmarker_num; j++ ) {
            rarea = (double)hist->prev_info[i].marker.area / (double)hist->wmarker_info[j].area;
            if( rarea < 0.7 || rarea > 1.43 ) continue;
            rlen = ( (hist->wmarker_info[j].pos[0] - hist->prev_info[i].marker.pos[0])
                   * (hist->wmarker_info[j].pos[0] - hist->prev_info[i].marker.pos[0])
                   + (hist->wmarker_info[j].pos[1] - hist->prev_info[i].marker.pos[1])
                   * (hist->wmarker_info[j].pos[1] - hist->prev_info[i].marker.pos[1]) ) / hist->wmarker_info[j].area;
            if( rlen < 0.5 ) break;
        }
        if( j == hist->wmarker_num ) {
            hist->wmarker_info[hist->wmarker_num] = hist->prev_info[i].marker;
            hist->wmarker_num++;
        }
    }


    *marker_num  = hist->wmarker_num;
    *marker_info = hist->wmarker_info;

    return 0;   

 }


float* PatternsDetectorArtk::Mat2Gl(cv::Mat* mat){
	float *gl;

	gl=new float[16];
	for(uint i=0;i<4;i++){
		for(uint j=0;j<4;j++){
			gl[i*4+j]=mat->at<float>(i,j);
		}
	}
	return  gl;
}

  cv::Mat* PatternsDetectorArtk::Gl2Mat(float *gl){
  cv::Mat *mat;

  mat=new cv::Mat(4,4,CV_32F);
	for(uint i=0;i<4;i++){
		for(uint j=0;j<4;j++){
			mat->at<float>(i,j)=gl[i*4+j];
		}
	}
	return mat;
}







}
