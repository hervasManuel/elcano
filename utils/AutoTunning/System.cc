// Copyright 2009 Isis Innovation Limited
#include "System.h"
#include "OpenGL.h"
#include <gvars3/GStringUtil.h>
#include <stdlib.h>
#include "ATANCamera.h"
#include "MapMaker.h"
#include "Tracker.h"
#include "ARDriver.h"
#include "MapViewer.h"
#include "MapSerializer.h"

#ifdef _LINUX
#include <fcntl.h>
#endif

#ifdef WIN32
#include <Windows.h>
#endif

using namespace TooN;

namespace PTAMM {

  using namespace CVD;
  using namespace std;
  using namespace GVars3;


  float* Mat2Gl(cv::Mat* mat){
    float *gl;

    gl=new float[16];
    for(uint i=0;i<4;i++){
      for(uint j=0;j<4;j++){
	gl[i*4+j]=mat->at<float>(i,j);
      }
    }
    return  gl;
  }

  cv::Mat* Gl2Mat(float *gl){
    cv::Mat *mat;

    mat=new cv::Mat(4,4,CV_32F);
    for(uint i=0;i<4;i++){
      for(uint j=0;j<4;j++){
	mat->at<float>(i,j)=gl[i*4+j];
      }
    }
    return mat;
  }



  System::System()
    : mGLWindow(mVideoSource.Size(), "PTAMM")
  {
    GUI.RegisterCommand("exit", GUICommandCallBack, this);
    GUI.RegisterCommand("quit", GUICommandCallBack, this);

    //PTAMM commands
    GUI.RegisterCommand("SwitchMap", GUICommandCallBack, this);
    GUI.RegisterCommand("NewMap", GUICommandCallBack, this);
    GUI.RegisterCommand("DeleteMap", GUICommandCallBack, this);
    GUI.RegisterCommand("ResetAll", GUICommandCallBack, this);

    GUI.RegisterCommand("LoadMap", GUICommandCallBack, this);
    GUI.RegisterCommand("SaveMap", GUICommandCallBack, this);
    GUI.RegisterCommand("SaveMaps", GUICommandCallBack, this);
  
    GV2.Register(mgvnLockMap, "LockMap", 0, SILENT);
    GV2.Register(mgvnDrawMapInfo, "MapInfo", 0, SILENT);
  
    GUI.RegisterCommand("KeyPress", GUICommandCallBack, this);

  
    mimFrameBW.resize(mVideoSource.Size());
    mimFrameRGB.resize(mVideoSource.Size());
    // First, check if the camera is calibrated.
    // If not, we need to run the calibration widget.
    Vector<NUMTRACKERCAMPARAMETERS> vTest;
  
    vTest = GV3::get<Vector<NUMTRACKERCAMPARAMETERS> >("Camera.Parameters", ATANCamera::mvDefaultParams, HIDDEN);
    mpCamera = new ATANCamera("Camera");
    mpCamera->SetImageSize(mVideoSource.Size());
  
    if(vTest == ATANCamera::mvDefaultParams)
      {
	cout << endl;
	cout << "! Camera.Parameters is not set, need to run the CameraCalibrator tool" << endl;
	cout << "  and/or put the Camera.Parameters= line into the appropriate .cfg file." << endl;
	exit(1);
      }


    //create the first map
    mpMap = new Map();
    mvpMaps.push_back( mpMap );
    mpMap->mapLockManager.Register(this);
        
    mpMapMaker = new MapMaker( mvpMaps, mpMap );
    mpTracker = new Tracker(mVideoSource.Size(), *mpCamera, mvpMaps, mpMap, *mpMapMaker);
    mpARDriver = new ARDriver(*mpCamera, mVideoSource.Size(), mGLWindow, *mpMap);
    mpMapViewer = new MapViewer(mvpMaps, mpMap, mGLWindow);
    mpMapSerializer = new MapSerializer( mvpMaps );

    //These commands have to be registered here as they call the classes created above
    GUI.RegisterCommand("NextMap", GUICommandCallBack, mpMapViewer);
    GUI.RegisterCommand("PrevMap", GUICommandCallBack, mpMapViewer);
    GUI.RegisterCommand("CurrentMap", GUICommandCallBack, mpMapViewer);
  
    GUI.RegisterCommand("Mouse.Click", GUICommandCallBack, mpARDriver);

    //create the menus
    GUI.ParseLine("GLWindow.AddMenu Menu Menu");
    GUI.ParseLine("Menu.ShowMenu Root");
    GUI.ParseLine("Menu.AddMenuButton Root \"Reset All\" ResetAll Root");
    GUI.ParseLine("Menu.AddMenuButton Root Reset Reset Root");
    GUI.ParseLine("Menu.AddMenuButton Root Spacebar PokeTracker Root");
    GUI.ParseLine("Menu.AddMenuButton Root Demos \"\" Demos");
    GUI.ParseLine("DrawAR=0");
    GUI.ParseLine("DrawMap=0");
    GUI.ParseLine("Menu.AddMenuToggle Root \"Draw AR\" DrawAR Root");

    GUI.ParseLine("GLWindow.AddMenu MapsMenu Maps");
    GUI.ParseLine("MapsMenu.AddMenuButton Root \"New Map\" NewMap Root");
    GUI.ParseLine("MapsMenu.AddMenuButton Root \"Serialize\" \"\" Serial");
    GUI.ParseLine("MapsMenu.AddMenuButton Serial \"Save Maps\" SaveMaps Root");
    GUI.ParseLine("MapsMenu.AddMenuButton Serial \"Save Map\" SaveMap Root");
    GUI.ParseLine("MapsMenu.AddMenuButton Serial \"Load Map\" LoadMap Root");

    GUI.ParseLine("LockMap=0");
    GUI.ParseLine("MapsMenu.AddMenuToggle Root \"Lock Map\" LockMap Root");
    GUI.ParseLine("MapsMenu.AddMenuButton Root \"Delete Map\" DeleteMap Root");
    GUI.ParseLine("MapInfo=0");
    GUI.ParseLine("MapsMenu.AddMenuToggle Root \"Map Info\" MapInfo Root");

    GUI.ParseLine("GLWindow.AddMenu MapViewerMenu Viewer");
    GUI.ParseLine("MapViewerMenu.AddMenuToggle Root \"View Map\" DrawMap Root");
    GUI.ParseLine("MapViewerMenu.AddMenuButton Root Next NextMap Root");
    GUI.ParseLine("MapViewerMenu.AddMenuButton Root Previous PrevMap Root");
    GUI.ParseLine("MapViewerMenu.AddMenuButton Root Current CurrentMap Root");


    mbDone = false;




    /* ARTK Initialization */
    /*Initialize*/
    __artkDetecting = false;
    __userMarks=new vector<ARTKMark>;
    __eval=0;
    __artkGlobalTrans=new cv::Mat(4,4,CV_32F);
    __artkDetecting = false;
    
    __artkDetector=new PatternsDetectorArtk();
    //    Logger::getInstance()->note("TrackingMethodOAbsolutePTAMM: Reading ARTK patterns configuration.");
    mars::Configuration::getConfiguration()->parseARTKPatternsFile(*__userMarks);
    _autoScaleDone=false;
    _autoOffsetDone=false;
  }


  /**
   * Destructor
   */
  System::~System()
  {
    if( mpMap != NULL )  {
      mpMap->mapLockManager.UnRegister( this );
    }

  }


  /**
   * Run the main system thread.
   * This handles the tracker and the map viewer.
   */
  void System::Run()
  {

    int offsetCount = 0;
    while(!mbDone)
      {
	
	/*--------------- PTAMM Stuff ----------------*/
	bool bWasLocked = mpMap->mapLockManager.CheckLockAndWait( this, 0 );
	mpMap->bEditLocked = *mgvnLockMap; //sync up the maps edit lock with the gvar bool.
      	mVideoSource.GetAndFillFrameBWandRGB(mimFrameBW, mimFrameRGB);  
	static bool bFirstFrame = true;
	if(bFirstFrame)
	  {
	    mpARDriver->Init();
	    bFirstFrame = false;
	  }      
	mGLWindow.SetupViewport();
	mGLWindow.SetupVideoOrtho();
	mGLWindow.SetupVideoRasterPosAndZoom();
	if(!mpMap->IsGood()) {
	  mpARDriver->Reset();
	}
	if(bWasLocked)  {
	  mpTracker->ForceRecovery();
	}
	static gvar3<int> gvnDrawMap("DrawMap", 0, HIDDEN|SILENT);
	static gvar3<int> gvnDrawAR("DrawAR", 0, HIDDEN|SILENT);
	bool bDrawMap = mpMap->IsGood() && *gvnDrawMap;
	bool bDrawAR = mpMap->IsGood() && *gvnDrawAR;
	mpTracker->TrackFrame(mimFrameBW, !bDrawAR && !bDrawMap);
	if(bDrawMap) {
	  mpMapViewer->DrawMap(mpTracker->GetCurrentPose());
	}
	else if(bDrawAR) {
	  if( !mpTracker->IsLost() ) {
	  }
	  mpARDriver->Render(mimFrameRGB, mpTracker->GetCurrentPose(), mpTracker->IsLost() );
	}
	if(*mgvnDrawMapInfo) {
	  DrawMapInfo();
	}


	/* -- ARToolKit Detection-- */
	/*Reseting the matrix*/
	for(unsigned int i=0;i<4;i++)
	  for(unsigned int j=0;j<4;j++)
	    __artkGlobalTrans->at<float>(i,j)=0.;

	if(artklocateCamera(__artkGlobalTrans,&__eval)){
	  float* d=Mat2Gl(__artkGlobalTrans);
	  Matrix16 matARTK(d);
	  delete d;
	  __artkDetecting=true;
	}else{
	  __artkDetecting = false;
	}/* --End of ARTK-- */
	double artkVertex[4][2];
	if(__artkDetecting){
	  for(int i=0;i<4;i++)
	    for(int j=0; j<2;j++)
	      artkVertex[i][j]=__userMarks->at(0).getMarkInfo()->vertex[i][j];
	  //Draw ARTK Points
	  glPointSize(6);
	  glEnable(GL_BLEND);
	  glEnable(GL_POINT_SMOOTH);
	  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	  glBegin(GL_POINTS);
	  glColor3f(1,1,1);
	  glVertex2i(artkVertex[0][0],artkVertex[0][1]);
	  glVertex2i(artkVertex[1][0],artkVertex[1][1]);
	  glVertex2i(artkVertex[2][0],artkVertex[2][1]);
	  glVertex2i(artkVertex[3][0],artkVertex[3][1]);
	  glEnd();
	  glDisable(GL_BLEND);
	}
	
	

	/* ---------- OFFSET -----------------*/
	/* El mapa est� escalado, realizar offset */
	if(_autoScaleDone && !_autoOffsetDone)
	  {
	    if(__artkDetecting && mpTracker->mpMap->IsGood() && mpTracker->getTrackingQuality()==2 &&  !mpTracker->IsLost()) //GOOD perception!
	      {

		offsetCount++;
		if(offsetCount>25)
		  {
		    TooN::Vector<3> tr = mpTracker->GetCurrentPose().get_translation();
		    TooN::Matrix<3> rt = mpTracker->GetCurrentPose().get_rotation().get_matrix();
		    
		    float d[16];
		    d[0]=rt[0][0];     d[4]=rt[0][1];     d[8]=rt[0][2];      d[12]=tr[0];
		    d[1]=-rt[1][0];    d[5]=-rt[1][1];    d[9]=-rt[1][2];     d[13]=-tr[1];
		    d[2]=rt[2][0];     d[6]=rt[2][1];     d[10]=rt[2][2];     d[14]=tr[2];
		    d[3]=0.f;          d[7]=0.f;          d[11]=0.f;          d[15]=1.f;
		    
		    cv::Mat mat (4,4,CV_32F,d);
		    //cv::Mat mat2 = mat.inv();

		    std::cout<<"ARTK Global trans (from camera) is: "<<std::endl<<*__artkGlobalTrans<<std::endl;
		    //std::cout<<"PTAMM Trans (to camera) is: "<<std::endl<<mat2<<std::endl;

		    cv::Mat PTAMMOffset = mat * (*__artkGlobalTrans);
		    cv::Mat PTAMMOffset2 = PTAMMOffset.inv();

		    std::cout<<"And Offset is..."<<std::endl<<PTAMMOffset<<std::endl;

		    //saveOffset(&PTAMMOffset);
		    saveOffset(&PTAMMOffset2);

		    _autoOffsetDone = true;
		  }
	      }
	  }/* --End autoOffset-- */




	   if(!_autoScaleDone)
	  {
	    /* Doing our stuff!!! */
	    if(__artkDetecting && mpTracker->mpMap->IsGood() && mpTracker->getTrackingQuality()==2 &&  !mpTracker->IsLost()) //GOOD perception!
	      {
		std::vector<MapPoint*>* candidates = get3DPointsFrom2DPoints(artkVertex);
		bool succeed = doScaleAndGetOffset(candidates);
		if(succeed) _autoScaleDone = true;
	      }
	  } /* --End autoScale-- */

	/* Drawing everything! :) */
	mGLWindow.DrawMenus();
	mGLWindow.swap_buffers();
	mGLWindow.HandlePendingEvents();
      }
  }


  /**
   * Parse commands sent via the GVars command system.
   * @param ptr Object callback
   * @param sCommand command string
   * @param sParams parameters
   */
  void System::GUICommandCallBack(void *ptr, string sCommand, string sParams)
  {
    if(sCommand=="quit" || sCommand == "exit") {
      static_cast<System*>(ptr)->mbDone = true;
    }
    else if( sCommand == "SwitchMap")
      {
	int nMapNum = -1;
	if( static_cast<System*>(ptr)->GetSingleParam(nMapNum, sCommand, sParams) ) {
	  static_cast<System*>(ptr)->SwitchMap( nMapNum );
	}
      }
    else if(sCommand == "ResetAll")
      {
	static_cast<System*>(ptr)->ResetAll();
	return;
      }
    else if( sCommand == "NewMap")
      {
	cout << "Making new map..." << endl;
	static_cast<System*>(ptr)->NewMap();
      }
    else if( sCommand == "DeleteMap")
      {
	int nMapNum = -1;
	if( sParams.empty() ) {
	  static_cast<System*>(ptr)->DeleteMap( static_cast<System*>(ptr)->mpMap->MapID() );
	}
	else if( static_cast<System*>(ptr)->GetSingleParam(nMapNum, sCommand, sParams) ) {
	  static_cast<System*>(ptr)->DeleteMap( nMapNum );
	}
      }
    else if( sCommand == "NextMap")  {
      static_cast<MapViewer*>(ptr)->ViewNextMap();
    }
    else if( sCommand == "PrevMap")  {
      static_cast<MapViewer*>(ptr)->ViewPrevMap();
    }
    else if( sCommand == "CurrentMap")  {
      static_cast<MapViewer*>(ptr)->ViewCurrentMap();
    }
    else if( sCommand == "SaveMap" || sCommand == "SaveMaps" || sCommand == "LoadMap")  {
      static_cast<System*>(ptr)->StartMapSerialization( sCommand, sParams );
    }
    else if( sCommand == "Mouse.Click" ) {
      vector<string> vs = ChopAndUnquoteString(sParams);
    
      if( vs.size() != 3 ) {
	return;
      }

      istringstream is(sParams);
      int nButton;
      ImageRef irWin;
      is >> nButton >> irWin.x >> irWin.y;
      static_cast<ARDriver*>(ptr)->HandleClick( nButton, irWin );
    
    }
    else if( sCommand == "KeyPress" )
      {
	if(sParams == "q" || sParams == "Escape")
	  {
	    GUI.ParseLine("quit");
	    return;
	  }

	bool bUsed = static_cast<System*>(ptr)->mpTracker->HandleKeyPress( sParams );

	if( !bUsed ) {
	  static_cast<System*>(ptr)->mpARDriver->HandleKeyPress( sParams );
	}
      }
    

  }


  /**
   * Parse and allocate a single integer variable from a string parameter
   * @param nAnswer the result
   * @param sCommand the command (used to display usage info)
   * @param sParams  the parameters to parse
   * @return success or failure.
   */
  bool System::GetSingleParam(int &nAnswer, string sCommand, string sParams)
  {
    vector<string> vs = ChopAndUnquoteString(sParams);
    
    if(vs.size() == 1)
      {
	//is param a number?
	bool bIsNum = true;
	for( size_t i = 0; i < vs[0].size(); i++ ) {
	  bIsNum = isdigit( vs[0][i] ) && bIsNum;
	}
      
	if( !bIsNum )
	  {
	    return false;
	  }

	int *pN = ParseAndAllocate<int>(vs[0]);
	if( pN )
	  {
	    nAnswer = *pN;
	    delete pN;
	    return true;
	  }
      }

    cout << sCommand << " usage: " << sCommand << " value" << endl;

    return false;
  }


  /**
   * Switch to the map with ID nMapNum
   * @param  nMapNum Map ID
   * @param bForce This is only used by DeleteMap and ResetAll, and is
   * to ensure that MapViewer is looking at a safe map.
   */
  bool System::SwitchMap( int nMapNum, bool bForce )
  {

    //same map, do nothing. This should not actually occur
    if(mpMap->MapID() == nMapNum) {
      return true;
    }

    if( (nMapNum < 0) )
      {
	cerr << "Invalid map number: " << nMapNum << ". Not changing." << endl;
	return false;
      }

  
    for( size_t ii = 0; ii < mvpMaps.size(); ii++ )
      {
	Map * pcMap = mvpMaps[ ii ];
	if( pcMap->MapID() == nMapNum ) {
	  mpMap->mapLockManager.UnRegister( this );
	  mpMap = pcMap;
	  mpMap->mapLockManager.Register( this );
	}
      }

    if(mpMap->MapID() != nMapNum)
      {
	cerr << "Failed to switch to " << nMapNum << ". Does not exist." << endl;
	return false;
      }
  
    /*  Map was found and switched to for system.
	Now update the rest of the system.
	Order is important. Do not want keyframes added or
	points deleted from the wrong map.
  
	MapMaker is in its own thread.
	System,Tracker, and MapViewer are all in this thread.
    */

    *mgvnLockMap = mpMap->bEditLocked;

 
    //update the map maker thread
    if( !mpMapMaker->RequestSwitch( mpMap ) ) {
      return false;
    }
  
    while( !mpMapMaker->SwitchDone() ) {
#ifdef WIN32
      Sleep(1);
#else
      usleep(10);
#endif
    }

    //update the map viewer object
    mpMapViewer->SwitchMap(mpMap, bForce);
      
    //update the tracker object
    //   mpARDriver->Reset();
    mpARDriver->SetCurrentMap( *mpMap );
  
    if( !mpTracker->SwitchMap( mpMap ) ) {
      return false;
    }

    return true;
  }



  /**
   * Create a new map and switch all
   * threads and objects to it.
   */
  void System::NewMap()
  {

    *mgvnLockMap = false;
    mpMap->mapLockManager.UnRegister( this );
    mpMap = new Map();
    mpMap->mapLockManager.Register( this );
    mvpMaps.push_back( mpMap );
  
    //update the map maker thread
    mpMapMaker->RequestReInit( mpMap );
    while( !mpMapMaker->ReInitDone() ) {
#ifdef WIN32
      Sleep(1);
#else
      usleep(10);
#endif
    }
    
    //update the map viewer object
    mpMapViewer->SwitchMap(mpMap);

    //update the tracker object
    mpARDriver->SetCurrentMap( *mpMap);
    mpARDriver->Reset();
    mpTracker->SetNewMap( mpMap );
    
    cout << "New map created (" << mpMap->MapID() << ")" << endl;
      
  }


  /**
   * Moves all objects and threads to the first map, and resets it.
   * Then deletes the rest of the maps, placing PTAMM in its
   * original state.
   * This reset ignores the edit lock status on all maps
   */
  void System::ResetAll()
  {

    //move all maps to first map.
    if( mpMap != mvpMaps.front() )
      {
	if( !SwitchMap( mvpMaps.front()->MapID(), true ) ) {
	  cerr << "Reset All: Failed to switch to first map" << endl;
	}
      }
    mpMap->bEditLocked = false;

    //reset map.
    mpTracker->Reset();
  
    //lock and delete all remaining maps
    while( mvpMaps.size() > 1 )
      {
	DeleteMap( mvpMaps.back()->MapID() );
      }

  }


  /**
   * Delete a specified map.
   * @param nMapNum map to delete
   */
  bool System::DeleteMap( int nMapNum )
  {
    if( mvpMaps.size() <= 1 )
      {
	cout << "Cannot delete the only map. Use Reset instead." << endl;
	return false;
      }


    //if the specified map is the current map, move threads to another map
    if( nMapNum == mpMap->MapID() )
      {
	int nNewMap = -1;
    
	if( mpMap == mvpMaps.front() ) {
	  nNewMap = mvpMaps.back()->MapID();
	}
	else {
	  nNewMap = mvpMaps.front()->MapID();
	}
    
	// move the current map users elsewhere
	if( !SwitchMap( nNewMap, true ) ) {
	  cerr << "Delete Map: Failed to move threads to another map." << endl;
	  return false;
	}
      }

  
 
    // find and delete the map
    for( size_t ii = 0; ii < mvpMaps.size(); ii++ )
      {
	Map * pDelMap = mvpMaps[ ii ];
	if( pDelMap->MapID() == nMapNum ) {

	  pDelMap->mapLockManager.Register( this );
	  pDelMap->mapLockManager.LockMap( this );
	  delete pDelMap;
	  mvpMaps.erase( mvpMaps.begin() + ii );

	  ///@TODO Possible bug. If another thread (eg serialization) was using this
	  /// and waiting for unlock, would become stuck or seg fault.
	}
      }
  
    return true;
  }


  /**
   * Set up the map serialization thread for saving/loading and the start the thread
   * @param sCommand the function that was called (eg. SaveMap)
   * @param sParams the params string, which may contain a filename and/or a map number
   */
  void System::StartMapSerialization(std::string sCommand, std::string sParams)
  {
    if( mpMapSerializer->Init( sCommand, sParams, *mpMap) ) {
      mpMapSerializer->start();
    }
  }


  /**
   * Draw a box with information about the maps.
   */
  void System::DrawMapInfo()
  {
    int nLines = static_cast<int>(mvpMaps.size()) + 2;
    int x = 5, y = 120, w = 160, nBorder = 5;
  
    mGLWindow.DrawBox( x, y, w, nLines, 0.7f );

    y += 17;

    glColor3f(1,1,1);
    std::ostringstream os;
    os << "Maps " << mvpMaps.size();
    mGLWindow.PrintString( ImageRef(x + nBorder,y + nBorder), os.str() );
    os.str("");
      
    for( size_t i = 0; i < mvpMaps.size(); i++ )
      {
	Map * pMap = mvpMaps[i];
	if( pMap == mpMap ) {
	  glColor3f(1,1,0);
	}
	else if( pMap->bEditLocked ) {
	  glColor3f(1,0,0);
	}
	else {
	  glColor3f(1,1,1);
	}
    
	os << "M: " << pMap->MapID() << "  P: " << pMap->vpPoints.size() << "  K: " << pMap->vpKeyFrames.size();
	mGLWindow.PrintString( ImageRef( x + nBorder , y + nBorder + (i+1)*17), os.str() );
	os.str("");
      }
  }



  /* AutoTunning definitions */

  /*Try to locate the camera just using the frames captured*/
  /*Global evaluation is the max of the local evaluations*/
  bool System::artklocateCamera(cv::Mat* globalTrans,float* globalEval){
    cv::Mat* frame=NULL;
    int marksFound=0; /*Marks for positioning (position and user marks)*/
    float sumEval=0;

    *globalEval=0;
   
    VideoSource* __uCam = &mVideoSource;

    /*-------------------------------------------PROCESS USER CAM-----------------------------------------*/
    __artkDetector->changeCamera(__uCam);

    //Grab the current frame
    if (__uCam!=NULL)
      frame=__uCam->getLastFrame();

    if(frame->rows==0){
      return false;
    }
       
    // /*Detecting ARTK patterns for the user cam*/
    if(__artkDetector->detect(frame,__userMarks,&__userHistory));

    for(unsigned int percep=0;percep<__userMarks->size();percep++){
      if(__userMarks->at(percep).isDetected()){ //Pattern detected
	/*Position mark*/
    	if(__userMarks->at(percep).isPositionMark()){
    	  marksFound+=1;
    	  /*Acumulate eval factors*/
    	  float localEval=__userMarks->at(percep).getEval(10,10);
   	  *globalEval=*globalEval<localEval?localEval:*globalEval;
    	  sumEval+=localEval;
    	}
      }
    }

    delete frame;

    if(marksFound>0){
      /*--------------Process user camera marks-----*/
      for(unsigned int percep=0;percep<__userMarks->size();percep++){
	if(__userMarks->at(percep).isDetected() && __userMarks->at(percep).isPositionMark()){
	  /*---------Position mark (from User Camera)--------------*/
	  cv::Mat* markGlobalTrans;
	  markGlobalTrans = new cv::Mat(__userMarks->at(percep).getRelativeMatrix()->inv() * *(__userMarks->at(percep).getPosMatrix()));
	  float markEval=__userMarks->at(percep).getEval(10,10);
	  *globalTrans=(*globalTrans)+ *markGlobalTrans * markEval/sumEval;
	}
      }
      return true;
    }

    return false;
  }




/* Aux functions */
/* Para terminar el escalado! */
void System::scale(float ratio){
  for(unsigned int i=0;i<mpMap->vpKeyFrames.size();i++){
    mpMap->vpKeyFrames[i]->se3CfromW.get_translation()*=ratio;
  }
  std::vector<MapPoint*>& points=mpMap->vpPoints;
  for(unsigned int j=0;j<points.size();j++){
    MapPoint* p=points.at(j);
    p->v3WorldPos[0]*=ratio;
    p->v3WorldPos[1]*=ratio;
    p->v3WorldPos[2]*=ratio;
    p->v3PixelRight_W[0] *= ratio;
    p->v3PixelRight_W[1] *= ratio;
    p->v3PixelRight_W[2] *= ratio;
    p->v3PixelDown_W[0] *= ratio;
    p->v3PixelDown_W[1] *= ratio;
    p->v3PixelDown_W[2] *= ratio;
    p->RefreshPixelVectors();
  }
}


float System::getDistanceMapPoints(MapPoint* p1, MapPoint* p2)
{
  return sqrt(pow(p1->v3WorldPos[0] - p2->v3WorldPos[0],2)+
	      pow(p1->v3WorldPos[1] - p2->v3WorldPos[1],2)+
	      pow(p1->v3WorldPos[2] - p2->v3WorldPos[2],2));
}

void System::saveOffset(cv::Mat* offset){

  std::cout<<"Writing Offset Matrix for map 0..."<<std::endl;
  std::ofstream file; 
  file.open("config/PTAMMOffset.mat",std::ios::out);

  if(!file.is_open()){
    std::cout<<"Error opening PTAMMOffset.mat file to write"<<std::endl;
    exit(-1);
  }
  
  Ogre::Matrix4 m(offset->at<float>(0,0),offset->at<float>(1,0),offset->at<float>(2,0),offset->at<float>(3,0),
  		  offset->at<float>(0,1),offset->at<float>(1,1),offset->at<float>(2,1),offset->at<float>(3,1),
  		  offset->at<float>(0,2),offset->at<float>(1,2),offset->at<float>(2,2),offset->at<float>(3,2),
  		  offset->at<float>(0,3),offset->at<float>(1,3),offset->at<float>(2,3),offset->at<float>(3,3));

  Ogre::Vector3 p = m.getTrans();
  Ogre::Quaternion o = m.extractQuaternion();

  std::cout<<"Offset position is: "<<p<<std::endl;
  std::cout<<"Offset Quaternion is: "<<o<<std::endl;

  file<<"# #Ogre::Vector3_Position #Ogre::Quaternion_Orientation"<<std::endl;
  file<<"[0] ";
  file<<p[0]<<" "<<p[1]<<" "<<p[2]<<" ";
  file<<o[0]<<" "<<o[1]<<" "<<o[2]<<" "<<o[3];
  file<<std::endl;
 
  std::cout<<"Wrote succesfully!"<<std::endl;
}


  /*
    Returns true if it succeed!
  */
  bool System::doScaleAndGetOffset(std::vector<MapPoint*>* candidates)
  {
    std::vector<Distance> distances[4];

    //Check if some of them are empty!
    int emptyCandidates = 0;
    for(int i=0;i<4;i++)
      {
	if(candidates[i].size()==0){
	  emptyCandidates++;
	}	
      }
    if(emptyCandidates>=2) 
      {
	std::cout<<"ERROR!!! Al menos queremos medidas para dos de los cuatro lados de la marca! :)"<<std::endl;
	return false;
      }

    
    int nDist = 0;
    float meanDist = 0;
    //Settings distances (sides of the marker)
    for(int i=0;i<4;i++)
      {
	for(unsigned int p1=0; p1<candidates[i].size();p1++)
	  {
	    for(unsigned int p2=0; p2<candidates[(i+1)%4].size();p2++)
	      {
		Distance d;
		d.p1 = candidates[i].at(p1);
		d.p2 = candidates[(i+1)%4].at(p2);
		d.dist = getDistanceMapPoints(d.p1, d.p2);
		distances[i].push_back(d);
		std::cout<<"In Distance "<<i<<" found: "<<d.dist<<std::endl;
		nDist++;
		meanDist += d.dist;
	      }
	  }
      }

  
    /* Queremos al menos 4 valores de lados */
    if(nDist<3)
      {
	std::cout<<"ERROR! Al menos queremos 3 distancias :), para calcular, y eso..."<<std::endl;
	return false;
      }

    meanDist = meanDist/nDist;

    /* Calculate varianze */
    float varianzeDist = 0;
    for(int i=0;i<4;i++)
      {
	for(unsigned int d=0; d<distances[i].size(); d++)
	  {
	    varianzeDist += pow(distances[i].at(d).dist - meanDist, 2);
	  }
      }
    
    varianzeDist = varianzeDist / nDist;

    std::cout<<"Mean: "<<meanDist<<std::endl;
    std::cout<<"Varianze: "<<varianzeDist<<std::endl;
    std::cout<<"Ratio: "<<(varianzeDist/meanDist)<<std::endl;


    /* La varianza debe ser menos del 5% de la media!!! */
    if((varianzeDist/meanDist)>0.05){
      std::cout<<"Mmmm.. la varianza de los datos es mayor del 5%! me fio yo de esto? NO!"<<std::endl;
      return false;
    }

    /* Ahora hay que seleccionar los puntos que m�s se acerquen a la media, y calcular el ratio con eso! :) */
    float choosenOnesDistances[4] = {0,0,0,0};
    for(int i=0;i<4;i++)
      {
	float minDistanceFromMean = 99999.9f; /* Infinite! :) */
	for(unsigned int d=0; d<distances[i].size(); d++)
	  {
	    if(pow(distances[i].at(d).dist-meanDist,2)<minDistanceFromMean)
	      {
		minDistanceFromMean = pow(distances[i].at(d).dist-meanDist,2);
		choosenOnesDistances[i] = distances[i].at(d).dist;
	      }
	  }
      }



    /* Calculando el ratio */

    float ratio=0;
    float usedDist=0;
    /* Media? */
    for(int i=0;i<4;i++)
      {
	if(choosenOnesDistances[i]!=0)
	  {
	    usedDist++;
	    ratio+=choosenOnesDistances[i];
	  }
      }

    ratio = ratio/usedDist;


    std::cout<<"Applying a Ratio of: "<<ratio<<std::endl;

    scale(ratio); //Scale and save :)

    std::cout<<"Se ha escalado! A calcular el offset! :)"<<std::endl;
    return true;


  }


  //Devuelve los identificadores de los 4 Puntos 3D
  //Returns -1 if no succeed
  std::vector<MapPoint*>* System::get3DPointsFrom2DPoints(double vertex[4][2])
  {
    int pointsCompared=0;
    std::vector<TrackerData*> vIterationSet;
    std::vector<MapPoint*>* candidatePoints = new std::vector<MapPoint*>[4];
       
    for(int vi=0;vi<4;vi++)
      {
	//	std::cout<<"Looking for VERTEX "<<vi<<std::endl;
	for(int i=0;i<LEVELS;i++){
	  vIterationSet = mpTracker->actualPoints[i];
	  for(vector<TrackerData*>::reverse_iterator it = vIterationSet.rbegin();
	      it!= vIterationSet.rend(); 
	      it++)
	    {
	      if(! (*it)->bFound)
		continue;
	      int difX = abs(vertex[vi][0] - (*it)->v2Image[0]);
	      int difY = abs(vertex[vi][1] - (*it)->v2Image[1]);
	      if(difX < PIXEL_OFFSET && difY < PIXEL_OFFSET){

		MapPoint* mp = &((*it)->Point);
		candidatePoints[vi].push_back(mp);
	      }
	    }
	}
      }
    return candidatePoints;

  }

}
