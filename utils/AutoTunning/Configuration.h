/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * Configuration.h
 *
 */

#ifndef CONFIGURATION_H_
#define CONFIGURATION_H_

#define CONFIG_FILE "config/mars.cfg"

#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <cctype>
#include <cstdio>
#include <sstream>
#include <vector>

#include "ARTKMark.h"
#include "Singleton.h"
#include "Matrix16.h"

namespace mars {

/**
 *
 * The configuration class. This will read the config file and act consequently.
 *
 */
class Configuration : public Singleton<Configuration> {
public:

	static Configuration* getConfiguration();



	/**
	 * ARTK RELATED
	 */
	const std::string ARTKDataPath(){ return _ARTKDataPath;}

	/**
	 *  Parses the ARTK Marks file
	 */
	void parseARTKPatternsFile(std::vector<ARTKMark>& marks);

	/**
	 * ARTK RELATED
	 */
	const std::string ARTKPatternsFile(){ return _ARTKPatternsFile;}

	/**
	 * X resolution
	 */
	const int  openGLScreenWidth(){ return _openGLScreenWidth;}

	/**
	 * Y resolution
	 */
	const int  openGLScreenHeight(){ return _openGLScreenHeight;}

	/**
	 * Returns the depth of the video mode.
	 */
	const int  openGLScreenDepth(){ return _openGLScreenDepth;}


private:

	Configuration();
	virtual~ Configuration();

	std::string _ARTKDataPath;
	std::string _ARTKPatternsFile;
	bool _openGLFullScreen;
	int  _openGLScreenWidth;
	int  _openGLScreenHeight;
	int  _openGLScreenDepth;
	
        std::ifstream __configFile; ///< input stream for the config file
        std::ifstream __patternsFile; ///< input stream for the patterns file

	friend class Singleton<Configuration>;

};


}

#endif /* CONFIGURATION_H_ */
