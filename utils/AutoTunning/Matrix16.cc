/*
 * Matrix16.cpp
 *
 */

#include "Matrix16.h"

namespace mars {


void Matrix16::printModelView(){
	Matrix16 m;
	glGetFloatv(GL_MODELVIEW_MATRIX, m.getData());
	std::cout << "--\nModelView\n--\n" << m << std::endl;
}


Matrix16::Matrix16() {
	for (unsigned i=0; i<16;i++)
		data[i]=0;
	data[0] = 1;
	data[5] = 1;
	data[10] = 1;
	data[15] = 1;
}

Matrix16::Matrix16(cv::Mat& m){
	if ((m.rows != 4) || (m.cols )){
	  //		Logger::getLogger()->warning("Matrix16:: Not a valid opencv Matrix. It should be a 4x4 matrix");
	} else {
		for (unsigned i=0; i<4;i++)
			for (unsigned j=0; j<4;j++)
				data[i*4+j] = m.at<float>(i, j); // FIXME - Correct?
	}
}

Matrix16::Matrix16(GLfloat* m) {
	for (unsigned i=0; i<16;i++)
		data[i]=m[i];
}


void Matrix16::setData(GLfloat* m){
	for (unsigned i=0; i < 16; i++)
		data[i]=m[i];
}


Matrix16::~Matrix16() {}

GLfloat* Matrix16::getTranslation(){
	return &data[12];
}



GLfloat* Matrix16::getData(){
	return &data[0];
}

std::ostream& operator<<(std::ostream& os, const Matrix16& m){

	os << std::setprecision (10) <<  std::fixed ;
	os << "|------------------------------------------------------------------------|\n";
	os << "|       RotX      |       RotY      |       RotZ      |    Translation   |\n";
	os << "|-----------------|-----------------|-----------------|------------------|\n";
	os << "| ";   os.width (15); os << m.data[0] << " | ";   os.width (15); os  << m.data[4] << " | ";   os.width (15); os  << m.data[8] <<  " | ";   os.width (15); os << m.data[12] << "  |\n";
	os << "| ";   os.width (15); os  <<  m.data[1] << " | ";   os.width (15); os  << m.data[5] << " | ";   os.width (15); os  << m.data[9] <<  " | ";   os.width (15); os << m.data[13] << "  |\n";
	os << "| ";   os.width (15); os  <<  m.data[2] << " | ";   os.width (15); os  << m.data[6] << " | ";   os.width (15); os  << m.data[10] << " | ";   os.width (15); os << m.data[14] << "  |\n";
	os << "| ";   os.width (15); os  <<  m.data[3] << " | ";   os.width (15); os  << m.data[7] << " | ";   os.width (15); os  << m.data[11] << " | ";   os.width (15); os << m.data[15] << "  |\n";
	os << "|------------------------------------------------------------------------|\n";
	return os;
}



}
