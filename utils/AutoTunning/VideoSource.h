// -*- c++ *--
// Copyright 2008 Isis Innovation Limited
//
// VideoSource.h
// Declares the VideoSource class
// 
// This is a very simple class to provide video input; this can be
// replaced with whatever form of video input that is needed.  It
// should open the video input on construction, and provide two
// function calls after construction: Size() must return the video
// format as an ImageRef, and GetAndFillFrameBWandRGB should wait for
// a new frame and then overwrite the passed-as-reference images with
// GreyScale and Colour versions of the new frame.
//

#ifndef _VIDEOSOURCE_H_
#define _VIDEOSOURCE_H_

#include <cvd/image.h>
#include <cvd/byte.h>
#include <cvd/rgb.h>
#include <uEye.h>
#include <opencv/cv.h>
#include "Matrix16.h"
#include "Configuration.h"

namespace PTAMM {

struct VideoSourceData;

class VideoSource
{
public:
VideoSource();
void GetAndFillFrameBWandRGB(CVD::Image<CVD::byte> &imBW, CVD::Image<CVD::Rgb<CVD::byte> > &imRGB);
CVD::ImageRef Size();
  
int getWidth(){ return width; }
int getHeight(){ return height; }
void calcProjectionMatrixFromCameraMatrix();
bool isCalibrated();
 void getPerspectiveMatrix(double m[3][4]);
 
 cv::Mat* getLastFrame(){return new cv::Mat(frameMat);}

private:
void init();
void *mptr;
CVD::ImageRef mirSize;
cv::Mat frameMat;
  
UEYE_CAMERA_INFO __cameraInfo;
IS_RECT rectAOI;

int width;
int height;

HIDS __hCam;

int __maxW;
int __maxH;
  
int __bitsPerPixel;
int __colorMode;
  
char* __imageMem;
int __memId;
  

float __fx;
float __fy;
float __cx;
float __cy;
cv::Mat* __posMatrix;

bool __calibrated;

cv::Mat __cameraMatrix;
cv::Mat __distCoeffs;

};


}

#endif
