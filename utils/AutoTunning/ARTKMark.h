/*
 * ARTKMark.h
 *
 */

#ifndef ARTKMARK_H_
#define ARTKMARK_H_

#define X_AXIS 0
#define Y_AXIS 1
#define Z_AXIS 2

#define ACTUATION_MARK 1
#define POSITION_MARK 2
#define USER_MARK 4

#include "Matrix16.h"
#include <AR/ar.h>

namespace mars {

class ARTKMark {
public:
	ARTKMark();
	//ARTKMark(const ARTKMark& obj); //Copy constructor!
	ARTKMark(Matrix16& m, std::string file, float size,int type);
	ARTKMark(std::string file, float size,int type);
	virtual ~ARTKMark();
	//void changeMatrix(int axis, float desp, float rot);

	/*Kind of mark*/
	bool isPositionMark();	
	bool isActuationMark();
	bool isUserMark();

	/*Getters and setters*/
	int getFoundId();
	void setFoundId(int foundId);
	void setDetected(bool detected);
	bool isDetected();
	int getId();
	double* getCenter();
	void setSize(const float& s );
	float getSize();
	int getType();
	void setFile(std::string file);
	std::string getFile();
	cv::Mat* getPosMatrix();  //Transformation intrinsec matrix
	float getEval(float distImp,float angImp);
	void setRelativeMatrix(cv::Mat& m);
	//void setRelativeMatrix(Matrix16& m);
	//cv::Mat* getGlobalMatrix();
	cv::Mat* getRelativeMatrix();
	void changeMatrix(int axis, float desp, float rot);
	void setMarkInfo(ARMarkerInfo* markerInfo);
	ARMarkerInfo* getMarkInfo();

private:
	cv::Mat __pMatrix;
	cv::Mat __rMatrix;   // The associated relative matrix
	std::string __file;  // The pattern file
	int __type;          //Actuation (a), position (p) or both (b) type
	float __size;
	double __center[2];
	ARMarkerInfo _markerInfo;

	/*
	  Is detected by ANY camera!
	 */
	bool __detected;
	int __id;
	int __foundId;

	float* Mat2Gl(cv::Mat* mat);
	cv::Mat* Gl2Mat(float *gl);

};

}

#endif /* ARTKMARK_H_ */
