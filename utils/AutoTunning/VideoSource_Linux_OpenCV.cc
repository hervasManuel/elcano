/*
 * Autor : Arnaud GROSJEAN (VIDE SARL)
 * This implementation of VideoSource allows to use OpenCV as a source for the video input
 * I did so because libCVD failed getting my V4L2 device
 *
 * INSTALLATION :
 * - Copy the VideoSource_Linux_OpenCV.cc file in your PTAM directory
 * - In the Makefile:
 *	- set the linkflags to
 LINKFLAGS = -L MY_CUSTOM_LINK_PATH -lblas -llapack -lGVars3 -lcvd -lcv -lcxcore -lhighgui
 *	- set the videosource to 
 VIDEOSOURCE = VideoSource_Linux_OpenCV.o
 * - Compile the project
 * - Enjoy !
 * 
 * Notice this code define two constants for the image width and height (OPENCV_VIDEO_W and OPENCV_VIDEO_H)
 */

#include "VideoSource.h"
//#include <cvd/Linux/v4lbuffer.h>
#include <cvd/colourspace_convert.h>
#include <cvd/colourspaces.h>
#include <gvars3/instances.h>
#include <opencv/highgui.h>
//#include <opencv/cxcore.h>


using namespace CVD;
using namespace std;
using namespace GVars3;
using namespace cv;
using namespace PTAMM;

#define OPENCV_VIDEO_W 640
#define OPENCV_VIDEO_H 480

bool VideoSource::isCalibrated(){
	return __calibrated;
}

void VideoSource::getPerspectiveMatrix(double m[3][4]){

	m[0][0] = __fx; m[0][1] = 0;    m[0][2] = __cx; m[0][3] = 0;
	m[1][0] = 0;    m[1][1] = __fy; m[1][2] = __cy; m[1][3] = 0;
	m[2][0] = 0;	m[2][1] = 0;    m[2][2] = 1;    m[2][3] = 0;

}

VideoSource::VideoSource()
{
  cout << "  VideoSource_Linux: Opening video source..." << endl;
  mptr = new VideoCapture(0);
  VideoCapture* cap = (VideoCapture*)mptr;
  if(!cap->isOpened()){
    cerr << "Unable to get the camera" << endl;
    exit(-1);
  }

  //Video Hack :S
  cap->set(CV_CAP_PROP_FRAME_WIDTH, 640);
  cap->set(CV_CAP_PROP_FRAME_HEIGHT, 480);
  width = 640;
  height = 480;

  cout << "  ... got video source." << endl;
  mirSize = ImageRef(OPENCV_VIDEO_W, OPENCV_VIDEO_H);

  init();
};

ImageRef VideoSource::Size()
{ 
  return mirSize;
};

void conversionNB(Mat frame, Image<byte> &imBW){
  Mat clone = frame.clone();
  Mat_<Vec3b>& frame_p = (Mat_<Vec3b>&)clone;
  for (int i = 0; i < OPENCV_VIDEO_H; i++){
    for (int j = 0; j < OPENCV_VIDEO_W; j++){	
      imBW[i][j] = (frame_p(i,j)[0] + frame_p(i,j)[1] + frame_p(i,j)[2]) / 3;
    }
  }

}

void conversionRGB(Mat frame, Image<Rgb<byte> > &imRGB){
  Mat clone = frame.clone();
  Mat_<Vec3b>& frame_p = (Mat_<Vec3b>&)clone;
  for (int i = 0; i < OPENCV_VIDEO_H; i++){
    for (int j = 0; j < OPENCV_VIDEO_W; j++){	
      imRGB[i][j].red = frame_p(i,j)[2];
      imRGB[i][j].green = frame_p(i,j)[1];
      imRGB[i][j].blue = frame_p(i,j)[0];
    }
  }
}

void VideoSource::GetAndFillFrameBWandRGB(Image<byte> &imBW, Image<Rgb<byte> > &imRGB)
{
  Mat frame;
  VideoCapture* cap = (VideoCapture*)mptr;
  *cap >> frame;
  frameMat = frame;
  conversionNB(frame, imBW);
  conversionRGB(frame, imRGB);
}


void VideoSource::init(){
	__fx = 0;
	__fy = 0;
	__cx = 0;
	__cy = 0;

	__calibrated = false;


	string cName = "calibs/user-calib.xml";
	try {
	  cv::FileStorage fs(cName.c_str(), cv::FileStorage::READ);
	  if (fs.isOpened()){
	    fs["cm"] >> __cameraMatrix;
	    fs["dm"] >> __distCoeffs;
	    string msg = "calibs/user-calib.xml found and loaded!";
	    std::cout<<msg<<std::endl;
	    __calibrated = true;
	    calcProjectionMatrixFromCameraMatrix();
	  } else {
	    std::cout<<"ERROR: "<<cName + " cannot be open, does it exist? Maybe you should calibrate that videosource"<<std::endl;
	  }

	} catch (cv::Exception e) {
	  std::cout<<"ERROR: Problem with calibs/ user-calib.xml found!"<<std::endl;
	}


	

}


  void VideoSource::calcProjectionMatrixFromCameraMatrix(){
	double fx = __cameraMatrix.at<double>(0,0);
	double fy = __cameraMatrix.at<double>(1,1);
	double cx = __cameraMatrix.at<double>(0,2);
	double cy = __cameraMatrix.at<double>(1,2);

	__fx = fx;
	__fy = fy;
	__cx = cx;
	__cy = cy;

	mars::Matrix16 m;

	GLfloat* data = m.getData();

	float near  = 0.1f;
	float far = 10000.0f;
	//float far = 100.0f;

	float w = (float) (mars::Configuration::getConfiguration()->openGLScreenWidth() );
	float h = (float) (mars::Configuration::getConfiguration()->openGLScreenHeight() );


	data[0]  = 2.0*fx/w;
	data[1]  = 0;
	data[2]  = 0;
	data[3]  = 0;

	data[4]  = 0;
	data[5]  = 2.0*fy/h;
	data[6]  = 0;
	data[7]  = 0;

	data[8]  = 2.0*(cx/w)-1.0;
	data[9]  = 2.0*(cy/h)-1.0;
	data[10] = -(far+near)/(far-near);
	data[11] = -1;

	data[12] = 0;
	data[13] = 0;
	data[14] = -2.0*far*near/(far-near);
	data[15] = 0;

}

