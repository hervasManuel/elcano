/*
* Autor : Arnaud GROSJEAN (VIDE SARL)
* This implementation of VideoSource allows to use OpenCV as a source for the video input
* I did so because libCVD failed getting my V4L2 device
*
* INSTALLATION :
* - Copy the VideoSource_Linux_OpenCV.cc file in your PTAM directory
* - In the Makefile:
*	- set the linkflags to
	LINKFLAGS = -L MY_CUSTOM_LINK_PATH -lblas -llapack -lGVars3 -lcvd -lcv -lcxcore -lhighgui
*	- set the videosource to 
	VIDEOSOURCE = VideoSource_Linux_OpenCV.o
* - Compile the project
* - Enjoy !
* 
* Notice this code define two constants for the image width and height (OPENCV_VIDEO_W and OPENCV_VIDEO_H)
*/

#include "VideoSource.h"
//#include <cvd/Linux/v4lbuffer.h>
#include <cvd/colourspace_convert.h>
#include <cvd/colourspaces.h>
#include <gvars3/instances.h>
#include <opencv/highgui.h>
#include <opencv/cxcore.h>
#include <opencv/cv.h>

#include <iostream>
#include <string>
#include <sstream>



using namespace CVD;
using namespace std;
using namespace GVars3;
using namespace cv;
using namespace PTAMM;

#define OPENCV_VIDEO_W 640
#define OPENCV_VIDEO_H 480

template <typename T>
std::string toString(T t){
	std::stringstream ss;
	ss << t;
	return ss.str();
}


PUEYE_CAMERA_LIST getUEyeCameraList(){

//	PUEYE_CAMERA_LIST cList = new UEYE_CAMERA_LIST;
//	cList->dwCount = 0;
//
//	 if (is_GetCameraList (cList) != IS_SUCCESS)
//	 {
//		 Logger::getInstance()->error("VideoDeviceUEye:: no uEye camera found.");
//	 } else {
//		 Logger::getInstance()->note("VideoDeviceUEye:: " +
//				 toString(cList->dwCount) +
//				 " uEye camera/s found.");
//	 }

	PUEYE_CAMERA_LIST cList;
	cList = new UEYE_CAMERA_LIST;
	cList->dwCount = 0;
	DWORD nCams_uEYE = 0;
	if (is_GetCameraList (cList) == IS_SUCCESS){

		nCams_uEYE = cList->dwCount;

		cout << "[*] Number of Cameras:: " << nCams_uEYE << endl;

		delete cList;

		cList = (PUEYE_CAMERA_LIST) new char[sizeof(DWORD) + nCams_uEYE * sizeof(UEYE_CAMERA_INFO)];
		cList->dwCount = nCams_uEYE;

		cout << sizeof(DWORD) + nCams_uEYE * sizeof(UEYE_CAMERA_INFO) << endl;
		cout << cList->dwCount << endl;

		if (is_GetCameraList(cList) == IS_SUCCESS)	{
			cout << "[*] OK :)" << endl;
			for (int c=0; c < (int) nCams_uEYE; ++c){
				cout << "-- UEYE CAMERA --" << endl;
				cout << "CameraID :" << cList->uci[c].dwCameraID << endl;
				cout << "Ser No.  :" << cList->uci[c].SerNo << endl;
				cout << "DeviceID :" << cList->uci[c].dwDeviceID << endl;
				cout << "In use   :" << cList->uci[c].dwInUse << endl;
				cout << "SensorID :" << cList->uci[c].dwSensorID << endl;
			}
			cout << "--" << endl;
		}
	  }


	return cList;
}


VideoSource::VideoSource()
{
  

	cout << "  VideoSource_Linux: Opening video source..." << endl;


	int uEyeNext=0;
	PUEYE_CAMERA_LIST pCam;
	pCam = getUEyeCameraList();
	__cameraInfo = pCam->uci[uEyeNext], ("uEye" + toString(pCam->uci[uEyeNext].SerNo));


	__hCam = (HIDS) __cameraInfo.dwDeviceID;
	mptr = (void*) &__hCam;

	int ret = is_InitCamera(&__hCam, NULL);

	is_EnableAutoExit(__hCam, IS_ENABLE_AUTO_EXIT);

	if (ret == IS_SUCCESS){

		SENSORINFO sInfo;                 // sensor information
		is_GetSensorInfo(__hCam, &sInfo); // query

		is_SetExternalTrigger (__hCam, IS_SET_TRIGGER_OFF);

		__maxW = sInfo.nMaxWidth;
		__maxH = sInfo.nMaxHeight;

		cout << "maxW:: " << __maxW << endl;
		cout << "maxH:: " << __maxH << endl;
		
		__maxW = OPENCV_VIDEO_W ;
		__maxH = OPENCV_VIDEO_H;

		__bitsPerPixel = 24;

		__colorMode = IS_CM_BGR8_PACKED;

		is_SetColorMode(__hCam, __colorMode);

		// Allocate memory for the image
		is_AllocImageMem(__hCam, __maxW, __maxH, __bitsPerPixel, &__imageMem, &__memId);

		is_SetImageMem(__hCam, __imageMem, __memId);

		int xPos, yPos = 0;
                
                //is_AOI
                //
                IS_RECT rectAOI;
 
                rectAOI.s32X     = 0;
                rectAOI.s32Y     = 0;
                rectAOI.s32Width = __maxW;
                rectAOI.s32Height = __maxH;
                
                is_AOI(__hCam, IS_AOI_IMAGE_SET_AOI ,(void*)&rectAOI, sizeof(rectAOI));
                
                is_SetPixelClock(__hCam, 16);
                
		double nFPS;
		is_SetFrameRate(__hCam, 30.0, &nFPS);

		is_EnableEvent(__hCam, IS_SET_EVENT_FRAME);

		is_CaptureVideo(__hCam, IS_DONT_WAIT);

		width = 640;

		height = 480;



	}

	init();

  cout << "  ... got video source." << endl;
  mirSize = ImageRef(OPENCV_VIDEO_W, OPENCV_VIDEO_H);
};

ImageRef VideoSource::Size()
{ 
  return mirSize;
};

void conversionNB(Mat frame, Image<byte> &imBW){
	Mat clone = frame.clone();
	Mat_<Vec3b>& frame_p = (Mat_<Vec3b>&)clone;
	for (int i = 0; i < OPENCV_VIDEO_H; i++){
		for (int j = 0; j < OPENCV_VIDEO_W; j++){	
		imBW[i][j] = (frame_p(i,j)[0] + frame_p(i,j)[1] + frame_p(i,j)[2]) / 3;
		}
	}
}

void conversionRGB(Mat frame, Image<Rgb<byte> > &imRGB){
	Mat clone = frame.clone();
	Mat_<Vec3b>& frame_p = (Mat_<Vec3b>&)clone;
	for (int i = 0; i < OPENCV_VIDEO_H; i++){
		for (int j = 0; j < OPENCV_VIDEO_W; j++){	
		imRGB[i][j].red = frame_p(i,j)[2];
		imRGB[i][j].green = frame_p(i,j)[1];
		imRGB[i][j].blue = frame_p(i,j)[0];
		}
	}
}

void VideoSource::GetAndFillFrameBWandRGB(Image<byte> &imBW, Image<Rgb<byte> > &imRGB)
{
  Mat frame;
  
  if (is_WaitEvent(__hCam, IS_SET_EVENT_FRAME, 1) == IS_SUCCESS) {
    IplImage i;
    IplImage* img = new IplImage;
    
    img->nSize=112;
    img->ID=0;
    img->nChannels=3;
    img->alphaChannel=0;
    img->depth=8;
    img->dataOrder=0;
    img->origin=0;
    img->align=4;
    img->width=__maxW;
    img->height=__maxH;
    img->roi=NULL;
    img->maskROI=NULL;
    img->imageId=NULL;
    img->tileInfo=NULL;
    img->imageSize=3*__maxW*__maxH;
    img->imageData=(char*)__imageMem;  //the pointer to imagaData
    img->widthStep=3*__maxW;
    img->imageDataOrigin=(char*)__imageMem; //and again
    
    
    frame = Mat(img, false);
    frameMat = frame;
 
  }else{
    return;
  }
  
  conversionNB(frame, imBW);
  conversionRGB(frame, imRGB);
}


void VideoSource::init(){
	__fx = 0;
	__fy = 0;
	__cx = 0;
	__cy = 0;

	__calibrated = false;


	string cName = "calibs/uEye4002747943-calib.xml";
	try {
	  cv::FileStorage fs(cName.c_str(), cv::FileStorage::READ);
	  if (fs.isOpened()){
	    fs["cm"] >> __cameraMatrix;
	    fs["dm"] >> __distCoeffs;
	    string msg = "calibs/uEye4002747943-calib.xml found and loaded!";
	    //	Logger::getLogger()->note(msg);
	    __calibrated = true;
	    calcProjectionMatrixFromCameraMatrix();
	  } else {
	    //	Logger::getLogger()->error(cName + " cannot be open, does it exist? Maybe you should calibrate that videosource");
	  }

	} catch (cv::Exception e) {
	  //	  string msg = "Problem with calibs/" + __name + "-calib.xml found!";
	}



}

bool VideoSource::isCalibrated(){
	return __calibrated;
}

void VideoSource::getPerspectiveMatrix(double m[3][4]){

	m[0][0] = __fx; m[0][1] = 0;    m[0][2] = __cx; m[0][3] = 0;
	m[1][0] = 0;    m[1][1] = __fy; m[1][2] = __cy; m[1][3] = 0;
	m[2][0] = 0;	m[2][1] = 0;    m[2][2] = 1;    m[2][3] = 0;

}


  void VideoSource::calcProjectionMatrixFromCameraMatrix(){
	double fx = __cameraMatrix.at<double>(0,0);
	double fy = __cameraMatrix.at<double>(1,1);
	double cx = __cameraMatrix.at<double>(0,2);
	double cy = __cameraMatrix.at<double>(1,2);

	__fx = fx;
	__fy = fy;
	__cx = cx;
	__cy = cy;

	mars::Matrix16 m;

	GLfloat* data = m.getData();

	float near  = 0.1f;
	float far = 10000.0f;
	//float far = 100.0f;

	float w = (float) (Configuration::getConfiguration()->openGLScreenWidth() );
	float h = (float) (Configuration::getConfiguration()->openGLScreenHeight() );


	data[0]  = 2.0*fx/w;
	data[1]  = 0;
	data[2]  = 0;
	data[3]  = 0;

	data[4]  = 0;
	data[5]  = 2.0*fy/h;
	data[6]  = 0;
	data[7]  = 0;

	data[8]  = 2.0*(cx/w)-1.0;
	data[9]  = 2.0*(cy/h)-1.0;
	data[10] = -(far+near)/(far-near);
	data[11] = -1;

	data[12] = 0;
	data[13] = 0;
	data[14] = -2.0*far*near/(far-near);
	data[15] = 0;

}
