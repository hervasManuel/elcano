#include <mysql_connection.h>
#include <mysql_driver.h>
#include <cppconn/prepared_statement.h>
#include <ImageDAO.h>

ImageDAO::ImageDAO(){
	Broker = Agent::getAgent();
	Con = Broker->getConnection();
}

int ImageDAO::deleteImage(Image *imag){
	sql::PreparedStatement *stmt;
	int result = 0;
	stmt = Con->prepareStatement("DELETE FROM Image WHERE idImage = ?");
	stmt->setString(1, imag->getName());
	result= stmt->executeUpdate();
	stmt->close();
	delete stmt;

	return result;
}

int ImageDAO::deleteAll(){
	sql::PreparedStatement *stmt;
	int result= 0;
	stmt = Con->prepareStatement("DELETE FROM Image");
	result = stmt->executeUpdate();
	stmt->close();
	delete stmt;

	return result;
}

int ImageDAO::insertImage(Image *img){
	int result=0;
	sql::PreparedStatement *stmt;
	stmt = Con->prepareStatement("INSERT INTO Image(path, name, edgeHist, yCoeff, cbCoeff, crCoeff, scalableHist, sign) VALUES (?,?,?,?,?,?,?,?)");
	stmt->setString(1, img->getPath());
	stmt->setString(2, img->getName());
	stmt->setString(3, img->getStringEdgeHist());
	stmt->setString(4, img->getStringYCoeff());
	stmt->setString(5, img->getStringCbCoeff());
	stmt->setString(6, img->getStringCrCoeff());
	stmt->setString(7, img->getStringScalableHist());
	stmt->setString(8, img->getStringSign());


	result= stmt->executeUpdate();
	stmt->close();

	return result;
}

vector<Image*> ImageDAO::select(string condition){
	vector<Image*> images;
	sql::Statement *stmt;

	stmt = Con->createStatement();
	sql::ResultSet *res  = stmt->executeQuery(condition);
	while(res->next())
	{
		Image *aux= new Image(res->getInt("idImage"), res->getString("name"),res->getString("path"),
				res->getString("yCoeff"), res->getString("cbCoeff"), res->getString("crCoeff"),
				res->getString("scalableHist"), res->getString("sign"), res->getString("edgeHist"));
		images.push_back(aux);
	 }
	 res->close();
	 stmt->close();

	return images;
}

int ImageDAO::selectn(String condition){
	sql::Statement *stmt;
	int nelements= 0;
	stmt = Con->createStatement();
	sql::ResultSet *res  = stmt->executeQuery(condition);
	 while(res->next())
	 {
		 nelements= res->getInt(1);
	 }
	 res->close();
	 stmt->close();

	return nelements;
}

void ImageDAO::closeDAO(){
	Con->close();
}
