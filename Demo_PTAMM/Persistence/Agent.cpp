#include <Agent.h>

#include <mysql_connection.h>

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>


Agent* Agent::instance = NULL;

Agent::Agent(){

	sql::Driver *driver;

	driver = get_driver_instance();
	
	try{
		for(int i=0;i<100;i++){
			sql::Connection	*con;
			con = driver->connect(def_url, def_user_name, def_password);
			con->setSchema(def_schema);
			con->close();
			connections.push_back(con);
		}
	}
	catch(sql::SQLException &e){
	  std::cout << "Error initializing the data base" << std::endl;
	  std::cout << e.what() << std::endl;
	}
}

Agent* Agent::getAgent(){

	if(instance == NULL) {
		instance=new Agent();
	}
	return instance;
}

sql::Connection* Agent::getConnection(){
	int i=0;
	if(instance == NULL) {
		instance=new Agent();
	}

	while(!instance->connections.at(i)->isClosed()){
		i++;
	}

	sql::mysql::MySQL_Driver *driver;
	driver = sql::mysql::get_mysql_driver_instance();

	instance->connections.at(i)= driver->connect(def_url, def_user_name, def_password);
	instance->connections.at(i)->setSchema(def_schema);

	return instance->connections.at(i);
}
