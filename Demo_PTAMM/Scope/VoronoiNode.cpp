#include <VoronoiNode.h>
#include <string.h>

VoronoiNode::VoronoiNode(vector<Image*> im, vector<Image*> plane){
	P = NULL;
	c_r= 100;
	pi= im;
	images_PlaneP = plane;
}

VoronoiNode::VoronoiNode(vector<Image*> pi2, Image *mainImage, vector<Image*> plane){
	pi = pi2;
	P = mainImage;
	images_PlaneP= plane;
	//If there are elements on the plane, radio is initialized

	if(images_PlaneP.size()!=0)
		c_r = cobertor_radio(P, plane);
}

VoronoiNode::VoronoiNode(double father_c_r, Image *mainImage, vector<Image*> plane){
	P = mainImage;
	images_PlaneP= plane;
	//If the plane would be composed by 1 element, the cobertor radio is the same as the
	//father. In other case the cobertor radio respect the P node is searched.

	if(images_PlaneP.size()==1)
		c_r= father_c_r;
	else
		c_r= cobertor_radio(P, plane);
}

double VoronoiNode::cobertor_radio(Image *mainImage, vector<Image*> images){
	double max= -1.0;
	Comparator *c = new Comparator();
	for(int i=0; i<images.size(); i++){
		double diff=c->compareImages(mainImage, images.at(i));
		if(max<diff){
			max= diff;
		}
	}
	return max;
}

vector<Image*> VoronoiNode::getImagesPlaneP(){
	return images_PlaneP;
}

Image* VoronoiNode::getP(){
	return P;
}

void VoronoiNode::setP(Image* p){
	P = p;
}

vector<Image*> VoronoiNode::getPi(){
	return pi;
}

void VoronoiNode::setPi(vector<Image*> pi){
	this->pi = pi;
}

double VoronoiNode::getR_c(){
	return c_r;
}

void VoronoiNode::setR_c(double cR){
	c_r = cR;
}

String VoronoiNode::toString(){
	std::ostringstream msgStream;
	msgStream << "Main image: " << P->getName() << "\nCobertor Radio" << c_r << ";Num_elements: " << images_PlaneP.size() << "\nDi:\n";

	for(int i=0; i< images_PlaneP.size(); i++)
		msgStream << "Image(" << i << "):" << images_PlaneP.at(i)->getName() << "\n";

	msgStream << "Pi: \n";

	if(!pi.empty()){
		for(int i=0; i< pi.size(); i++)
			msgStream << "P(" << i << "):" << pi.at(i)->getName() << "\n";
	}
	else
		msgStream << "null";
	return msgStream.str();
}
