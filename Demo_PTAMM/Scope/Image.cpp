/*****************************************************************************************
 * Image.cpp
 *
 *
 * It represents an image. This image is represented by its Id, its name, its path and
 * its descriptors (ColorLayout, EdgeHistogram and ScalableColor).
 *
 * Date: 20/09/2011
 *
 *****************************************************************************************/

#include <Image.h>
#include <iostream>

/*Constructor with independence of the image descriptors. Descriptors builders are passed*/

using namespace std;


Image::Image(String name, String path){
	this->name = name;
	this->path = path;
	cv::Mat matFrame = cv::imread(path+name);
	cv::Mat matFrameGray =cv::imread(path+name,0);
	Frame *_frame = new Frame(matFrame);
	Frame *_frameGray = new Frame(matFrameGray,false,true,false);
	CLD = Feature::getColorLayoutD(_frame,6,3);
	EHD = Feature::getEdgeHistogramD(_frame);
	SCD = Feature::getScalableColorD(_frame ,true);

}

/*Constructor used in the data load from the data base. Id of the data base is used and the descriptors loaded automatically.*/

Image::Image(int id, String name, String path, String YCoeff, String CbCoeff, String CrCoeff, String ScalableHist, String Sign, String EdgeHist){
	idImage = id;
	this->name = name;
	this->path = path;
	CLD = new XM::ColorLayoutDescriptor();
	CLD->SetCbCoeff(getCcCoeffFromString(CbCoeff));
	CLD->SetCrCoeff(getCcCoeffFromString(CrCoeff));
	CLD->SetYCoeff(getYCoeffFromString(YCoeff));
	EHD = new XM::EdgeHistogramDescriptor();
	EHD->SetEdgeHistogramElement(getEdgeHistogramFromString(EdgeHist));
	SCD = new XM::ScalableColorDescriptor();
	SCD->SetScalableHistogram(getScalableHistFromString(ScalableHist), getScalableHistFromString(Sign), 256, 0);
}

int Image::getIdImage(){
	return idImage;
}

XM::ColorLayoutDescriptor Image::getCLD(){
	return *CLD;
}

XM::EdgeHistogramDescriptor Image::getEHD(){
	return *EHD;
}

String Image::getName(){
	return name;
}

String Image::getPath(){
	return path;
}

XM::ScalableColorDescriptor Image::getSCD(){
	return *SCD;
}

String Image::getStringEdgeHist(){
	String EdgeHist = "";
	std::ostringstream elements;

	for(int i=0; i < EHD->GetSize(); i++){
		elements << EHD->GetEdgeHistogramElement()[i];
	}

	EdgeHist = elements.str();

	return EdgeHist;
}

char* Image::getEdgeHistogramFromString(String EdgeHistogram){
	char *str = (char *)EdgeHistogram.c_str();
	return str;
}

String Image::getStringCbCoeff(){
	String CbCoeff = "";
	ostringstream elements;
	for(int i=0; i<CLD->GetNumberOfCCoeff(); i++)
		elements << CLD->GetCbCoeff()[i] << " ";

	CbCoeff = elements.str();

	return CbCoeff;
}

int* Image::getCcCoeffFromString(String CcCoeffString){
	int CcCoeffInt[3];
	int i=0;
	char *str = strdup(CcCoeffString.c_str());
	char *a = strtok(str," ");
	while (a != NULL)
	  {
		CcCoeffInt[i++] = atoi(a);
		a = strtok (NULL, " ");
	  }
	return CcCoeffInt;
}

String Image::getStringCrCoeff(){
	String CrCoeff = "";
	std::ostringstream elements;
	for(int i=0; i<CLD->GetNumberOfCCoeff(); i++)
		elements << CLD->GetCrCoeff()[i] << " ";

	CrCoeff = (String)elements.str();

	return CrCoeff;
}



String Image::getStringYCoeff(){
	String YCoeff = "";
	std::ostringstream elements;
	for(int i=0; i<CLD->GetNumberOfYCoeff(); i++)
		elements << CLD->GetYCoeff()[i] << " ";

	YCoeff = (String)elements.str();

	return YCoeff;
}

int* Image::getYCoeffFromString(String YCoeffString){
	int YCoeffInt[6];
	int i=0;
	char *str = strdup(YCoeffString.c_str());
	char *a = strtok(str," ");
	while (a != NULL)
	  {
		YCoeffInt[i++] = atoi(a);
		a = strtok (NULL, " ");
	  }
	return YCoeffInt;
}

String Image::getStringScalableHist(){
	String ScalableHist = "";
	std::ostringstream elements;
	for(int i=0; i<SCD->GetNumberOfCoefficients(); i++)
		elements << (int)SCD->GetCoefficient(i) << " ";

	ScalableHist = (String)elements.str();

	return ScalableHist;
}

String Image::getStringSign(){
	String Sign = "";
	std::ostringstream elements;
	for(int i=0; i<SCD->GetNumberOfCoefficients(); i++)
		elements << SCD->GetCoeffSign(i) << " ";

	Sign = (String)elements.str();

	return Sign;
}

unsigned long* Image::getScalableHistFromString(String ScalableHistString){
	unsigned long ScalableHistLong[512];
	int i=0;
	char *str = (char*) ScalableHistString.c_str();
	char *a = strtok(str," ");
	while (a != NULL)
	  {
		ScalableHistLong[i++] = (unsigned long)atoi(a);
		a = strtok (NULL, " ");
	  }
	return ScalableHistLong;
}




