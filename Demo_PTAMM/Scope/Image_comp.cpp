#include <Image_comp.h>

Image_comp::Image_comp(Image* img, float comp){
	this->img = img;
	this->comp = comp;
}

Image* Image_comp::getImage(){
	return img;
}

void Image_comp::setImage(Image* imgNew){
	img = imgNew;
}

float Image_comp::getComp(){
	return comp;
}

void Image_comp::setComp(float compNew){
	comp = compNew;
}

int Image_comp::compareTo(Image_comp* imgNew){

    if(comp == imgNew->getComp()) {
    	return 0;
    } else {
    	if(comp> imgNew->getComp())
    		return 1;
    	else
    		return -1;
    }
}
