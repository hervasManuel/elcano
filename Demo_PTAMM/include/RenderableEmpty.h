/*
 * RenderableEmpty.h
 *
 */

#ifndef RENDERABLEEMPTY_H_
#define RENDERABLEEMPTY_H_

#include "Renderable.h"
#include "Matrix16.h"
#include "World.h"
#include "FontFactory.h"

#include "RenderableText.h"

#include <GL/gl.h>


namespace mars {

/**
 * A classic empty object, with the three axis
 */
class RenderableEmpty: public mars::Renderable {
public:

	/**
	 * Default constructor
	 */
	RenderableEmpty();

	/**
	 * Destructor
	 */
	virtual ~RenderableEmpty();

	/**
	 * Draw the empty
	 */
	void draw();



private:

	/**
	 * Draws the axis
	 */
	void drawAxis();

	/**
	 * Draws the text
	 */
	void drawText();



};

}

#endif /* RENDERABLEEMPTY_H_ */
