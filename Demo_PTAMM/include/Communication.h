#ifndef COMMUNICATION_H_
#define COMMUNICATION_H_

#include <Ice/Ice.h>
#include <UserManager.h>
#include <string>
#include <PersistentProfile.h>
#include <MobileService.h>
#include <MobileDeviceI.h>
#include <pthread.h>
#include <IceGrid/IceGrid.h>

class Communication{
 private:
  Ice::CommunicatorPtr ic;
  Ice::ObjectPrx baseMD;
  Ice::ObjectPrx baseMS;
  Ice::ObjectPrx baseU;

  Ice::ObjectAdapterPtr adapter;
  Ice::ObjectPrx md;

  Elcano::MobileDevicePtr object;
  Elcano::DevProfilePtr ident;
  Elcano::MobileServicePrx MSPrx;
  Elcano::MobileDevicePrx MDPrx;
  Elcano::UserManagerPrx um;
  MobileDeviceI * dev;
  pthread_t __thread_id;
  bool UmOK;
  bool MsOk;
  bool MdOk;
  static void* exec(void* thr);
  void sirviente();
  static void* exec2(void* thr);
  void sirviente2();
  OpenLS::Route *_ruta;
 public:
   Communication(int argc, char **argv);
  ~Communication();
  TaskSeq getTasks();
  OpenLS::Route getRoute();
  void selectTask(std::string name);
  void login();
  void exit();
};

#endif
