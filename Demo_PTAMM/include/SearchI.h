/* -*- coding: utf-8; mode: c++ -*- */
#include <ASDF.h>
#include <Ice/Ice.h>
#include <IceStorm/IceStorm.h>
#include <MLP/Location.h>
#include <LocationAdminI.h>
#include <string>

class SearchI : virtual public ASD::Search {
 public:
  SearchI(const MLP::LocationAdminPrx& loc);
  void lookup(const ASD::ListenerPrx&, const std::string&, const PropertyService::Properties&, const Ice::Current&);
  void discover(const ASD::ListenerPrx&, const Ice::Current&);
private:
  MLP::LocationAdminPrx _loc;

};
