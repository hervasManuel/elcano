/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * TrackingMethodOAbsolutePhotoPos.h
 *
 */

#ifndef ABSOLUTE_PHOTOPOS_H_
#define ABSOLUTE_PHOTOPOS_H_

#include <iostream>
#include <vector>
#include "FramesBuffer.h"
#include "TrackingMethodOAbsolute.h"
#include "VideoDevice.h"
#include "PhotoPos.h"

using namespace std;

namespace mars{

/* This class implements an absolute tracking method based on ARToolKit perceptions*/

class TrackingMethodOAbsolutePhotoPos : public TrackingMethodOAbsolute {
public:
	/* Public methods */
	TrackingMethodOAbsolutePhotoPos(VideoSource* vS_0);
	virtual ~TrackingMethodOAbsolutePhotoPos();
	Elcano::pos getPosition();
	float getRotation();
	void loopThread();
	void updatePosition(float ARTKX, float ARTKY);


private:
	/* Private variables */
	VideoSource* __uCam;
	std::vector<int> __params;
	photoPos* __riki;
	Elcano::pos __posicion;
	float __rot;
	Elcano::pos __ARTKPosition;
};

}

#endif /* ABSOLUTE_PHOTOPOS_H_*/

