/*
 * RenderableLinePath.h
 *
 */

#ifndef RENDERABLELINEPATH_H_
#define RENDERABLELINEPATH_H_

#define SEPARACION_FLECHAS 0.5


#include "Renderable.h"
#include "Vector3D.h"
#include "Logger.h"
#include "World.h"

#include <vector>

namespace mars {

class RenderableLinePath: public mars::Renderable {
public:
	RenderableLinePath();

	/**
	 * Adds a point to the path.
	 */
	void addPoint(const Point3D& p);

	/**
	 * Reset the path. Erases all the points
	 */
	void reset();

	/**
	 * Draws the path.
	 */
	void draw();


	/**
	 * Sets the line color
	 * @param r Red
	 * @param g Green
	 * @param b Blue
	 * @param a Alpha
	 */
	void setColor(const float& r, const float& g, const float& b, const float& a);

	/**
	 * Sets the line color
	 * @param r Red
	 * @param g Green
	 * @param b Blue
	 * @param a Alpha
	 */
	void setColor(const float& r, const float& g, const float& b);


	/**
	 * Returns the current color
	 */
	float* getColor();

	/**
	 * Sets the alpha value
	 */
	void setAlpha(const float& a);

	/**
	 * Returns the current line width
	 */
	float getWidth();

	/**
	 * Sets the line width
	 */
	void setWidth(const float& w);


	/**
	 * Used by LUA. Adds a point
	 */
	void addPointLUA(const float& x, const float& y, const float& z);


	void updateUserPos(float ux, float uy, float uz);

	virtual ~RenderableLinePath();

private:

	void drawPOI(const Point3D& p);

	void drawLine(const Point3D& b, const Point3D& e, bool drawTraingle);

	std::vector<Point3D> __poi;

	typedef std::vector<Point3D>::iterator poi_it;

	float __color[4];

	float __width;

	bool __updateUserPos;
	Point3D __userPos;
};

}

#endif /* RENDERABLELINEPATH_H_ */
