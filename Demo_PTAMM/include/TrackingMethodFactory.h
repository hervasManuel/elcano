/*
 * TrackingMethodFactory.h
 *
 */

#ifndef TRACKINGMETHODFACTORY_H_
#define TRACKINGMETHODFACTORY_H_

#include "VideoSource.h"
#include "TrackingMethodOAbsoluteArtkBz.h"
#include "TrackingMethodOAbsolutePhotoPos.h"
//#include "TrackingMethodORelativePixelFlow.h"
#include "TrackingMethodOAbsolutePTAMM.h"
#include "TrackingController.h"
#include "TrackingMethod.h"
#include "Logger.h"
#include <map>


namespace mars {

enum trackingMethodType{
	ARTK,
	//PIXELFLOW,
	PTAMM,
	PHOTOPOS
};


class TrackingMethodFactory : public Singleton<TrackingMethodFactory>{
public:
	TrackingMethod* createTrackingMethod(const std::string& name,
	                                     trackingMethodType type,
	                                     VideoSource* vS1);

	virtual ~TrackingMethodFactory();
	std::map<std::string, TrackingMethod*> __tMethods;
	typedef std::map<std::string, TrackingMethod*>::iterator tmIt;

private:
	TrackingMethodFactory();

	friend class Singleton<TrackingMethodFactory>;



};

}

#endif /* TRACKINGMETHODFACTORY_H_ */
