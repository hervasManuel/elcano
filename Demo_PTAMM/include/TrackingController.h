/*
 * TrackingController.h
 *
 */

#ifndef TRACKINGCONTROLLER_H_
#define TRACKINGCONTROLLER_H_

#include <vector>

#include "TrackingMethodO.h"
#include "TrackingMethodOAbsolute.h"
#include "TrackingMethodORelative.h"
#include "ParticleFilter.h"
#include "Singleton.h"
#include "World.h"
#include "RenderableConstructor.h"
#include "Camera3D.h"

namespace mars {

/**
 * The controller of the tracking methods. It collects the tracking methods data, process it and updates
 * the camera.
 */
class TrackingController : public Singleton<TrackingController>{
public:

	/**
	 * Publishes a Relative method to the controller
	 *
	 * @param tMR Optical Relative Tracking Method
	 */
	void publishMethodRelative(TrackingMethodORelative* tMR);

	/**
	 * Publishes an Optical Absolute Tracking Method
	 *
	 * @param tMA Tracking Optical Absolute Method
	 */
	void publishMethodAbsolute(TrackingMethodOAbsolute* tMA);

	/**
	 * Sets the camera3D (eventually an openGL one) that will be modified by the results of the tracking
	 */
	void setCamera(Camera3D* camera);

	/**
	 * Returns the controller itself (it's a singleton)
	 */
	static TrackingController* getController();

	/**
	 * Returns the last computed tUserL
	 */
	tUserL getComputedUserL() const;

	/**
	 * TEMPORAL FIX, WILL BE REMOVED ASAP
	 */
	void tempFixDeleteMe();

private:

//	static TrackingController* __instance; ///< Instance, it's a sngleton

	TrackingController();
	virtual ~TrackingController();

	void computeAllMethods(); ///< Computes All methods
	void lock(); ///< Blocks the access to the data
	void unlock(); ///< Free the access to the data


	vector<TrackingMethodOAbsolute*> __methodsAbsolute; ///< Vector of absolute tracking methods
	vector<TrackingMethodORelative*> __methodsRelative; ///< vector of relative tracking methods

	tUserL __computedUserL; ///< Last computed tUserL (the user location and lookto ... and up)

	Camera3D* __camera; ///< The current 3D camera

	pthread_mutex_t mutex_userl; ///< Internal mutex

	ParticleFilter* __filter;
	//Filtro_Particulas* __filter;

	friend class Singleton<TrackingController>;



	//Demos variables
	int __ARTKWait;
	int __failsARTK;
};

}

#endif /* TRACKINGCONTROLLER_H_ */
