/*
 * RenderableWidgetLogger.h
 *
 */

#ifndef RENDERABLEWIDGETLOGGER_H_
#define RENDERABLEWIDGETLOGGER_H_

#include "RenderableWidgetConsole.h"
#include "Singleton.h"
#include "Logger.h"
#include "World.h"

namespace mars {

/**
 * This class holds the Logger a special console together.
 */
class RenderableWidgetLogger: public RenderableWidgetConsole, public Singleton<RenderableWidgetLogger> {

	/**
	 * INTERNAL, NOT TO BE USED
	 */
	static void aError(const std::string& m);

	/**
	 * INTERNAL, NOT TO BE USED
	 */
	static void aWarning(const std::string& m);

	/**
	 * INTERNAL, NOT TO BE USED
	 */
	static void aNote(const std::string& m);

private:

	RenderableWidgetLogger();
	virtual ~RenderableWidgetLogger();

	friend class Singleton<RenderableWidgetLogger>;
};

}

#endif /* RENDERABLEWIDGETLOGGER_H_ */
