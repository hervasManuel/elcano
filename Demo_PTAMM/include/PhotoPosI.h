#ifndef PHOTOPOSI_H_
#define PHOTOPOSI_H_

/* -*- coding: utf-8; mode: c++ -*- */
#include <Ice/Ice.h>
#include <Ice/Application.h>
#include <Ricardo.h>


class photoPosI : public Elcano::photoPos{
private:
  Elcano::pos _posicion;
  int _rotacion;
public:
  virtual void infPos(const Elcano::pos& position, Ice::Int rot, const Ice::Current&);
  Elcano::pos getPos();
  int getRot();
};

#endif
