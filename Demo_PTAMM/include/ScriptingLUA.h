/*
 * ScriptingLUA.h
 *
 */

#ifndef SCRIPTINGLUA_H_
#define SCRIPTINGLUA_H_

#include <string>
#include <map>

#include <luabind/luabind.hpp>

#include "Singleton.h"

#include "mars.h"


namespace mars {

class ScriptingLUA : public Singleton<ScriptingLUA> {

public:
	void executeScript(const std::string & file);
	void execLUAFile(const string& file);

private:
	ScriptingLUA();
	~ScriptingLUA();
	static void Init();

	friend class Singleton<ScriptingLUA>;

	lua_State* __luaState;

	std::map<std::string, lua_State* > __scripts;
};

}

#endif /* SCRIPTINGLUA_H_ */
