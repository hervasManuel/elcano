/*****************************************************************************************
 * Image.h
 * 
 *
 * It represents an image. This image is represented by its Id, its name, its path and
 * its descriptors (ColorLayout, EdgeHistogram and ScalableColor).
 *
 * Date: 20/09/2011
 * 
 *****************************************************************************************/
 
#include <Feature.h>
#include <Descriptors.h>
#include <string.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>
 
class Image {
 
 	private:
			int idImage;
 	 		string name;
 			string path;
 			XM::ColorLayoutDescriptor *CLD;
 			XM::EdgeHistogramDescriptor *EHD;
 			XM::ScalableColorDescriptor *SCD;
 			
	public:
	
			/*Constructor with independence of the image descriptors. Descriptors builders are passed*/
	 
	 		Image(String name, String path);
	 		
	 		/*Constructor used in the data load from the data base. Id of the data base is used and the descriptors loaded automatically.*/
	 
	 		Image(int id, String name, String path, String YCoeff, String CbCoeff, String CrCoeff, String ScalableHist, String Sign, String EdgeHist);
	 		
	 		int getIdImage();
	 		
	 		String getName();
	 		
	 		String getPath();
	 		
	 		XM::EdgeHistogramDescriptor getEHD();
	
			XM::ColorLayoutDescriptor getCLD();
			
			XM::ScalableColorDescriptor getSCD();
			
			String getStringEdgeHist();

			char* getEdgeHistogramFromString(String EdgeHistogram);

			String getStringYCoeff();

			int* getYCoeffFromString(String YCoeffString);

			String getStringCbCoeff();

			int* getCcCoeffFromString(String CbCoeffString);

			String getStringCrCoeff();

			String getStringScalableHist();

			unsigned long* getScalableHistFromString(String ScalableHistString);

			String getStringSign();

 };
 
 
