/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * TrackingMethodOAbsolute.h
 *
 */

#ifndef TRACKINGMETHODOABSOLUTE_H_
#define TRACKINGMETHODOABSOLUTE_H_

#include "TrackingMethod.h"
#include "TrackingMethodO.h"

#include <vector>
#include <iostream>

namespace mars {

/**
 * This is the base class for an Absolute Tracking Method.
 */
class TrackingMethodOAbsolute: public TrackingMethodO {
public:
	TrackingMethodOAbsolute();

	/**
	 * This constructor takes the number of perceptions to save.
	 *
	 * @param nRecords number of records to keep.
	 *
	 */
	TrackingMethodOAbsolute(const unsigned int& nRecords);


	virtual ~TrackingMethodOAbsolute();


};

}

#endif /* TRACKINGMETHODABSOLUTE_H_ */
