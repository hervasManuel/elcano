/*
 * PatternsDetectorArtk.h
 *
 */

#ifndef PATTERNSDETECTORARTK_H_
#define PATTERNSDETECTORARTK_H_

#include <AR/ar.h>
#include <AR/gsub.h>
#include <AR/param.h>
#include "ARTKMark.h"
#include "VideoDevice.h"
#include "Configuration.h"
#include "ARTKHistory.h"

namespace mars {

class PatternsDetectorArtk {
public:
  /*Public methods*/
  PatternsDetectorArtk();
  bool detect(cv::Mat* frame,vector<ARTKMark>* marks,ARTKHistory* hist);
    bool changeCamera(VideoSource* cam0);
    int detectMarkerHist( ARUint8 *dataPtr, int thresh, ARMarkerInfo **marker_info, int *marker_num,ARTKHistory* hist);

private:
  /* --ARToolKit variables-- */
  ARMarkerInfo *markerInfo;
  int markerNum;
  //std::vector<mars::ARTKMark> marks;

  /* --Configuration variables-- */
  int thres;
  float cf_min;

  
  /*Private methods*/

  void readConfiguration();
  int checkPatternVisibility(int perceptionId);


  /*Util - TO DEPRECATE*/
  float* Mat2Gl(cv::Mat* mat);
  cv::Mat* Gl2Mat(float *gl);

};
}



#endif /* PATTERNSDETECTORARTK_H_ */
