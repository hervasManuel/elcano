/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/* This is the base class for a VideoCapture */

/*
 * VideoCapture.h
 *
 */

#ifndef VIDEOCAPTURE_H_
#define VIDEOCAPTURE_H_

#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include "FramesBuffer.h"
#include "VideoSource.h"
#include "VideoDevice.h"
#include "VideoFile.h"

#ifdef WUEYE
#include <uEye.h>
#endif

using namespace std;

namespace mars{
/**
 * This is the base class for a VideoCapture.
 * A VideoCapture represents an entity able to hold VideoSources
 * and able to return frames of those VideoSources.
 *
 *
 * @see VideoSource
 */
class VideoCapture{
private:
	/**
	 * Adds a thread which captures frames from the VideoSource.
	 *
	 * @param vS the VideoSource used to get frames from.
	 */
	void addThread(VideoSource* vS);


public:
	/**
	 *  Default constructor.
	 */
	VideoCapture();



	virtual void addNewVideoSource(unsigned dev, const std::string& name)=0;
	virtual void addNewVideoSource(const string& path, const std::string& name)=0;



	/**
	 * Returns the frame number nFrame from vS video source.
	 *
	 * @param vS where to get the frame from.
	 * @param nFrame number of frame to return.
	 *
	 */
	cv::Mat* getFrame(VideoSource* vS, unsigned nFrame);


	/**
	 * Returns the last frame from vS video source.
	 *
	 * @param vS where to get the last frame from.
	 *
	 */
	cv::Mat* getLastFrame(VideoSource* vS);

	/**
	 * Returns the video device at the given position.
	 *
	 * @param position the video device.
	 */
	VideoDevice* getVideoDevice(unsigned videoDevicePosition);

	VideoDevice* getVideoDevice(std::string name);

	/**
	 * Returns the video file at the given position.
	 *
	 * @param position the video file.
	 */
	VideoFile* getVideoFile(unsigned videoFilePosition);

	VideoFile* getVideoFile(std::string name);

	string getName();

	void setDone();

	virtual ~VideoCapture();



protected:

	/**
	 * Adds a VideoDevice to this VideoCapture
	 *
	 * @param vD video device.
	 */
	virtual void addVideoSource_(VideoDevice* vD);


	/**
	* Adds a VideoFile to this VideoCapture
	*
	* @param vF video file.
	*/
	void addVideoSource_(VideoFile* vF);


	string __videoCaptureName; ///< The name of the VideoCapture e.g. OPENCV

	vector<VideoDevice*> __videoDeviceList; ///< The list of VideoDevices (custom VideoSource)
	vector<VideoFile*>   __videoFileList; ///< The list of VideoFiles (custom VideoSource)

	map<string, VideoDevice*> __videoDeviceMap; ///< The list of VideoDevices (custom VideoSource)
	map<string, VideoFile*>   __videoFileMap; ///< The list of VideoFiles (custom VideoSource)

	vector<int> __threads; ///< The list of threads associated to this VideoCapture.

	/**
	 *
	 * Used by threads. Not to be used by a coder.
	 *
	 */
	static void* exec(void *thr);



};


}


#endif /* VIDEOCAPTURE_H_ */
