#ifndef _LOCATIONLISTENERI_H_
#define _LOCATIONLISTENERI_H_

#include <Ice/Ice.h>
#include <MLP/Location.h>

class LocationListenerI : public MLP::LocationListener{
 private:
  MLP::PointPtr _pos;
 public:
  LocationListenerI();
  ~LocationListenerI();
  
  void locateReport(const MLP::Position& pos, const Ice::Current&) const;
  void locateSeqReport(const MLP::PositionSeq&, const Ice::Current&) const;
  void locateRangeReport(const MLP::PositionSeq&, const Ice::Current&) const;
  MLP::PointPtr getPos();
};
#endif
