// -*- mode:c++; coding:utf-8 -*-
#ifndef _LOCATIONADMINI_H_
#define _LOCATIONADMINI_H_

#include <MLP/Location.h>
#include <Ice/Ice.h>
#include <IceStorm/IceStorm.h>


class LocationAdminI : virtual public MLP::LocationAdmin{

 public:
  IceStorm::TopicPrx _topic;
  LocationAdminI(IceStorm::TopicPrx topic);
  void addListener(const MLP::LocationListenerPrx &listener, const Ice::Current&);
  void removeListener(const MLP::LocationListenerPrx &listener, const Ice::Current&);
  MLP::LocationDataInfo getLocationDataInfo(const Ice::Current&) const;

};

#endif
