/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * ARTKHistory.h
 *
 */

/* ARTKHistory */

#ifndef ARTKHISTORY_H_
#define ARTKHISTORY_H_

/* #include <iostream> */
/* #include <vector> */
/* #include "FramesBuffer.h" */
/* #include "TrackingMethodOAbsolute.h" */
/* #include "VideoDevice.h" */
/* #include "RenderableFactory.h" */
/* #include "ARTKMark.h" */
/* #include "PatternsDetectorArtk.h" */
#include <AR/ar.h>

using namespace std;

namespace mars{

class ARTKHistory{
public:
  ARMarkerInfo2          *marker_info2;
  ARMarkerInfo           *wmarker_info;
  int                    wmarker_num;  
  arPrevInfo             prev_info[AR_SQUARE_MAX];
  int                    prev_num;  

  ARTKHistory();

private:

};

}

#endif /* ARTKHISTORY_H_*/

