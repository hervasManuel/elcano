/*
 * SoundTool.h
 *
 */

#ifndef SoundTool_H_
#define SoundTool_H_

#include "Uncopyable.h"
#include "Logger.h"

#include <memory>
#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>

#include <festival/festival.h>


#include "Configuration.h"

#include <map>
#include <string>



namespace mars {

/**
 *  The class representing the audio tool
 */
class SoundTool  : public Singleton<SoundTool> {
public:
	SoundTool();
	virtual ~SoundTool();

	/**
	 * Plays a SDL Mix_Music sound
	 */
	static void playSound(Mix_Music* m);

	/**
	 * Creates a SDL Mix_Music from a sound file
	 * @param name name of the sound file
	 */
	Mix_Music* getSound(const std::string& name);

	/**
	 * Renders a wave file from a string using Festival
	 * @param name name of the wave file that will be created
	 * @param text text to be said
	 */
	static void createWAVEfromText(std::string name, std::string text, bool wfs=false);

	/**
	 * Is the audio system available on this computer?
	 */
	bool audioAvailable();

	/**
	 * Used for playing a wave file from LUA
	 */
	void playSoundLUA(std::string s);

	/**
	 * Used to render and play a text from LUA
	 */
	void playTextLUA(std::string s);

private:
	SDL_AudioSpec __audioFormat;
	bool __audioAvailable;

	std::map<string, Mix_Music*> __sounds;

	friend class Singleton<SoundTool>;

};

}

#endif /* SoundTool_H_ */
