/* -*- coding: utf-8; mode: c++ -*- */

#ifndef _ANUNCPOS_H_
#define _ANUNCPOS_H_

#include <ASDF.h>
#include <SearchI.h>
#include <pthread.h>

class anuncPos
{
private:
  Ice::CommunicatorPtr ic;
  Ice::ObjectPrx obj;
  IceStorm::TopicManagerPrx topicManager;
  IceStorm::TopicPrx topic;
  IceStorm::TopicPrx ASDA;
  IceStorm::TopicPrx ASDS;
  IceStorm::QoS qos;
  Ice::ObjectAdapterPtr adapter;
  Ice::ObjectAdapterPtr adapterSearch;
  pthread_t __thread_id;
  bool conectado;
  MLP::LocationListenerPrx publicar;

  static void* exec(void* thr);
  void sirviente();
public:
  anuncPos(int argc, char **argv);
  virtual ~anuncPos();
  void publicarPos(float x, float y, float z);
};

#endif
