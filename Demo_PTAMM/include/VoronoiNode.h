/*
 * VoronoiNode.h
 *
 */

#ifndef VORONOINODE_H_
#define VORONOINODE_H_

#include <opencv/highgui.h>
#include <Comparator.h>

class VoronoiNode {

	private:
		vector<Image*> pi;
		vector<Image*> images_PlaneP;
		Image *P;
		double c_r;


	public:

		/**
		 * Constructor that uses an image array that represent the splits, and a plane or set of points
		 * that are in the same voronoi plane, that is, the points that are in a c_r distance.
		 *
		 */

		VoronoiNode(vector<Image*> im, vector<Image*> plane);

		/**
		 * Constructor that uses an image array that represent the splits, and a plane or set of points
		 * that are in the same Voronoi plane, that is, the points that are in a c_r distance.
		 * Furthermore initializes the center of the plane.
		 *
		 */
		VoronoiNode(vector<Image*> pi2, Image *mainImage, vector<Image*> plane);

		/**
		 * Constructor that initializes the center of the plane. Is used in leaf nodes. For this reason
		 * pi is null.
		 *
		 */
		VoronoiNode(double father_c_r, Image *mainImage, vector<Image*> plane);

		/**
		 * Method that calculates the cobertor radio of a set of images respect of a point,
		 * that is, the maximum distance between the images of the array to the center.
		 *
		 */

		double cobertor_radio(Image *mainImage, vector<Image*> images);

		vector<Image*> getImagesPlaneP();

		Image* getP();

		void setP(Image* p);

		vector<Image*> getPi();

		void setPi(vector<Image*> pi);

		double getR_c();

		void setR_c(double cR);

		/**
		 * It returns a description of the object in text mode composed by
		 * the description of all its components.
		 */
		String toString();

};


#endif /* VORONOINODE_H_ */
