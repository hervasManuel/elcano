/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/


/*
 * World.h
 *
 */

#ifndef WORLD_H_
#define WORLD_H_

#include "Renderable.h"
#include "RenderableModel.h"
#include "RenderableWidget.h"

#include "Light.h"
#include "Texture.h"
#include "VideoSource.h"
#include "Camera3D.h"
#include "Logger.h"
#include "FontFactory.h"
#include "Singleton.h"

#include "Tools.h"

#include <SDL/SDL.h>
#include <SDL/SDL_opengl.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include <opencv/cv.h>


#include <vector>


namespace mars {

class World : public Singleton<World> {

public:

//	static World* getInstance();

	/**
	* This adds a renderable object to our world.
	* The world is responsible for drawing all the objects.
	*
	* @param r pointer (reference) to a renderable.
	*
	* @see Renderable
	* @see RenderableModel
	*/
	void addRenderable(Renderable* r);

	/**
	 * This adds a light object to our world.
	 *
	 *
	 * @param r pointer (reference) to a Light.
	 *
	 * @see Light
	 */
	void addLight(Light* l);


	/**
	 * Enables the lighting
	 */
	void enableLighting();

	/**
	 * Disables the lighting
	 */
	void disableLighting();

	/**
	 * Is lighting enabled?
	 */
	bool isLightingEnabled();

	/**
	 * Adds a new virtual camera
	 */
	void addCamera(Camera3D* camera);

	/**
	 * Adds a real camera
	 */
	void addCamera(VideoSource* vs);

	/**
	 * Select a camera by its order of addition
	 */
	void selectCamera(unsigned n);

	/**
	 * Adds a widget
	 */
	void addWidget(RenderableWidget* w);

	/**
	 * Sets a console widget as the main debugging console
	 */
	void setConsole(RenderableWidget* w);

	/**
	 * DEPRECATED, functional but should not be used. Use step instead
	 */
	void worldLoop();

	/**
	 * Steps the world. Renders one frame
	 */
	bool step();

	/**
	 * Sets the videosource from where the backgournd frames will be taken
	 */
	void setBackgroundSource(VideoSource* vS);

	/**
	 * Returns the selected virtual camera
	 */
	Camera3D* getSelectedCamera();


	/*
	 * Should the background frame be resized?
	 */
	void setFrameResizing(bool b);

	// FPS related
	/**
	 * Enable or disabel the FPS calculation
	 */
	void   showFPS(bool b) { __showFPS = b; }

	/**
	 * Returns the current number of frames per second
	 */
	double getFPS() { return __fps; }

private:

	friend class Singleton<World>;

	World();
	virtual ~World();

	RenderableWidget* __console;

	/**
	 *
	 * Reserves n Renderable slots so reallocation will not be necessary.
	 *
	 * @param n slots reserved
	 */
	void reserveRenderableSlots(unsigned nSlots);
	void reserveLightSlots(unsigned nSlots);
	void drawBackground();
	void draw();
	void updateLights();
	void setBackgroundImage(cv::Mat* image);
	bool eventsCall();


	void setUpProjection();

	Camera3D* __defaultCamera;

	std::vector<Renderable*> __renderables;

	std::vector<RenderableWidget*> __widgets;

	std::vector<Light*>    __lights;


	bool __lighting;

	bool __done	;

	cv::Mat __backgroundImage;

	VideoSource* __backgroundVS;

	std::vector<Camera3D*> __cameras;

	unsigned __selectedCamera;

	bool __allWire;
	bool __allWireTex;
	bool __allWireTrans;
	bool __allWireXor;
	bool __allWireAnimated;
	bool __allWireBackEnabled;

	bool __resizeFrames;

	FontFactory* __ff;

	FTFont* __dFont;

	int __width;
	int __height;

//	Texture* __buttonTexture;

	// FPS ...
	unsigned __nSteps;
	unsigned __t0;
	float __fps;
	bool __showFPS;

	bool __bgSet;

};


}

#endif /* WORLD_H_ */
