/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * Renderable.h
 *
 */

#ifndef RENDERABLE_H_
#define RENDERABLE_H_

#include <string>
#include <iostream>
#include <vector>

#include <GL/gl.h>

#include "Vector3D.h"
#include "Camera3D.h"
#include "Logger.h"

#include <pthread.h>
#include <semaphore.h>

using namespace std;

namespace mars {

enum rtype {r2D, r3D};


/**
 * This class represents a Renderable object
 *
 */
class Renderable {
public:
	Renderable();

	/**
	 * draw interface ... this must be implemented by the offspring
	 */
	virtual void draw()=0;

	/**
	 * Destructor
	 */
	virtual ~Renderable();

	/**
	 * Draws a normal
	 */
	static void drawNormal(GLfloat* beginPoint, GLfloat* endPoint); //FIXME FIX! :)

	/**
	 * Sets the position
	 *
	 * @param x x component
	 * @param y y component
	 * @param z z component
	 */
	void setPosition(const float& x, const float& y, const float& z);
	void setPosition(const int& x, const int& y);



	/**
	 * Sets the initials angles
	 *
	 * @param x x component
	 * @param y y component
	 * @param z z component
	 */
	void setAngles(const float& x, const float&yx, const float& z);

	/**
	 * Sets the render mode of the renderable:
	 *
	 * @param mode rendering mode
	 * 0 - Normal mode
	 * 1 - WireFrame not culled mode.
	 * 2 - WireFrame culled mode.
	 *
	 */
	void setRenderMode(unsigned mode);


	/**
	 * Returns a pointer to 3 floats (Position x,y,z)
	 */
	Point3D getPosition();

	float getScaleX();

	/**
	 * Returns a pointer to 3 floats (Angles x,y,z)
	 */
	float* getAngles();

	/**
	 * Return true if it's a 3d renderable
	 */
	virtual bool is3D();

	/**
	 * Do we want to use a Matrix for the transformation?
	 *
	 * @param b true or false
	 */
	void useMatrix(bool b);

	/**
	 * Set the ModelView matrix to be used if useMatrix is true.
	 *
	 * @param m the ModelView Matrix to be used
	 * @see Matix16
	 */
	void setMatrix(const Matrix16& m);

	/**
	 * Returns the internal matrix
	 *
	 * @see Matrix16
	 */
	Matrix16 getMatrix() const;

	/**
	 * Add a rotation in X angle.
	 *
	 * @param angle angle in radians to be added.
	 */
	void addRotationX(float angle);

	/**
	 * Add a rotation in Y angle.
	 *
	 * @param angle angle in radians to be added.
	 */
	void addRotationY(float angle);


	/**
	 * Add a rotation in Z angle.
	 *
	 * @param angle angle in radians to be added.
	 */
	void addRotationZ(float angle);


	/**
	 * Increments x position
	 * @param xInc the increment
	 */
	void addX(float xInc);

	/**
	 * Increments y position
	 * @param yInc the increment
	 */
	void addY(float yInc);
	/**
	 * Increments z position
	 * @param zInc the increment
	 */
	void addZ(float zInc);

	/**
	 * Is this renderable visible?
	 * Returns a boolean answering this question.
	 */
	bool isShown();

	/**
	 * Sets this renderable as visible
	 */
	void show();

	/**
	 * Hides this renderable
	 */
	void hide();

	/**
	 * Sets the renderable scale.
	 *
	 * @param x x-scale
	 * @param y y-scale
	 * @param z z-scale
	 *
	 */
	void setScale(const float& x, const float& y, const float& z);

	void actAsLight(bool b, unsigned n = 0);

	cv::Mat getRelativePosition();

protected:


	void lock();
	void unlock();

	virtual void computePositionRotation();

	string __name; ///< Name

	Point3D __position; ///< Position
	float       __rotX; ///< RotationX
	float       __rotY; ///< RotationY
	float       __rotZ; ///< RotationZ

	unsigned __type; ///< Object type

	bool __wire; ///< Draw as wireframe
	bool __cull; ///< Culling?

	bool __wireXor; ///< Use a xor while wire?
	bool __wireTextured; ///< Use texture while wire?

	GLfloat __wireColor[4]; ///< Color of the wire
	GLfloat __wireFillColor[4]; ///< Color of the fill of the wire

	GLfloat __wireNormalWidth; ///< Normal width of the wire
	GLfloat __wireMaxWidth; ///< Maximum width of the wire
	GLfloat __wireMinWidth; ///< Minimum width of the wire

	bool __wireAnimated; ///< Is wire animated?

	bool __allTransparentWhileWire; ///< Totally transparent?

	unsigned __wireAnimStep; ///< The current animation sted of the wire
	unsigned __wireAnimLastTime; ///< DEPRECATED

	bool __wireBackEnabled; ///< Wire back lines enabled?

	bool __useMatrix; ///< Use Matrix?
	Matrix16 __internalMat; ///< our Matrix...

	bool __shown;

	float __scaleX, __scaleY, __scaleZ;

	bool __actAslight;
	unsigned __nLight;

	pthread_mutex_t mutex_render;
};

}

#endif /* RENDERABLE_H_ */
