/*
 * RenderableText.h
 *
 */

#ifndef RENDERABLETEXT_H_
#define RENDERABLETEXT_H_

#include "Renderable.h"
#include "FontFactory.h"
#include "Matrix16.h"

#include "Logger.h"

#include "World.h"

namespace mars {

/**
 * This class represents a renderable text object.
 */
class RenderableText: public Renderable {
public:



	/**
	 * Sets the text
	 *
	 * @param str the text , e.g. (char*) "text"
	 */
	void setText(char* str);

	/**
	 * Sets the text
	 *
	 * @param str the text e.g. "text"
	 */
	void setText(std::string str);

	/**
	 * Sets the facesize
	 *
	 * @param s face size
	 */
	void setSize(unsigned s);

	/**
	 * Returns the text as a string
	 */
	std::string getText();

	/**
	 * Draw the text
	 */
	void draw();

	/**
	 * Sets the render mode
	 *
	 * @param mode render mode:
	 *
	 */ //TODO complete this
	//void setRenderMode(unsigned int mode);

	/**
	 * Sets the color of the text
	 *
	 * @param r red component
	 * @param g green component
	 * @param b blue component
	 */
	void setColor(GLfloat r, GLfloat g, GLfloat b);


	/**
	 *
	 * Returns a point that contains the text bounds
	 *
	 */
	float* getBounds();

	/**
	 * Returns the text center (position) as a 3 float vector
	 *
	 */
	float* getCenter();


private:

	/**
	 * Default constructor (fontType is POLYGON by default)
	 */
	RenderableText(FontType fType = POLYGON);

	/**
	 * Another constructor
	 *
	 * @param font the fontname, e.g. "LiberationSans-Regular.ttf"
	 * @param str the text , e.g. (char*) "text"
	 * @param fontType (POLYGON | EXTRUDED | BUTTON) defaults to POLYGON.
	 */
	RenderableText(std::string font, char* str, FontType fType = POLYGON);

	virtual ~RenderableText();

	void animateWire();

	/**
	 * Draws a bounding box around the text
	 */
	void drawBBox();

	/**
	 * Updates the text properties
	 */
	void updateText();

//	unsigned __textMode; ///< Mode

	std::string __text; ///< Text itself
	FTFont* __font; ///< Font being used

	unsigned __fontType; ///< Type

	float cx; ///< CenterX
	float cy; ///< CenterY
	float cz; ///< CenterZ


	float x1; ///< bbox x1
	float y1; ///< bbox y1
	float z1; ///< bbox z1
	float x2; ///< bbox x2
	float y2; ///< bbox y2
	float z2; ///< bbox z2

	GLfloat __color[3]; ///< Text color

	friend class RenderableConstructor;
	friend class RenderableWidgetButton;
};

}

#endif /* RENDERABLETEXT_H_ */
