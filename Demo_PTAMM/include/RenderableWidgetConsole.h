/*
 * RenderableWidgetConsole.h
 *
 */

#define FONT_SIZE 16

#ifndef RENDERABLEWIDGETCONSOLE_H_
#define RENDERABLEWIDGETCONSOLE_H_


#include <list>
#include <string>

#include "VideoOutputSDLOpenGL.h"
#include "RenderableWidget.h"
//#include "FontFactory.h"
#include "World.h"
#include "Logger.h"


#define TEXT_ALPHA 1.0  // Value between 0.0 and 1.0

namespace mars {

/**
 * Possible message colors
 */
enum msgColor {RED, GREEN, YELLOW, WHITE};

/**
 * This class represents a console's line
 */
class Line{
public:



	/**
	 * Returns the line's text
	 */
	std::string getText() const {
		return text;
	}

	/**
	 * Sets the color from the available possibilities
	 * @see msgColor
	 */
	void setColor(msgColor color){
		float r, g, b;

		switch (color){
		case RED:
			r = 1.0; g = 0.0; b = 0.0;
			break;

		case GREEN:
			r = 0.0; g = 1.0; b = 0.0;
			break;

		case YELLOW:
			r = 1.0; g = 1.0; b = 0.0;
			break;

		case WHITE:

		default:
			r = 1.0; g = 1.0; b = 1.0;
		}

		this->color[0] = r;
		this->color[1] = g;
		this->color[2] = b;
		this->color[3] = TEXT_ALPHA;
	}

	/**
	 * Returns the current color as an RGBA array
	 */
	const float* getColor() const {
		return color;
	}

	/**
	 * Sets the text of the line
	 */
	void setText(const std::string& text){
		this->text = text;
	}

private:
	std::string text;
	float color[4];
};


/**
 * This class represents a 2-D text console
 */
class RenderableWidgetConsole: public mars::RenderableWidget {
public:

	virtual ~RenderableWidgetConsole();
	RenderableWidgetConsole();
	RenderableWidgetConsole(const unsigned & w , const unsigned & h, unsigned nLines = 10 );


	/**
	 * Draws it
	 */
	void draw();

	/**
	 * Adds a note message
	 */
	void addNote(std::string msg);

	/**
	 * Adds a warning message
	 */
	void addWarning(std::string msg);

	/**
	 * Adds an error message
	 */
	void addError(std::string msg);

private:

	void handleMouseMotion(int x, int y) {}
	void handleMouseClick(int button, int x, int y){}
	void handleMouseRelease(int button, int x, int y){}

	void init();

	void drawBackground();

	void drawLines();

	void drawSingleLine(Line theLine, unsigned i);

	unsigned __nLines;
	unsigned __nVisibleLines;

	std::list<Line> __textLines;

	float    __lineSeparation;
	unsigned __xMargin;
	unsigned __yMargin;
	unsigned __fontSize;

	float __fontH; // font's height

	FTFont* __font;


	friend class RenderableConstructor;
	friend class RenderableWidgetLogger;
};

}

#endif /* RENDERABLEWIDGETCONSOLE_H_ */
