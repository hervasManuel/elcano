/*
 * Quaternion.h
 *
 */


#ifndef QUATERNION_H_
#define QUATERNION_H_

#include <GL/gl.h>
#include "Matrix16.h"

#include "TUserL.h"

#include <math.h>

namespace mars {

/**
 * This class represents a Quaternion
 */
class Quaternion {
public:
	/**
	 * Default constructor
	 */
	Quaternion();

	/**
	 * Constructs a quaternion with 4 floats
	 *
	 * @param _w w component
	 * @param _y y component
	 * @param _z z component
	 */
	Quaternion(GLfloat _w, GLfloat _x, GLfloat _y, GLfloat _z);

	/**
	 * Constructs a quaternion from a Matrix16
	 *
	 * @param m a Matrix16 reference
	 */
	Quaternion(Matrix16&  m);

	Quaternion(const tUserL& t);

	/**
	 * Fills a Matrix16 based on the quaternion
	 *
	 * @param mat The Matrix16 were we'll hold the quaternion data
	 */
	void toMatrix16(Matrix16& mat);

	/**
	 * Multiply a quaternion
	 */
	Quaternion operator*(const Quaternion& r);


	/**
	 * Destructor
	 */
	virtual ~Quaternion();


	GLfloat w; ///< w component
	GLfloat x; ///< x component
	GLfloat y; ///< y component
	GLfloat z; ///< z component

};

}

#endif /* QUATERNION_H_ */
