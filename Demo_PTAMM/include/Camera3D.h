/*
 * Camera3D.h
 *
 */

#ifndef CAMERA3D_H_
#define CAMERA3D_H_

#include <string>
#include <GL/gl.h>
#include <GL/glu.h>
#include <iostream>

#include "Vector3D.h"
#include "Matrix16.h"

namespace mars {

/**
 * This Class represents a virtual camera.
 */
class Camera3D {
public:

	/***
	 * Default Constructor
	 *
	 * @param name Name of the camera
	 */
	Camera3D(std::string name="");

	/**
	 * Constructor that accepts a projection matrix and a camera name
	 *
	 * @param m Projection Matrix
	 * @param name Camera name
	 */
	Camera3D(Matrix16 m, std::string name="");

	/**
	 * Sets the eye position (i.e. the center of the camera)
	 *
	 * @param x x coord
	 * @param y y coord
	 * @param z z coord
	 */
	void setEye(const float& x, const float& y, const float& z);


	/**
	 * Sets the LookTo vector (i.e where is looking the camera to)
	 *
	 * @param x x coord
	 * @param y y coord
	 * @param z z coord
	 */
	void setLookTo(const float& x, const float& y, const float& z);


	/**
	 * Sets the up vector (i.e. the "rotation" around LookTo vector)
	 *
	 * @param x x coord
	 * @param y y coord
	 * @param z z coord
	 */
	void setUp(const float& x, const float& y, const float& z);

	/**
	 * Returns the up vector
	 */
	Vector3D getUp() const;

	/**
	 * Sets the camera name
	 *
	 * @param name new Camera's name
	 *
	 */
	void setName(std::string name);

	/**
	 * Returns the camera's name
	 */
	std::string getName();

	/**
	 * Executes the openGL gluLookAt(...) within this camera data.
	 */
	void use();

	/**
	 * Returns the eye of the camera.
	 */
	Vector3D getEye() const;

	/**
	 * Returns the LookTo
	 */
	Vector3D getLookTo() const;

	/**
	 * Default destructor
	 */
	virtual ~Camera3D();


	/**
	 * Sets the projection matrix of this camera
	 *
	 * @param m Matrix16
	 */
	void setProjectionMatrix(Matrix16 m);

	/**
	 * Sets the projection matrix of this camera
	 *
	 * @param m Pointer to 16 GLfloats.
	 */
	void setProjectionMatrix(GLfloat* m);

	/**
	 * Returns the projection matrix associated to this camera, as a Matrix16
	 */
	Matrix16 getProjectionMatrix();

private:

	unsigned __id; ///< Internal id

	Vector3D __eye; ///< Eye Point
	Vector3D __lookTo; ///< LookTo Vector
	Vector3D __up; ///< Up Vector

	std::string __name; ///< Name

	Matrix16 __projectionMatrix; ///< The projection matrix associated to this camera

};

}

#endif /* CAMERA3D_H_ */
