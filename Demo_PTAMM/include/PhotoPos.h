/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * PhotoPos.h
 *
 */




#ifndef PHOTOPOS_H_
#define PHOTOPOS_H_

#include <pthread.h>
#include <Indexer_controler.h>
#include <iostream>
#include <iomanip>
#include <fstream>
/* #include <Program_parameters.h> */
#include <time.h>
#include <Feature.h>


namespace Elcano{
  typedef struct{
    float x;
    float y;
    float z;
  }pos;
}

namespace mars{

class photoPos{
 private:

  int divide(std::vector<Image_comp*> _imgComp, int begin, int end);
  void quicksort(std::vector<Image_comp*> _imgComp, int begin, int end);
  int rotationToInt(std::string orientation);
  void searchPos(std::string name, int* rot, Elcano::pos* position);
  Image* obtainImage(std::string filePath, std::string fileName);
  Image_comp* search(std::string imagePath, std::string imageName);

  //Thread execution
  static void* exec(void* thr);
  
  string pathPhotoData; // = "/home/fran/fotos/coordenadasFotos.txt";
  VoronoiTree *tree;
  Elcano::pos position;
  int rotation;
  void execute();
  Indexer_controler *iC;
  pthread_t __thread_id;
  Image_comp* searched_img;

 public: 
  photoPos();
  ~photoPos();
  Elcano::pos getPos();
  int getRot();
  
};

}

#endif
