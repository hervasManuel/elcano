/*
 * PolyMesh.h
 *
 */

#ifndef POLYMESH_H_
#define POLYMESH_H_

#include "Mesh.h"

namespace mars {

/**
 * Represents a POLY mesh (it's gonna be DEPRECATED asap).
 */
class PolyMesh: public Mesh {
public:
	PolyMesh();
	virtual ~PolyMesh();
};

}

#endif /* POLYMESH_H_ */
