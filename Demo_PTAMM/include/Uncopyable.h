/*
 * Singleton.h
 *
 */

/* Singleton Template .. credits go to http://www.gamedev.net/community/forums/topic.asp?topic_id=301651 */

#ifndef UNCOPYABLE_H_
#define UNCOPYABLE_H_

#include <iostream>

namespace mars {


class uncopyable{ // Idea from Effictive C++ by Scott Meyers
protected:
	uncopyable() {}
	~uncopyable() {}
private:
	uncopyable(const uncopyable &);
	uncopyable & operator=(const uncopyable &);
};

}

#endif
