#include "ARTKMark.h"
#include "TrackingMethodOAbsolutePTAMM.h"

#define ADD (float)0.01
#define ROT (float)0.4

#define ROTATE 0
#define DISPLACE 1

class Fine{
 public:
  static bool _axisX;
  static bool _axisY;
  static bool _axisZ;
  static int _operation;
  static float _posX;
  static float _posY;
  static float _posZ;
  static float _rotX;
  static float _rotY;
  static float _rotZ;
  static mars::TrackingMethodOAbsolutePTAMM* _tM;
  static mars::Renderable* _r1;
  static mars::Renderable* _r2;

  static void updateRenderable();


  Fine(mars::TrackingMethodOAbsolutePTAMM* t, mars::Renderable* r1,mars::Renderable* r2);
  ~Fine();
  static void rotateBut(void* arg);
  static void dispBut(void* arg);
  static void actX(void* arg);
  static void actY(void* arg);
  static void actZ(void* arg);
  static void increment(void* arg);
  static void decrement(void* arg);
  static void incrementPlus(void* arg);
  static void decrementMinus(void* arg);
};
