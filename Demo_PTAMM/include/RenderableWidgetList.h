/*
 * RenderableWidgetList.h
 *
 */

#ifndef RENDERABLEWIDGETLIST_H_
#define RENDERABLEWIDGETLIST_H_

class RenderableWidgetButton;

#include <map>
#include <iostream>
#include <string>
#include <vector>

#include "RenderableWidget.h"
#include "RenderableWidgetButton.h"

#include "Tools.h"

namespace mars {

class RenderableWidgetList: public mars::RenderableWidget {
public:


	/**
	 * Adds a new action to the list
	 * @param text test to be shown in this action button
	 * @wFunc a WidgetFunction
	 *
	 * @see WidgetFunction
	 */
	void addAction(std::string text, const WidgetFunction& wFunc);


	/**
	 * Adds an action that will be executed as a LUA function
	 * @param text test to be shown in this action button
	 * @param func name of the LUA function to be called
	 *
	 */
	void addActionLUA(std::string text, std::string func);

	// Overwriting because of special needs ...
	void setPosition(const int& x, const int& y);

	/**
	 * Draws it
	 */
	void draw();

	/**
	 * Clear the options list
	 */
	void clearList();


	void setTextSize(uint size);

private:

	explicit RenderableWidgetList(const unsigned& w=200, const unsigned& h=200,
			const float& widthPortionUpDown = 0.2, const unsigned& nVisibleOptions = 4);

	virtual ~RenderableWidgetList();



	static void goUp(void* rWL);
	static void goDown(void* rWL);

	void updateOptionButtons();



	void calculateVisibility(RenderableWidgetButton* rB, uint lPos);
	void calculatePosition(RenderableWidgetButton* rB, uint wPos);

	void handleMouseMotion(int x, int y) {}
	void handleMouseClick(int button, int x, int y);
	void handleMouseRelease(int button, int x, int y);


	typedef vector<RenderableWidgetButton*>::iterator v_it;
	vector<RenderableWidgetButton*> __optionButtons;

	RenderableWidgetButton* __upButton;
	RenderableWidgetButton* __downButton;

	unsigned __w2; // 0.5 * widget's width
	unsigned __h2; // 0.5 * widget's height

	unsigned __navButtonWidth;
	unsigned __optionButtonHeight;
	unsigned __optionButtonWidth;

	unsigned __listPos; // Position List

	unsigned __nVisibleOptions;

	float __widthPortionUpDown;

	unsigned __totalOptions;

	vector<WidgetFunction> __actions;

	friend class RenderableConstructor;
};

}

#endif /* RENDERABLEWIDGETLIST_H_ */
