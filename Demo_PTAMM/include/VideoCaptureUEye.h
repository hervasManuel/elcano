/*
 * VideoCaptureUEye.h
 *
 */

#ifndef VIDEOCAPTUREUEYE_H_
#define VIDEOCAPTUREUEYE_H_

#include "VideoCapture.h"
#include "VideoDeviceUEye.h"

#include "uEye.h"

namespace mars {

class VideoCaptureUEye: public VideoCapture {
public:
	VideoCaptureUEye();
	virtual ~VideoCaptureUEye();

	/**
	 * Add a new uEye video source, based an a UEYE_CAMERA_INFO
	 * @param name nameof the video source
	 */
	void addNewVideoSource(UEYE_CAMERA_INFO cameraInfo, std::string name);

	/**
	 * not implemented
	 */
	void addNewVideoSource(unsigned int dev, const std::string& name){
		Logger::getInstance()->error("VideoCaputreUEye:: This video capture has not "
				"a real (unsigned dev, std::string name) constructor");
	}

	/**
	 * not implemented
	 */
	void addNewVideoSource(const std::string&, const std::string&){
		Logger::getInstance()->error("VideoCaputreUEye:: This video capture has not "
						"videofiles support");
	}


};

}

#endif /* VIDEOCAPTUREUEYE_H_ */
