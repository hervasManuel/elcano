/*
 * TextureFactory.h
 *
 */

#ifndef TEXTUREFACTORY_H_
#define TEXTUREFACTORY_H_

#include "Texture.h"
#include "Singleton.h"
#include "Logger.h"

namespace mars {

class TextureFactory : public Singleton<TextureFactory> {
public:
	const GLuint getGLTexture(const string& name);
	Texture* getTexture(const string& name);

private:
	TextureFactory();
	virtual ~TextureFactory();

	map<const string, Texture*> __textures;

	friend class Singleton<TextureFactory>;
};

}

#endif /* TEXTUREFACTORY_H_ */
