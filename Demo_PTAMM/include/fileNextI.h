#ifndef FILENEXTI_H_
#define FILENEXTI_H_
#include <Ice/Application.h>
#include <FileTransfer.h>

using namespace std; 
using namespace Ice;
using namespace FileTransfer;

class FileI
  : public File
{
public:
  
  FileI(FILE* fp, int num) :
    _fp(fp),
    _num(num),
    _bytes(new Ice::Byte[num])
  {
  }

  ~FileI()
  {
    delete[] _bytes;
  }
  
  virtual void next_async(const ::FileTransfer::AMD_File_nextPtr&, const ::Ice::Current&);

private:

  FILE* _fp;
  const int _num;
  Ice::Byte* _bytes;
  
};

class FileStoreI
  : public FileStore
{
public:
  FileStoreI();
  ~FileStoreI();
  virtual ::FileTransfer::FilePrx read(const ::std::string&, ::Ice::Int, const ::Ice::Current&);
  void write(const ::std::string&, ::Ice::Int, const ::Ice::ByteSeq&, const ::Ice::Current&);
private:
  ::Ice::ByteSeq _bytes;
};



/* #include <Ice/Application.h> */
/* #include <FileTransfer.h> */
/* #include <time.h> */

/* using namespace std; */
/* using namespace Ice; */
/* using namespace FileTransfer; */

/* class FileStore_readI */
/*   : public AMI_FileStore_read, */
/*     public IceUtil::Monitor<IceUtil::Mutex> */
/* { */
/* public: */
/*   FileStore_readI() : _done(false) */
/*   { */
/*   } */
/*   virtual void ice_response(const ::std::pair<const ::Ice::Byte*, const ::Ice::Byte*>&); */
/*   virtual void ice_exception(const ::Ice::Exception&); */
/*   void getData(::Ice::ByteSeq&); */
/* private: */
/*   bool _done; */
/*   auto_ptr<Ice::Exception> _exception; */
/*   Ice::ByteSeq _bytes; */
/* }; */

/* typedef IceUtil::Handle<FileStore_readI> FileStore_readIPtr; */

#endif
