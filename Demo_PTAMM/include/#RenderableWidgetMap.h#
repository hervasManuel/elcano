/*
 * RenderableWidgetMap.h
 *
 */

#ifndef RENDERABLEWIDGETMAP_H_
#define RENDERABLEWIDGETMAP_H_

class RenderableLinePath;

#include "RenderableWidget.h"
#include "RenderableWidgetImage2D.h"
#include "RenderableLinePath.h"
#include <map>


namespace mars {

/**
 * This class represents a widget map.
 */
class RenderableWidgetMap: public mars::RenderableWidget {
public:

	/**
	 * Adds a new method to represent
	 * @param name the unique name of the method
	 * @param fileName name of the image file containing the texture that will be used to represent it
	 */
	void addMethod(const std::string& name, const std::string& fileName);

	/**
	 * Sets a method position
	 * @param name the unique name of the method
	 * @param x relative x position
	 * @param y relative y position
	 */
	void setMethodPosition(const std::string& name, const float& x, const float& y);

	/**
	 * Sets the angle of the method's image
	 */
	void setMethodAngle(const std::string& name, const float& angle);


	/**
	 * Sets the widget position
	 */
	void setPosition(const int& x, const int& y);


	/**
	 * Sets the pixel representing (0,0) in our map.
	 */
	void setStartPoint(const int& x, const int& y);

	/**
	 * Draws it
	 */
	void draw();

	/**
	 * Resets the internal path
	 */
	void resetPath();

	/**
	 * Adds a point to the internal path
	 */
	void addPOI(const float& x, const float& y);

	/**
	 * Changes the width of the line used to render the path
	 */
	void setPathLineWidth(const float& w);

	void updateUserPos(float ux, float uy, float uz);

private:

	RenderableWidgetMap(const std::string& fileNameBack, const int& width, const int& height, const float& scale);
	virtual ~RenderableWidgetMap();


	RenderableWidgetMap();

	void handleMouseMotion(int x, int y){}
	void handleMouseClick(int button, int x, int y){}
	void handleMouseRelease(int button, int x, int y){}



	RenderableWidgetImage2D* __background;
	const float __scale;

	int __realStartX;
	int __realStartY;

	std::map<std::string, RenderableWidgetImage2D*> __methodPos; // the different tracking methods ...
	typedef std::map<std::string, RenderableWidgetImage2D*>::iterator met_it;

	friend class RenderableConstructor;

	RenderableLinePath* __thePath;
};

}

#endif /* RENDERABLEWIDGETMAP_H_ */
