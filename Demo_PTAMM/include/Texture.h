/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/** This class represents a texture, i.e. an image after all*/

/*
 * Texture.h
 *
 */

#ifndef TEXTURE_H_
#define TEXTURE_H_

#include <iostream>
#define NO_SDL_GLEXT
#include <SDL/SDL.h>
#include <SDL/SDL_opengl.h>
#include <GL/gl.h>
#include <string>
#include <algorithm>
#include <cctype>
#include <SDL/SDL_image.h>
#include <opencv/cv.h>


#include "Configuration.h"
#include "Logger.h"

using namespace std;

namespace mars {

/**
 * Represents and holds a texture
 */
class Texture {
public:
	Texture();


	/**
	 * Constructor that takes a path to a file and generates that texture
	 *
	 * @param pathToFile the path to the file, including its name.
	 */
	Texture(string pathToFile);


	/**
	 * Loads a texture from a file to memory
	 *
	 * @param pathToFile the path to the file, including its name.
	 */
	void loadFromFile(string pathToFile);

	/**
	 * Converts a captured opencv Mat to texture
	 */
	static void convertCVMatToTexture(cv::Mat* mat, GLuint* texture);

	/**
	 * Returns the openGL value of the generated texture
	 */
	GLuint getGLTexture();

	/**
	 * Returns true if the texture was successfully loaded
	 */
	bool isLoaded();

	virtual ~Texture();


	// Extra utilities

	static GLuint convertSurfaceToOpenGLTexture(SDL_Surface* sdlSurface);

	static void freeOpenGLTexture(GLuint tex);



private:
	string       __textureName; ///< Name of the texture
	string       __textureDescription; ///< Optional Description
	GLuint       __glTexture;       ///< OpenGL texture
	GLenum       __glTextureFormat; ///< texture format
	bool         __loaded; ///< Was it successfully loaded?

};

}

#endif /* TEXTURE_H_ */
