/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * RenderableModel.h
 *
 */

#ifndef RENDERABLE3D_H_
#define RENDERABLE3D_H_

#include <string>
#include <iostream>
#include <algorithm>
#include <cctype>

#include <dae.h>
#include <dom/domCOLLADA.h>

#include "Vector3D.h"
#include "Renderable.h"
#include "TriangleMesh.h"
#include "PolyMesh.h"
#include "Texture.h"
#include "TextureFactory.h"
#include "Logger.h"
#include "Camera3D.h"

#include <GL/gl.h>
#include <GL/glu.h>

#include "Configuration.h"

#include "Matrix16.h"

namespace mars {




class RenderableModel: public Renderable {
public:

	/**
	 * Alternative constructor that loads a 3D file
	 * It uses libg3d so this supports all the models it supports.
	 *
	 * @param pahToFile path to the model file
	 */
	RenderableModel(std::string pathToFile);

	/**
	 * Draws this renderable using openGL
	 */
	void draw();

	virtual ~RenderableModel();

	/**
	 * Returns a vector of the textures this renderable has
	 */
	vector<Texture*> getTextures();

	/**
	 * Sets the textures vector to the given one
	 *
	 * @param textures the textures vector
	 */
	void setTextures(vector<Texture*> textures);

	/**
	 * Enables Xor Wire
	 */
	void enableWireXor();

	/**
	 * Disables Xor Wire
	 */
	void disableWireXor();

	/**
	 * Enables wire textured
	 */
	void enableWireTextured();

	/**
	 * Disables wire textured
	 */
	void disbaleWireTextured();


	/**
	 * Returns the current color of the wire
	 */
	GLfloat* getWireColor();

	/**
	 * Sets the color of the wire
	 *
	 * @param r red component
	 * @param g green component
	 * @param b blue component
	 */
	void setWireColor(const float& r, const float& g, const float& b);

	/**
	 * Returns the fill color of the wire
	 */
	GLfloat* getWireFillColor();

	/**
	 * Sets the fill color of the wire
	 *
	 * @param r red component
	 * @param g green component
	 * @param b blue component
	 */
	void setWireFillColor(const float& r, const float& g, const float& b);

	/**
	 * Returns the amount of wire's transparency as a float
	 */
	GLfloat getWireTrans();

	/**
	 * Sets the amount of wire's transparency
	 *
	 * @param a Amount of transparency [0 - transparent , 1 - opaque]
	 */
	void setWireTrans(const float& a);

	/**
	 * Returns the amount of wire fill transparency as a float
	 */
	GLfloat getWireFillTrans();

	/**
	 * Sets the amount of wire fill transparency
	 *
	 * @param a Amount of transparency [0 - transparent , 1 - opaque]
	 */
	void setWireFillTrans(const float& a);

	/**
	 * Enables the wire animation
	 */
	void enableWireAnim();

	/**
	 *  Disables the wire animation
	 */
	void disableWireAnim();

	/**
	 * Enables All transparency while in wire mode.
	 */
	void enableAllTransparentWhileWire();

	/**
	 * Disables All transparency while in wire mode.
	 */
	void disableAllTransparentWhileWire();

	/**
	 * Enables the back lines while on wire mode.
	 */
	void enableBackWire();

	/**
	 * Disables back lines while on wire mode.
	 */
	void disableBackWire();

	/**
	 * Sets the value used as normal width for the wire mode
	 */
	void setWireNormalWidth(const GLfloat& w);

	/**
	 * Sets the value used as max width for the wire mode
	 */
	void setWireMaxWidth(const GLfloat& w);

	/**
	 * Sets the value used as min width for the wire mode
	 */
	void setWireMinWidth(const GLfloat& w);

	/**
	 * Returns true :)
	 */
	bool is3D();


protected:
	RenderableModel();

	/**
	 * Loads a DAE file. Only triangle meshes supported.
	 *
	 */
	void loadWithColladaDOM(string pathToFile);

	/**
	 * Creates internally the openGL call lists used to render the object with
	 */
	void createCallLists();

	/**
	 *  Draws the wireframe
	 */
	void drawWire();

	/**
	 * Animates the wire (1 step a time)
	 */
	void animateWire();

	string __filePath;    ///< Stores the path of the model file

	vector<Mesh*> __meshes; ///< List of meshes of the renderable

	vector<Texture*> __textures; ///< Vector of the textures.

	vector<GLuint> __callLists; ///< Vector of call lists.

	bool __wMeshes; ///< for the "no meshes" warning

	friend class RenderableConstructor;

};

}

#endif /* RENDERABLE3D_H_ */
