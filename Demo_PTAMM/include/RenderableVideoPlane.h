/*
 * RenderableVideoPlane.h
 *
 */

#ifndef RENDERABLEVIDEOPLANE_H_
#define RENDERABLEVIDEOPLANE_H_

#include "Renderable.h"
#include "VLCPlayer.h"
#include "Texture.h"
#include "TextureFactory.h"
#include "Configuration.h"
#include "World.h"
#include <SDL/SDL.h>

#include "VideoOutputSDLOpenGL.h"

namespace mars {

class RenderableVideoPlane : public Renderable {

public:

	/**
	 * Default constructor, please USE RenderableConstructor instead.
	 *
	 * @see RenderableConstructor
	 */
	RenderableVideoPlane(std::string name, std::string fileName, unsigned w=2,
	                     unsigned h=1, bool isURL=false);

	virtual ~RenderableVideoPlane();

	/**
	 * Mandatory :)
	 */
	void draw();


	/**
	 * Plays the video.
	 */
	void play();


	/**
	 * Stops the video.
	 */
	void stop();


	void playURL(const string& URL);

	void playFile(const string& fileName);

private:


	RenderableVideoPlane();

	void drawPlane();

	void init();

	OneMedia* __sMedia;

	GLuint __currentTexture;

	unsigned __width;
	unsigned __heigh;

	Texture* __defaultTexture;

//	SDL_Surface* __surface;


	std::string __name;

	std::string __currentFile;

	bool __isURL;


};

}

#endif /* RENDERABLEVIDEOPLANE_H_ */
