/*
 * TrackingMethod.h
 *
 */

#ifndef TRACKINGMETHOD_H_
#define TRACKINGMETHOD_H_

#include "Vector3D.h"
#include "Renderable.h"
#include "Logger.h"
#include "VideoSource.h"
#include "Matrix16.h"
#include "TUserL.h"

#include <sstream>
#include <string>

#include <pthread.h>
#include <semaphore.h>

#include <iostream>
#include <vector>
#include <map>

#include "Quaternion.h"

using namespace std;

#define SLOW_MODE_WAIT 1 //Time in seconds

namespace mars {



/**
 * The base class for all the tracking methods. It acts like a runnable.
 */
class TrackingMethod {
public:
	TrackingMethod();
	virtual ~TrackingMethod();
	TrackingMethod(const unsigned int& nRecords);
	string getMethodName();
	tUserL* getLastUserL();
	bool isDone();
	vector<tUserL*>* getHistoricalRecord();
	static const tUserL Mat2TUserL(Matrix16& m, const float& weight);
	static const Quaternion UserL2Quaternion(tUserL t);
	bool isDetecting();
	void pause();
	void resume();
	void slowMode(); //Waits a sec between iterations
	void fastMode();

protected:

	void lock();
	void unlock();

	bool isReady();

	/**
	 * Interface for the Thread of the Tracking Method
	 */
	void Thread(){
	  lock();	
	  __paused = false;
	  __slowMode = false;

	  while (!__done){
	     if(!__paused){
	      loopThread();
	      if(__slowMode){
		sleep(SLOW_MODE_WAIT);
	      }
	    }else{
	      pthread_yield();
	    }
	  }
	  unlock();
	};

	virtual void loopThread() = 0;

	/**
	 * Used internally.
	 * It should never be used by the coder.
	 */
	static void* exec(void* thr);


	/**
	 * Stores a record
	 *
	 * @param userL a perception represented as an user position and orientation.
	 * @see tUserL
	 */
	void addRecord(const tUserL& userL);


	int __threadError;  ///< Thread error, just in case it happens
	pthread_t __thread_id; ///< Thread id

	bool __done;

	tUserL __userL; ///< The camera structure, this represents the output of the perception.

	vector<tUserL*> __historicalRecordOfPerceptions; ///< A record of the last perceptions.

	unsigned int __maxHistoricalRecords;

	std::string __methodName; ///< name of the method

protected:
	bool __ready;
	bool __paused; //Patch! Don't like! it's safe!
	bool __detecting;
	bool __slowMode; //WOW

private:

	void startMethod();
	void stopMethod();

	std::map<string, Renderable*> __renderables;

	pthread_mutex_t mutex_tracking;

	friend class TrackingController;




};

}

#endif /* TRACKINGMETHOD_H_ */
