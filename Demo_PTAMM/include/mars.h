/*
 * mars.h
 *
 */

#ifndef MARS_H_
#define MARS_H_


#include "FramesBuffer.h"
#include "VideoCaptureOpenCV.h"
#include "VideoFileOpenCV.h"
#include "VideoDeviceOpenCV.h"
#include "VideoCaptureFactory.h"
#include "VideoOutputCV.h"
#include "VideoEnumerate.h"
#include "VideoSource.h"
#include "RenderableModel.h"
#include "Light.h"
#include "Configuration.h"
#include "SoundFactory.h"
#include "ScriptingLUA.h"
#include "TrackingMethodFactory.h"
#include "Logger.h"
#include "VideoOutputSDLOpenGL.h"
#include "World.h"
#include "FontFactory.h"
#include "Matrix16.h"
#include "Quaternion.h"
#include "RenderableText.h"
#include "RenderableEmpty.h"
#include "RenderableWIFICell.h"
#include "RenderableLinePath.h"

#include "Logger.h"

#include "ARTKMark.h"
#include "TrackingMethodOAbsoluteArtkBz.h"
#include "TrackingController.h"

#include "TrackingMethodORelativePixelFlow.h"



#include "RenderableModelOrej.h"
#include "RenderableWidget.h"
#include "RenderableWidgetButton.h"
#include "RenderableConstructor.h"
#include "RenderableWidgetLogger.h"
#include "RenderableVideoPlane.h"
#include "RenderableWidgetImage2D.h"
#include "RenderableWidgetConsole.h"
#include "RenderableWidgetMap.h"
#include "RenderableWidgetList.h"

#include "VLCPlayer.h"

#include "Tools.h"

#include <opencv/cv.h>

#include "Communication.h"
#include "PhotoPos.h"
#include "anuncPos.h"
#include "WifiPos.h"

#include <string.h>
#include <stdio.h>
#include <iostream>
#include <sstream>


#define PATH_HEIGHT 0.2
#define EXT_CAM false //Enables or disables the ext cam
//ARCO -> MARS
#define ARCO_OX 60
#define ARCO_OY 30.3
#define ARCO_PHI 45*PI/180
//MARS -> ARCO
#define MARS_OX -21.2
#define MARS_OY 63.7
#define MARS_PHI 45*PI/180

//OFFSETS RANDOM
#define OFFSET_PHOTOPOS 1
#define OFFSET_WIFI 2

#define ELCANO_X_UP_LIMIT 20
#define ELCANO_X_DOWN_LIMIT -0.1
#define ELCANO_Y_UP_LIMIT 50
#define ELCANO_Y_DOWN_LIMIT -0.1
#endif /* MARS_H_ */
