/*
 * RenderableLineDemo.h
 *
 */

#ifndef RENDERABLELINEDEMO_H_
#define RENDERABLELINEDEMO_H_

#include "Renderable.h"
#include "World.h"

namespace mars {

class RenderableLineDemo : public Renderable{
public:

	void setP(Point3D b, Point3D e);

	void draw();

	RenderableLineDemo();
	virtual ~RenderableLineDemo();

private:

	Point3D _b, _e;
};

}

#endif /* RENDERABLELINEDEMO_H_ */
