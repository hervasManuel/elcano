#ifndef MOBILEDEVICE_H_
#define MOBILEDEVICE_H_

#include <Ice/Ice.h>
#include <MobileService.h>

using namespace Elcano;

class MobileDeviceI: public MobileDevice{
 private:
  TaskSeq _tareas;
  OpenLS::Route _ruta;

 public:
  virtual void notifyRoute(const OpenLS::Route&, const Ice::Current&);
  virtual void notifyTasks(const TaskSeq&, const Ice::Current&);
  OpenLS::Route getRoute();
  TaskSeq getTasks();
};

#endif
