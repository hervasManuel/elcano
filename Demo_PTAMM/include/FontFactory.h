/*
 * FontFactory.h
 *
 */

#ifndef FONTFACTORY_H_
#define FONTFACTORY_H_

#include "Configuration.h"
#include "Logger.h"

#include <iostream>
#include <map>
#include <string>

#include <FTGL/ftgl.h>
#include "Singleton.h"

using namespace std;

enum FontType {EXTRUDED, POLYGON, BUTTON}; //< Type of font

namespace mars {


/**
 * Factory of fonts
 *
 * Used to create and mantain the fonts used by mars.
 */
class FontFactory : public Singleton<FontFactory> {
public:

	/**
	 * Returns the factory instance (it's a singleton)
	 */
	static FontFactory* getFontFactory();

	/**
	 * Returns the font given by the parameter
	 *
	 * @param fontName the font.
	 */
	FTFont* getFont(string fontName);

	/**
	 * Returns the default font
	 */
	FTFont* getDefaultFont();

	/**
	 * Returns the Button font
	 */
	FTFont* getButtonFont();

	/**
	 * Returns the Console font
	 */
	FTFont* getConsoleFont();

	/**
	 * Adds a new font, by name
	 *
	 * @param fontName filename of the font (located at defaultFontPath)
	 * @param typeFont it can be either POLYGON or EXTRUDED, defaults to POLYGON.
	 *
	 * @see defaultFontPath
	 */
	void addFont(string fontName, unsigned typeFont=POLYGON);



private:
	FontFactory();
	virtual ~FontFactory();

	static FontFactory* __instance; ///< Singleton's Instance

	FTFont* __defaultFont; ///< Default Font

	FTFont* __buttonFont; ///< Buttons Font

	FTFont* __consoleFont; ///< Console Font

	map<string, FTFont*> __fontList; ///< Hash Table that contains the fonts

	map<string, FTFont*>::iterator __fontIterator;

	friend class Singleton<FontFactory>;

};

}

#endif /* FONTFACTORY_H_ */
