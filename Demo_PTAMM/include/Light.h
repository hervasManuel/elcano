/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * Light3D.h
 *
 */

#ifndef LIGHT_H_
#define LIGHT_H_

#include <GL/gl.h>
#include <iostream>




namespace mars {

/**
 * This class represents a light
 */
class Light {
public:
	/**
	 * Default constructor
	 */
	Light();

	/**
	 * Sets the Ambient Color (rgba)
	 */
	void setAmbient(const GLfloat& r, const GLfloat& g, const GLfloat& b,
			const GLfloat& a);

	/**
	 * Sets the diffuse Color (rgba)
	 */
	void setDiffuse(const GLfloat& r, const GLfloat& g, const GLfloat& b, const GLfloat& a);

	/**
	 * Sets the diffuse Specular (rgba)
	 */
	void setSpecular(const GLfloat& r, const GLfloat& g, const GLfloat& b, const GLfloat& a);

	/**
	 * Sets the position of the light (x,y,z / w)
	 */
	void setPosition(const GLfloat& x, const GLfloat& y, const GLfloat& z, const GLfloat& w);

	/**
	 * Returns the ambient components
	 *
	 */
	const GLfloat* getAmbient();

	/**
	 * Returns the diffuse components
	 *
	 */
	const GLfloat* getDiffuse();


	/**
	 * Returns the specular components
	 *
	 */
	const GLfloat* getSpecular();

	/**
	 * Return the position
	 */
	const GLfloat* getPosition();


	/**
	 * Default destructor
	 */
	virtual ~Light();

private:

    GLfloat __ambient[4];  ///< Ambient Component
	GLfloat __diffuse[4];  ///< Difusse Component
	GLfloat __specular[4]; ///< Specular Component

	GLfloat __position[4]; ///< Postion (4th coord represents a directional or omnidirectional light)

};

}

#endif /* LIGHT_H_ */
