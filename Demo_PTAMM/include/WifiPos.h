#include <Ice/Ice.h>
#include <MLP/Location.h>
#include <LocationListenerI.h>
#include <pthread.h>

class WifiPos{
 private:
  Ice::CommunicatorPtr ic;
  Ice::ObjectPrx baseLA;
  Ice::ObjectPrx baseLL;
  Ice::ObjectPrx baseU;

  Ice::ObjectAdapterPtr adapter;
  Ice::ObjectPrx md;

  LocationListenerI * LL;
  MLP::LocationListenerPtr object;
  MLP::LocationListenerPrx LLPrx;
  MLP::LocationAdminPrx LAPrx;
  pthread_t __thread_id;
  bool LAOk;

  static void* exec(void* thr);
  void sirviente();

 public:
  WifiPos(int argc, char **argv);
  ~WifiPos();
  MLP::ShapePtr getWifiPos();
  bool connect();

};

