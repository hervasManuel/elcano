/*
 * RenderableConstructor.h
 *
 */

#ifndef RenderableConstructor_H_
#define RenderableConstructor_H_

class Light;
class RenderableVideoPlane;
class RenderableWidgetImage2D;
class RenderableWidgetMap;
class RenderableWIFICell;
class RenderableWidgetList;
class RenderableEmpty;
class RenderableText;



#include <iostream>
#include <map>
#include "Singleton.h"
#include "Renderable.h"
#include "RenderableModel.h"
#include "RenderableEmpty.h"
#include "RenderableWidget.h"
#include "RenderableWidgetButton.h"
#include "RenderableVideoPlane.h"
#include "RenderableLinePath.h"
#include "RenderableWIFICell.h"
#include "RenderableWidgetImage2D.h"
#include "RenderableWidgetMap.h"
#include "RenderableWidgetList.h"
#include "RenderableText.h"
#include "RenderableModelOrej.h"

#include "Logger.h"

namespace mars {

enum rType {DAE, WBUTTON};

class RenderableConstructor : public Singleton<RenderableConstructor>{
public:

	/**
	 * Returns a new Button (RenderableWidgetButton)
	 *
	 * @param name The name of the object. This should be UNIQUE.
	 * @param text The text on the button.
	 * @param x x position.
	 * @param y y position.
	 * @param w Button's width
	 * @param h Button's height
	 *
	 */
	RenderableWidgetButton* createWButton(const string & name, const string & text, int x, int y, int w, int h);

	/**
	 * Returns a new Model (RenderableModel)
	 *
	 * @param name The name of the object. This should be UNIQUE.
	 * @param file Absolute path to dae file.
	 */
	RenderableModel* createModel(const string & name, const std::string & file);
	RenderableModelOrej* createModelOrej(const string & name, const std::string & fileOrej, const std::string & fileTex,  float sx=0, float sy=0, float sz=0);


   /**
	* Returns an already creaded button.
	*
	* @param name The button's name, as named when created.
	*/
	RenderableWidgetButton* getWButton(const string & name);


	/**
	 * Returns an already creaded RenderableModel.
	 *
	 * @param name The model's name, as named when created.
	 */
	RenderableModel* getModel(const string & name);

	/**
	 * Creates a light.
	 *
	 * @param name UNIQUE name.
	 */
	Light* createLight(const string & name);

	/**
	 * Returns a light by his name.
	 *
	 * @name Light name.
	 */
	Light* getLight(const string & name);

	/**
	 * Creates a empty.
	 *
	 * @param name RenderableEmpty's name (UNIQUE)
	 */
	RenderableEmpty* createEmpty(const string & name);

	/**
	 * Returns an empty by its name.
	 *
	 * @param name RenderableEmpty's name (UNIQUE)
	 */
	RenderableEmpty* getEmpty(const string & name);

	/**
	 * Create a plane with video.
	 *
	 * @param name UNIQUE name.
	 * @param fileName video (as it should be found on the videos directory)
	 *
	 * @see Configuration
	 *
	 */
	RenderableVideoPlane* createVideoPlane(const string & name, const string & fileName,
	                                       const unsigned& w, const unsigned& h,
	                                       bool isURL = false);

	/**
	 * Returns a video plane by its name.
	 *
	 * @param name Its unique name, as when it was created.
	 */
	RenderableVideoPlane* getVideoPlane(const string & name);

	/**
	 * Creates an image2D widget
	 *
	 * @param textureFile the name of the file containing an image.
	 */
	RenderableWidgetImage2D* createImage2D(const string& name,
			const string& textureFile, const unsigned& w, const unsigned& h);


	/**
	 * Returns an image2D by its name.
	 *
	 * @param name Its unique name, as when it was created.
	 */
	RenderableWidgetImage2D* getImage2D(const string& name);



	/**
	 * Creates a renderable wifi cell.
	 * @param name Its unique name, as when it was created.
	 */
	RenderableWIFICell* createWIFICell(const string& name, const float& radius);

	/**
	 * Returns a renderable WIFI Cell by its name.
	 *
	 * @param name Its unique name, as when it was created.
	 */
	RenderableWIFICell* getWIFICell(const string& name);


	/**
	 * Creates a renderable line path
	 * @param name Its unique name, as when it was created.
	 */
	RenderableLinePath* createLinePath(const string& name);


	/**
	 * Returns a renderable line path by its name.
	 * @param name Its unique name, as when it was created.
	 */
	RenderableLinePath* getLinePath(const string& name);


	/**
	 * Creates a Map :)
	 *
	 * @param fileNameBack file containing the background (image file, suports transparency)
	 * @param width x size
	 * @param height y size
	 * @param scale the scale value comparing real world and map :)
	 */
	RenderableWidgetMap* createWMap(const std::string& name,
			const std::string& fileNameBack,
			const int& width, const int& height,
			const float& scale);


	/**
	 * Returns a map by his unique name
	 */
	RenderableWidgetMap* getWMap(const std::string& name);

	/**
	 * Creates a List Widget
	 *
	 * @param name Its unique name
	 * @param w Width
	 * @param h Height
	 * @param widthPortionUpDown [0..0.5] percentage to be used by nav arrows
	 * @param nVisibleOptions :)
	 */
	RenderableWidgetList* createWList(const std::string& name,
			const unsigned& w, const unsigned& h,
			const float& widthPortionUpDown = 0.2,
			const unsigned& nVisibleOptions = 5);

	/**
	 * Returns a List by his unique name
	 */
	RenderableWidgetList* getWList(const std::string& name);


	/**
	 * Creates a Text Object
	 *
	 *  @param name Its unique name
	 *  @param font Which font to use (name of file - e.g. "FreeSans.ttf" )
	 *  @param str The initial text
	 *  @param fType @see FontType
	 */
	RenderableText* createText(const std::string& name,
	                           const std::string& font,
	                           char* str,
	                           FontType fType = POLYGON);


	RenderableText* createTextLUA(const std::string& name, const string& str);

	/**
	 * Returns a Text Object by his unique name
	 */
	RenderableText* getText(const std::string& name);



private:
	RenderableConstructor();
	virtual ~RenderableConstructor();

	std::map<std::string,RenderableVideoPlane*> __videoPlanes;
	std::map<std::string,RenderableWidgetButton*> __buttons;
	std::map<std::string,RenderableModel*> __models;
	std::map<std::string,RenderableEmpty*> __emptys;
	std::map<std::string,Light*> __lights;
	std::map<std::string,RenderableWidgetImage2D*> __images2D;
	std::map<std::string,RenderableLinePath*> __paths;
	std::map<std::string,RenderableWIFICell*> __wifis;
	std::map<std::string,RenderableWidgetMap*> __maps;
	std::map<std::string,RenderableWidgetList*> __lists;
	std::map<std::string,RenderableText*> __texts;


	friend class Singleton<RenderableConstructor>;

	RenderableModel* __defaultModel;
};

}

#endif /* RenderableConstructor_H_ */
