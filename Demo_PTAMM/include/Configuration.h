/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * Configuration.h
 *
 */

#ifndef CONFIGURATION_H_
#define CONFIGURATION_H_

#define CONFIG_FILE "mars.cfg"

#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <cctype>
#include <cstdio>
#include <sstream>
#include <vector>
#include <luabind/luabind.hpp>

#include "Logger.h"
#include "ARTKMark.h"
#include "Singleton.h"



#include "Matrix16.h"

using namespace std;

namespace mars {

/**
 * Structure that holds a Stream URL config
 */
struct urlStreamConfig{
	string name;
	string URL;
	string description;
};

/**
 *
 * The configuration class. This will read the config file and act consequently.
 *
 */
class Configuration : public Singleton<Configuration> {
public:

	static Configuration* getConfiguration();

	/**
	 *  Parses the ARTK Marks file
	 */
	void parseARTKPatternsFile(std::vector<ARTKMark>& marks);


	/**
	 * Returns the image configured as the default frame returned by a FramesBuffer
	 * @see FramesBuffer
	 */
	const string defaultImage(){ return _defaultImage; }

	/**
	 * Returns the default texture of a RenderableVideoPlane
	 *
	 * @see RenderableVideoPlane
	 */
	const string defaultVideoTexture(){ return _defaultVideoTexture; }

	/**
	 * Returns the path where mars will try to locate the textures.
	 *
	 * @see TextureFactory
	 */
	const string defaultTexturePath(){ return _defaultTexturePath; }

	/**
	 * Returns the path where mars will try to locate the videos.
	 */
	const string defaultVideoPath(){ return _defaultVideoPath; }

	/**
	 * Returns the path where mars will try to locate the fonts.
	 */
	const string defaultFontPath(){ return _defaultFontPath;}

	/**
	 * Returns the name of the default font.
	 */
	const string defaultFont(){ return _defaultFont;}

	/**
	 * Returns the name of the default button font.
	 */
	const string buttonsFont(){ return _buttonsFont;}

	/**
	 * Returns the name of the default console font.
	 */
	const string consoleFont(){ return _consoleFont;}

	/**
	 * Returns the name of the default button texture.
	 */
	const string defaultButtonTexture(){ return _defaultButtonTexture;}

	/**
	 * ARTK RELATED
	 */
	const string ARTKDataPath(){ return _ARTKDataPath;}

	/**
	 * ARTK RELATED
	 */
	const string ARTKPatternsFile(){ return _ARTKPatternsFile;}

	/**
	 * Returns the path where mars will try to locate the sounds.
	 */
	const string defaultSoundPath(){ return _defaultSoundPath;}

	/**
	 * Configured as a full screen application?
	 */
	const bool openGLFullScreen(){ return _openGLFullScreen;}

	/**
	 * X resolution
	 */
	const int  openGLScreenWidth(){ return _openGLScreenWidth;}

	/**
	 * Y resolution
	 */
	const int  openGLScreenHeight(){ return _openGLScreenHeight;}

	/**
	 * Returns the depth of the video mode.
	 */
	const int  openGLScreenDepth(){ return _openGLScreenDepth;}





private:

	Configuration();
	virtual~ Configuration();

	void parseConfig(); ///< Parses he config file
	void addproperty(string property, string value, int nLine); ///< Parses the result of a line
	void addStreamURL(string csv, int nLine); ///< Adds a new Stream URL


	string _defaultImage;
	string _defaultVideoTexture;
	string _defaultTexturePath;
	string _defaultVideoPath;
	string _defaultFontPath;
	string _defaultFont;
	string _buttonsFont;
	string _consoleFont;
	string _defaultButtonTexture;
	string _ARTKDataPath;
	string _ARTKPatternsFile;
	string _defaultSoundPath;
	bool _openGLFullScreen;
	int  _openGLScreenWidth;
	int  _openGLScreenHeight;
	int  _openGLScreenDepth;


	ifstream __configFile; ///< input stream for the config file
	ifstream __patternsFile; ///< input stream for the patterns file

	vector<urlStreamConfig*> __streamsURL; ///< Vector of streams.

	lua_State* lState;

	friend class Singleton<Configuration>;
	friend class WidgetFunction;
	friend class ScriptingLUA;

};


}

#endif /* CONFIGURATION_H_ */
