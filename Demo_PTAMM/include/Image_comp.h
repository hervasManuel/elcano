/*
 * Image_comp.h
 *
 */

#ifndef IMAGE_COMP_H_
#define IMAGE_COMP_H_

#include <VoronoiTree.h>

class Image_comp{

private:

	Image* img;
	float comp;

public:

	Image_comp(Image* img, float comp);

	Image* getImage();

	void setImage(Image* imgNew);

	float getComp();

	void setComp(float compNew);

	/**
	 * Method that compares tow objects Image_Comp. These are compared respect the distance.
	 */

	int compareTo(Image_comp* imgNew);



};

#endif /* IMAGE_COMP_H_ */
