/*
 * Comparator.h
 *
 */

#ifndef COMPARATOR_H_
#define COMPARATOR_H_

#include <Image.h>


class Comparator{
	private:
		double weightcld;
		double weightehd;
		double weightscd;

	public:

		Comparator();
		double compareImages(Image *a, Image *b);
};

#endif /* COMPARATOR_H_ */
