/*
 * RenderableWidgetImage2D.h
 *
 */

#ifndef RENDERABLEWIDGETIMAGE2D_H_
#define RENDERABLEWIDGETIMAGE2D_H_

#include "RenderableWidget.h"
#include "Logger.h"
#include "TextureFactory.h"
#include "Configuration.h"
#include "World.h"
#include "Matrix16.h"
#include "VideoOutputSDLOpenGL.h"

namespace mars {

class RenderableWidgetImage2D: public mars::RenderableWidget {
public:

	/**
	 * Sets the image 2-D angle
	 */
	void setAngle2D(const float& angle){
		__angle2D = angle;
	}

	void draw();

private:
	RenderableWidgetImage2D(); // You *MUST* use the constructor with an image as a parameter!

	Texture* __imageTexture;

	float __angle2D;

	void handleMouseMotion(int x, int y){}
	void handleMouseClick(int button, int x, int y){}
	void handleMouseRelease(int button, int x, int y){}


	RenderableWidgetImage2D(std::string textureFile, const unsigned w=1, const unsigned h = 1);

	virtual ~RenderableWidgetImage2D();

	friend class RenderableWidgetMap;
	friend class RenderableWidgetButton;
	friend class RenderableConstructor;

};

}

#endif /* RENDERABLEWIDGETIMAGE2D_H_ */
