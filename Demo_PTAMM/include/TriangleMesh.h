/*
 * TriangleMesh.h
 *
 */

#ifndef TRIANGLEMESH_H_
#define TRIANGLEMESH_H_

#include <vector>
#include <map>
#include <iostream>

#include "Vector3D.h"
#include "Logger.h"
#include "Mesh.h"


namespace mars {

typedef Vector3D Vertex;

using namespace std;



class TriangleMesh: public Mesh {
public:
	TriangleMesh();

	virtual ~TriangleMesh();



};

}

#endif /* TRIANGLEMESH_H_ */
