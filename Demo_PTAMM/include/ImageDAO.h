/*
 * ImageDAO.h
 *
 */

#ifndef IMAGEDAO_H_
#define IMAGEDAO_H_

#include <string>
#include <mysql_connection.h>
#include <Agent.h>
#include <Image_comp.h>

class ImageDAO {

private:

	Agent *Broker;
	sql::Connection *Con;

public:

	/**
	 * Builder by default of ImageDAO class.
	 * It creates the Agent and it obtains a connection.
	 *
	 * */
	ImageDAO();

	/**
	 * CRUD operation delete; it deletes an image from the database
	 * returns the number of deleted images.
	 *
	 * deleteAll() cleans the database
	 *
	 * */

	 int deleteImage(Image *imag);

	 int deleteAll();

	 /**
	 * CRUD operation insert; it inserts an image into the database.
	 *
	 * */

	 int insertImage(Image *img);

	 /**
	 * Select CRUD operation. It returns all the elements which are indicated in the
	 * condition string.
	 *
	 * */

	 vector<Image*> select(string condition);

	 /**
	 * Select CRUD operation. It returns the number elements that carry out the condition string.
	 *
	 **/

	 int selectn(string condition);

	 /**
	 * CloseDAO. Close the connection to optimize the connections management
	 *
	 * */
	 void closeDAO();

};



#endif /* IMAGEDAO_H_ */
