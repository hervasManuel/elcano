/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * PixelFlow.h
 *
 */

#ifndef TRACKINGMETHOD_O_PIXELFLOW_H_
#define TRACKINGMETHOD_O_PIXELFLOW_H_

class RenderableWidgetFPoints;

#include <iostream>
#include <opencv/cv.h>
#include <vector>
#include "FramesBuffer.h"
#include "TrackingMethodORelative.h"
#include "RenderableWidgetFPoints.h"
#include "VideoDevice.h"
#include "World.h"
#include "Logger.h"
#include "Configuration.h"


using namespace std;
using namespace cv;

namespace mars {
///
/// This class implements a pixelFLow method for the MARS project
///

class TrackingMethodORelativePixelFlow: public TrackingMethodORelative {
public:
	TrackingMethodORelativePixelFlow(VideoSource* vS);
	virtual ~TrackingMethodORelativePixelFlow();

	void loopThread();
	void initThread();

private:
	TrackingMethodORelativePixelFlow();

	RenderableWidgetFPoints* __FPWidget;
	vector<KeyPoint> __keyPointsNew;
	vector<KeyPoint> __keyPointsOld;
	VideoSource* __vs;
	int __of;
	SURF* __surf;

};


}
#endif /* PIXELFLOW_H_ */
