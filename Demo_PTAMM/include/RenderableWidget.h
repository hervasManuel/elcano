/*
 * RenderableWidget.h
 *
 */

#ifndef RENDERABLEWIDGET_H_
#define RENDERABLEWIDGET_H_

#include <iostream>

#include "FontFactory.h"
#include "Renderable.h"
#include "Texture.h"
#include "Configuration.h"
#include "Tools.h"

#include <GL/gl.h>
#include <SDL/SDL.h>
#include <map>
#include <luabind/luabind.hpp>


#define MAX_BUTTONS 40

namespace mars {

/**
 * Available event types
 */
enum wEventType {
		/**
		 * Click event
		 */
		CLICK,
		/**
		 * Select event
		 */
		SELECT,
		/**
		 * Pressed key event
		 */
		KEY};


/**
 * This struct holds the functions executed by any widget events.
 */
struct WidgetFunction{
	WidgetFunction(void (*f_) (void*), void* data_) : f(f_), data(data_) {}

	WidgetFunction(std::string name) : __name(name) { f = &__f; data = (void*) __name.c_str();
										Logger::getLogger()->note("WidgetFunction:: LUA function " + toString(reinterpret_cast<char*>(data)) + " published");}

private:

	WidgetFunction();


	static void __f(void* data) {
		char* cTemp = reinterpret_cast<char*>(data);
#ifndef NDEBUG
		cout << "FUNCION => " << cTemp << endl;
#endif
		try{
			luabind::call_function<void>( Configuration::getConfiguration()->lState , cTemp );
		} catch (...) {
			Logger::getInstance()->warning("Error calling lua function:: " + toString(cTemp) + " : does it exist?");
		}
	}

	std::string __name;
	void (*f) (void*);
	void* data;

	friend class RenderableWidget;
	friend class RenderableWidgetList;
};

class RenderableWidget: public mars::Renderable {
public:
	/**
	 * Sets the widgets heigh
	 *
	 * @param h Height
	 */
	void setHeight(const unsigned& h);

	/**
	 * Returns the height of the widget
	 *
	 */
	const unsigned & getHeight() const;

	/**
	 * Sets the width
	 *
	 * @param w width
	 */
	void setWidth(const unsigned& w);

	/**
	 *
	 * Returns the width
	 *
	 */
	const unsigned & getWidth() const;

	bool isSelected() const;

	/**
	 * Selects this widget
	 */
	void select();

	/**
	 *
	 * Unselects this widget
	 *
	 */
	void unSelect();


	/**
	 *
	 * Handles an SDL event
	 *
	 *
	 * @param event SDL_Event
	 *
	 */
	void handleEvent(SDL_Event event);


	/**
	 * Publishes an event to be handled
	 * @param eType event type
	 * @param fptr a pointer to the function that will be executed when this event is triggered
	 * @param data a pointer to the data that will be used as the function parameter
	 * @param k (Optional) the key corresponding to a KEY event.
	 *
	 * @see wEventType
	 */
	void publishHandledEvent(wEventType eType, void (*fptr) (void*), void* data, SDLKey k=SDLK_LAST);


	/**
	 * Publishes LUA event
	 * @param type name of the event type
	 * @param func name of the LUA function to call
	 */
	void publishHandledEventLUA(std::string type,std::string func);




	/**
	 *
	 * Draws (x,y) is the center.
	 *
	 */
	virtual void draw()=0;

protected:

	virtual void handleMouseMotion(int x, int y) = 0;
	virtual void handleMouseClick(int button, int x, int y) = 0;
	virtual void handleMouseRelease(int button, int x, int y) = 0;
	void handleKey(SDLKey k) { executeKeyEvent(k); }

	/**
	 * Default constructor.
	 *
	 * Creates a Widget of w x h pixels
	 *
	 */
	RenderableWidget(const unsigned & w=1, const unsigned & h=1);
	virtual ~RenderableWidget();

	bool __mouseButtonClicked[MAX_BUTTONS];

	bool anyButtonClicked() const;

	void executeClickEvents();
	void executeSelectEvents();
	void executeKeyEvent(SDLKey k);

	int __x1, __x2, __y1, __y2;

//	FTFont* __font;

	unsigned __height;
	unsigned __width;

private:



	void init();

	bool __selected;

	std::vector<WidgetFunction> __callBacksClick;
	std::vector<WidgetFunction> __callBacksSelect;
	std::map<SDLKey, WidgetFunction> __callBacksKey;





};

}

#endif /* RENDERABLEWIDGET_H_ */
