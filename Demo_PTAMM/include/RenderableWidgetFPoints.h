/*
 * RenderableWidgteFPoints.h
 *
 */

#ifndef RENDERABLEWIDGTEFPOINTS_H_
#define RENDERABLEWIDGTEFPOINTS_H_

#include "RenderableWidget.h"

#include "VideoOutputSDLOpenGL.h"

#include <vector>


namespace mars {

class Point2Di{
public:
	int x;
	int y;
	float size;

	Point2Di(int x_, int y_, float size_) { x = x_; y = y_; size = size_; }
};

class RenderableWidgetFPoints: public mars::RenderableWidget {
public:

	/**
	 * draws it
	 */
	void draw();

	/**
	 * Returns the color of the feature points
	 */
    GLfloat* getColor() { return __color; }

    /**
     * Return the size of the points
     */
    GLfloat getSize() const{ return __size; }

    /**
     * Sets the color of the points
     */
    void setColor(GLfloat r, GLfloat g, GLfloat b, GLfloat a){
    	__color[0] = r; __color[1] = g; __color[2] = b; __color[3] = a; }

    /**
     * Sets the size of the points
     */
    void setSize(GLfloat size){ __size = size; }

    /**
     * Clear all the points
     */
    void clearPoints() { __points.clear(); }

    /**
     * Adds a point
     */
    void addPoint(int x, int y, float size) { __points.push_back(Point2Di(x, y, size*20)); }


private:

	GLfloat __size;
	GLfloat __color[4];

	vector<Point2Di> __points;

	void handleMouseMotion(int x, int y) {}
	void handleMouseClick(int button, int x, int y){}
	void handleMouseRelease(int button, int x, int y){}


	RenderableWidgetFPoints();
	virtual ~RenderableWidgetFPoints();

	friend class TrackingMethodORelativePixelFlow;


};

}

#endif /* RENDERABLEWIDGTEFPOINTS_H_ */
