/*
 * RenderableWidgetButton.h
 *
 */

#ifndef RENDERABLEWIDGETBUTTON_H_
#define RENDERABLEWIDGETBUTTON_H_

#include <string>
#include <GL/gl.h>

#include "RenderableWidget.h"
#include "Texture.h"
#include "TextureFactory.h"
#include "Configuration.h"
#include "Logger.h"
#include "FontFactory.h"
#include "VideoOutputSDLOpenGL.h"
#include "RenderableText.h"

#include "SoundTool.h"


namespace mars {

class RenderableWidgetButton: public mars::RenderableWidget {
public:

	/**
	 * Sets the button's text
	 */
	void setText(const std::string& text);

	/**
	 * Gets the current button's text
	 */
	const std::string& getText() const;


	/**
	 * Draws it
	 */
	void draw();

	/**
	 * Should MARS try to autoscale proportions
	 *
	 */
	void useAutoScale(bool b = false);

	/**
	 * Makes the button to use a custom texture file
	 * @param name name of the file containing the texture image
	 */
	void setTexture(const string & name);


	void textColorSelected(const float& r, const float& g, const float& b, const float& a = 1.0);
	void textColorNotSelected(const float& r, const float& g, const float& b, const float& a = 1.0);
	void textColorClicked(const float& r, const float& g, const float& b, const float& a = 1.0);

	void setTextSize(uint size);

private:

	RenderableWidgetButton();
	RenderableWidgetButton(const unsigned & w , const unsigned & h);

	virtual ~RenderableWidgetButton();

	void handleMouseMotion(int x, int y);
	void handleMouseClick(int button, int x, int y);
	void handleMouseRelease(int button, int x, int y);

	void drawBackground();
	void drawText() const;

	void initButton();

	std::string __text;

	bool __hasTexture;

	Texture* __texture;

	RenderableText* __font;

	float __colorSelected[4];
	float __colorNotSelected[4];
	float __colorClicked[4];

	int __margin;

	bool __autoScale;

	uint __size;

	friend class RenderableWidgetList;
	friend class RenderableConstructor;

};

}

#endif /* RENDERABLEWIDGETBUTTON_H_ */
