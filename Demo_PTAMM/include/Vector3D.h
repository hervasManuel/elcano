/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * Vector3D.h
 *
 */

#ifndef VECTOR3D_H_
#define VECTOR3D_H_

#include <cmath>
#include <iostream>

namespace mars {

const float Pi = 3.14159265;

class Vector3D {
public:
	Vector3D();

	Vector3D(const float& x_, const float& y_, const float& z_);

	void setComponets(const float& x_, const float& y_, const float& z_ );

	Vector3D operator+(const Vector3D& other);
	Vector3D operator-(const Vector3D& other);
	Vector3D operator*(const float& scalar);

	Vector3D rotate(float* m);

	float operator*(const Vector3D& other);

	bool operator==(const Vector3D& v);

	void normalize();

	float lenght();


	/**
	 * Returns the cross product of the two given vectors.
	 */
	static Vector3D crossProduct(const Vector3D& v, const Vector3D& w);

	/**
	 * Return angle between two vectors
	 */
	static float angleBetween(const Vector3D& v, const Vector3D& w);

	virtual ~Vector3D();

	friend std::ostream& operator<<(std::ostream& os, const Vector3D& v);

	// 3 coords ;)
	float x;
	float y;
	float z;


};

typedef Vector3D Vertex;
typedef Vector3D Point3D;

}

#endif /* VECTOR3D_H_ */
