/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/* This is the base class for a tracking method */

/*
 * TrackingMethodO.h
 *
 */

#ifndef TRACKINGMETHODO_H_
#define TRACKINGMETHODO_H_

#include "FramesBuffer.h"
#include "VideoCapture.h"

#include "TrackingMethod.h"

#include <iostream>

#include <vector>
#include <map>

#include <string>
#include <pthread.h>

#include "Logger.h"

namespace mars {

/**
 * This is the base class for a Tracking Method
 *
 */
class TrackingMethodO: public TrackingMethod {

public:
	TrackingMethodO(const unsigned int& nRecords);
	void addVideoSource(VideoSource* vS);
	VideoSource* getVideoSource(unsigned int n);
	virtual ~TrackingMethodO();


protected:

	TrackingMethodO();

	vector<VideoSource*> __vSources; ///< Associated VideoSources


};

}

#endif /* TRACKINGMETHOD_H_ */
