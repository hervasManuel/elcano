/*
 * SoundFactory.h
 *
 */

#ifndef SOUNDFACTORY_H_
#define SOUNDFACTORY_H_

#include "Uncopyable.h"
#include "Logger.h"

#include <memory>
#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>

#include <festival/festival.h>


#include "Configuration.h"

#include <map>
#include <string>



namespace mars {

class SoundFactory  : public Singleton<SoundFactory> {
public:
	SoundFactory();
	virtual ~SoundFactory();

	static void playSound(Mix_Music* m);

	Mix_Music* getSound(const std::string& name);

	static void createWAVEfromText(std::string name, std::string text, bool wfs=false);

	bool audioAvailable();

private:
	SDL_AudioSpec __audioFormat;
	bool __audioAvailable;

	std::map<string, Mix_Music*> __sounds;

	friend class Singleton<SoundFactory>;

};

}

#endif /* SOUNDFACTORY_H_ */
