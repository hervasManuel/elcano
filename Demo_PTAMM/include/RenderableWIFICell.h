/*
 * RenderableWIFICell.h
 *
 */

#ifndef RENDERABLEWIFICELL_H_
#define RENDERABLEWIFICELL_H_

#include "Renderable.h"
#include "World.h"
#include <GL/glu.h>

namespace mars {

/**
 * This class represents a WIFI Cell. It's graphically represented a cylinder.
 */
class RenderableWIFICell: public mars::Renderable {
public:



	/**
	 *
	 */
	void draw();

	/**
	 * Returns the height
	 */
    double getHeight() const;

	/**
	 * Returns the radius
	 */
    double getRadius() const;

    /**
     * Sets the height
     */
    void setHeight(double __height);

    /**
     * Sets the radius
     */
    void setRadius(double __radius);

private:

	RenderableWIFICell(const double& radius);

	virtual ~RenderableWIFICell();

	void init();

	RenderableWIFICell();

	double __radius;
	double __height;

	GLUquadricObj* __cylinder;

	friend class RenderableConstructor;
};

}

#endif /* RENDERABLEWIFICELL_H_ */
