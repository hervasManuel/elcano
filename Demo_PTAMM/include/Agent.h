/*
 * Agent.h
 *
 *
 *  Agent for the connection to the MySQL server. It implements the singleton pattern.
 * 	It keeps a maximum of 100 connections
 */

#ifndef AGENT_H_
#define AGENT_H_

#include <mysql/mysql.h>
#include <mysql_connection.h>
#include <mysql_driver.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <GL/glut.h>
#include <vector>


#include <opencv/cv.h>
#include <opencv/highgui.h>

#define def_url "tcp://127.0.0.1:3306"

#define def_user_name "root" /*user name (default =your login name)*/

#define def_password "1234" /*password (default =none)*/

#define def_schema "arges"

class Agent {

	private:

			static Agent* instance;
			std::vector<sql::Connection*> connections;

	protected:

			Agent();


	public:

			/**
			 * Static method that returns the Agent instance.
			 * If it's not created, it will be created.
			 *
			 * */

			static Agent* getAgent();

			/**
			 * It returns a connection that is not being used.
			 *
			 * */

			sql::Connection* getConnection();

};

#endif /* AGENT_H_ */
