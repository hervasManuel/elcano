/*
 * Mesh.h
 *
 */

#ifndef MESH_H_
#define MESH_H_

#include "Vector3D.h"
#include "Logger.h"


#include <vector>
#include <string>
#include <iostream>

using namespace std;

namespace mars {

enum type {TRI, POLY}; //< enum for the mesh type

#define DELTA_ANGLE 10 //< DEPRECATED

/**
 * Stores a Texture Coordinate
 */
struct TexCoord{
	float s; //< the s (x axis) coord [0,1]
	float t; //< the t (y axis) coord [0,1]
};


/**
 * Stores a Single Face Info
 */
struct Face{

	float    normal[3]; //< Stores the normal of the face
	unsigned vertex_count; //< Number of vertex of this face

	vector<int> vertexes; //<< Index of the vertexes of this faces. 3, 4, 7, 5 means that the vertexes located at those positions are used.
	vector<TexCoord*> texCoords; //< Vector of texCoords.

	~Face(){ //< Destructor, cleans the memory
		for (unsigned i=0; i < texCoords.size(); i++)
			delete texCoords.at(i);
	}

};

/**
 * Stores an edge, DEPRECATED.
 */
struct Edge{
	Point3D p1;
	Point3D p2;
	unsigned nFace;
	Vector3D normal;
};

/**
 * Stores a rotation
 */
struct Rotation{
	float x;
	float y;
	float z;

	float r;
};

/**
 * Stores a scale
 */
struct Scale{
	float x;
	float y;
	float z;
};


/**
 * Base class for a mesh
 */
class Mesh {
public:

	/**
	 * Default constructor
	 */
	Mesh();

	/**
	 * Default Destructor
	 */
	virtual ~Mesh();

	/**
	 * Sets the name of the mesh
	 */
	void setName(const char* name);

	/**
	 * Sets the initial translation associated to this mesh
	 *
	 * @param x x coord
	 * @param y y coord
	 * @param z z coord
	 */
	void setInitialTranslation(const float& x, const float& y, const float& z);

	/**
	 * Reserves memory for the faces and vertexes.
	 *
	 * @param x x coord
	 * @param y y coord
	 * @param z z coord
	 */
	void reserveMemForVectors(unsigned nVerts, unsigned nFaces);

	/**
	 * Adds a vertex to the mesh using three floats
	 *
	 * @param x x coord
	 * @param y y coord
	 * @param z z coord
	 */
	void addVertex(const float& x, const float& y, const float& z);


	/**
	 * Returns the initial translation as a point to the 3 coords.
	 */
	float* getInitialTranslation();

	/**
	 * Adds a vertex
	 *
	 * @param v vertex
	 */
	void addVertex(Vertex* v);

	/**
	 * Adds a face
	 *
	 * @param face a face
	 */
	void addFace(Face* face);

	/**
	 * Adds a rotation structure for the mesh
	 *
	 * @param r a rotation structure
	 * @see Rotation
	 */
	void addRotation(Rotation* r);

	/**
	 * Adds a scale
	 *
	 * @param s scale structure
	 * @see Scale
	 *
	 */
	void addScale(Scale* s);

	/**
	 * Returns the number of faces of the mesh
	 */
	unsigned getCountOfFaces();

	/**
	 * Return the numbers of rotations
	 */
	unsigned getCountOfRotations();

	/**
	 * Return the number of Scales
	 */
	unsigned getCountOfScales();

	/**
	 * Return the number of wires, DEPRECATED
	 */
	unsigned getCountOfWires();

	/**
	 * Returns a wire, DEPRECATED
	 *
	 * @param n number of wire to return
	 */
	Edge* getWire(unsigned n);

	/**
	 * Returns a scale structure
	 *
	 * @param nScale number of scale to return
	 * @see Scale
	 */
	Scale* getScale(unsigned nScale);

	/**
	 * Returns a rotation structure
	 *
	 * @param nRot the number of rotation
	 * @see Rotation
	 */
	Rotation* getRotation(unsigned nRot);

	/**
	 * Returns a vertex
	 *
	 * @param nVertex number of vertex
	 */
	Vertex* getVertex(unsigned nVertex);

	/**
	 * Return a vertex of a given face
	 *
	 * @param nFace number of face
	 * @param nVertex number of vertex of that face
	 */
	Vertex* getVertexOfFace(unsigned nFace, unsigned nVertex);

	/**
	 * Returns a face
	 *
	 * @param nFace number of face
	 */
	Face* getFace(unsigned nFace);

	/**
	 * Return the mesh name
	 */
	string getName();

	/**
	 * Returns the type of the mesh
	 */
	unsigned getType();

	/**
	 * Prepare the mesh for a wireframe based on edges, DEPRECATED
	 */
	void prepareWireFrame();

	/**
	 * Returns true if the mesh has texture info
	 */
	bool hasTexCoords();

	/**
	 * Sets the mesh to a textured one
	 */
	void setHasTexCoords();


protected:

	void generateEdges(); //< DEPRECATED
	void removeDoubleEdges(); //< DEPRECATED
	bool simplifyEdges(vector<unsigned> v, Vector3D& normal); //< DEPRECATED

	vector<Face*>   __faces;      ///< Faces
	vector<Vertex*> __vertexes;   ///< Vertexes
	vector<Vertex*> __normals;    ///< Normals

	vector<Edge*> __edges; ///< DEPRECATED
	vector<Edge*> __rEdges; ///< DEPRECATED
	vector<Edge*> __sEdges; ///< DEPRECATED

	vector<Rotation*> __initialRotations; ///< vector with the initial rotations
	vector<Scale*> __initialScales; ///< vector with the initial scales

	unsigned __type; ///< Type of mesh

	float __initialTranslation[3]; ///< the initial translation of the mesh

	bool __hasTexCoords; ///< Has it texture coordinates?

	std::string __name; ///< Mesh's name
};

}

#endif /* MESH_H_ */
