/*
 * VideoDeviceUEye.h
 *
 */

#ifndef VIDEODEVICEUEYE_H_
#define VIDEODEVICEUEYE_H_

#include <VideoDevice.h>
#include <string>

#include <Configuration.h>

#include <uEye.h>
#include <Logger.h>
#include <Tools.h>

#include <Logger.h>


namespace mars {

/**
 * Represents a video device for the uEye cameras
 */
class VideoDeviceUEye: public mars::VideoDevice {
public:

	VideoDeviceUEye(UEYE_CAMERA_INFO cameraInfo, std::string name);
	VideoDeviceUEye(UEYE_CAMERA_INFO cameraInfo);
	virtual ~VideoDeviceUEye();

	/**
	 * Return the list of uEye cameras connected to this computer
	 */
	static PUEYE_CAMERA_LIST getUEyeCameraList();

protected:
	void ThreadCaptureFrame();

private:
	//VideoDeviceUEye();
	void init();

	UEYE_CAMERA_INFO __cameraInfo;

	HIDS __hCam;
        
        IS_RECT rectAOI;
        
	int __maxW;
	int __maxH;

	int __bitsPerPixel;
	int __colorMode;

	char* __imageMem;
	int __memId;

};

}

#endif /* VIDEODEVICEUEYE_H_ */
