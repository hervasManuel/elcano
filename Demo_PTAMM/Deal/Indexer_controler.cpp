#include <Indexer_controler.h>
#include <cstdio>
#include <dirent.h>
#include <string.h>
#include <algorithm>

using namespace std;


Indexer_controler::Indexer_controler(){

	//Init DAO

	try {
		idao= new ImageDAO();
		int empty= idao->selectn("SELECT COUNT(*) FROM Image");

		if (empty==0)
			cout << "The data base is empty" << endl;

	} catch (exception *e) {
		// TODO Auto-generated catch block
		cout << "Exception "  << endl;
	}
}

void Indexer_controler::index(String path){

	bool ok_file;
	int countindex=0;

	//The path is opened and the files in this path are checked

	DIR *dip;
	struct dirent *dit;
	char *pathchar;
	pathchar = (char *) path.c_str();
	vector<String> files;
	if ((dip = opendir(pathchar)) != NULL)
	{
		int i=0;
		while ((dit = readdir(dip)) != NULL)
		{
			i++;
			files.push_back(dit->d_name);
		}
		cout << i << " files found" << endl;
	}


	/*
	 * For each file read, it has to check if they're images
	 */
	for (vector<String>::iterator itFiles = files.begin() ; itFiles != files.end(); itFiles++) {
		ok_file= true;

		string identMin = *itFiles;
		std::transform(identMin.begin(), identMin.end(), identMin.begin(), (int( * )(int))std::tolower);

		//If the file is an image...
		if(identMin.length()>4 && (identMin.substr(identMin.length()-4,4)==".jpg" || identMin.substr(identMin.length()-4,4)==".png" ||
				identMin.substr(identMin.length()-5,5)==".jpeg" || identMin.substr(identMin.length()-4,4)==".bmp" )){

			cout << "Indexing image " << *itFiles << endl;

			//The image is read

			try {
				cv::Mat matFrame = cv::imread(path + *itFiles);
				//Frame *image = new Frame(matFrame,true,false,false);

			} catch (exception &e){
				cout << "Exception, file not found " << e.what() << endl;
				ok_file= false;
			}


			//If the file is right, descriptors will be searched
			if(ok_file){

				Image *img = new Image(*itFiles, path);
				idao->insertImage(img);
				countindex++;

				if(countindex>0)
					cout << countindex << " images have been indexed in the data base" << endl;
				}
				else{
					cout << "The directory is not correct" << endl;
					exit(0);
				}
			}
	}

}

vector<Image*> Indexer_controler::getIndex(){
	vector<Image*> images_array;
	try {
		images_array = idao->select("SELECT * FROM Image");
	} catch (exception *e) {
		// TODO Auto-generated catch block
		cout << e->what() << endl;
	}
	return images_array;
}

bool Indexer_controler::deleteBD (){
	try {
		idao->deleteAll();
		cout << "Database deleted" << endl;
		return true;
	} catch (exception *e) {
		cout << e->what() << endl;
		return false;
	}
}
