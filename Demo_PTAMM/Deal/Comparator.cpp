

#include <Comparator.h>

	Comparator::Comparator(){
		weightcld = 0.3;
		weightehd = 0.2;
		weightscd = 0.5;
	}
   /**
    * It calculate the distance between 2 images taking the comparators of CLD and EHD
    * given certain weights.
    *
    * a -> First image
    * b -> Second image
    * weightcld -> Weight of the CLD descriptor
    * weightehd -> Weight of the EHD descriptor
    * weightscd -> Weight of the SCD descriptor
    *
    * res -> Distance between [0, 1]
    */

   double Comparator::compareImages(Image *a, Image *b){
	   return (a->getCLD().distanceNorm(&b->getCLD())* weightcld) +
			   (a->getEHD().distanceNorm(&b->getEHD())* weightehd) +
			   (a->getSCD().distanceNorm(&b->getSCD())* weightscd);
   }
