#include <VoronoiTree.h>
#include <iostream>

int VoronoiTree::calculateMinPointDistance(Image *image, vector<Image*> images){
	double min= 100000000000.0;
	int nDi= -1;
	Comparator *c = new Comparator();
	for(int i=0; i< images.size(); i++){
		double diff = c->compareImages(image, images.at(i));
		if(diff<min){
			min= diff;
			nDi= i;
		}
	}
	return nDi;
}

void VoronoiTree::buildSons(vector<Image*> images){
	vector<vector<Image*> > set_images;

	for(int i=0; i< k; i++){
		vector<Image*> imagesI;
		imagesI.push_back(root->getPi().at(i));
		set_images.push_back(imagesI);
	}

	while (images.size()>0){
		int PiMinDistance= calculateMinPointDistance(images.at(0), root->getPi());
		set_images.at(PiMinDistance).push_back(images.at(0));
		images.erase(images.begin());

	}


	for(int i=0; i<k; i++){
		VoronoiTree *vT = new VoronoiTree(k, root->getPi().at(i), set_images.at(i), root->getR_c());
		Di.push_back(vT);
	}
}

int VoronoiTree::maxdispersion(int num_pi, int k, vector<Image*> set_image, vector<Image*> pdi){
	double diff;
	double maxx= -5;
	int pivot = 0;
	Comparator *c = new Comparator();
	if(num_pi==0){
		double rand =  (double)random()/RAND_MAX;
		return (int)set_image.size()*rand;//To make max dispersion
	}
	else{
		for(int i= 0; i<set_image.size(); i++){
			diff=0.0;
			for(int j=0; j<pdi.size(); j++){
				diff+= c->compareImages(set_image.at(i), pdi.at(j));
			}
			if(diff> maxx){
				pivot= i;
				maxx= diff;
			}
		}
	}
	return pivot;
}

VoronoiTree::VoronoiTree(int k, vector<Image*> images){
	//Root is calculated
	this->k=k;
	vector<Image*> aux;
	aux = images;
	vector<Image*> pi;

	for(int i=0; i< k; i++){
		int pivot = maxdispersion(i, k, aux, pi);
		pi.push_back(aux.at(pivot));
		aux.erase(aux.begin()+pivot);
	}
	root = new VoronoiNode(pi, images);

	buildSons(aux);
}

VoronoiTree::VoronoiTree(int k, Image *main, vector<Image*> images, double rc_c){
	if(images.size()>= k){
		//Root is calculated
		this->k=k;
		vector<Image*> aux;
		aux = images;
		vector<Image*> pi;

		for(int i=0; i< k; i++){
			int pivot= maxdispersion(i, k, aux, pi);
			pi.push_back(aux.at(pivot));
			aux.erase(aux.begin()+pivot);
		}
		root= new VoronoiNode(pi, main, images);

		buildSons(aux);
	}
	else
		root= new VoronoiNode(rc_c, main, images);
}

vector<VoronoiTree*> VoronoiTree::getDi() {
		return Di;
}

VoronoiNode* VoronoiTree::getRoot() {
		return root;
}

void VoronoiTree::setRaiz(VoronoiNode *root) {
		this->root = root;
}

String VoronoiTree::toString(){
	std::ostringstream message;
	message << "Root: \n" << root;
	if(!Di.empty()){
		message << "Sons: \n";
		for(int i=0; i<Di.size(); i++){
			message << "son: " << i << "\n" << Di.at(i)->getRoot()->toString() << "\n";
		}

	}
	else{
		message << "It haven't got sons";
	}
	return message.str();
}

vector<Image*> VoronoiTree::searchVTree(double similarity, Image *a){
	//We see in the main nodes
	vector<Image*> images;
	Comparator *c = new Comparator();
	double dist = 0.0;
      

	if(!Di.empty()){
          
		for(int i=0; i< Di.size(); i++){
			//If the comparison with the main image - r_c < similar, it isn't reject (d(a, pi))
			dist = c->compareImages(a, Di.at(i)->getRoot()->getP());
			for(int j=0;j< 80 ; j++)
				Di.at(i)->getRoot()->getP()->getEHD().GetEdgeHistogramElement()[j];
			if(dist - Di.at(i)->getRoot()->getR_c() < similarity){
				//We search in the k tree level the distance to splits
				if(dist < similarity){
					if(Di.at(i)->getDi().empty()){
						for(int j=0; j<Di.at(i)->getRoot()->getImagesPlaneP().size(); j++){
							if(c->compareImages(a, Di.at(i)->getRoot()->getImagesPlaneP().at(j))< similarity){
								images.push_back(Di.at(i)->getRoot()->getImagesPlaneP().at(j));
							}
						}
					}

					vector<Image*> ims = Di.at(i)->searchVTree(similarity,a);
					for(vector<Image*>::iterator it = ims.begin(); it!=ims.end(); it++)
						images.push_back(*it);

				}

			}
		}
	}
	return images;
}

