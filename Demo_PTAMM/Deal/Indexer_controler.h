/*
 * Indexer_controler.h
 *
 */

#ifndef INDEXER_CONTROLER_H_
#define INDEXER_CONTROLER_H_

#include <ImageDAO.h>

class Indexer_controler {
	private:

		ImageDAO *idao;

	public:

		/**
		 * Standard constructor of the class. It initializes the DAO and checks if the connection is
		 * correct.
		 */

		Indexer_controler();

		/**
		 * Index method. It receives the path of the images to be indexed.
		 * 1. To open the directory.
		 * 2. To obtain the files.
		 * 3. To check if the files are images.
		 * 4. To insert in the data base.
		 *
		 */

		void index(String path);

		/**
		 * getIndex. It obtains all the images from the data base and they're returned in a
		 * ArrayList
		 *
		 */
		vector<Image*> getIndex();

		/**
		 * deleteBD. It deletes all the information from the data base
		 */
		bool deleteBD ();
};

#endif /* INDEXER_CONTROLER_H_ */
