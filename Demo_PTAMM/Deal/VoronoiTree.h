/*
 * VoronoiTree.h
 *
 */

#ifndef VORONOITREE_H_
#define VORONOITREE_H_

#include <highgui.h>
#include <VoronoiNode.h>

class VoronoiTree{
	private:
			VoronoiNode *root;
			int k;
			vector<VoronoiTree*> Di;

			void buildSons(vector<Image*> images);

			int calculateMinPointDistance(Image *image, vector<Image*> images);

			int maxdispersion(int num_pi, int k, vector<Image*> set_image, vector<Image*> pdi);


	public:
			VoronoiTree(int k, vector<Image*> images);

			VoronoiTree(int k, Image *main, vector<Image*> images, double rc_c);

			vector<VoronoiTree*> getDi();

			VoronoiNode* getRoot();

			void setRaiz(VoronoiNode *root);

			String toString();

			vector<Image*> searchVTree(double similarity, Image *a);

};

#endif /* VORONOITREE_H_ */
