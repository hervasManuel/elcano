/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/* 
   Lista los dispositivos de captura V4L2
*/

#include "VideoEnumerate.h"

#include <linux/videodev2.h>
#include <sys/ioctl.h>
#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <vector>
#include <string>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <algorithm>

using namespace std;

typedef struct v4l2_capability sVideo;

namespace mars{

/// Devuelve una lista de cadenas con los dispositivos V4L2
vector<string> getDevVideoNames(){

	vector<string> vVideoDevices;
	string name;
	DIR *dir;
	struct dirent *ent;
	size_t ocurr;


	dir = opendir ("/dev/");
	if (dir != NULL) {
		while ((ent = readdir (dir)) != NULL) {
			name = ent->d_name;
			ocurr = name.find("video");
			if (!ocurr) {
				vVideoDevices.push_back("/dev/"+name);
#ifndef NDEBUG
				Logger::getLogger()->note("Found v4l2 device -> /dev/" + name);
#endif
			}
		}
		closedir (dir);
	} else {
		Logger::getLogger()->error("VideoEnumerate:: Listing /dev/");
		return vVideoDevices; // Vector con 0 elementos.
	}

	return vVideoDevices;

}



void printDevices(){

	int fp;
	vector<string>::iterator it;

	sVideo v; // estructura para recoger las caracteristicas del dispositivo

	vector<string> vDevices = getDevVideoNames();

	for( it = vDevices.begin(); it != vDevices.end(); it++ ){
		cout << "---------------------------" << endl;
		cout << "Consultando el dispositivo " << *it << endl;
		fp = open(it->c_str(),O_RDONLY);
		if (fp == -1){
			cout << "Error al abrir el fichero: " << *it << endl;
			exit(-1);
		}
		ioctl(fp, VIDIOC_QUERYCAP, &v);
		cout << "[*] DRIVER: " << v.driver << endl;
		cout << "[*] CARD: " << v.driver << endl;
		cout << "[*] CAPABILITIES:" << v.capabilities << endl;
		if  (v.capabilities & 0x00000001)
			cout << "  -> Video Capture" << endl;
		if  (v.capabilities & 0x00000002)
			cout << "  -> Video Output" << endl;
		cout << "---------------------------" << endl;
		close(fp);
	}

}

/// Devuelve un vector de cadenas con los dispositivos de V4L que pueden capturar video.
vector<string> getDevVideoCaptureNames(){

	int fp;
	vector<string>::iterator it;

	sVideo v; // estructura para recoger las caracteristicas del dispositivo

	vector<string> vDevices = getDevVideoNames();
	vector<string> vCaptureDevices;

	for( it = vDevices.begin(); it != vDevices.end(); it++ ){


#ifndef NDEBUG
		cout << "---------------------------" << endl;
		cout << "Asking Device ... " << *it << endl;
#endif

		fp = open(it->c_str(),O_RDONLY);

		if (fp == -1){
			Logger::getLogger()->error("VideoEnumerate:: Opening " + *it);
			exit(-1);
		}

		ioctl(fp, VIDIOC_QUERYCAP, &v);

		if  (v.capabilities & 0x00000001){
#ifndef NDEBUG
			cout << "Device " << *it << " is able to capture video." << endl;
#endif
			vCaptureDevices.push_back(*it);
		}
		close(fp);
	}

	sort(vCaptureDevices.begin(),vCaptureDevices.end());

	return vCaptureDevices;

}

}
