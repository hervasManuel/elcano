/*
 * ARTKMark.cpp
 *
 */

#include "ARTKMark.h"


namespace mars {

ARTKMark::ARTKMark() { 
  __type=POSITION_MARK;
  __size = 1; 
  __detected=false;

 }

  //ARTKMark::ARTKMark(const ARTKMark& obj){
  // cout<<"LLAMANDO AL CONSTRUCTOR DE COPIA WIIII"<<endl;

//   __detected=obj.isDetected();
//   __id=obj.getId();
//   __foundId=obj.getFoundId();

//   __pMatrix= obj.getPosMatrix()->clone();


//   __rMatrix=new cv::Mat(obj.getRelativeMatrix()->clone());

//   setFile(obj.getFile());
//   __type=obj.getType();
//   __size=obj.getSize();
//   __center[0]=obj.getCenter()[0];
//   __center[1]=obj.getCenter()[1];


//}

ARTKMark::ARTKMark(Matrix16& m, std::string file, float size, int type){  
  __type=type; 
  
  setFile(file);
  __size = size;
  __detected=false;

  //Initialize matrix
  __rMatrix=cv::Mat(4,4,CV_32F,cv::Scalar(0));
  __rMatrix.at<float>(0,0)=1.;
  __rMatrix.at<float>(1,1)=1.;
  __rMatrix.at<float>(2,2)=1.;
  __rMatrix.at<float>(3,3)=1.;
  
  if((__id=arLoadPatt(__file.c_str()))==-1){
  Logger::getLogger()->error("TrackingMethodOAbsoluteArtkBz: Error loading the pattern ");
  }

  __pMatrix =(cv::Mat(cv::Size(4,4), CV_32F, m.getData())).clone(); 
    
}

ARTKMark::ARTKMark(std::string file, float size,int type){ 

     __type=type; 
      
    //Initialize relative matrix
    __rMatrix=cv::Mat(4,4,CV_32F,cv::Scalar(0));
    __rMatrix.at<float>(0,0)=1.;
    __rMatrix.at<float>(1,1)=1.;
    __rMatrix.at<float>(2,2)=1.;
    __rMatrix.at<float>(3,3)=1.;

    setFile(file); 
    __size = size; 
    __detected=false;

    if((__id=arLoadPatt(__file.c_str()))==-1){
      Logger::getLogger()->error("TrackingMethodOAbsoluteARTK: Error loading the pattern ");
    }    
}

float ARTKMark::getEval(float distImp, float angImp){
  float eval;
  
  //-Calculate the distance--------
  cv::Mat distVector(1,4,CV_32F,cv::Scalar(0));
  float distance;
  distVector.at<float>(0,0)=__rMatrix.at<float>(3,0);
  distVector.at<float>(0,1)=__rMatrix.at<float>(3,1);
  distVector.at<float>(0,2)=__rMatrix.at<float>(3,2);
  distVector.at<float>(0,3)=1;
  
  distance=sqrt(pow(distVector.at<float>(0,0),2)+
		pow(distVector.at<float>(0,1),2)+
		pow(distVector.at<float>(0,2),2));
  
  //-Calculate the angle-----------
  cv::Mat knownVector(1,4,CV_32F,cv::Scalar(0));
  knownVector.at<float>(0,2)=1;
  
  cv::Mat resultVector;
  float angle;  
  resultVector=knownVector*(__rMatrix);
  angle=acos(resultVector.at<float>(0,2)*knownVector.at<float>(0,2));
  
  
  /* Calculate the eval factor-------
     Angle: between 0-1:

     angle - PI/2       
     ang_fac = --------------- = 2*angle/PI - 1
     PI/2
     Dist: calculate a value between 0 and 1!
  */
  
  eval=1/distance*distImp +  angImp*(2*angle/3.1415-1);
  
  return eval;
}

int ARTKMark::getId(){ return __id;}


void ARTKMark::setRelativeMatrix(cv::Mat& m){
  for(unsigned int i=0;i<4;i++)
    for(unsigned int j=0;j<4;j++)
      __rMatrix.at<float>(i,j)=m.at<float>(i,j);

}

// void ARTKMark::setRelativeMatrix(Matrix16& m){
// for(unsigned int i=0;i<4;i++)
//     for(unsigned int j=0;j<4;j++)
//       __rMatrix->at<float>(i,j)=m.getData()[j*4+i];
// }

  
void ARTKMark::setFile(std::string file){__file = file;}

std::string ARTKMark::getFile(){ return __file; }

cv::Mat* ARTKMark::getPosMatrix(){ return &__pMatrix; }
 
int ARTKMark::getFoundId(){ return __foundId; }

void ARTKMark::setFoundId(int foundId){ __foundId=foundId;}

void ARTKMark::setDetected(bool detected){ __detected=detected;}

double* ARTKMark::getCenter(){return __center;}

  int ARTKMark::getType(){return __type;}

bool ARTKMark::isDetected(){return __detected;}

bool ARTKMark::isActuationMark(){return (__type & ACTUATION_MARK)==ACTUATION_MARK;}
  
bool ARTKMark::isPositionMark(){return (__type & POSITION_MARK)==POSITION_MARK;}

bool ARTKMark::isUserMark(){return (__type & USER_MARK)==USER_MARK;}

cv::Mat* ARTKMark::getRelativeMatrix(){ return &__rMatrix;}

// cv::Mat* ARTKMark::getGlobalMatrix(){ 

//   cv::Mat*  globalTrans=new cv::Mat( __rMatrix->inv() * __pMatrix);
  
//   return globalTrans;
// }


void ARTKMark::setSize(const float& s ){__size = s;}

float ARTKMark::getSize(){return __size;}


  /*TO DEPRECATE functions*/

  float* ARTKMark::Mat2Gl(cv::Mat* mat){
	float *gl;

	gl=new float[16];
	for(uint i=0;i<4;i++){
		for(uint j=0;j<4;j++){
			gl[i*4+j]=mat->at<float>(i,j);
		}
	}
	return  gl;
}

cv::Mat* ARTKMark::Gl2Mat(float *gl){
  cv::Mat *mat;

  mat=new cv::Mat(4,4,CV_32F);
	for(uint i=0;i<4;i++){
		for(uint j=0;j<4;j++){
			mat->at<float>(i,j)=gl[i*4+j];
		}
	}
	return mat;
}

// void ARTKMark::changeMatrix(int axis, float desp, float rot){

//   float* glOldMatrix;
//   float glNewMatrix[16];
//   cv::Mat* oldMat;

//   //Obtain the glMatrix
//   glOldMatrix=Mat2Gl(&__pMatrix);

//   //Push the matrix in the OGL stack
//   glPushMatrix();
//   glLoadIdentity();
//   glLoadMatrixf(glOldMatrix);

//   if(axis==X_AXIS){
//     glTranslatef(desp,0,0);
//     glRotatef(rot,1,0,0);
//   }else if(axis==Y_AXIS){
//     glTranslatef(0,desp,0);
//     glRotatef(rot,0,1,0);
//   }else{
//     glTranslatef(0,0,desp);
//     glRotatef(rot,0,0,1);
//   }

//   //Obtain the matrix from the OGL stack
//   glGetFloatv(GL_MODELVIEW_MATRIX,glNewMatrix);
//   glPopMatrix();

//   //Push the new Mat in the ARTKMark and freeing memory
//   oldMat=&__pMatrix;
//   __pMatrix=*(Gl2Mat(glNewMatrix));


// }


ARTKMark::~ARTKMark() {}

}


