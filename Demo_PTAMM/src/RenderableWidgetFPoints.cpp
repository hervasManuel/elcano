/*
 * RenderableWidgteFPoints.cpp
 *
 */

#include "RenderableWidgetFPoints.h"

namespace mars {

RenderableWidgetFPoints::RenderableWidgetFPoints() {
	__size = 1;
	__color[0] = 0.3;
	__color[1] = 1;
	__color[2] = 0.3;
	__color[3] = 0.60;
}

RenderableWidgetFPoints::~RenderableWidgetFPoints() {

}


void RenderableWidgetFPoints::draw(){
	// Ortho proj.
	VideoOutputSDLOpenGL::getInstance()->setOrtho();
	//---------------------------------------------
	glPushMatrix();

	glDisable(GL_TEXTURE_2D);
	glDisable(GL_LIGHTING);
	glColor4fv(__color);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	for (uint c=0; c < __points.size(); ++c){
		glPointSize(__points[c].size * 0.01 );
		glBegin(GL_POINTS);
		glVertex2i(__points[c].x, __points[c].y);
		glEnd();
	}
	glDisable(GL_BLEND);
	glEnable(GL_TEXTURE_2D);
	if (World::getInstance()->isLightingEnabled())
		glEnable(GL_LIGHTING);
	glPopMatrix();
	//---------------------------------------------
	// View proj.
	VideoOutputSDLOpenGL::getInstance()->setView();
}

}
