/*
 * RenderableWIFICell.cpp
 *
 */

#include "RenderableWIFICell.h"

namespace mars {


RenderableWIFICell::RenderableWIFICell(): __radius(1.0), __height(1) {
	init();
}

RenderableWIFICell::RenderableWIFICell(const double& radius) : __radius(radius), __height(1) {
	init();
}

RenderableWIFICell::~RenderableWIFICell() {
	gluDeleteQuadric(__cylinder);
}

void RenderableWIFICell::init(){
	__cylinder =  gluNewQuadric();
	__rotX = 90;
}


void RenderableWIFICell::draw(){

	glPushMatrix();

	glDisable(GL_LIGHTING);

	glColor4f(1.0f, 1.0f, 1.0f, 0.5f);



	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glDisable(GL_TEXTURE_2D);


	computePositionRotation();


	#if 0
		Matrix16::printModelView();
	#endif

	gluCylinder(__cylinder , __radius, __radius, __height, 40, 40);

	if (World::getInstance()->isLightingEnabled())
		glEnable(GL_LIGHTING);

	glDisable(GL_BLEND);

	glPopMatrix();
}

inline double RenderableWIFICell::getHeight() const{
	return __height;
}

inline double RenderableWIFICell::getRadius() const{
	return __radius;
}

inline void RenderableWIFICell::setHeight(double __height){
	this->__height = __height;
}

inline void RenderableWIFICell::setRadius(double __radius){
	this->__radius = __radius;
}


}
