/*
 * RenderableWidgetList.cpp
 *
 */

#include "RenderableWidgetList.h"

namespace mars {

const float RWL_half_size = 0.5;
//const float RWL_width_portion = 0.2;
//const unsigned RWL_nVisibleOptions = 5;


RenderableWidgetList::RenderableWidgetList(const unsigned& w, const unsigned& h,
		const float& widthPortionUpDown, const unsigned& nVisibleOptions)
	: RenderableWidget(w, h) , __listPos(0), __totalOptions(0) {

	if (widthPortionUpDown < 0.0 || widthPortionUpDown > 0.5) {
		Logger::getInstance()->warning("RenderableWidgetList:: up/down portion should"
				" be in [0.0 - 0.5], you asked for " + toString(widthPortionUpDown) );
		__widthPortionUpDown = 0.2;
	} else {
		__widthPortionUpDown = widthPortionUpDown;
	}
	__nVisibleOptions = nVisibleOptions;

	__navButtonWidth = unsigned(float(w) * __widthPortionUpDown );

	__optionButtonHeight = unsigned(
			float(h)  / float(__nVisibleOptions)
			);

	__optionButtonWidth = unsigned(
			float(w - __navButtonWidth)
			);

	__w2 = unsigned(float(w) * RWL_half_size);
	__h2 = unsigned(float(h) * RWL_half_size);

	// Half of the width for each one, 20% of widget's height.
	__upButton = new RenderableWidgetButton(__navButtonWidth, __h2 );

	__downButton = new RenderableWidgetButton(__navButtonWidth, __h2 );

	__upButton->publishHandledEvent(CLICK, RenderableWidgetList::goUp, (void*) this);
	__downButton->publishHandledEvent(CLICK, RenderableWidgetList::goDown, (void*) this);

	__upButton->setTexture("up-button.png");
	__downButton->setTexture("down-button.png");


	World::getInstance()->addWidget(__upButton);
	World::getInstance()->addWidget(__downButton);

	__name = "WidgetList_Prototype";

}

void RenderableWidgetList::goUp(void* rWL){


	RenderableWidgetList* myThis = reinterpret_cast<RenderableWidgetList*>(rWL);

#ifndef NDEBUG
	cout << "Calling UP: " << myThis->__name << endl;
#endif

	// if there's no point of moving through the list ... return
	if ( (myThis->__totalOptions <= myThis->__nVisibleOptions) || (myThis->__listPos == 0) )
		return;

	// __totalOptions > RWL_nOptions
	if (  myThis->__listPos != 0  ){
		myThis->__listPos--;
		myThis->updateOptionButtons();
	}

}

void RenderableWidgetList::goDown(void* rWL){
	RenderableWidgetList* myThis = reinterpret_cast<RenderableWidgetList*>(rWL);

#ifndef NDEBUG
	cout << "Calling DOWN: " << myThis->__name << endl;
#endif

	int diff = myThis->__totalOptions - myThis->__nVisibleOptions;

	// if there's no point of moving through the list ... return
	if ( (diff <= 0) || (myThis->__listPos == uint(diff) ) )
		return;

	// __totalOptions > RWL_nOptions
	if (  myThis->__listPos != uint(diff)  ){
			myThis->__listPos++;
			myThis->updateOptionButtons();
	}

}


void RenderableWidgetList::updateOptionButtons(){

	v_it it;
	uint lPos = 0;
	for (it = __optionButtons.begin(); it != __optionButtons.end(); ++it){
		calculateVisibility(*it, lPos);
		++lPos;
	}

}

void RenderableWidgetList::handleMouseClick(int button, int x, int y){
	// 4 - wheel up
	// 5 - wheel down
#if 0
	cout << "---" << endl;
	cout << "bounds :: " << __x1 << " :: " << __x2 << " :: " << __y1 << " :: " << __y2 << endl;
#endif

	if (button >= MAX_BUTTONS) return;
	if (x > __x1 && x < __x2 && y > __y1 && y < __y2){
		__mouseButtonClicked[button] = true;
	}
}

void RenderableWidgetList::handleMouseRelease(int button, int x, int y){
	if (button >= MAX_BUTTONS) return;

	if (__mouseButtonClicked[4]){
		RenderableWidgetList::goUp(this);
	}
	if (__mouseButtonClicked[5]){
		RenderableWidgetList::goDown(this);
	}
	__mouseButtonClicked[button] = false;

}

void RenderableWidgetList::calculateVisibility(RenderableWidgetButton* rB,
		uint lPos){

	if ( (lPos < __listPos) || (lPos >= __listPos + __nVisibleOptions) ){
		rB->hide();
#if 0
		cout << "Hiding: " << lPos << endl;
#endif
	}
	else {
		rB->show();
		// If shown, calculate position...
		uint wPos = lPos - __listPos; // >= 0
#if 0
		cout << "Showing: " << lPos << endl;
		cout << "LISTPOS: " << __listPos << endl;
		cout << "Global Position : " << lPos
				<< " --> Render Order Position: " << wPos << endl;
#endif
		calculatePosition(rB, wPos);

	}
}

void RenderableWidgetList::calculatePosition(RenderableWidgetButton* rB,
		uint wPos){

	uint pX = __position.x - (__navButtonWidth * 0.5);
	uint pBeginY = (__position.y - __h2 + (__optionButtonHeight *0.5) );
	uint pY = pBeginY + (wPos * __optionButtonHeight);
	rB->setPosition(pX, pY);

}



void RenderableWidgetList::setPosition(const int& x, const int& y){

	__position.x = x;
	__position.y = y;

	uint pX = __position.x;
	uint pY = __position.y;

#ifndef NDEBUG   //----------------- DEBUG INFO -----------------------
	cout << " X:: " << pX << " - Y:: " << pY << endl;
#endif  //----------------- DEBUG INFO -----------------------

	__upButton->setPosition(pX + __w2 - (__navButtonWidth * 0.5),
			pY - float(__h2 * RWL_half_size));

	__downButton->setPosition(pX + __w2 - (__navButtonWidth * 0.5),
			pY + float(__h2 * RWL_half_size));

#ifndef NDEBUG	//----------------- DEBUG INFO -----------------------
	cout << "Navigation Buttons: " << endl;
	cout << " - up   : " << __upButton->getPosition().x << " , "
			<< __upButton->getPosition().x << endl;
	cout << " - down : " << __downButton->getPosition().y << " , "
			<< __downButton->getPosition().y << endl;
#endif //----------------- DEBUG INFO -----------------------

	// Bounds
	__x1 = __position.x - __w2;
	__x2 = __position.x + __w2;
	__y1 = __position.y - __h2;
	__y2 = __position.y + __h2;

	updateOptionButtons();

}


void RenderableWidgetList::setTextSize(uint size){
	for (uint i= 0; i < __optionButtons.size(); ++i){
		__optionButtons.at(i)->setTextSize(size);
	}
}


void RenderableWidgetList::draw(){
}


void RenderableWidgetList::addAction(std::string text, const WidgetFunction& wFunc ){
#ifndef NDEBUG
	cout << "[DEBUG]:: RenderableWidgetList: Adding action " << endl;
#endif

	RenderableWidgetButton* temp;
	temp = new RenderableWidgetButton(__optionButtonWidth, __optionButtonHeight);
	temp->useAutoScale(false);
	temp->setText(text);
	temp->setTexture("option-button.png");
	temp->publishHandledEvent(CLICK, wFunc.f, wFunc.data );
	__optionButtons.push_back(temp);

	World::getInstance()->addWidget(temp);

	__totalOptions = __optionButtons.size();

	updateOptionButtons();
}

void RenderableWidgetList::addActionLUA(std::string text, std::string func){
	__actions.push_back(WidgetFunction(func));
	addAction(text, __actions.at(__actions.size()-1));
}

void RenderableWidgetList::clearList(){
	v_it it;
	for (it = __optionButtons.begin(); it != __optionButtons.end(); ++it){
		delete *it;
	}
	__optionButtons.clear();
}

RenderableWidgetList::~RenderableWidgetList() {
	// Clean memory
	Logger::getInstance()->note("RenderableWidgetList:: Cleaning memory");
	v_it it;
	for (it = __optionButtons.begin(); it != __optionButtons.end(); ++it)
		delete *it;
}

}
