/* Test */

/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/** ONLY FOR TESTING PURPOSES...
 *
 * Dummy main for the MARS hybrid tracking system
 *
 */
#include <iostream>
#include "PixelFlow.h"
#include "FramesBuffer.h"
#include "VideoCaptureOpenCV.h"
#include "VideoFileOpenCV.h"
#include "VideoDeviceOpenCV.h"
#include "VideoCaptureFactory.h"
#include "VideoOutputCV.h"
#include "VideoEnumerate.h"
#include "VideoSource.h"
#include "RenderableModel.h"
#include "Light.h"
#include "Configuration.h"
#include "Logger.h"
#include "VideoOutputSDLOpenGL.h"
#include "World.h"
#include "FontFactory.h"
#include "Matrix16.h"
#include "Quaternion.h"
#include "RenderableText.h"
#include "RenderableEmpty.h"
#include "Logger.h"
#include "ARTKMark.h"
#include "TrackingMethodOAbsoluteArtkBz.h"
#include "TrackingController.h"
#include "RenderableWidget.h"
#include "RenderableWidgetButton.h"
#include "RenderableFactory.h"
#include <opencv/cv.h>

using namespace std;

bool writeMatrix(Mat* mat,String cameraName){
    ofstream posMatFile;
    string path=cameraName+"-pos.mat";
  try{
    posMatFile.open(path.c_str() , ios::out);
  } catch (std::exception e){
    cout<<"Error abriendo el archivo: "<<path<<endl;
    return false;
  }

  if (!posMatFile.is_open()){
    cout<<"Error abriendo el archivo: "<<cameraName+"pos-mat."<<endl;
    return false;
  }else{
    char newLine[255];
    setlocale (LC_NUMERIC, "C"); // ... so we have an uniform config file...
    
    float *d=((float*)(mat->data));

    sprintf(newLine,"%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f",
	    d[0],d[1],d[2],d[3],d[4],d[5],d[6],d[7],
	    d[8],d[9],d[10],d[11],d[12],d[13],d[14],d[15]);

    for(int j=0;newLine[j]!='\0';j++)
      posMatFile.put(newLine[j]);

    posMatFile.put('\n');
  
    posMatFile.close();
}

  

  return true;
}

int main(int argc, char *argv[]){
	cout << "--------------------------------------------------------" << endl;
	cout << "M.A.R.S  Copyright (C) 2013  Oreto Research Group (UCLM)" << endl;
	cout << "--------------------------------------------------------" << endl;
	cout << "Static camera calibrator"<<endl;
	cout << endl;

	/*Reading arguments*/
	if(argc!=2){
	  cout<<endl;
	  cout<<"Error in arguments"<<endl;
	  cout<<"Usage: posExtCam <camera_name>"<<endl<<endl;
	  return 0;
	}

	char* cameraName=argv[1];

	mars::Logger::getLogger()->note("Beginning Execution");

	/*Getting video devices*/
	mars::VideoCapture* vC = mars::VideoCaptureFactory::getFactory()->createVideoCapture(OPENCV);
	vector<string> deviceNames;

	deviceNames = mars::getDevVideoCaptureNames();
	mars::VideoDevice* vD_1=NULL;

	if (deviceNames.size() == 0){
		mars::Logger::getLogger()->error("No video devices found... trying to "
				"continue using a video file");
	} else {

		unsigned firstVideoDevice = atoi(& deviceNames.at(0)[mars::getDevVideoCaptureNames().at(0).size()-1]);

		cout << "--------------------------------" << endl;
		cout << "Device number " << firstVideoDevice << " being used." << endl;
		cout << "--------------------------------" << endl;


		vC->addVideoSource(new mars::VideoDeviceOpenCV(firstVideoDevice, "test")); // Let's add a videosource
		vD_1 = vC->getVideoDevice(0);
	}


	if (vD_1 == NULL) {
		mars::Logger::getLogger()->error("No video device loaded. HALTING.");
		return 0;
	}

	mars::VideoOutputSDLOpenGL* vOGL;
	vOGL = mars::VideoOutputSDLOpenGL::getInstance();


	mars::TrackingMethodOAbsoluteArtkBz* tM;
	tM = new mars::TrackingMethodOAbsoluteArtkBz(vD_1);

	mars::World* theWorld = mars::World::getInstance();

	mars::Camera3D* c = vD_1->getOpenGLCamera();
	c->setEye(0,0,20);
	c->setLookTo(0,0,-6);

	theWorld->addCamera(c);

	mars::TrackingController* tC=mars::TrackingController::getController();
	tC->publishMethodAbsolute(tM);
	tC->setCamera(c);

	theWorld->setBackgroundSource(vD_1);

	unsigned nSteps = 0;
	Uint32 t0 = SDL_GetTicks();
	
	/*Getting the marks vector. Need a Marks factory?*/
	vector<mars::ARTKMark>* marks;
	marks=tM->getUserMarks();
	
	bool posicionated=false;

	while(theWorld->step()){
	
	  /*Looking for detected marks*/
	  if(!posicionated){
	    for(unsigned int i=0;i<marks->size();i++){
	      if(marks->at(i).isDetected()){
		cv::Mat* camMatrix=new cv::Mat( marks->at(i).getRelativeMatrix()->inv() * (*marks->at(i).getPosMatrix()));
		writeMatrix(camMatrix,cameraName);
		cout<<"Fichero guardado correctamente!"<<endl;
		cout<<"Pulse ESC para salir."<<endl;
		posicionated=true;
		
		cout<<"Debug: entender si es ésta o la inversa ;)"<<endl;
		cout<<"Debe de estar esto!"<<endl;
		for(int j=0;j<4;j++){
		  for(int k=0;k<4;k++){
		    cout<<camMatrix->at<float>(j,k)<<" ";
		  }
		  cout<<endl;
		}
		cout<<endl;
		
		
	      }
	    }
	  }
	  
  
#ifndef NDEBUG
	  nSteps++;
	  if (nSteps == 1000){
	    double t = 1000.0 / ( (SDL_GetTicks() - t0) / (float) (nSteps) );
	    std::cout << "[DEBUG]: FPS = " << t << endl;
	    t0 = SDL_GetTicks();
	    nSteps = 0;
	  }
#endif
	  
	  tC->tempFixDeleteMe();
	}
	
	std::cout << "Exiting..." << std::endl;
	
	return 0;
}
