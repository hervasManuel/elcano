/*
 * VideoCaptureUEye.cpp
 *
 */

#include "VideoCaptureUEye.h"

namespace mars {

VideoCaptureUEye::VideoCaptureUEye() { }

VideoCaptureUEye::~VideoCaptureUEye() { }

void VideoCaptureUEye::addNewVideoSource(UEYE_CAMERA_INFO cameraInfo, std::string name){
	VideoCapture::addVideoSource_(new mars::VideoDeviceUEye(cameraInfo, name));
}


}
