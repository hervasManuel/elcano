/*
 * TrackingController.cpp
 *
 */

#include "TrackingController.h"
#include "TrackingMethodOAbsoluteArtkBz.h"
#include <pthread.h>

namespace mars {



void TrackingController::tempFixDeleteMe(){
	computeAllMethods();
}

TrackingController::TrackingController(){
	pthread_mutex_init(&mutex_userl, 0);
	__filter=new ParticleFilter();

	__ARTKWait = 0;
	__failsARTK = 0;
}

TrackingController::~TrackingController(){
	pthread_mutex_destroy(&mutex_userl);

	Logger::getLogger()->note("Ending Tracking Controller");
	for (unsigned i = 0; i < __methodsAbsolute.size(); ++i)
	{
		__methodsAbsolute.at(i)->stopMethod();
		delete __methodsAbsolute.at(i);
	}

	for (unsigned i = 0; i < __methodsRelative.size(); ++i){
		__methodsRelative.at(i)->stopMethod();
		delete __methodsRelative.at(i);
	}

	delete __filter;

	
}

void TrackingController::lock(){
	pthread_mutex_lock(&mutex_userl);
}

void TrackingController::unlock(){
	pthread_mutex_unlock(&mutex_userl);
}

void TrackingController::setCamera(Camera3D* camera){
	__camera = camera;
}


TrackingController* TrackingController::getController(){
	return getInstance();
}

void TrackingController::publishMethodRelative(TrackingMethodORelative* tMR){
	__methodsRelative.push_back(tMR);
	tMR->startMethod();
}
void TrackingController::publishMethodAbsolute(TrackingMethodOAbsolute* tMA){
	__methodsAbsolute.push_back(tMA);
	tMA->startMethod();
}

tUserL TrackingController::getComputedUserL() const{
	return __computedUserL;
}


void TrackingController::computeAllMethods(){
  if (__methodsRelative.empty() && __methodsAbsolute.empty() ){
    cout << "Vectors are empty" << endl;
    return;
  }
  
  /* By convention
     ARTK = 0
     PTAMM = 1
  */ 
  if(__methodsAbsolute.size()==2){ //Having, maybe, PTAMM and ARTK
    TrackingMethodOAbsoluteArtkBz* tARTK = (TrackingMethodOAbsoluteArtkBz*)__methodsAbsolute.at(0);
    TrackingMethodOAbsolute* tPTAMM = __methodsAbsolute.at(1);
    TrackingMethod* tCurrent;
    
    if(!tARTK->isDetecting() && tARTK->actuationMarksFound()==0){ //ARTK into slow mode, let's try with PTAMM
      if(__failsARTK<3) {__failsARTK++;}
      if(__failsARTK==3){
	__failsARTK++;
	tARTK->slowMode();
	tPTAMM->resume();
      }
      tCurrent = tPTAMM;
    }else{//Let's do it with ARTK
      __failsARTK = 0;
      tARTK->fastMode();
      tPTAMM->pause();
      tCurrent = tARTK;
    }


    // So far, only the last one from the first Absolute Tracking Method is taken
    //FIX Aquí está la Question!
    //__computedUserL = *(__methodsAbsolute.at(0)->getLastUserL());
    
    
    
    if(!tCurrent->isDetecting()){//NO estamos detectando
      //World::getInstance()->disableDrawing();
      //RenderableConstructor::getInstance()->getModel("PLANO")->hide();
      //Lanzamos a riki-master
      
    }else{ //SI estamos detectando
      //__computedUserL = *(tCurrent->getLastUserL());      

      //World::getInstance()->enableDrawing();
      //RenderableConstructor::getInstance()->getModel("PLANO")->show();
      if(tCurrent->getHistoricalRecord()->size()==0)
	return;
      
      __filter->filterAbsolute(tCurrent->getHistoricalRecord(),&__computedUserL);
      
      //__computedUserL = *(tCurrent->getLastUserL());      

      __camera->setEye(__computedUserL.__Eye.x,
		       __computedUserL.__Eye.y,
		       __computedUserL.__Eye.z);
      
      __camera->setLookTo(__computedUserL.__LookTo.x,
			  __computedUserL.__LookTo.y,
			  __computedUserL.__LookTo.z);
      
      
      __camera->setUp(__computedUserL.__Up.x,
		      __computedUserL.__Up.y,
		      __computedUserL.__Up.z);
      
    }
    



  }else{//Any other amount of Tracking Methods, just take the first one and locate!
    // So far, only the last one from the first Absolute Tracking Method is taken
    //__computedUserL = *(__methodsAbsolute.at(0)->getLastUserL());
    
    if(__methodsAbsolute.at(0)->getHistoricalRecord()->size()==0)
      return;
    
    __filter->filterAbsolute(__methodsAbsolute.at(0)->getHistoricalRecord(),&__computedUserL);
    
    std::cout<<"PTAMM is detecting. The user's position is "<<__computedUserL.__Eye.x<<","<<__computedUserL.__Eye.y<<std::endl;
    
    __camera->setEye(__computedUserL.__Eye.x,
		     __computedUserL.__Eye.y,
		     __computedUserL.__Eye.z);
    
    __camera->setLookTo(__computedUserL.__LookTo.x,
			__computedUserL.__LookTo.y,
			__computedUserL.__LookTo.z);
    
    
    __camera->setUp(__computedUserL.__Up.x,
		    __computedUserL.__Up.y,
		    __computedUserL.__Up.z);

  }
  
    
}
  
}
