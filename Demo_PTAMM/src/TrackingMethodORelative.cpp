/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * TrackingMethodORelative.cpp
 *
 */

#include "TrackingMethodORelative.h"

namespace mars {

TrackingMethodORelative::TrackingMethodORelative() {


}

TrackingMethodORelative::TrackingMethodORelative(const unsigned int& nRecords) {
	__maxHistoricalRecords= nRecords;
	__historicalRecordOfPerceptions.reserve(nRecords); // Reserve nRecords slots.
}




tUserL* TrackingMethodORelative::getLastIncrement(){
	return &__lastIncrement;
}


tUserL* TrackingMethodORelative::getIncrementAt(unsigned  n){

	static tUserL tempIncrement;

	if (n == __maxHistoricalRecords -1 )
		return &__lastIncrement;
	if (n > __maxHistoricalRecords -1 ){
		std::cout << "[WARNING]: You asked for an increment out of range." << endl;
		return &__lastIncrement;
	}
	if (n<1){
		Logger::getLogger()->warning("TrackingMethodORelative:: " + __methodName + " :: You asked for an increment not available.");
		return  &__lastIncrement;
	}
//	tempIncrement.__positionCenter =   __historicalRecordOfPerceptions.at(n)->__positionCenter - __historicalRecordOfPerceptions.at(n-1)->__positionCenter;
//	tempIncrement.__positionLookingTo =   __historicalRecordOfPerceptions.at(n)->__positionLookingTo - __historicalRecordOfPerceptions.at(n-1)->__positionLookingTo;
//	tempIncrement.__rotation = __historicalRecordOfPerceptions.at(n)->__rotation - __historicalRecordOfPerceptions.at(n-1)->__rotation;
	return &tempIncrement;
}


TrackingMethodORelative::~TrackingMethodORelative() {}

}
