/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/**
 *
 * Calibration for the MARS hybrid tracking system
 *
 */


#ifdef WUEYE          // uEye

#define __LINUX__     // There's something wrong about make if
				      // we don't do this explicit declaration
	                  // wtf, bbq

#include <uEye.h>

#endif

#include <Tools.h>


#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <iomanip>

using namespace std;
using namespace mars;

enum cType{
	OPENCV,
	UEYE
};

void usage(char* execname){
	cout << "Usage: " << execname << " camera_name wBoard hBoard squareSize nSnapShots <nDevice> <WUEYE>" << endl << endl;
	cout << "        camera_name -  camera's name. It should be use to load the calibration" << endl;
	cout << "        wBoard      -  # of corners (width. )          (int greater than 2)" << endl;
	cout << "        hBoard      -  # of corners (height.)          (int greater than 2)" << endl;
	cout << "        squareSize  -  size of a square" << endl;
	cout << "        nSnapShots  -  # of snapshots                 (int greater than 0)" << endl;
	cout << "        nDevice     -  # of device or VALID URL (optional - defaults to 0)" << endl;
	cout << "        WUEYE       -                            (if set, use UEYE driver)" << endl;
	cout << endl;

	exit(-1);
}


// This from OpenCV examples
void calcChessBoardCorners(cv::Size boardSize,
                           float squareSize,
                           vector<cv::Point3f>& corners){

	corners.resize(0);
	for (int i=0; i < boardSize.height; i++)
		for (int j=0; j < boardSize.width; j++)
			corners.push_back(cv::Point3f(float (j*squareSize), float (i*squareSize), 0));
}

int main(int argc, char* argv[]){


	cType cameraType = OPENCV;
	cout << "--------------------------------------------------------" << endl;
	cout << "M.A.R.S  Copyright (C) 2013  Oreto Research Group (UCLM) " << endl;
	cout << "--------------------------------------------------------" << endl << endl;
	cout << endl << endl << endl ;


	bool is_file = false;



	if (argc < 6 || argc > 8)
		usage(argv[0]);


	unsigned nDevice = 0;
	int wBoard = atoi(argv[2]);
	int hBoard = atoi(argv[3]);
	float squareSize = (float) atof(argv[4]);
	int nSnapShots = atoi(argv[5]);


	std::string dev;
	if (argc > 6){
		dev = toString(argv[6]);
		if (dev.size()>7){
			if (dev.substr(0,7).compare("http://") == 0){
				is_file = true;
				cout << "[*] Device = " << dev << endl;
			}
		} else {
			nDevice = atoi(argv[6]);
			cout << "[*] Device = " << nDevice << endl;
		}
	}

	if (argc == 8){
		if (toString(argv[7]).compare("WUEYE") == 0 ){
			cout << "[*] with UEYE camera" << endl;
			cameraType = UEYE;
#ifndef WUEYE
			cout << "[*] Not compiled with UEYE support." << endl;
			exit(-4);
#endif
		}
	} else {
		cameraType = OPENCV;
	}

	if (wBoard <  3 || hBoard < 3 ){
		cout << "**[Error]** Bad wBoard or hBoard argument. Not a valid integer" << endl << endl;
		usage(argv[0]);
	}

	if (squareSize == 0){
		cout << "**[Error]** Bad squareSize" << endl << endl;
		usage(argv[0]);
	}

	if (nSnapShots < 0 ){
		cout << "**[Error]** Bad nSnapShots. Not a valid integer" << endl << endl;
		usage(argv[0]);
	}

	cout << "[*] squareSize = " << squareSize << endl;

	cout << "[*] wBoard = " << wBoard << endl;
	cout << "[*] hBoard = " << hBoard << endl;

	int aBoard = wBoard * hBoard;

	cout << "[*] Total  = " << aBoard << endl;
	cout << endl;

	cout << "[*] nSnapShots = " << nSnapShots << endl;

	string name = argv[1];

	cv::Size boardSize = cv::Size(wBoard, hBoard);



#ifdef WUEYE
	int nCams_uEYE = 0;
	UEYE_CAMERA_LIST* m_pCamList;
	if (cameraType == UEYE){
		if (is_GetNumberOfCameras( &nCams_uEYE ) == IS_SUCCESS) {

			cout << "[*] Number of Cams:: " << nCams_uEYE << endl;
			m_pCamList = (UEYE_CAMERA_LIST*) new char [sizeof (DWORD)
			                                           + nCams_uEYE
			                                           * sizeof
			                                           (UEYE_CAMERA_INFO)];
			m_pCamList->dwCount = nCams_uEYE;
			if (nCams_uEYE > 0){
				if (is_GetCameraList (m_pCamList) == IS_SUCCESS){
					cout << "[*] Camera List OK" << endl;
					for (int c=0; c < (int) nCams_uEYE; ++c){
						cout << "-- UEYE CAMERA --" << endl;
						cout << "CameraID :" << m_pCamList->uci[c].dwCameraID << endl;
						cout << "Ser No.  :" << m_pCamList->uci[c].SerNo << endl;
						cout << "DeviceID :" << m_pCamList->uci[c].dwDeviceID << endl;
						cout << "In use   :" << m_pCamList->uci[c].dwInUse << endl;
						cout << "SensorID :" << m_pCamList->uci[c].dwSensorID << endl;
					}
					cout << "--" << endl;
				}
				else {
					cout << "Error: Getting list of uEye cameras" << endl;
					exit(-5);
				}
			}
		}
		else {
			cout << "Error: No ueye Camera found" << endl;
			exit(-3);
		}
	}
#endif

	cv::VideoCapture* cap;
	// Initialize OpenCV Camera ...
	if (cameraType == OPENCV){
		if (is_file){
			try{
				cap = new cv::VideoCapture(dev);
			} catch (cv::Exception e) {
				std::cout << "Error: " << e.err << std::endl;
				exit(-3);
			}
		}
		else{
			cap = new cv::VideoCapture(nDevice);
		}
		try{
			if (!cap->isOpened()){
				cout << "**[Error]** Opening device (" << nDevice << ")" << endl;
				exit(-2);
			}
		} catch (cv::Exception e) {
			std::cout << "Error: " << e.err << std::endl;
			exit(-3);
		}
	}

#ifdef WUEYE
	HIDS __hCam = (HIDS) 0;
	int __maxW;
	int __maxH;
	int __memId;
	char* __imageMem;

	__maxW = 640;
	__maxH = 480;



	IplImage i;
	IplImage* img_ = &i;

	if (cameraType == UEYE){

		img_->nSize=112;
		img_->ID=0;
		img_->nChannels=3;
		img_->alphaChannel=0;
		img_->depth=8;
		img_->dataOrder=0;
		img_->origin=0;
		img_->align=4;
		img_->width=__maxW;
		img_->height=__maxH;
		img_->roi=NULL;
		img_->maskROI=NULL;
		img_->imageId=NULL;
		img_->tileInfo=NULL;
		img_->imageSize=3*__maxW*__maxH;
		img_->imageData=(char*)__imageMem;  //the pointer to imagaData
		img_->widthStep=3*__maxW;
		img_->imageDataOrigin=(char*)__imageMem; //and again



		if (nDevice+1 > (uint) nCams_uEYE){
			cout << "**[Error]** No camera found at uEye device: " << nDevice << endl;
			exit(-2);
		}

		__hCam = (HIDS) (m_pCamList->uci[nDevice].dwDeviceID | IS_USE_DEVICE_ID );

		int ret = is_InitCamera(&__hCam, NULL);

		if (ret == IS_SUCCESS){

			is_EnableAutoExit(__hCam, IS_ENABLE_AUTO_EXIT);

			SENSORINFO sInfo;                 // sensor information
			is_GetSensorInfo(__hCam, &sInfo); // query

			__maxW = sInfo.nMaxWidth;
			__maxH = sInfo.nMaxHeight;

			cout << "maxW:: " << __maxW << endl;
			cout << "maxH:: " << __maxH << endl;

			__maxW = 640;
			__maxH = 480;

			int __bitsPerPixel = 24;
			int __colorMode = IS_CM_BGR8_PACKED;

			is_SetColorMode(__hCam, __colorMode);

			cout << "colorMode:: " << __colorMode << endl;

			// Allocate memory for the image
			is_AllocImageMem(__hCam, __maxW, __maxH, __bitsPerPixel, &__imageMem, &__memId);
			is_SetImageMem(__hCam, __imageMem, __memId);

			int xPos, yPos = 0;

			is_SetAOI(__hCam, IS_SET_IMAGE_AOI, &xPos, &yPos, &__maxW, &__maxH);
			is_SetImagePos(__hCam, 0, 0);
			is_SetImageSize(__hCam, __maxW, __maxH);


		}
		else {
			cout << "**[Error]** Initializing uEye device: " << nDevice << endl;
			exit(-3);
		}
	} // if ueye
#endif



	cv::namedWindow("Calibrate");
	cv::namedWindow("Snap");

	std::vector< std::vector<cv::Point3f> > aObjectPoints;
	std::vector< std::vector<cv::Point2f> > aImagePoints;
	std::vector<cv::Mat> aRotations;
	std::vector<cv::Mat> aTranslations;

	cv::Mat cameraMatrix;
	cv::Mat distCoeffs;

	cameraMatrix = cv::Mat::eye(3, 3, CV_64F);
	distCoeffs = cv::Mat::zeros(5, 1, CV_64F);


	int   nSuccesses  = 0;

	cv::Mat img;


	if (cameraType == OPENCV){
		try{
			*cap >> img;
		} catch (cv::Exception e) {
			std::cout << "Error: " << e.err << std::endl;
			exit(-3);
		}
	}


#ifdef WUEYE
	else if (cameraType == UEYE){

		if (is_FreezeVideo(__hCam, IS_WAIT) == IS_SUCCESS){


			is_LockSeqBuf (__hCam, IS_IGNORE_PARAMETER, __imageMem);

			img_->nSize=112;
			img_->ID=0;
			img_->nChannels=3;
			img_->alphaChannel=0;
			img_->depth=8;
			img_->dataOrder=0;
			img_->origin=0;
			img_->align=4;
			img_->width=__maxW;
			img_->height=__maxH;
			img_->roi=NULL;
			img_->maskROI=NULL;
			img_->imageId=NULL;
			img_->tileInfo=NULL;
			img_->imageSize=3*__maxW*__maxH;
			img_->imageData=(char*)__imageMem;  //the pointer to imagaData
			img_->widthStep=3*__maxW;
			img_->imageDataOrigin=(char*)__imageMem; //and again

			img = cv::Mat(img_, true);


			is_UnlockSeqBuf (__hCam, IS_IGNORE_PARAMETER, __imageMem);

		}
	}
#endif

	try{
		cv::imshow("Calibrate", img);
	} catch (cv::Exception e) {
		std::cout << "Error: " << e.err << std::endl;
		std::cout << "\n\n[ERROR] marsCalibrate:: Did you entered a correct url? http://..." << endl;
		std::cout << "Bye ..." << endl;
		exit(-3);

	}

	cv::Mat imgGray;

	vector<cv::Point3f> chessboard3D;
	for(int j = 0; j < hBoard; ++j)
		for(int i = 0; i < wBoard; ++i)
			chessboard3D.push_back(cv::Point3f(i*squareSize, j*squareSize, 0));


	while (nSuccesses < nSnapShots){ // Repeat until we have all the needed snapshots
		char key = cv::waitKey(30);
		if (key == 27) { // Esc?
			cout << "bye!" << endl;
#ifdef WUEYE
			if (cameraType == UEYE){
				is_FreeImageMem( __hCam, __imageMem, __memId);

				if (__hCam)
					is_ExitCamera( __hCam); // FIXME EXIT CAMERA
			}
#endif
			if (cameraType == OPENCV){
				if(cap->isOpened())
					cap->release();
			}
			exit(1);
		}
		else if (key == 32){


			cv::cvtColor(img, imgGray, CV_BGR2GRAY);

			vector<cv::Point2f> corners; // Array of points for the squares

			bool found = cv::findChessboardCorners(img, boardSize,
					corners, CV_CALIB_CB_ADAPTIVE_THRESH);

			cv::drawChessboardCorners(img, boardSize, (cv::Mat) corners, found);

			if (found) {
				nSuccesses++;
				cout << "Number of successes = " << nSuccesses << endl;
				cv::cornerSubPix(imgGray, corners, cv::Size(11,11), cv::Size(-1,-1),
						cv::TermCriteria(cv::TermCriteria::MAX_ITER + cv::TermCriteria::EPS, 30, 0.1));

				aObjectPoints.push_back(chessboard3D);
				aImagePoints.push_back(corners);

				cv::imshow("Snap", img);

				if (nSuccesses == nSnapShots)
					break;

			} else {
				cout << "Not a valid frame!" << endl;
			}

		}


		if (cameraType == OPENCV){
			try{
				*cap >> img;

				cv::imshow("Calibrate", img);
			} catch (cv::Exception e){
				cout << endl << "Exception caught: " << e.msg << endl;
			}
		}
#ifdef WUEYE
		if (cameraType == UEYE){
				if (is_FreezeVideo(__hCam, IS_WAIT) == IS_SUCCESS){

					img_->nSize=112;
					img_->ID=0;
					img_->nChannels=3;
					img_->alphaChannel=0;
					img_->depth=8;
					img_->dataOrder=0;
					img_->origin=0;
					img_->align=4;
					img_->width=__maxW;
					img_->height=__maxH;
					img_->roi=NULL;
					img_->maskROI=NULL;
					img_->imageId=NULL;
					img_->tileInfo=NULL;
					img_->imageSize=3*__maxW*__maxH;
					img_->imageData=(char*)__imageMem;  //the pointer to imagaData
					img_->widthStep=3*__maxW;
					img_->imageDataOrigin=(char*)__imageMem; //and again

					img = cv::Mat(img_, true);

					cv::imshow("Calibrate", img);
					}
			}
#endif
		}



	if (nSuccesses == nSnapShots){
		// Calibrate
		cout << "Calibrating ..." << endl;
		cv::calibrateCamera(aObjectPoints, aImagePoints, img.size(), cameraMatrix, distCoeffs,
				aRotations, aTranslations);

		bool ok = cv::checkRange(cameraMatrix) && cv::checkRange(distCoeffs);

		if (ok)
			cout << "OK!" << endl;
		else
			cout << "NOT OK!" << endl;

		string mFileName = name + "-calib.xml";

		cout << setprecision( 3 ) << right << fixed;

		cout << "Camera Matrix" << endl;
		for (int j=0; j < cameraMatrix.rows; ++j){
			for (int i=0; i < cameraMatrix.cols; ++i){
				cout << cameraMatrix.at<double>(i, j) << " ";
			}
			cout << endl;
		}
		cout << endl;

		cout << "fx = " << cameraMatrix.at<double>(0,0) << endl;
		cout << "fy = " << cameraMatrix.at<double>(1,1) << endl;
		cout << "cx = " << cameraMatrix.at<double>(0,2) << endl;
		cout << "cy = " << cameraMatrix.at<double>(1,2) << endl;

		cout << "Distortion Coeffs. Matrix " << endl;
		cout << endl;
		for (int j=0; j < distCoeffs.rows; ++j){
			for (int i=0; i < distCoeffs.cols; ++i){
				cout << distCoeffs.at<float>(i, j) << " ";
			}
			cout << endl;
		}
		cv::FileStorage fs(mFileName.c_str(), cv::FileStorage::WRITE);

		fs << "cm" << cameraMatrix;
		fs << "dm" << distCoeffs;

	}

	cv::Mat mapx;
	cv::Mat mapy;
	cv::Mat uimg;



	while (1){


		if (cameraType == OPENCV)
			*cap >> img;
#ifdef WUEYE
		else if (cameraType == UEYE){
			if (is_FreezeVideo(__hCam, IS_WAIT) == IS_SUCCESS){
				img = cv::Mat(img_, true);
			}
		}
#endif


		char key = cv::waitKey(15);
		if (key == 27)
			break;
		cv::undistort(img, uimg, cameraMatrix, distCoeffs);
		cv::imshow("Calibrate", img);
		cv::imshow("Snap", uimg);
	}


	cout << endl << "Width x Height  = " << img.size().width << " x " << img.size().height << endl;



	cv::destroyWindow("Calibrate");
	cv::destroyWindow("Snap");

#ifdef WUEYE
	if (cameraType == UEYE){
		is_FreeImageMem( __hCam, __imageMem, __memId);

		if (__hCam)
			is_ExitCamera( __hCam); // FIXME EXIT CAMERA
	}
#endif

	if (cameraType == OPENCV) {
		if(cap->isOpened())
				cap->release();
	}


	return 0;
}
