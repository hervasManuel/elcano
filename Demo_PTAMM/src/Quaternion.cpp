/*
 * Quaternion.cpp
 *
 */

#include "Quaternion.h"


namespace mars {

Quaternion::Quaternion() {}

Quaternion::~Quaternion() {}

Quaternion::Quaternion(GLfloat _w, GLfloat _x, GLfloat _y, GLfloat _z):
		w(_w), x(_x), y(_y), z(_z) {

}

Quaternion::Quaternion(Matrix16& m){

	GLfloat* mat = m.getData();

	w = sqrt (mat[0]+mat[5]+mat[10]+mat[15]) / 2;

	GLfloat k = 4.0 * w;

	x = (mat[9] - mat[6]) / k;
	y = (mat[2] - mat[8]) / k;
	z = (mat[4] - mat[1]) / k;
}

Quaternion::Quaternion(const tUserL& t){
	// TODO Construct a quaternion from a tUserL
}


void Quaternion::toMatrix16(Matrix16& mat){

	GLfloat m[16];

	GLfloat xy = x * y;
	GLfloat xz = x * z;
	GLfloat wz = w * z;
	GLfloat yz = y * z;
	GLfloat wx = w * x;
	GLfloat wy = w * y;
	GLfloat x2 = x * x;
	GLfloat y2 = y * y;
	GLfloat z2 = z * z;

	m[0] = 1 - 2 * (y2 + z2);
	m[1] = 2 * (xy + wz);
	m[2] = 2 * (xz - wy);
	m[3] = 0;

	m[4] = 2 * (xy - wz);
	m[5] = 1 - 2 * (x2 + z2);
	m[6] = 2 * (yz + wx);
	m[7] = 0;

	m[8] = 2 * (xz + wy);
	m[9] = 2 * (yz - wx);
	m[10] = 1 - 2 * (x2 + y2);
	m[11] = 0;

	m[12] = 0;
	m[13] = 0;
	m[14] = 0;
	m[15] = 1;


	mat.setData(m);

}

Quaternion Quaternion::operator*(const Quaternion& r){
	Quaternion temp;

	temp.w = w * r.w - x * r.x - y * r.y - z * r.z;
	temp.x = y * r.z - z * r.y + r.w * x + w * r.x;
	temp.y = z * r.x - x * r.z + r.w * y + w * r.y;
	temp.z = x * r.y - y * r.x + r.w * z + w * r.z;

	return temp;
}

}
