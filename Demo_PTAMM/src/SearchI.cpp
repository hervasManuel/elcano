#include <SearchI.h>

SearchI::SearchI(const MLP::LocationAdminPrx &loc){
  _loc = loc;
}

void SearchI::lookup(const ASD::ListenerPrx& cb, const std::string& tid, const PropertyService::Properties& pop, const Ice::Current&){
  cb->adv(_loc);

}
void SearchI::discover(const ASD::ListenerPrx&, const Ice::Current&){
}
