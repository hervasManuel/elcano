/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * TrackingMethodO.cpp
 *
 */


#include "TrackingMethodO.h"

namespace mars {


TrackingMethodO::TrackingMethodO() {
	__vSources.reserve(2);
}
TrackingMethodO::TrackingMethodO(const unsigned int& nRecords) {
	__maxHistoricalRecords= nRecords;
	__historicalRecordOfPerceptions.reserve(nRecords); // Reserve nRecords slots.
}


void TrackingMethodO::addVideoSource(VideoSource* vS){
#ifndef NDEBUG
	std::cout << "TrackingMethod:: ( " << __methodName
			  << " ) : Adding VideoSource" << endl << flush;
#endif
	__vSources.push_back(vS);
	__ready = true;
}

VideoSource* TrackingMethodO::getVideoSource(unsigned int n){
	if (n < __vSources.size()){
		return __vSources.at(n);
	} else {
		Logger::getLogger()->warning("MethodO:: " + __methodName + ":: getVideoSource out of bounds, returning last position");
		return __vSources.at(__vSources.size()-1);

	}
}

TrackingMethodO::~TrackingMethodO() {}

}
