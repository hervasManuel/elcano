/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * TrackingMethodOAbsolutePTAMM.cpp
 *
 */

#include "TrackingMethodOAbsolutePTAMM.h"

namespace mars {

using namespace PTAMM;

/* vS_0 is supposed to be the main camera */
TrackingMethodOAbsolutePTAMM::TrackingMethodOAbsolutePTAMM(VideoSource* vS_0){
	__methodName="PTAMM ABSOLUTE TRACKING METHOD";

	if(vS_0==NULL){
		Logger::getLogger()->error("TrackingMethodOAbsolutePTAMM: Error in PTAMM tracking method: video source 0 is NULL");
		exit(0);
	}

	addVideoSource(vS_0);

	mVideoSource = vS_0;

	GVars3::GUI.RegisterCommand("SwitchMap", GUICommandCallBack, this);
	GVars3::GV2.Register(mgvnLockMap, "LockMap", 0, GVars3::SILENT);
	GVars3::GV2.Register(mgvnDrawMapInfo, "MapInfo", 0, GVars3::SILENT);
  
	mimFrameBW.resize(CVD::ImageRef(mVideoSource->getWidth(),mVideoSource->getHeight()));

	int frameWidth = mVideoSource->getWidth();
	int frameHeight = mVideoSource->getHeight();

	double p[3][4];
	mVideoSource->getPerspectiveMatrix(p);
 
	// double k[5];
	//mVideoSource->getDistortionMatrix(k);


	TooN::Vector<NUMTRACKERCAMPARAMETERS>  cameraParams = TooN::makeVector(p[0][0]/frameWidth,
									 p[1][1]/frameHeight,
									 p[0][2]/frameWidth,
									 p[1][2]/frameHeight,
									 0);
	mpCamera = new ATANCamera("Camera",cameraParams);

	mpCamera->SetImageSize(CVD::ImageRef(mVideoSource->getWidth(),mVideoSource->getHeight()));
  

	//create the first map
	mpMap = new Map();
	mvpMaps.push_back( mpMap );
	mpMap->mapLockManager.Register(this);
        
	mpTracker = new Tracker(CVD::ImageRef(mVideoSource->getWidth(),mVideoSource->getHeight()), *mpCamera, mvpMaps, mpMap);
	mpMapSerializer = new MapSerializer( mvpMaps );
	

	/////////////////////////////////////////////////////
	//Loading the maps! :)------------------------------
	//Loading just the first one! :)
	if( mpMapSerializer->Init( "LoadMap", "", *mpMap) ) {
	  mpMapSerializer->start();
	  sleep(1);
	}
	
	//Loading the others!
	for(int i=0;i<NUM_MAPS-1;i++){
	  NewMap();
	  if( mpMapSerializer->Init( "LoadMap", "", *mpMap) ) {
	    mpMapSerializer->start();
	    sleep(1);
	  }
	}

	//Initialitings offset matrices
	for(int i=0;i<NUM_MAPS;i++){
	  __mapsOffsets[i]=new cv::Mat(4,4,CV_32F);
	}

	readMapsOffsets();
}


void TrackingMethodOAbsolutePTAMM::loopThread(){

  cv::Mat* frame = mVideoSource->getLastFrame();
  
  if(frame->rows>0){
    //Check if the map has been locked by another thread, and wait for release.
    bool bWasLocked = mpMap->mapLockManager.CheckLockAndWait( this, 0 );
    
    mpMap->bEditLocked = *mgvnLockMap; //sync up the maps edit lock with the gvar bool.
    
    GetAndFillFrameBW(frame);  
    
    if(bWasLocked)  {
      mpTracker->ForceRecovery();
    }
    
    mpTracker->TrackFrame(mimFrameBW);
  
    if(!mpTracker->IsLost()){
      __detecting = true;

      int currentMap = mpTracker->getCurrentMap()->MapID();
        
      TooN::Vector<3> tr = mpTracker->GetCurrentPose().get_translation();
      TooN::Matrix<3> rt = mpTracker->GetCurrentPose().get_rotation().get_matrix();

      float d[16];

      //This is the trans to change the camera viewing from +z to -z
      // d[0]=rt[0][0];     d[4]=rt[0][1];     d[8]=rt[0][2];      d[12]=tr[0];
      // d[1]=-rt[1][0];    d[5]=-rt[1][1];    d[9]=-rt[1][2];     d[13]=-tr[1];
      // d[2]=-rt[2][0];    d[6]=-rt[2][1];    d[10]=-rt[2][2];    d[14]=-tr[2];
      // d[3]=0.f;          d[7]=0.f;          d[11]=0.f;          d[15]=1.f;

      //std::cout<<"Applying aspect ratio!: "<<__mapsAspectRatios[currentMap]<<std::endl;
      d[0]=rt[0][0];     d[4]=rt[0][1];     d[8]=rt[0][2];      d[12]=tr[0];
      d[1]=-rt[1][0];    d[5]=-rt[1][1];    d[9]=-rt[1][2];     d[13]=-tr[1];
      d[2]=rt[2][0];     d[6]=rt[2][1];     d[10]=rt[2][2];     d[14]=tr[2];
      d[3]=0.f;          d[7]=0.f;          d[11]=0.f;          d[15]=1.f;


      cv::Mat mat (4,4,CV_32F,d);
      cv::Mat mat2 = mat.inv();

      cv::Mat mat3 = mat2 * *(__mapsOffsets[currentMap]);

      Matrix16 m((float*)mat3.data);      

      tUserL t;
      t=Mat2TUserL(m,1.0);

      addRecord(t); 
    }
    
  }
  
  delete frame;
  

}
    


TrackingMethodOAbsolutePTAMM::~TrackingMethodOAbsolutePTAMM(){

  if( mpMap != NULL )  {
    mpMap->mapLockManager.UnRegister( this );
  }
  
  Logger::getLogger()->note("TrackingMethodOAbsolutePTAMM: deleting tracking method. Bye!");
}

void TrackingMethodOAbsolutePTAMM::GUICommandCallBack(void *ptr, string sCommand, string sParams)
{
   if( sCommand == "SwitchMap")
    {
      int nMapNum = -1;
      if( static_cast<TrackingMethodOAbsolutePTAMM*>(ptr)->GetSingleParam(nMapNum, sCommand, sParams) ) {
	static_cast<TrackingMethodOAbsolutePTAMM*>(ptr)->SwitchMap( nMapNum );
      }
    }
}


/**
 * Parse and allocate a single integer variable from a string parameter
 * @param nAnswer the result
 * @param sCommand the command (used to display usage info)
 * @param sParams  the parameters to parse
 * @return success or failure.
 */
bool TrackingMethodOAbsolutePTAMM::GetSingleParam(int &nAnswer, string sCommand, string sParams)
{
  vector<string> vs = GVars3::ChopAndUnquoteString(sParams);
  
  if(vs.size() == 1)
  {
    //is param a number?
    bool bIsNum = true;
    for( size_t i = 0; i < vs[0].size(); i++ ) {
      bIsNum = isdigit( vs[0][i] ) && bIsNum;
    }
      
    if( !bIsNum )
    {
      return false;
    }

    int *pN = GVars3::ParseAndAllocate<int>(vs[0]);
    if( pN )
    {
      nAnswer = *pN;
      delete pN;
      return true;
    }
  }

  cout << sCommand << " usage: " << sCommand << " value" << endl;

  return false;
}


/**
 * Switch to the map with ID nMapNum
 * @param  nMapNum Map ID
 * @param bForce This is only used by DeleteMap and ResetAll, and is
 * to ensure that MapViewer is looking at a safe map.
 */
bool TrackingMethodOAbsolutePTAMM::SwitchMap( int nMapNum, bool bForce )
{

  //same map, do nothing. This should not actually occur
  if(mpMap->MapID() == nMapNum) {
    return true;
  }

  if( (nMapNum < 0) )
  {
    cerr << "Invalid map number: " << nMapNum << ". Not changing." << endl;
    return false;
  }

  
  for( size_t ii = 0; ii < mvpMaps.size(); ii++ )
  {
    Map * pcMap = mvpMaps[ ii ];
    if( pcMap->MapID() == nMapNum ) {
      mpMap->mapLockManager.UnRegister( this );
      mpMap = pcMap;
      mpMap->mapLockManager.Register( this );
    }
  }

  if(mpMap->MapID() != nMapNum)
  {
    cerr << "Failed to switch to " << nMapNum << ". Does not exist." << endl;
    return false;
  }
  
  /*  Map was found and switched to for system.
      Now update the rest of the system.
      Order is important. Do not want keyframes added or
      points deleted from the wrong map.
  
      MapMaker is in its own thread.
      TrackingMethodOAbsolutePTAMM,Tracker, and MapViewer are all in this thread.
  */

  *mgvnLockMap = mpMap->bEditLocked;


  if( !mpTracker->SwitchMap( mpMap ) ) {
    return false;
  }

  return true;
}



/**
 * Create a new map and switch all
 * threads and objects to it.
 */
void TrackingMethodOAbsolutePTAMM::NewMap()
{

  *mgvnLockMap = false;
  mpMap->mapLockManager.UnRegister( this );
  mpMap = new Map();
  mpMap->mapLockManager.Register( this );
  mvpMaps.push_back( mpMap );
  

  //update the tracker object
  mpTracker->SetNewMap( mpMap );
    
  cout << "New map created (" << mpMap->MapID() << ")" << endl;
      
}


/**
 * Moves all objects and threads to the first map, and resets it.
 * Then deletes the rest of the maps, placing PTAMM in its
 * original state.
 * This reset ignores the edit lock status on all maps
 */
void TrackingMethodOAbsolutePTAMM::ResetAll()
{

  //move all maps to first map.
  if( mpMap != mvpMaps.front() )
  {
    if( !SwitchMap( mvpMaps.front()->MapID(), true ) ) {
      cerr << "Reset All: Failed to switch to first map" << endl;
    }
  }
  mpMap->bEditLocked = false;

  //reset map.
  mpTracker->Reset();
  
 }




/**
 * Set up the map serialization thread for saving/loading and the start the thread
 * @param sCommand the function that was called (eg. SaveMap)
 * @param sParams the params string, which may contain a filename and/or a map number
 */
void TrackingMethodOAbsolutePTAMM::StartMapSerialization(std::string sCommand, std::string sParams)
{
  if( mpMapSerializer->Init( sCommand, sParams, *mpMap) ) {
    mpMapSerializer->start();
  }
}


void TrackingMethodOAbsolutePTAMM::GetAndFillFrameBW(cv::Mat* frame){
  Mat_<Vec3b>& frame_p = (Mat_<Vec3b>&)*frame;
  
  for (int i = 0; i < mVideoSource->getHeight(); i++){
    for (int j = 0; j < mVideoSource->getWidth(); j++){
      mimFrameBW[i][j]  = (frame_p(i,j)[0] + frame_p(i,j)[1] + frame_p(i,j)[2]) / 3;
    }
  }
  
  
}


void TrackingMethodOAbsolutePTAMM::readMapsOffsets(){

  ifstream file; 
  file.open("PTAMMOffset.mat",ios::in);
  if(!file.is_open()){
    std::cout<<"Error opening PTAMMOffset.mat"<<std::endl;
    std::cout<<"The format must be:"<<std::endl;
    std::cout<<"[#MapID] #offsetMatrix"<<std::endl;
    exit(-1);
  }

  string line;
  while(!file.eof()){
    getline(file,line);
    if(line.size()==0) continue;
    if(line[0]!='#'){
      setlocale (LC_NUMERIC, "C"); // ... so we have an uniform config file...
      
      int mapId;
      sscanf(line.c_str(),"[%d]",&mapId);
      if(mapId>=mvpMaps.size()) continue;
      sscanf(line.c_str()+4,"%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f",
	     //	     &(__mapsAspectRatios[mapId]),
       	     &(__mapsOffsets[mapId]->at<float>(0,0)), &(__mapsOffsets[mapId]->at<float>(0,1)),&(__mapsOffsets[mapId]->at<float>(0,2)),&(__mapsOffsets[mapId]->at<float>(0,3)),
      	     &(__mapsOffsets[mapId]->at<float>(1,0)), &(__mapsOffsets[mapId]->at<float>(1,1)),&(__mapsOffsets[mapId]->at<float>(1,2)),&(__mapsOffsets[mapId]->at<float>(1,3)),
      	     &(__mapsOffsets[mapId]->at<float>(2,0)), &(__mapsOffsets[mapId]->at<float>(2,1)),&(__mapsOffsets[mapId]->at<float>(2,2)),&(__mapsOffsets[mapId]->at<float>(2,3)),
      	     &(__mapsOffsets[mapId]->at<float>(3,0)), &(__mapsOffsets[mapId]->at<float>(3,1)),&(__mapsOffsets[mapId]->at<float>(3,2)),&(__mapsOffsets[mapId]->at<float>(3,3)));

    /*Applying Aspect Ratio */
      // __mapsOffsets[mapId]->at<float>(3,0)*=__mapsAspectRatios[mapId];
      //__mapsOffsets[mapId]->at<float>(3,1)*=__mapsAspectRatios[mapId];
      //__mapsOffsets[mapId]->at<float>(3,2)*=__mapsAspectRatios[mapId];
    

    }

  }
}

  int TrackingMethodOAbsolutePTAMM::getTrackingQuality(){
    return mpTracker->getTrackingQuality();
  }

  void TrackingMethodOAbsolutePTAMM::printMapsPoints(){
	for(unsigned int i=0;i<mvpMaps.size();i++){
	  Map* m = mvpMaps.at(i);
	  std::cout<<"\tMap #"<<i<<": "<<m->vpPoints.size()<<" points."<<std::endl;
    }
  }

  Map* TrackingMethodOAbsolutePTAMM::getCurrentMap(){
    return mpMap;

  }


}

