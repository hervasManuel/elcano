/*
 * RenderableEmpty.cpp
 *
 */

#include "RenderableEmpty.h"

namespace mars {

RenderableEmpty::RenderableEmpty() {
}

RenderableEmpty::~RenderableEmpty() {
}



void RenderableEmpty::draw(){

	glPushMatrix();

	computePositionRotation();


#if 0
	Matrix16::printModelView();
#endif

	glPushMatrix();
	drawAxis();
	glPopMatrix();

	drawText();

	glPopMatrix();
}


void RenderableEmpty::drawText(){ }



void RenderableEmpty::drawAxis(){

	glDisable(GL_LIGHTING);

	glEnable(GL_LINE_SMOOTH);

	glLineWidth(3);

	glDisable(GL_TEXTURE_2D);

	glBegin(GL_LINES);

	// X axis
	glColor3f(1.0,0.0,0.0);
	glVertex3f(0,0,0);
	glVertex3f(1.0,0,0);

	glVertex3f(1.0,0,0);
	glVertex3f(0.9,0.1,0);

	glVertex3f(1.0,0,0);
	glVertex3f(0.9,-0.1,0);

	// Y axis
	glColor3f(0.0,1.0,0.0);
	glVertex3f(0,0,0);
	glVertex3f(0,1.0,0);

	glVertex3f(0,1.0,0);
	glVertex3f(0.1,0.9,0);

	glVertex3f(0,1.0,0);
	glVertex3f(-0.1,0.9,0);

	// Z Axis
	glColor3f(0.0,0.0,1.0);
	glVertex3f(0,0,0);
	glVertex3f(0,0,1.0);

	glVertex3f(0,0,1.0);
	glVertex3f(0,0.1,0.9);

	glVertex3f(0,0,1.0);
	glVertex3f(0,-0.1,0.9);

	// Center Bisec.
	glColor3f(1,1,1);
	glVertex3f(0,0,0);
	glVertex3f(0.1,0.1,0.1);

	glEnd();

	glEnable(GL_TEXTURE_2D);


	glDisable(GL_LINE_SMOOTH);

	if (World::getInstance()->isLightingEnabled())
		glEnable(GL_LIGHTING);
}

}
