/* -*- coding: utf-8; mode: c++ -*- */
#include <anuncPos.h>
#include <time.h>

using namespace MLP;
using namespace std;
using namespace ASD;

anuncPos::anuncPos(int argc, char **argv)
{
  ic = Ice::initialize(argc, argv);
  conectado = false;
  obj = ic->stringToProxy("Elcano.IceStorm/TopicManager");
  try{
    topicManager = IceStorm::TopicManagerPrx::checkedCast(obj);
    conectado = true;
  }catch(const Ice::ConnectionRefusedException){
    conectado = false;
  }catch(const Ice::DNSException){
    conectado = false;
  }
  if(conectado){
    while(!ASDA){
      try{
	ASDA = topicManager->retrieve("ASDA");
      }catch(const IceStorm::NoSuchTopic&){
	cerr << "No se encuentra el topic ASDA";
      }
    }
    while(!ASDS){
      try{
	ASDS = topicManager->retrieve("ASDS");
      }catch(const IceStorm::NoSuchTopic&){
	cerr << "No se encuentra el topic ASDS";
      }
    }

    while(!topic){
      try{
	topic = topicManager->retrieve("pos3D");
      }catch(const IceStorm::NoSuchTopic&){
	try{
	  topic = topicManager->create("pos3D");
	}catch (const IceStorm::TopicExists&){
	}
      }
    }
    Ice::ObjectPrx topicPub = topic->getPublisher()->ice_oneway();
    publicar = MLP::LocationListenerPrx::uncheckedCast(topicPub);
    //ASDA Topic
    Ice::ObjectPrx ASDApub = ASDA->getPublisher()->ice_oneway();
    ListenerPrx anounc = ListenerPrx::uncheckedCast(ASDApub);

    //Object Adapter LocationAdmin
    adapter
      = ic->createObjectAdapterWithEndpoints(
					     "LAAdapter", "default -p 10006");

    LocationAdminPtr topicAdmin = new LocationAdminI(topic);
    Ice::ObjectPrx a = adapter->add(topicAdmin,
				    ic->stringToIdentity("LAA"));
    LocationAdminPrx lap = LocationAdminPrx::uncheckedCast(a);

    //Anunciamiento  de Servicio Disponible
    anounc->adv(lap);

    SearchPtr anouncement =  new SearchI(lap);
    adapterSearch
      = ic->createObjectAdapterWithEndpoints(
					     "SIadapter", "default -p 10007");


    Ice::ObjectPrx saI  = adapterSearch->add(topicAdmin,
					     ic->stringToIdentity("SI"));

    SearchPrx sprx = SearchPrx::uncheckedCast(a);

    ASDS->subscribe(qos,sprx);

    adapter->activate();
    adapterSearch->activate();

    pthread_create( &__thread_id, NULL, &anuncPos::exec, this );
  }
}
anuncPos::~anuncPos(){}


void* anuncPos::exec(void* thr)
{
  ((anuncPos*)thr)->sirviente();
}
void anuncPos::sirviente()
{
  ic->waitForShutdown();
}

void anuncPos::publicarPos(float x, float y, float z)
{
  if(conectado){
    
    MLP::Position posic;
    MLP::Coord posicion;
    time_t tiem;
    posicion.x = x;
    posicion.y = y;
    posicion.z = z;
    time(&tiem);
    posic.time = tiem; 
      
    posic.theShape = new MLP::Point(posicion);
    posic.msid = "ar:00:0c:f1:5d:70:e0";
    publicar->locateReport(posic);
  }
}

