/*
 * ScriptingLUA.cpp
 *
 */

#include "ScriptingLUA.h"

namespace mars {

void ScriptingLUA::Init(){
	cout << "Initializing Script System" << endl;
}

ScriptingLUA::ScriptingLUA() {
	Logger::getLogger()->note("Initializing Scripting Support");
	__luaState = lua_open();
	luabind::open(__luaState);

	luabind::module(__luaState) [
	 luabind::class_<RenderableConstructor>("RenderableConstructor")
	 .def("createModel", &RenderableConstructor::createModel)
	 .def("createWButton", &RenderableConstructor::createWButton)
	 .def("createLight", &RenderableConstructor::createLight),
	 luabind::def("getRenderableConstructor", &RenderableConstructor::getInstance),

	 luabind::class_<World>("World")
	 .def("addWidget", &World::addWidget),
	 luabind::def("getWorld", &World::getInstance),

	 luabind::class_<RenderableWidget>("RenderableWidget")
	 .def( "setPosition", ( void(Renderable::*) (const int&, const int&) ) &Renderable::setPosition),

	 luabind::class_<RenderableWidgetButton>("RenderableWidgetButton")
	 .def("setPosition", ( void(Renderable::*) (const int&, const int&) ) &Renderable::setPosition),

	 luabind::class_<RenderableModel>("RenderableModel")
	 .def("setPosition", ( void(Renderable::*) (const float&, const float&, const float&) )&Renderable::setPosition),

	 luabind::class_<Light>("Light")
	 .def("setPosition", &Light::setPosition)
	 .def("setAmbient", &Light::setAmbient)
	 .def("setDiffuse", &Light::setDiffuse)
	 .def("setSpecular", &Light::setSpecular)
	 ,


	 luabind::def("init", &ScriptingLUA::Init)

	 ];


}

void ScriptingLUA::execLUAFile(const string& file){

	int i = luaL_dofile(
		__luaState,
		file.c_str()
		);

	if (i != 0)
		Logger::getInstance()->error("Error executing script : " + file);

}

ScriptingLUA::~ScriptingLUA() {
	Logger::getLogger()->note("Ending Scripting Support");
	lua_close(__luaState);
}

void ScriptingLUA::executeScript(const std::string & file){

}

}
