/*
 * Mesh.cpp
 *
 */

#include "Mesh.h"

namespace mars {

Mesh::Mesh() {
	__initialTranslation[0] = 0;
	__initialTranslation[1] = 0;
	__initialTranslation[2] = 0;
	__hasTexCoords = false;

}

Mesh::~Mesh() {}

bool Mesh::hasTexCoords(){
	return __hasTexCoords;
}

void Mesh::setName(const char* name){
	__name = name;
	__initialTranslation[0] = 0;
	__initialTranslation[1] = 0;
	__initialTranslation[2] = 0;
}

void Mesh::setHasTexCoords(){
	__hasTexCoords = true;
}

void Mesh::setInitialTranslation(const float& x, const float& y, const float& z){

#ifndef NDEBUG
	cout << "[DEBUG]:      ··Setting Initial Translation: " << x << ", " << y << ", " << z << endl;
#endif
	__initialTranslation[0] = x;
	__initialTranslation[1] = y;
	__initialTranslation[2] = z;

}

void Mesh::addRotation(Rotation* r){
#ifndef NDEBUG
	cout << "[DEBUG]:      ·· Adding Rotation: "  << r->x << " " << r->y << " "
			<< r->z << " " << r->r << " " << endl;
#endif
	__initialRotations.push_back(r);

}

void Mesh::addScale(Scale* s){
#ifndef NDEBUG
	cout << "[DEBUG]:      ·· Adding Scale: "  << s->x << " " << s->y << " "
				<< s->z << endl;
#endif
	__initialScales.push_back(s);
}

float* Mesh::getInitialTranslation(){
	return __initialTranslation;
}
unsigned Mesh::getCountOfRotations(){
	return __initialRotations.size();
}

Rotation* Mesh::getRotation(unsigned nRot){
	return __initialRotations.at(nRot);
}

unsigned Mesh::getCountOfScales(){
	return __initialScales.size();
}

Scale* Mesh::getScale(unsigned nScale){
	return __initialScales.at(nScale);
}
unsigned Mesh::getCountOfWires(){
	return __rEdges.size();
}


Edge* Mesh::getWire(unsigned n){
	return __rEdges.at(n);
}


void Mesh::reserveMemForVectors(unsigned nVerts, unsigned nFaces){
	__vertexes.reserve(nVerts);
	__faces.reserve(nFaces);
}

void Mesh::addVertex(const float& x, const float& y, const float& z){
	Vertex* v = new Vertex(x,y,z);
	__vertexes.push_back(v);
}

void Mesh::addVertex(Vertex* vertex){
	__vertexes.push_back(vertex);
}


Vertex* Mesh::getVertex(unsigned nVertex){
	return __vertexes[nVertex];
}

Vertex* Mesh::getVertexOfFace(unsigned nFace, unsigned nVertex){
	Face* f = __faces[nFace];
	int vertexIndex = f->vertexes[nVertex];
	return __vertexes[vertexIndex];
}

void Mesh::addFace(Face* face){
	__faces.push_back(face);
}

Face* Mesh::getFace(unsigned nFace){
	return __faces[nFace];
}


string Mesh::getName(){
	return __name;
}

unsigned Mesh::getCountOfFaces(){
	return __faces.size();
}

void Mesh::generateEdges(){
	for (unsigned i=0; i < __faces.size(); i++){
			Edge* tEdge = new Edge();

			tEdge->nFace = i;

			tEdge->p1.x = __vertexes.at(__faces.at(i)->vertexes.at(0))->x;
			tEdge->p1.y = __vertexes.at(__faces.at(i)->vertexes.at(0))->y;
			tEdge->p1.z = __vertexes.at(__faces.at(i)->vertexes.at(0))->z;
			tEdge->p2.x = __vertexes.at(__faces.at(i)->vertexes.at(1))->x;
			tEdge->p2.y = __vertexes.at(__faces.at(i)->vertexes.at(1))->y;
			tEdge->p2.z = __vertexes.at(__faces.at(i)->vertexes.at(1))->z;

			__edges.push_back(tEdge);

			tEdge = new Edge();

			tEdge->nFace = i;

			tEdge->p1.x = __vertexes.at(__faces.at(i)->vertexes.at(0))->x;
			tEdge->p1.y = __vertexes.at(__faces.at(i)->vertexes.at(0))->y;
			tEdge->p1.z = __vertexes.at(__faces.at(i)->vertexes.at(0))->z;
			tEdge->p2.x = __vertexes.at(__faces.at(i)->vertexes.at(2))->x;
			tEdge->p2.y = __vertexes.at(__faces.at(i)->vertexes.at(2))->y;
			tEdge->p2.z = __vertexes.at(__faces.at(i)->vertexes.at(2))->z;

			__edges.push_back(tEdge);

			tEdge = new Edge();

			tEdge->nFace = i;

			tEdge->p1.x = __vertexes.at(__faces.at(i)->vertexes.at(1))->x;
			tEdge->p1.y = __vertexes.at(__faces.at(i)->vertexes.at(1))->y;
			tEdge->p1.z = __vertexes.at(__faces.at(i)->vertexes.at(1))->z;
			tEdge->p2.x = __vertexes.at(__faces.at(i)->vertexes.at(2))->x;
			tEdge->p2.y = __vertexes.at(__faces.at(i)->vertexes.at(2))->y;
			tEdge->p2.z = __vertexes.at(__faces.at(i)->vertexes.at(2))->z;

			__edges.push_back(tEdge);


	}
#ifndef NDEBUG
	std::cout << "[DEBUG]: TriangleMesh:: " << __name << ":: Edges = " << __edges.size() <<endl;
#endif

}

void Mesh::removeDoubleEdges(){


vector<unsigned> facesSharing;


#ifndef NDEBUG
	std::cout << "[DEBUG]: Mesh:: " << __name << ":: Removing Edges ..." << endl;
#endif

	bool repeated = false;
	for (unsigned i=0; i < __edges.size(); i++){
		facesSharing.push_back(__edges.at(i)->nFace);
		for (unsigned j=i+1; j < __edges.size(); j++){
			if (
					( (__edges.at(i)->p1 == __edges.at(j)->p1) && (__edges.at(i)->p2 == __edges.at(j)->p2) )
				    ||
					( (__edges.at(i)->p2 == __edges.at(j)->p1) && (__edges.at(i)->p1 == __edges.at(j)->p2) )
				){
		        #ifndef NDEBUG
				std::cout << "[DEBUG]: Mesh:: " << __name << ":: Coincidence ... Faces:: " << __edges.at(i)->nFace << " and " <<  __edges.at(j)->nFace <<endl;
				std::cout << "[DEBUG]: " << __edges.at(i)->p1.x << " " <<  __edges.at(i)->p1.y << " " <<__edges.at(i)->p1.z << endl;
				std::cout << "[DEBUG]: " << __edges.at(i)->p2.x << " " <<  __edges.at(i)->p2.y << " " <<__edges.at(i)->p2.z << endl << endl;
				std::cout << "[DEBUG]: " << __edges.at(j)->p1.x << " " <<  __edges.at(j)->p1.y << " " <<__edges.at(j)->p1.z << endl;
				std::cout << "[DEBUG]: " << __edges.at(j)->p2.x << " " <<  __edges.at(j)->p2.y << " " <<__edges.at(j)->p2.z << endl << endl;
				#endif
				repeated = true;
				facesSharing.push_back(__edges.at(j)->nFace);
			}
		}
		if (repeated){
			Vector3D newNormal;
			if (!simplifyEdges(facesSharing, newNormal)){
				__edges.at(i)->normal = newNormal;
				__rEdges.push_back(__edges.at(i));
			}
		}
		repeated = false;
		facesSharing.clear();
	}


#ifndef NDEBUG
	std::cout << "[DEBUG]: Mesh:: " << __name << ":: REdges = " << __rEdges.size() <<endl;
#endif

}

bool Mesh::simplifyEdges(vector<unsigned> v, Vector3D& normal){
	if (v.size() != 2 ) {
		Logger::getLogger()->error("!!! MESH:: ESTRUCTURAL EDGES FAILURE!!! (Maybe not a quads/tri mesh?)");
		return false;
	}

	Vector3D a, b;
	a.x = __faces.at(v.at(0))->normal[0];
	a.y = __faces.at(v.at(0))->normal[1];
	a.z = __faces.at(v.at(0))->normal[2];
	b.x = __faces.at(v.at(1))->normal[0];
	b.y = __faces.at(v.at(1))->normal[1];
	b.z = __faces.at(v.at(1))->normal[2];

	normal = a + b ;
	normal.normalize();


	float angle = Vector3D::angleBetween(a,b);
	float angledeg = angle*180/Pi;

#if 0
	std::cout << "ANGLE RAD::" << angle << endl;
	std::cout << "ANGLE DEG::" << angledeg << endl;
#endif

	if (angledeg < DELTA_ANGLE)
		return true;

	return false;

}

unsigned Mesh::getType(){
	return __type;
}

void Mesh::prepareWireFrame(){
#if 0
	std::cout << "[DEBUG]: Mesh:: " << __name << ":: Preparing WireFrame." << endl;
#endif
	generateEdges();
	removeDoubleEdges();

}


}
