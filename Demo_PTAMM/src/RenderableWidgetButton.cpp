/*
 * RenderableWidgetButton.cpp
 *
 */

#include "RenderableWidgetButton.h"

namespace mars {

RenderableWidgetButton::RenderableWidgetButton() : RenderableWidget(1,1) {
	initButton();
}

RenderableWidgetButton::RenderableWidgetButton(const unsigned & w , const unsigned & h ) : RenderableWidget (w, h) {
	initButton();
}

RenderableWidgetButton::~RenderableWidgetButton() {
	delete __font;
}

void RenderableWidgetButton::initButton(){
	__autoScale = false;

	__margin = 0;

	__hasTexture = false;
	__texture = TextureFactory::getInstance()->getTexture(Configuration::getConfiguration()->defaultButtonTexture());

	if (__texture->isLoaded())
		__hasTexture = true;
	__font = new RenderableText(BUTTON);

	__font->setColor(__colorNotSelected[0], __colorNotSelected[1], __colorNotSelected[2]);

	__colorSelected[0] = 0.0; 	__colorSelected[1] = 0.0;
	__colorSelected[2] = 0.0; 	__colorSelected[3] = 0.0;

	__colorNotSelected[0] = 1.0; 	__colorNotSelected[1] = 1.0;
	__colorNotSelected[2] = 1.0; 	__colorNotSelected[3] = 1.0;

	__colorClicked[0] = 0.0; __colorClicked[1] = 1.0;
	__colorClicked[2] = 1.0; __colorClicked[3] = 1.0;
	__x1 = 0; __x2 = 0; __y1 = 0; __y2 = 0;



}

void RenderableWidgetButton::textColorSelected(const float& r,
                                               const float& g,
                                               const float& b,
                                               const float& a){
	__colorSelected[0]	= r;
	__colorSelected[1]	= g;
	__colorSelected[2]	= b;
	__colorSelected[3]	= a;
}

void RenderableWidgetButton::textColorNotSelected(const float& r,
                                                  const float& g,
                                                  const float& b,
                                                  const float& a){
	__colorNotSelected[0]	= r;
	__colorNotSelected[1]	= g;
	__colorNotSelected[2]	= b;
	__colorNotSelected[3]	= a;
}

void RenderableWidgetButton::textColorClicked(const float& r,
                                              const float& g,
                                              const float& b,
                                              const float& a){
	__colorClicked[0]	= r;
	__colorClicked[1]	= g;
	__colorClicked[2]	= b;
	__colorClicked[3]	= a;
}


void RenderableWidgetButton::useAutoScale(bool b){
	__autoScale = b;
}

void RenderableWidgetButton::setTexture(const string & name){
	__texture = TextureFactory::getInstance()->getTexture(name);
}

void RenderableWidgetButton::setText(const std::string& text){
	__text = text;
	__font->setText(__text.c_str());

	uint size = getWidth()/__text.size()*1.8;
	//	cout << "SIZE" << size << endl;
	bool fits = false;
	if (size < 1) {
		fits = true;
		__font->setSize(10);
	}
	while (!fits) {
		__font->setSize(size);
		float x1 = __font->getBounds()[0];
		float x2 = __font->getBounds()[3];
		float y1 = __font->getBounds()[1];
		float y2 = __font->getBounds()[4];
		float diffW = x2 - x1;
		float diffH = y2 - y1;
		if ( (diffW >= __width*0.8) || (diffH >= __height*0.8) ){
			size--;
		}
		else {
			fits = true;
			__size = size;
		}
	}
	//	cout << "SIZE:: " << size << endl;

}

void RenderableWidgetButton::setTextSize(uint size){
	__size = size;
}


void RenderableWidgetButton::handleMouseMotion(int x, int y){

	if (x > __x1 && x < __x2 && y > __y1 && y < __y2){
		select();
	}
	else unSelect();
}

void RenderableWidgetButton::handleMouseClick(int button, int x, int y){

	// 1 - left button
	// 2 - middle button
	// 3 - right button
	// 4 - wheel up
	// 5 - wheel down

#if 0
	printf("Mouse button %d pressed at (%d,%d)\n",
	       button, x, y);
#endif

	if (button >= MAX_BUTTONS) return;
	if (x > __x1 && x < __x2 && y > __y1 && y < __y2){
		__mouseButtonClicked[button] = true;
#if 0
		printf("Clicked!\n");
#endif
		SoundTool::playSound(SoundTool::getInstance()->getSound("click.wav"));
	}
}

void RenderableWidgetButton::handleMouseRelease(int button, int x, int y){
	if (button >= MAX_BUTTONS) return;

	if (x > __x1 && x < __x2 && y > __y1 && y < __y2){
		if (__mouseButtonClicked[1]) executeClickEvents();
	}
	__mouseButtonClicked[button] = false;
}

const std::string& RenderableWidgetButton::getText() const{
	return __text;
}

void RenderableWidgetButton::draw() {
	// Ortho proj.
	VideoOutputSDLOpenGL::getInstance()->setOrtho();
	//---------------------------------------------
	drawBackground();
	if (__text.size() > 0)
		drawText();
	//---------------------------------------------
	// View proj.
	VideoOutputSDLOpenGL::getInstance()->setView();
}

void RenderableWidgetButton::drawBackground(){

	glPushMatrix();

	int h2 = getHeight() / 2;
	int w2 = getWidth() / 2;

	glLoadIdentity();

	glTranslatef(__position.x , Configuration::getConfiguration()->openGLScreenHeight()-__position.y, 0);

	glDisable(GL_LIGHTING);

	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, __texture->getGLTexture());

	float x1, x2, y1, y2;
	int dx, dy;

	if (!__autoScale){

		//h2 = getWidth()  * 0.07;

		x1 = -w2 ;
		x2 = +w2 ;

		y1 = -h2; // * 1.8;
		y2 = +h2 ;

		dx = (x2 - x1) / 2;
		dy = (y2 - y1) / 2;

	} else {
		x1 = __font->getBounds()[0];
		x2 = __font->getBounds()[3];

		y1 = __font->getBounds()[1];
		y2 = __font->getBounds()[4];

		dx = (x2 - x1) / 2;
		dy = (y2 - y1) / 2;

		x1 = x1 - dx - __margin;
		x2 = x2 - dx + __margin;
		y1 = y1 - dy - __margin;
		y2 = y2 - dy + __margin;

	}

	__x1 = __position.x + x1;
	__x2 = __position.x + x2;

	__y1 = __position.y + y1;  //+ dy*(1/1.8);
	__y2 = __position.y + y2;  //+ dy*(1/1.8);

#if 0
	cout << "---" << endl;
	cout << "bounds :: " << __x1 << " :: " << __x2 << " :: " << __y1 << " :: " << __y2 << endl;
#endif

	glBegin(GL_QUADS);
		glTexCoord3f(0, 0, 0); glVertex3f(x1, y2, 0);
		glTexCoord3f(1, 0, 0); glVertex3f(x2, y2, 0);
		glTexCoord3f(1, 1, 0); glVertex3f(x2, y1, 0);
		glTexCoord3f(0, 1, 0); glVertex3f(x1, y1, 0);
	glEnd();


	if (World::getInstance()->isLightingEnabled())
		glEnable(GL_LIGHTING);

	glDisable(GL_BLEND);

	glPopMatrix();

}

void RenderableWidgetButton::drawText() const{

	if (__mouseButtonClicked[1]){
		__font->setColor(__colorClicked[0], __colorClicked[1], __colorClicked[2]);
	} else if (this->isSelected()){
		__font->setColor(__colorSelected[0], __colorSelected[1], __colorSelected[2]);
	} else {
		__font->setColor(__colorNotSelected[0], __colorNotSelected[1], __colorNotSelected[2]);
	}

#if 0
	cout << __position.x <<  " ::::: " << Configuration::getConfiguration()->openGLScreenHeigh-__position.y << endl;
#endif

	__font->setSize(__size);

	__font->setPosition(__position.x,Configuration::getConfiguration()->openGLScreenHeight()-__position.y, 0 );

	glDisable(GL_LIGHTING);
	glPushMatrix();
	glLoadIdentity();

	__font->draw();

	if (World::getInstance()->isLightingEnabled())
		glEnable(GL_LIGHTING);

	glPopMatrix();
}


} // namespace
