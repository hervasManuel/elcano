/*
 * RenderableText.cpp
 *
 */

#include "RenderableText.h"

namespace mars {

RenderableText::RenderableText(FontType fType){
	cx = 0; cy = 0; cz = 0;
	x1 = 0; y1 = 0; z1 = 0;
	x2 = 0; y2 = 0; z2 = 0;

	__rotX = 0; __rotY = 0; __rotZ = 0;

	__name = "Generic Text";

	if (fType == BUTTON){
		__font = FontFactory::getFontFactory()->getButtonFont();
	}
	else{
		__font = FontFactory::getFontFactory()->getDefaultFont();
	}

	if (!__font->CharMap(ft_encoding_unicode))
		Logger::getLogger()->warning("Unable to set unicode encoding");
	__fontType = fType;
	__color[0] = 1.0; __color[1] = 1.0; __color[2] = 1.0;
	updateText();
}

RenderableText::RenderableText(std::string fontname, char* str, FontType fType) {
	cx = 0; cy = 0; cz = 0;
	x1 = 0; y1 = 0; z1 = 0;
	x2 = 0; y2 = 0; z2 = 0;

	__rotX = 0; __rotY = 0; __rotZ = 0;

	__name = "Generic Text";
	FontFactory::getFontFactory()->addFont(fontname, fType);
	__font = FontFactory::getFontFactory()->getFont(fontname);
	if (!__font->CharMap(ft_encoding_unicode))
		Logger::getLogger()->warning("Unable to set unicode encoding");
	__text = str;
	__fontType = fType;
	__color[0] = 1.0; __color[1] = 1.0; __color[2] = 1.0;
//	updateText();
}

void RenderableText::setColor(GLfloat r, GLfloat g, GLfloat b){
	__color[0] = r; __color[1] = g; __color[2] = b;
}

void RenderableText::setText(char* str){
	__text = str;
//	updateText();
}

void RenderableText::setSize(unsigned s){
	__font->FaceSize(s);
	//	updateText();
}

void RenderableText::setText(std::string str){
	lock();
	__text = str;
	//	updateText();
	unlock();
}

void RenderableText::animateWire(){
	if ((SDL_GetTicks() - __wireAnimLastTime) > 20){
		__wireAnimLastTime = SDL_GetTicks();
		__wireAnimStep++;
		if (__wireAnimStep > 13 ){
			__wireAnimStep=1;
		}
	}
}

void RenderableText::draw(){

	lock();

	updateText();

	if (__fontType == POLYGON){
		glDisable(GL_LIGHTING);
		glDisable(GL_TEXTURE_2D);
		glEnable(GL_BLEND);
	} else if (__fontType == BUTTON){
		glDisable(GL_LIGHTING);
		glEnable(GL_TEXTURE_2D);
        glDisable(GL_DEPTH_TEST);
        glEnable(GL_BLEND);
        glNormal3f(0.0, 0.0, 1.0);
	} else {
		glDisable(GL_TEXTURE_2D);
	}

	glEnable(GL_POLYGON_SMOOTH);

	if (__wire){
		glEnable(GL_LINE_SMOOTH);
		glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);

		glLineWidth(__wireNormalWidth*__wireAnimStep+__wireMinWidth);
		glColor4f(__wireColor[0]*__wireAnimStep, __wireColor[1] / __wireAnimStep, __wireColor[2] / __wireAnimStep,  __wireColor[3]*__wireAnimStep);
	}

	if (__wireAnimated)
			animateWire();
	else
		__wireAnimStep = 1;


	glPushMatrix();


	glColor3fv(__color);


	computePositionRotation();

	glTranslatef(cx, cy, cz);


#if 0
	Matrix16 m;

	glGetFloatv(GL_MODELVIEW_MATRIX, m.getData());
	cout << ":: " << __name << " :: " << m << endl;
#endif



	glPushMatrix();
	__font->Render(__text.c_str());
	glPopMatrix();

	glColor3f(1, 1, 1);

#if 0
		cout << "--" << endl;
		cout << __text << endl;
		cout << "LX:: " << x1 << " LY:: " << y1 << " LZ:: " << z1 << endl;
		cout << "UX:: " << x2 << " UY:: " << y2 << " UZ:: " << z2 << endl;
		cout << "--" << endl;
		cout << "cX:: " << cx << " cY:: " << cy << " cZ:: " << cz << endl;
		cout << "rX:: " << __rotX << " rY:: " << __rotY <<  " rZ:: " << __rotZ << endl;
		cout << "POS " << __position.x << " - " << __position.y << " - " << __position.z << endl;
#endif
#if 0
		drawBBox();
#endif

	glPopMatrix();

	if (__wire){
		glDisable(GL_LINE_SMOOTH);
		glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
	}

	if (__fontType == POLYGON)
		if (World::getInstance()->isLightingEnabled())
			glEnable(GL_LIGHTING);

	unlock();
}

void RenderableText::drawBBox(){

	glDisable(GL_LIGHTING);

	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE); // GL_ONE_MINUS_SRC_ALPHA

	glColor3f(0.0, 1.0, 0.0);
	// Draw the front face
	glBegin(GL_LINE_LOOP);
	glVertex3f(x1, y1, z1);
	glVertex3f(x1, y2, z1);
	glVertex3f(x2, y2, z1);
	glVertex3f(x2, y1, z1);
	glEnd();

	glEnable(GL_LIGHTING);

	glDisable(GL_LINE_SMOOTH);
	glDisable(GL_BLEND);

}

float* RenderableText::getCenter(){
	return &cx;
}

float* RenderableText::getBounds(){
	return &x1;
}

void RenderableText::updateText(){
	FTBBox bbox = __font->BBox(__text.c_str());
	x1 = bbox.Lower().Xf(); y1 = bbox.Lower().Yf(); z1 = bbox.Lower().Zf();
	x2 = bbox.Upper().Xf(); y2 = bbox.Upper().Yf(); z2 = bbox.Upper().Zf();
	cx = - ((x2 - x1) /2 );
	cy = - ((y2 - y1) /2 );
	cz = - ((z2 - z1) /2 );

#if 0
	cout << "--" << endl;
	cout << __text << endl;
	cout << "LX:: " << x1 << " LY:: " << y1 << " LZ:: " << z1 << endl;
	cout << "UX:: " << x2 << " UY:: " << y2 << " UZ:: " << z2 << endl;
	cout << "--" << endl;
	cout << "cX:: " << cx << " cY:: " << cy << " cZ:: " << cz << endl;
	cout << "rX:: " << __rotX << " rY:: " << __rotY <<  " rZ:: " << __rotZ << endl;
#endif
}

RenderableText::~RenderableText() {}

}
