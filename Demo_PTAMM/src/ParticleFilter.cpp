/*
 * ParticleFilter.cpp
 *
 */

#include "ParticleFilter.h"

namespace mars{
 
  ParticleFilter::ParticleFilter(){}
  
  void ParticleFilter::filterAbsolute(vector<tUserL*>* historicalRecord,tUserL* posResult){
    if(historicalRecord->size()>=4){
      tUserL* pos1=historicalRecord->at(historicalRecord->size()-1);
      tUserL* pos2=historicalRecord->at(historicalRecord->size()-2);
      tUserL* pos3=historicalRecord->at(historicalRecord->size()-3);
      tUserL* pos4=historicalRecord->at(historicalRecord->size()-4);

      /*We make a weight average*/
      /*Calculating eye*/
      posResult->__Eye.x=pos1->__Eye.x*0.5+
	pos2->__Eye.x*0.25+
	pos3->__Eye.x*0.15+
	pos4->__Eye.x*0.10;

      posResult->__Eye.y=pos1->__Eye.y*0.5+
	pos2->__Eye.y*0.25+
	pos3->__Eye.y*0.15+
	pos4->__Eye.y*0.10;

      posResult->__Eye.z=pos1->__Eye.z*0.5+
	pos2->__Eye.z*0.25+
	pos3->__Eye.z*0.15+
	pos4->__Eye.z*0.10;

      /*Calculating LookTo*/
      posResult->__LookTo.x=pos1->__LookTo.x*0.5+
	pos2->__LookTo.x*0.25+
	pos3->__LookTo.x*0.15+
	pos4->__LookTo.x*0.10;

     posResult->__LookTo.y=pos1->__LookTo.y*0.5+
	pos2->__LookTo.y*0.25+
	pos3->__LookTo.y*0.15+
	pos4->__LookTo.y*0.10;

     posResult->__LookTo.z=pos1->__LookTo.z*0.5+
	pos2->__LookTo.z*0.25+
	pos3->__LookTo.z*0.15+
	pos4->__LookTo.z*0.10;

     
      /*Calculating Up*/
      posResult->__Up.x=pos1->__Up.x*0.5+
	pos2->__Up.x*0.25+
	pos3->__Up.x*0.15+
	pos4->__Up.x*0.10;
     
      posResult->__Up.y=pos1->__Up.y*0.5+
	pos2->__Up.y*0.25+
	pos3->__Up.y*0.15+
	pos4->__Up.y*0.10;
     
      posResult->__Up.z=pos1->__Up.z*0.5+
	pos2->__Up.z*0.25+
	pos3->__Up.z*0.15+
	pos4->__Up.z*0.10;

    }else{
      historicalRecord->at(historicalRecord->size()-1); /*Return de last position*/    
      //      std::cout<<"Filter does not work!"<<std::endl;
    }
  }
  ParticleFilter::~ParticleFilter(){}
  
}
