/*
 * FontFactory.cpp
 *
 */

#include "FontFactory.h"

namespace mars {


FontFactory* FontFactory::getFontFactory(){
	return FontFactory::getInstance();
}

FTFont* FontFactory::getDefaultFont(){
	return __defaultFont;
}

FTFont* FontFactory::getButtonFont(){
	return __buttonFont;
}

FTFont* FontFactory::getConsoleFont(){
	return __consoleFont;
}

FTFont* FontFactory::getFont(string fontName){
	__fontIterator = __fontList.find(fontName);
	if (__fontIterator != __fontList.end())
		return __fontList.at(fontName);
	else
		return __defaultFont;
}

FontFactory::FontFactory() {
  string df = Configuration::getConfiguration()->defaultFontPath();
  df += Configuration::getConfiguration()->defaultFont();

	__defaultFont = new FTPolygonFont(df.c_str());

	if (__defaultFont->Error()){
		Logger::getLogger()->error("FontFactory:: Can't open default font: " + df);
	} else {
	  __fontList[Configuration::getConfiguration()->defaultFont()] = __defaultFont;
	}

	__buttonFont = new FTBufferFont(df.c_str());
	if (__buttonFont->Error()){
			Logger::getLogger()->error("FontFactory:: Can't open button font: " + df);
	} else {
	  __fontList[Configuration::getConfiguration()->defaultFont()+"Button"] = __buttonFont;
	}

	__consoleFont = new FTBufferFont(df.c_str());
	if (__consoleFont->Error()){
			Logger::getLogger()->error("FontFactory:: Can't open console font: " + df);
	} else {
	  __fontList[Configuration::getConfiguration()->defaultFont()+"Console"] = __consoleFont;
	}

}

void FontFactory::addFont(string fontName, unsigned typeFont){
	if (__fontList.find(fontName) != __fontList.end()){
		Logger::getLogger()->warning("FontFactory:: There is a font called " + fontName + " already.");
		return;
	} else { // new font
	  string df = Configuration::getConfiguration()->defaultFontPath();
		df += fontName;
		FTFont* tempFont;
		switch(typeFont){
			case POLYGON:
				tempFont = new FTPolygonFont(df.c_str());
#ifndef NDEBUG
				cout << "[DEBUG]: polyfont" << endl;
#endif
				break;
			case EXTRUDED:
				tempFont = new FTExtrudeFont(df.c_str());
				tempFont->Depth(1);
				//tempFont->Outset(0, 3);
#ifndef NDEBUG
				cout << "[DEBUG]: extruded font" << endl;
#endif
				break;
			default:
				tempFont = new FTPolygonFont(df.c_str());
		}

		if (tempFont->Error()){
			Logger::getLogger()->warning("FontFactory:: Error Adding " + df);
		} else {
			__fontList[fontName] = tempFont;
		}
	}
}

FontFactory::~FontFactory() {
#ifndef NDEBUG
	std::cout << "[DEBUG]: FontFactory:: Cleaning Up." << endl;
#endif
//	__fontIterator = __fontList.begin();
//	while(__fontIterator != __fontList.end()){
//		delete (__fontIterator->second);
//	}


}

}
