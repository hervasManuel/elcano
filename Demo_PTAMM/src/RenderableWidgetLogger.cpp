/*
 * RenderableWidgetLogger.cpp
 *
 */

#include "RenderableWidgetLogger.h"

namespace mars {

RenderableWidgetLogger::RenderableWidgetLogger() : RenderableWidgetConsole(640, 150) {
		setPosition(320,75);
		hide();
		Logger::getInstance()->setConsoleFunctions(RenderableWidgetLogger::aError,
		                                           RenderableWidgetLogger::aWarning,
		                                           RenderableWidgetLogger::aNote);
		World::getInstance()->setConsole(this);
}

RenderableWidgetLogger::~RenderableWidgetLogger() {
	Logger::getInstance()->__hasConsole = false;
}

void RenderableWidgetLogger::aError(const std::string& msg){
	RenderableWidgetLogger::getInstance()->addError(msg);
}

void RenderableWidgetLogger::aWarning(const std::string& msg){
	RenderableWidgetLogger::getInstance()->addWarning(msg);
}

void RenderableWidgetLogger::aNote(const std::string& msg){
	RenderableWidgetLogger::getInstance()->addNote(msg);
}


}
