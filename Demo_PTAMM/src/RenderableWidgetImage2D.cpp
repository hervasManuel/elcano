/*
 * RenderableWidgetImage2D.cpp
 *
 */

#include "RenderableWidgetImage2D.h"

namespace mars {

RenderableWidgetImage2D::RenderableWidgetImage2D(std::string textureFile,
		const unsigned w, const unsigned h) : RenderableWidget(w, h), __angle2D(0) {

	__imageTexture = TextureFactory::getInstance()->getTexture(textureFile);

}


void RenderableWidgetImage2D::draw(){

	// Ortho proj.
	VideoOutputSDLOpenGL::getInstance()->setOrtho();
	//---------------------------------------------

	glPushMatrix();

	int h2 = getHeight() / 2;
	int w2 = getWidth() / 2;

	glLoadIdentity();

	glTranslatef(__position.x , Configuration::getConfiguration()->openGLScreenHeight()-__position.y, 0);

	glRotatef(__angle2D, 0.0, 0.0, 1.0); // ROTATION ??????

	glDisable(GL_LIGHTING);

	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, __imageTexture->getGLTexture());

	float x1, x2, y1, y2;

	x1 = -w2;
	x2 = +w2;

	y1 = -h2;
	y2 = +h2 ;

#if 0
	cout << "pos X:: " << __position.x << endl;
	cout << "pos Y:: " << __position.y << endl;
	cout << "X1:: " << x1 << endl;
	cout << "X2:: " << x2 << endl;
	cout << "Y1:: " << y1 << endl;
	cout << "Y2:: " << y2 << endl;
	Matrix16::printModelView();
#endif





	glBegin(GL_QUADS);
		glTexCoord3f(0, 0, 0); glVertex3f(x1, y2, 0);
		glTexCoord3f(1, 0, 0); glVertex3f(x2, y2, 0);
		glTexCoord3f(1, 1, 0); glVertex3f(x2, y1, 0);
		glTexCoord3f(0, 1, 0); glVertex3f(x1, y1, 0);
	glEnd();


	if (World::getInstance()->isLightingEnabled())
		glEnable(GL_LIGHTING);

	glDisable(GL_BLEND);

	glPopMatrix();

	//---------------------------------------------
	// View proj.
	VideoOutputSDLOpenGL::getInstance()->setView();

}

RenderableWidgetImage2D::~RenderableWidgetImage2D() {

}




}
