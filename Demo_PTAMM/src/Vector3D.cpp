/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * Vector3D.cpp
 *
 */

#include "Vector3D.h"

namespace mars {



Vector3D::Vector3D(){};

Vector3D::Vector3D(const float& x_, const float& y_, const float& z_){
	x = x_;
	y = y_;
	z = z_;
}

Vector3D Vector3D::operator+(const Vector3D& other){
	Vector3D temp;

	temp.x = x + other.x;
	temp.y = y + other.y;
	temp.z = z + other.z;

	return temp;
}

Vector3D Vector3D::operator-(const Vector3D& other){
	Vector3D temp;

	temp.x = x - other.x;
	temp.y = y - other.y;
	temp.z = z - other.z;

	return temp;
}

Vector3D Vector3D::operator*(const float& scalar){
	Vector3D temp;

	temp.x = x * scalar;
	temp.y = y * scalar;
	temp.z = z * scalar;

	return temp;
}

float Vector3D::operator*(const Vector3D& other){

	return (other.x * x + other.y * y + other.z * z);
}

bool Vector3D::operator==(const Vector3D& v){
	if ( (v.x == x) && (v.y == y) && (v.z == z) ) return true;
	return false;
}

float Vector3D::lenght(){
	return (sqrt(x*x+y*y+z*z));
}

void Vector3D::normalize(){
	float l = lenght();
	if (l!=0){
		x = x / l;
		y = y / l;
		z = z / l;
	}
}


std::ostream& operator<<(std::ostream& os, const Vector3D& v){
	os << "( " <<  v.x << " , " << v.y << " , " << v.z << " )";
	return os;
}

Vector3D Vector3D::crossProduct(const Vector3D& v, const Vector3D& w){
	Vector3D normal;


	normal.x = (v.y * w.z) - (w.y * v.z);
	normal.y = (v.z * w.x) - (w.z * v.x);
	normal.z = (v.x * w.y) - (w.x * v.y);

	return normal;
}

Vector3D Vector3D::rotate(float* m){
	Vector3D temp;

	temp.x = (m[0] * x) + (m[4] * y) + (m[8] * z);
	temp.y = (m[1] * x) + (m[5] * y) + (m[9] * z);
	temp.z = (m[2] * x) + (m[6] * y) + (m[10] * z);

	return temp;

}


float Vector3D::angleBetween(const Vector3D& v, const Vector3D& w){
	float vl = sqrt(v.x*v.x+v.y*v.y+v.z*v.z);
	float wl = sqrt(w.x*w.x+w.y*w.y+w.z*w.z);
	float vw = v.x*w.x + v.y*w.y + v.z*w.z;

	return acos( vw / (vl*wl) );
}

void Vector3D::setComponets(const float& x_, const float& y_, const float& z_ ){
	x = x_;
	y = y_;
	z = z_;
}

Vector3D::~Vector3D() {}

}
