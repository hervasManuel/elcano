/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * FramesBuffer.cpp
 *
 */


#include "FramesBuffer.h"



namespace mars {

FramesBuffer::FramesBuffer() {
	// By default, we'll use a 5 frames buffer (a 5 elements list of OpenCV Mat's)
	__maxSavedFrames = 5;
	__frames.reserve(5);
	pthread_mutex_init(&mutex_frame, 0);

#ifndef NDEBUG
	std::stringstream ss;
	ss << &mutex_frame;
	mutex_frame_str = ss.str();
#endif
}



FramesBuffer::FramesBuffer(const unsigned& maxSavedFrames) {
	__maxSavedFrames = maxSavedFrames;
	__frames.reserve(maxSavedFrames);
}


void FramesBuffer::lock(){
	pthread_mutex_lock(&mutex_frame);
}

void FramesBuffer::unlock(){
	pthread_mutex_unlock(&mutex_frame);
}

void FramesBuffer::addFrame(Mat* newFrame){

//	lock();

#if 0
	std::cout << "[DEBUG]: MUTEX WRITE IN (" << mutex_frame_str << ")" << endl << flush;
#endif

	if (__frames.size() >= __maxSavedFrames){ // If the max number of frames
		                                      // is reached => Free the mem.

		delete __frames.at(0);
		__frames.erase(__frames.begin());

	}
	__frames.push_back(newFrame);             // Add the new frame


#if 0
	std::cout << "[DEBUG]: MUTEX WRITE OUT (" << mutex_frame_str << ")" << endl << flush;
#endif

//	unlock();


#if 0
	std::cout << "[DEBUG]: Frames Vector Size: " << __frames.size() << endl;
	std::cout << "[DEBUG]: Last Mat added: " << __frames.back() << endl;
#endif

}

cv::Mat* FramesBuffer::getFrame(const unsigned& nFrame){

#if 0
	std::cout << "[DEBUG]: Trying to return frame number: " << nFrame << endl;
#endif

//	lock();
	if (nFrame >= (__maxSavedFrames) ) {
		std::cout << "Bad Number of Frame (" << nFrame << ")." << endl;
		return NULL;
	}
	IplImage i = *__frames.at(nFrame);
	cv::Mat* frame = new Mat(&i, true);
//	unlock();
	return frame;
}

cv::Mat* FramesBuffer::getLastFrame(){

#if 0
	std::cout << "[DEBUG]: Trying to return last frame." << endl;
#endif

//	lock(); // Lock the buffer. Respect mutex :)

#if 0
	std::cout << "[DEBUG]: MUTEX READ IN (" << mutex_frame_str << ")" << endl << flush;
#endif


	if (__frames.empty()){
		std::cout << "Empty" << std::endl;
		return NULL;
	}

//	std::cout << "SIZE: " << __frames.size() << std::endl;
	//IplImage i = *__frames.at(__frames.size() - 1);
	cv::Mat* frame =  new Mat(*__frames.at(__frames.size()-1));

#if 0
	std::cout << "[DEBUG]: MUTEX READ OUT (" << mutex_frame_str << ")" << endl << flush;
#endif

//	unlock();


	return frame;
}

unsigned FramesBuffer::size(){
	return __frames.size();
}

bool FramesBuffer::isEmpty(){
	return __frames.empty();
}

FramesBuffer::~FramesBuffer() {
#ifndef NDEBUG
	std::cout << "[DEBUG]: FramesBuffer Destructor::: Destroying mutex" << std::endl;
#endif
	pthread_mutex_destroy(&mutex_frame);

	#ifndef NDEBUG
	std::cout << "[DEBUG]: FramesBuffer Destructor::: Destroying frames" << std::endl;
#endif
	for (unsigned i = 0; i < __frames.size(); i++){
		delete __frames.at(i);
	}


}

}
