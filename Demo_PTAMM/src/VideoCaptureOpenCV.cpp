/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along 		with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/


/*
 * VideoCapture.cpp
 *
 */

#include "VideoCaptureOpenCV.h"
#include "VideoFileOpenCV.h"
#include <iostream>

namespace mars {

VideoCaptureOpenCV::VideoCaptureOpenCV() {


}


void VideoCaptureOpenCV::addNewVideoSource(unsigned dev, const std::string& name){
	VideoCapture::addVideoSource_(new mars::VideoDeviceOpenCV(dev, name));
}

void VideoCaptureOpenCV::addNewVideoSource(const std::string& path, const std::string& name){
	VideoCapture::addVideoSource_(new mars::VideoFileOpenCV(path, name));
}

VideoCaptureOpenCV::~VideoCaptureOpenCV() {}

}
