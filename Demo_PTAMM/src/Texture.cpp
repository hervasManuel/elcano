/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * Texture.cpp
 *
 */

#include "Texture.h"


namespace mars {

Texture::Texture() {


}

Texture::Texture(string pathToFile) {
	loadFromFile(pathToFile);
}

void Texture::loadFromFile(string pathToFile){

	__glTexture=NULL;

	// Let's get the extension of the file
	string extension = pathToFile.substr( pathToFile.find_last_of(".")+1,
			                              pathToFile.length()-1);

    __textureName = pathToFile.substr(pathToFile.find_last_of("/")+1,
									  pathToFile.length() -
									  pathToFile.find_last_of("/") -
									  extension.length()-2 );

    string fileName = __textureName + "." + extension;

	// UpperCase extension!
	std::transform(extension.begin(), extension.end(),
			       extension.begin(), ::toupper);

#if 1//ndef NDEBUG
	cout << "[DEBUG]: ---------------------------------------------------------------\n";
    cout << "[DEBUG]:   ···Trying to load: " << pathToFile << endl;
    cout << "[DEBUG]:   ···Extensión: " << extension << endl << flush;
#endif




    // load the image onto a surface
    SDL_Surface* __sdlSurface = NULL;

    __sdlSurface = IMG_Load(pathToFile.c_str());

    if (__sdlSurface==NULL){
#if 1//ndef NDEBUG
    	cout << "[DEBUG]:   ···Not found: " << pathToFile << endl;
        cout << "[DEBUG]:   ···Trying to load: " << Configuration::getConfiguration()->defaultTexturePath() + fileName << endl;
#endif
        __sdlSurface = IMG_Load((Configuration::getConfiguration()->defaultTexturePath() + fileName).c_str());

    }

    if (__sdlSurface!=NULL){ // Loaded

    	// Let's check if it's a power of 2 texture.
    	if ( (__sdlSurface->w & (__sdlSurface->w-1) ) !=0 ){
    		Logger::getLogger()->warning("Texture:: " + fileName +  " :: Image's width is not a power of 2");
    	}
    	if ( (__sdlSurface->h & (__sdlSurface->h-1) ) !=0 ){
    		Logger::getLogger()->warning("Texture:: " + fileName +  " :: Image's height is not a power of 2");
    	}

    	switch(__sdlSurface->format->BytesPerPixel){

			case 4: //It has an alpha channel
				if (__sdlSurface->format->Rmask == 0x000000ff){
					__glTextureFormat = GL_RGBA;
				}
				else{
					__glTextureFormat = GL_BGRA;
				}
				break;

			case 3: // No alpha
				if (__sdlSurface->format->Rmask == 0x000000ff){
					__glTextureFormat = GL_RGB;
				}
				else{
					__glTextureFormat = GL_BGR;
				}
				break;

			default:
				Logger::getLogger()->warning("Texture:: " + fileName +  " :: The image is probably broken.");

    	} // end-switch

    	// generate the texture
    	glGenTextures(1, &__glTexture);
    	if (__glTexture == GL_INVALID_OPERATION){
    		cout << "Error glGenTextures" << endl << flush;
    		exit(0);
		}

    	// Bind the texture
   		glBindTexture( GL_TEXTURE_2D, __glTexture );


   		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
   		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );


// Temporaly disabled .. th mipmpas will not be computed on gpu becouse not all our hardware
// is compablible.
// -------------
//   		gluBuild2DMipmaps(GL_TEXTURE_2D,__glTextureFormat,__sdlSurface->w,
//   				__sdlSurface->h, __glTextureFormat, GL_UNSIGNED_BYTE, __sdlSurface->pixels);
// --------------

   		glTexImage2D( GL_TEXTURE_2D, 0, __sdlSurface->format->BytesPerPixel, __sdlSurface->w, __sdlSurface->h, 0,
   			 __glTextureFormat, GL_UNSIGNED_BYTE, __sdlSurface->pixels );

#ifndef NDEBUG
   		cout << "[DEBUG]:   ···Texture OpenGL = " << __glTexture << endl;
cout << "[DEBUG]: ---------------------------------------------------------------\n";
#endif

   		__loaded = true;

    	// free the surface
    	SDL_FreeSurface( __sdlSurface );

    } else { // Not loaded
    	Logger::getLogger()->error("Texture:: " + fileName +  " :: Image can't be loaded.");
    	Logger::getLogger()->warning("Texture:: " + fileName +  " :: Continuing without loading it!");
    	__loaded= false;
    }

}

GLuint Texture::getGLTexture(){
	return __glTexture;
}

bool Texture::isLoaded(){
	return __loaded;
}

void Texture::convertCVMatToTexture(cv::Mat* mat, GLuint* texture){

	IplImage image = mat->operator _IplImage();

	glGenTextures(1, texture);

	glBindTexture( GL_TEXTURE_2D,  *texture); //bind the texture to its array
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image.width, image.height,0, GL_BGR, GL_UNSIGNED_BYTE, image.imageData);
	return;

}


GLuint Texture::convertSurfaceToOpenGLTexture(SDL_Surface* sdlSurface){

	if (sdlSurface == NULL){
		cout << "Surface is Null" << endl;
		return 0;
	}

//	GLenum glTextureFormat;

//	switch(sdlSurface->format->BytesPerPixel){
//
//	case 4: //It has an alpha channel
//		if (sdlSurface->format->Rmask == 0x000000ff){
//			glTextureFormat = GL_RGBA;
//		}
//		else{
//			glTextureFormat = GL_BGRA;
//		}
//		break;
//
//	case 3: // No alpha
//		if (sdlSurface->format->Rmask == 0x000000ff){
//			glTextureFormat = GL_RGB;
//		}
//		else{
//			glTextureFormat = GL_BGR;
//		}
//		break;
//
//	default:
//		Logger::getLogger()->warning("Texture:: No texture format found");
//
//	} // end-switch

//	cout << "BPS :: " << (int) sdlSurface->format->BytesPerPixel << endl;

	GLuint tempTexture;

	glGenTextures(1, &tempTexture);

	if (tempTexture == GL_INVALID_OPERATION){
		cout << "Error glGenTextures" << endl << flush;
		return tempTexture;
	}

	glBindTexture( GL_TEXTURE_2D,  tempTexture);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	glTexImage2D( GL_TEXTURE_2D, 0, sdlSurface->format->BytesPerPixel, sdlSurface->w, sdlSurface->h, 0,
			GL_BGRA, GL_UNSIGNED_BYTE, sdlSurface->pixels );

//	cout << "Temptexture :: " << tempTexture << endl;

	return tempTexture;
}

void Texture::freeOpenGLTexture(GLuint tex){
	glDeleteTextures(1, &tex);
}

Texture::~Texture() {
	// Delete the texture
#ifndef NDEBUG
	cout << "[DEBUG]: Deleting Texture: " << __textureName << endl;
#endif
	glDeleteTextures(1, &__glTexture);
}

}
