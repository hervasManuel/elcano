/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/*
 *
 *  Renderable.cpp
 *
 */

#include "Renderable.h"


namespace mars {

Renderable::Renderable() {

#ifndef NDEBUG
	std::cout << "Creating Renderable " << std::endl;
#endif
	pthread_mutex_init(&mutex_render, 0);

	__scaleX = 1;
	__scaleY = 1;
	__scaleZ = 1;

	__useMatrix=false;

	__shown = true;

	__rotX = 0;
	__rotY = 0;
	__rotZ = 0;
	__position.x = 0;
	__position.y = 0;
	__position.z = 0;

	__cull = false;
	__allTransparentWhileWire = false;

	__wire = false;

	__wireXor = false;
	__wireTextured = false;

	__wireColor[0] = 1.0;
	__wireColor[1] = 1.0;
	__wireColor[2] = 1.0;
	__wireColor[3] = 1.0;

	__wireFillColor[0] = .40;
	__wireFillColor[1] = .40;
	__wireFillColor[2] = .40;
	__wireFillColor[3] = 0.3;

	__wireNormalWidth = 2.0;
	__wireMaxWidth = 4;
	__wireMinWidth = 1;

	__wireAnimated = false;
	__wireBackEnabled = false;
	__wireAnimStep = 1;

	__wireAnimLastTime = 0;


	__type = r2D;

	__actAslight = false;
	__nLight = 0;

	__internalMat.getData()[14] = -5;

}

void Renderable::setPosition(const float& x, const float& y, const float& z){
	__position.x = x;
	__position.y = y;
	__position.z = z;

}

void Renderable::setPosition(const int& x, const int & y){
	__position.x = x;
	__position.y = y;
}


void Renderable::setAngles(const float& x, const float& y, const float& z){
	__rotX = x;
	__rotY = y;
	__rotZ = z;

}

bool Renderable::isShown(){
	return __shown;
}

bool Renderable::is3D(){
	return false;
}

float* Renderable::getAngles(){
	return &__rotX;
}

  float Renderable::getScaleX(){
    return __scaleX;
  }

Point3D Renderable::getPosition(){
	return __position;
}


void Renderable::drawNormal(GLfloat* beginPoint, GLfloat* endPoint){
	glBegin(GL_LINES);
		glVertex3fv(beginPoint);
		glVertex3fv(endPoint);
	glEnd();
}

void Renderable::setScale(const float& x, const float& y, const float& z){
	__scaleX = x;
	__scaleY = y;
	__scaleZ = z;
}

void Renderable::useMatrix(bool b){
	__useMatrix = b;
}

void Renderable::setMatrix(const Matrix16& m){
	__internalMat = m;
}

Matrix16 Renderable::getMatrix() const{
	return __internalMat;
}

void Renderable::show(){
	__shown = true;
}

void Renderable::hide(){
	__shown = false;
}


void Renderable::actAsLight(bool b, unsigned n){
	__actAslight = b;
	__nLight = n;
}

void Renderable::computePositionRotation(){



	if (__useMatrix){
		glLoadIdentity();
		glLoadMatrixf(__internalMat.getData());

	} else {
		glTranslatef(__position.x, __position.y, __position.z);
		glRotatef(__rotX,1.0f,0.0f,0.0f);
		glRotatef(__rotY,0.0f,1.0f,0.0f);
		glRotatef(__rotZ,0.0f,0.0f,1.0f);

	}

	glScalef(__scaleX, __scaleY, __scaleZ);

#if 0
	Matrix16::printModelView();
#endif


}

  //Returns the ModelView matrix relative to the origin
cv::Mat Renderable::getRelativePosition(){
  glPushMatrix();
  glLoadIdentity();
  
  glTranslatef(__position.x, __position.y, __position.z);
  
  
  glRotatef(__rotX,1.0f,0.0f,0.0f);
  glRotatef(__rotY,0.0f,1.0f,0.0f);
  glRotatef(__rotZ,0.0f,0.0f,1.0f);
  
  glScalef(__scaleX, __scaleY, __scaleZ);
  
  Matrix16::printModelView();

  float m[16];
  glGetFloatv(GL_MODELVIEW_MATRIX, m);

  cv::Mat RelativeMat(4,4,CV_32F);
  for(int i=0;i<4;i++){
    for(int j=0;j<4;j++){
      RelativeMat.at<float>(i,j)=m[i*4+j];
    }
  }

  glPopMatrix();


  return RelativeMat;
}

void Renderable::setRenderMode(unsigned mode){
	if (mode > 5){
		Logger::getLogger()->warning("Renderable:: Wrong render mode."
				"It should be a number between 0 and 4.");
		return;
	}

	std::cout<<"Setting render mode: "<<mode<<std::endl;
	
	switch(mode){
	case 0: __wire = false; __cull = false; __wireTextured = false; __allTransparentWhileWire = false; __wireXor = false; break;
	case 1: __wire = true;  __cull = false; break;
	case 2: __wire = true;  __cull = true;  break;
	case 3: __wire = true;  __cull = true;  __wireTextured = true; break;
	case 4: __wire = true;  __cull = true;  __allTransparentWhileWire = true; break;
	case 5: __wire = true;  __cull = true;  __wireXor = true; break;
	}
}

void Renderable::addRotationX(float angle){
	__rotX += angle;
}

void Renderable::addRotationY(float angle){
	__rotY += angle;
}

void Renderable::addRotationZ(float angle){
	__rotZ += angle;
}

void Renderable::addX(float xInc){
	__position.x += xInc;
}

void Renderable::addY(float yInc){
	__position.y += yInc;
}

void Renderable::addZ(float zInc){
	__position.z += zInc;
}

Renderable::~Renderable() {
	pthread_mutex_destroy(&mutex_render);
}

void Renderable::lock(){
	pthread_mutex_lock(&mutex_render);
}

void Renderable::unlock(){
	pthread_mutex_unlock(&mutex_render);
}

}
