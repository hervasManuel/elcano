/*
 * World.cpp
 *
 */

#include "World.h"

namespace mars {


World::World() {

	__width = Configuration::getConfiguration()->openGLScreenWidth();
	__height = Configuration::getConfiguration()->openGLScreenHeight();

//	__buttonTexture = NULL;
	__console = NULL;

	__resizeFrames = false;

	__bgSet = false;

	__defaultCamera = new Camera3D();

	__lighting = false;

	__done = false;

	__selectedCamera = 0;

	__allWire = true;
	__allWireTrans = false;
	__allWireTex = false;
	__allWireAnimated = false;
	__allWireBackEnabled = false;

	__nSteps = 0;
	__t0 = SDL_GetTicks();
	__showFPS = false;

	__ff = FontFactory::getFontFactory();
	__dFont = __ff->getDefaultFont();

	if (!__dFont->Error()){
		if (!__dFont->CharMap(ft_encoding_unicode)){
			Logger::getLogger()->warning("World:: Cannot set default font encoding.");
		}
	} else {
		Logger::getLogger()->warning("World:: Cannot get default font.");
	}


#ifndef NDEBUG
	cout << "[DEBUG]: Creating World :)" << endl;
#endif

//	glEnable(GL_NORMALIZE);
//	glEnable(GL_LIGHTING);

	glEnable(GL_TEXTURE_2D);

}

void World::setConsole(RenderableWidget* w){
	__console = w;
//	__widgets.push_back(w);
}

void World::addRenderable(Renderable* r){
	__renderables.push_back(r);
}

void World::setFrameResizing(bool b){
	__resizeFrames = b;
}

void World::addLight(Light* l){
#ifndef NDEBUG
	cout << "[DEBUG]: ---------------------------------------------------------------" << endl;
	cout << "[DEBUG]: Adding light" << endl;
	cout << "[DEBUG]: -- " << endl;
	const GLfloat* a = l->getAmbient();
	cout << "[DEBUG]: Ambient : {" << a[0] << ", " << a[1] << ", " << a[2] << ", " << a[3] << "}" << endl;
	const GLfloat* d = l->getDiffuse();
	cout << "[DEBUG]: Diffuse : {" << d[0] << ", " << d[1] << ", " << d[2] << ", " << d[3]  << "}" << endl;
	const GLfloat* p = l->getPosition();
	cout << "[DEBUG]: Position: (" << p[0] << ", " << p[1] << ", " << p[2] <<  ")" << endl;
	cout << "[DEBUG]: ---------------------------------------------------------------" << endl;
#endif
	if (__lights.size() > 7){
		Logger::getLogger()->warning("World:: Lights Vector is full. Maximum number of light is 8.");
	} else {
		__lights.push_back(l);

		int i = __lights.size()-1;
		GLuint light= GL_LIGHT0;
		switch(i){
			case 0: light = GL_LIGHT0; break;
			case 1: light = GL_LIGHT1; break;
			case 2: light = GL_LIGHT2; break;
			case 3: light = GL_LIGHT3; break;
			case 4: light = GL_LIGHT4; break;
			case 5: light = GL_LIGHT5; break;
			case 6: light = GL_LIGHT6; break;
			case 7: light = GL_LIGHT7; break;
		}
		glLightfv(light, GL_AMBIENT,   __lights.at(i)->getAmbient() );
		glLightfv(light, GL_DIFFUSE,   __lights.at(i)->getDiffuse() );
		glLightfv(light, GL_POSITION,  __lights.at(i)->getPosition() );
		glEnable(light);

	}

}


void World::updateLights(){
	GLuint light = GL_LIGHT0;
	for (unsigned i = 0; i < __lights.size(); i++){
		switch(i){
			case 0: light = GL_LIGHT0; break;
			case 1: light = GL_LIGHT1; break;
			case 2: light = GL_LIGHT2; break;
			case 3: light = GL_LIGHT3; break;
			case 4: light = GL_LIGHT4; break;
			case 5: light = GL_LIGHT5; break;
			case 6: light = GL_LIGHT6; break;
			case 7: light = GL_LIGHT7; break;
		}
		glLightfv(light, GL_AMBIENT,   __lights.at(i)->getAmbient() );
		glLightfv(light, GL_DIFFUSE,   __lights.at(i)->getDiffuse() );
		glLightfv(light, GL_POSITION,  __lights.at(i)->getPosition() );
	}
}


void World::setBackgroundSource(VideoSource* vS){
	__backgroundVS = vS;
	if (vS != NULL) __bgSet = true;
}

void World::setBackgroundImage(cv::Mat* mat){

  flip(*mat,__backgroundImage,0);
  

}


void World::drawBackground(){


if (__resizeFrames){
	cv::Mat temp;

	cv::resize(__backgroundImage, temp, cv::Size(__width, __height), 0, 0, cv::INTER_CUBIC);
	__backgroundImage = temp;
}

#if 0
	cout << __backgroundImage.cols << endl;
	cout << __backgroundImage.rows << endl;
#endif

	glDisable(GL_TEXTURE_2D);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	gluOrtho2D(0.0,__backgroundImage.cols,__backgroundImage.rows,0.0);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glDisable(GL_DEPTH_TEST);
	glDrawPixels(__backgroundImage.cols,__backgroundImage.rows,GL_BGR,
			GL_UNSIGNED_BYTE,__backgroundImage.data);
	glEnable(GL_DEPTH_TEST);
	glPopMatrix();
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glEnable(GL_TEXTURE_2D);

}

void World::draw(){

	vector<Renderable*>::iterator it;
	it = __renderables.begin();
	while(it!=__renderables.end()){
		if (((Renderable*) *it)->isShown())
			((Renderable*) *it)->draw();
		it++;
	}

	vector<RenderableWidget*>::iterator itw;
	itw = __widgets.begin();
	while(itw!=__widgets.end()){
		if (((Renderable*) *itw)->isShown())
			((Renderable*) *itw)->draw();
		itw++;
	}

	if (__console != NULL)
		if (__console->isShown())
			__console->draw();

}

void World::addWidget(RenderableWidget* w){
	//__renderables.push_back(w);
	__widgets.push_back(w);
}

Camera3D* World::getSelectedCamera(){
	if (!__cameras.empty())
		return __cameras.at(__selectedCamera);
	else
		return __defaultCamera;
}

void World::worldLoop(){
	// This is only used for demos... it should no be used for a regular build

	cout << "[DEBUG]: Setting World Loop up" << endl;

    while (!__done){

    	step();
    	eventsCall();

    }// end while loop


}

bool World::eventsCall(){
	SDL_Event event;


    while (SDL_PollEvent(&event)){
    	if (event.type ==SDL_QUIT){
    		__done=true;
    	}
#if 0
    	if (event.type == SDL_MOUSEBUTTONDOWN){
    		printf("Mouse button %d pressed at (%d,%d)\n",
    				event.button.button, event.button.x, event.button.y);
    	}
    	else
    	if (event.type == SDL_MOUSEMOTION){
    		printf("Mouse moved by %d,%d to (%d,%d)\n",
    				event.motion.xrel, event.motion.yrel,
    				event.motion.x, event.motion.y);
    		vector<RenderableWidget*>::iterator it;
    		for (it=__widgets.begin(); it==__widgets.end(); ++it){
    			(*it)->handleEvent(event);
    		}

    	}
#endif

    	vector<RenderableWidget*>::iterator it;
    	for (it=__widgets.begin(); it!=__widgets.end(); ++it){
    		(*it)->handleEvent(event);
    	}

    	if ( event.type == SDL_KEYDOWN ){

#ifndef NDEBUG
    		cout <<  "KeyCode: " << (int) (event.key.keysym.sym) << endl;
#endif
    		switch(event.key.keysym.sym){


    		case 186:

    			if (__console==NULL) break;

    			if (__console->isShown()){
#ifndef NDEBUG
    			cout << "Hiding console" << endl;
#endif
    				__console->hide();
    			}
    			else{
#ifndef NDEBUG
    			cout << "Showing console" << endl;
#endif
    				__console->show();
    			}
    			break;

    		case SDLK_ESCAPE:
		  //__done = true;
		  SDL_Quit();
		  return false;
		  break;
		  
    		// case SDLK_l: // Lights on/off
//     			__lighting = !__lighting;
// #ifndef NDEBUG
//     			cout << "[KEY L] ";
// #endif
//      			if (__lighting){
// #ifndef NDEBUG
//     				cout << "Enabling Lighting" << endl;
// #endif
//      				glEnable(GL_LIGHTING);
//     			}
//     			else{
// #ifndef NDEBUG
//     				cout << "Disabling Lighting" << endl;
// #endif
//      				glDisable(GL_LIGHTING);
//     			}
//     			break;

//     		case SDLK_w: //All renderables on wireframe
//     			__allWire = !__allWire;
// #ifndef NDEBUG
//     			cout << "[KEY W] ";
// #endif

//     			__allWireTrans = false;
//     			__allWireTex = false;
//     			__allWireXor = false;

//     			if (__allWire){
// #ifndef NDEBUG
//     				cout << "Enabling All Wired" << endl;
// #endif
//      				vector<Renderable*>::iterator it = __renderables.begin();
//     				while (it!=__renderables.end()){
//     					(*it)->setRenderMode(2);
//     					it++;
//     				}
//     			}
//     			else{
// #ifndef NDEBUG
//     				cout << "Disabling All Wired" << endl;
// #endif
//      				vector<Renderable*>::iterator it = __renderables.begin();
//     				while (it!=__renderables.end()){
//     					(*it)->setRenderMode(0);
//     					it++;
//     				}
//     			}
//     			break;

//     		case SDLK_t:
//     			__allWireTex = !__allWireTex;

// #ifndef NDEBUG
//     			cout << "[KEY T] ";

// #endif
//      			__allWire = false;
//     			__allWireTrans = false;
//     			__allWireXor = false;

//     			if (__allWireTex){
// #ifndef NDEBUG
//     				cout << "Enabling All Textured Wire" << endl;
// #endif
//      				vector<Renderable*>::iterator it = __renderables.begin();
//     				while (it!=__renderables.end()){
//     					(*it)->setRenderMode(3);
//     					it++;
//     				}
//     			}
//     			else{
// #ifndef NDEBUG
//     				cout << "Disabling All Textured Wire" << endl;
// #endif
//      				vector<Renderable*>::iterator it = __renderables.begin();
//     				while (it!=__renderables.end()){
//     					(*it)->setRenderMode(0);
//     					it++;
//     				}
//     			}
//     			break;
//     		case SDLK_z:
//     			__allWireTrans = !__allWireTrans;

// #ifndef NDEBUG
//     			cout << "[KEY z] ";
// #endif

//     			__allWire = false;
//     			__allWireTex = false;
//     			__allWireXor = false;

//     			if (__allWireTrans){
// #ifndef NDEBUG
//     				cout << "Enabling All Wired Transparent (disabling depth test...)" << endl;
// #endif
//      				vector<Renderable*>::iterator it = __renderables.begin();
//     				while (it!=__renderables.end()){
//     					(*it)->setRenderMode(4);
//     					it++;
//     				}
//     			}
//     			else{
// #ifndef NDEBUG
//     				cout << "Disabling All Wired Transparent (Enabling depth test...)" << endl;
// #endif
//      				vector<Renderable*>::iterator it = __renderables.begin();
//     				while (it!=__renderables.end()){
//     					(*it)->setRenderMode(0);
//     					it++;
//     				}
//     			}
//     			break;
//     		case SDLK_x:
//     			__allWireXor = !__allWireXor;

//     			__allWire = false;
//     			__allWireTex = false;
//     			__allWireTrans = false;



//     			cout << "[KEY x] ";

//     			if (__allWireXor){
// #ifndef NDEBUG
//     				cout << "Enabling All Wired Xor" << endl;
// #endif
//      				vector<Renderable*>::iterator it = __renderables.begin();
//     				while (it!=__renderables.end()){
//     					(*it)->setRenderMode(5);
//     					it++;
//     				}
//     			}
//     			else{
// #ifndef NDEBUG
//     				cout << "Disabling All Wired Xor" << endl;
// #endif
//      				vector<Renderable*>::iterator it = __renderables.begin();
//     				while (it!=__renderables.end()){
//     					(*it)->setRenderMode(0);
//     					it++;
//     				}
//     			}
//     			break;

//     		case SDLK_a:
//     			__allWireAnimated = !__allWireAnimated;

// #ifndef NDEBUG
//     			cout << "[KEY a] ";
// #endif

//     			if (__allWireAnimated){
// #ifndef NDEBUG
//     				cout << "Enabling All Wired Animations" << endl;
// #endif
//      				vector<Renderable*>::iterator it = __renderables.begin();
//     				while (it!=__renderables.end()){
//     					if ((*it)->is3D())
//     						static_cast <RenderableModel*> (*it)->enableWireAnim();
//     					it++;
//     				}
//     			}
//     			else{
// #ifndef NDEBUG
//     				cout << "Disabling All Wired Animations" << endl;
// #endif
//      				vector<Renderable*>::iterator it = __renderables.begin();
//     				while (it!=__renderables.end()){
//     					if ((*it)->is3D())
//     						static_cast <RenderableModel*> (*it)->disableWireAnim();
//     					it++;
//     				}
//     			}
//     			break;

//     		case SDLK_b:

//     			__allWireBackEnabled = !__allWireBackEnabled;

// #ifndef NDEBUG
//     			cout << "[KEY B] ";
// #endif

//     			if (__allWireBackEnabled){
// #ifndef NDEBUG
//     				cout << "Enabling All Wired Backs" << endl;
// #endif
//      				vector<Renderable*>::iterator it = __renderables.begin();
//     				while (it!=__renderables.end()){
//     					if ((*it)->is3D())
//     						static_cast <RenderableModel*> (*it)->enableBackWire();
//     					it++;
//     				}
//     			}
//     			else{
// #ifndef NDEBUG
//     				cout << "Disabling All Wired Backs" << endl;
// #endif
//      				vector<Renderable*>::iterator it = __renderables.begin();
//     				while (it!=__renderables.end()){
//     					if ((*it)->is3D())
//     						static_cast <RenderableModel*> (*it)->disableBackWire();
//     					it++;
//     				}
//     			}
//     			break;

		default: 
		  break;

    		} // end switch

    	} // end if keydown

    }// end while poll

    return true;
}

bool World::step(){


	__nSteps++;
	if (__nSteps == 1000){
		double t = 1000.0 / ( (SDL_GetTicks() - __t0) / (float) (__nSteps) );

		if (__showFPS){
			Logger::getInstance()->note(toString(t)+ " fps");
		}

		__fps = t;
		__t0 = SDL_GetTicks();
		__nSteps = 0;

	}


	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	if (__bgSet){
		cv::Mat* i = __backgroundVS->getLastFrame();
		setBackgroundImage(i);
		delete i;
	}


	drawBackground();


	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();


	if (!__cameras.empty()){
		__cameras[__selectedCamera]->use();
	}

	updateLights();


	this->draw();

	SDL_GL_SwapBuffers();

	return eventsCall();

}

void World::addCamera(Camera3D* camera){
	__cameras.push_back(camera);
}

void World::addCamera(VideoSource* vs){
	Camera3D* c = vs->getOpenGLCamera();
	__cameras.push_back(c);
	c->setEye(0,0,20);
	c->setLookTo(0,0,-6);
	selectCamera(__cameras.size()-1);
}


void World::selectCamera(unsigned n){
	if (n > __cameras.size()-1){
		Logger::getLogger()->warning("World:: Camera index out of bounds.");
	} else {
		__selectedCamera = n;
	}
	setUpProjection();
}

void World::enableLighting(){
	__lighting = true;
	glEnable(GL_LIGHTING);
}

void World::setUpProjection(){


	// Setup the viewport for using all the screen.
	glViewport( 0, 0, (GLsizei) __width, (GLsizei) __height);

	glMatrixMode(GL_PROJECTION);

	glLoadIdentity();
	glLoadMatrixf(__cameras.at(__selectedCamera)->getProjectionMatrix().getData());
	//gluPerspective(54.0f, ratio, 0.1f, 100.0f); // 45 is the human vision fovy.

//	Matrix16 m;
//	glGetFloatv(GL_PROJECTION_MATRIX, m.getData());
//	std::cout << "--\Projection\n--\n" << m << std::endl;

#ifndef NDEBUG
	cout << "[DEBUG]: ------------------------------------- " << endl;
	cout << "[DEBUG]:  custom projection matrix loaded      " << endl;
	cout << "[DEBUG]: ------------------------------------- " << endl;
#endif

	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();
}

void World::disableLighting(){
	__lighting = false;
	glDisable(GL_LIGHTING);
}

bool World::isLightingEnabled(){
	return __lighting;
}

//Texture*  World::getButtonTexture(){
//	if (__buttonTexture == NULL)
//		__buttonTexture = new Texture(Configuration::getConfiguration()->defaultButtonTexture() );
//	return __buttonTexture;
//}


World::~World() {
	Logger::getLogger()->note("The end of the world :)");
	delete __defaultCamera;
	//delete __buttonTexture;
}

}

