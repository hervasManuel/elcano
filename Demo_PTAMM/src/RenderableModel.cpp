/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * RenderableModel.cpp
 *
 */

#include "RenderableModel.h"

namespace mars {


RenderableModel::RenderableModel() {
	__name = "unknown";

	//Initialize
	__position.x = 0;
	__position.y = 0;
	__position.z = 0;

	__wire = false;

	__type= r3D;

	setRenderMode(0);

}

RenderableModel::RenderableModel(std::string pathToFile){

	__name = "unknown";

//	// Save the path (and the whales!!)
//	__filePath = pathToFile;

	//Initialize
	__position.x = 0;
	__position.y = 0;
	__position.z = 0;

	__wire = false;

	__type= r3D;

	setRenderMode(2);



	// Let's load the file
	loadWithColladaDOM(pathToFile);

}



void RenderableModel::loadWithColladaDOM(string pathToFile){


	// Save the path (and the whales!!)
	__filePath = pathToFile;

	// Let's get the extension of the file
	string extension = pathToFile.substr( pathToFile.find_last_of(".")+1,
				                          pathToFile.length()-1);

	//And the name
	__name = pathToFile.substr(pathToFile.find_last_of("/")+1,
			pathToFile.length()-pathToFile.find_last_of("/")-extension.length()-2);

#ifndef NDEBUG
	cout << "[DEBUG]: Renderable3D:: Name: " << __name << endl;
#endif

	// UpperCase it!
	std::transform(extension.begin(), extension.end(),
			extension.begin(), ::toupper);


#ifndef NDEBUG
		cout << "[DEBUG]: Renderable3D:: Trying to load: " << pathToFile << endl;
		cout << "[DEBUG]: Renderable3D:: Extension: " << extension << endl << flush;
#endif

		__wMeshes = true;

	setlocale (LC_NUMERIC, "C");

	DAE* dae;
	dae=new DAE;

	daeElement* root = dae->open(pathToFile);

	if (!root) {
		Logger::getLogger()->warning("Error opening DAE. Is it a valid collada file?");
	    return;
	}

#ifndef NDEBUG
	cout << "[DEBUG]: ---------------------------------------------------------------\n";
	for (size_t i = 0; i < root->getAttributeCount(); i++) {
		cout << "[DEBUG]: attr " << i << " name: " << root->getAttributeName(i) << endl;
		cout << "[DEBUG]: attr " << i << " value: " << root->getAttribute(i) << endl;
	}
	cout << "[DEBUG]: ---------------------------------------------------------------\n";
#endif

	vector<daeElement*> geometries = dae->getDatabase()->typeLookup(domGeometry::ID());

#ifndef NDEBUG
	cout << endl << endl;
	cout << "[DEBUG]: ---------------------------------------------------------------\n";
	cout << "[DEBUG]: Renderable3D:: " << geometries.size()
	     << "[DEBUG]:  geometries to be loaded ..." << endl;
	cout << "[DEBUG]: Renderable3d:: DAE :: Geometries: " << endl;
	for (size_t i = 0; i < geometries.size(); i++){
		cout << "[DEBUG]: · geometry " << i << " id: "
			 << geometries[i]->getAttribute("id") << endl;
	}
	cout << "[DEBUG]: ---------------------------------------------------------------\n";
#endif

	vector<daeElement*> nodes = dae->getDatabase()->typeLookup(domNode::ID());
#ifndef NDEBUG
	cout << endl << endl;
	cout << "[DEBUG]: ---------------------------------------------------------------\n";
	cout << "[DEBUG]: Renderable3D:: " << nodes.size()
	     << " nodes ..." << endl;
	cout << "[DEBUG]: Renderable3d:: DAE :: Nodes: " << endl;
	for (size_t i = 0; i < nodes.size(); i++){
		cout << "[DEBUG]: · node " << i << " id: "
			 << nodes[i]->getAttribute("id") << endl;
	}
	cout << "[DEBUG]: ---------------------------------------------------------------\n";
#endif


	// Let's look for meshes and process them.
	for (unsigned nGeom = 0; nGeom < geometries.size(); nGeom++){
		string gName = geometries[nGeom]->getAttribute("id");
		domGeometry* d = (domGeometry*) geometries[nGeom];
		domMesh* m = d->getMesh();

		// Not a mesh? Skip.
		if (!m){
#ifndef NDEBUG
			cout << "[DEBUG]: " << gName << " is not a mesh, skipping\n";
#endif
			continue;
		}

//--------------------------------------------
//		Poly Meshes
//--------------------------------------------
		if (m->getPolygons_array().getCount() > 0){
			Logger::getLogger()->note("Renderable:: " + __name + ":: Polygon mesh ...");
			domPolygons_Array polys = m->getPolygons_array();

			unsigned nPolyGroups = polys.getCount();
			PolyMesh* pMesh = new PolyMesh();
			// Set the name based on ID
			pMesh->setName(d->getAttribute("id").c_str());
			for (unsigned nPolys=0; nPolys < nPolyGroups; nPolys++){
				domPolygonsRef polyRef = polys[nPolys];
				//Now let's check for input arrays, where the real data is.
				int nArrays = 0;
				bool hasTexCoords = false;
				int vertexOffset=0;
				int texCoordOffset=0;
				for (unsigned nIArray=0; nIArray < polyRef->getInput_array().getCount(); nIArray++){

					string semantic = polyRef->getInput_array()[nIArray]->getSemantic();
#ifndef NDEBUG
					cout << "[DEBUG]: Semantic: " << semantic << endl;
#endif
					// We want to keep the offsets of the data.
					hasTexCoords = false;
					if (semantic.compare("VERTEX") == 0){
						vertexOffset = nIArray;
					}
					else
						if (semantic.compare("TEXCOORD") == 0){
							texCoordOffset = nIArray;
							hasTexCoords = true;

					}
#ifndef NDEBUG
						else{
							// Note we don't need normals, we'll calculate it
							cout << "[DEBUG]: " << semantic << " : Will not be used." << endl;

						}
#endif
					nArrays++; // One more array of data.

				} // end for offsets (nIArray)

				const domVerticesRef vertsRef = m->getVertices();
				int nInputs = vertsRef->getInput_array().getCount();

				domInputLocalRef fPosRef;
				domInputLocalRef fTexRef;

				for (int nInput=0; nInput < nInputs; nInput++){
					domInputLocalRef localRef = vertsRef->getInput_array()[nInput];
					string semanticI = localRef->getSemantic();
					if (semanticI.compare("POSITION")==0){
						fPosRef = localRef; // That's what we wanted
					}
				} // end for input arrays.

				domFloat_arrayRef flPosArray = m->getSource_array()[vertexOffset]->getFloat_array();
				if(flPosArray){ // We have the vertex array finally!
					// Get them as a list of floats
					const domListOfFloats& vertexList = m->getSource_array()[vertexOffset]->getFloat_array()->getValue();
					int stride = 3;
					unsigned nVertexes = flPosArray->getCount() / stride;
					unsigned nFaces = polyRef->getCount();
					pMesh->reserveMemForVectors(nVertexes, nFaces);
#ifndef NDEBUG
					cout << "[DEBUG]: ·Number of vertexes: " << nVertexes << endl;
					cout << "[DEBUG]: ·Number of faces: " << nFaces << endl;
#endif
					// Add the vertexes to our mesh
					for (unsigned nVert=0; nVert < nVertexes; nVert++){
#if 0
						cout << __name << " X:::: " << vertexList[nVert*3] << endl;
						cout << __name << " Y:::: " << vertexList[nVert*3+1] << endl;
						cout << __name << " Z:::: " << vertexList[nVert*3+2] << endl;
#endif
						pMesh->addVertex(vertexList[nVert*3], vertexList[nVert*3+1], vertexList[nVert*3+2]);
					}

					// We get the list of integer representing our list of faces ...
					// Please read collada doc, cause it has an offset ...
//					int nFaces_ = polyRef->getP_array().getCount();

					//Texture Coords
					vector<TexCoord*> texCoords;
					if (hasTexCoords){
						domFloat_arrayRef flTexArray = m->getSource_array()[texCoordOffset]->getFloat_array();
						if (flTexArray){
							const domListOfFloats& coordsList = flTexArray->getValue();

							// div 2 'cause of the S,P values
							for (unsigned t = 0; t < flTexArray->getCount(); t+=2){ //
								TexCoord* tc;
								tc = new TexCoord();
								tc->s = coordsList[t];
								tc->t = coordsList[t+1];
								texCoords.push_back(tc);
								pMesh->setHasTexCoords();

							}

						} else {
							hasTexCoords = false;
							pMesh->setHasTexCoords();
						}
					}


					// Add the faces to our mesh
					for(unsigned nFace=0; nFace < nFaces; nFace++){
						Face* f = new Face();

						domListOfUInts lFace= polyRef->getP_array()[nFace]->getValue();

						f->vertex_count = lFace.getCount()/nArrays;

						for (unsigned iF = 0; iF < f->vertex_count; iF++){
							f->vertexes.push_back(lFace[(nArrays*iF)]);
						}




						if (pMesh->hasTexCoords()){


							for (unsigned iF = 0; iF < f->vertex_count; iF++){
								f->texCoords.push_back(texCoords.at(lFace[(nArrays*iF)+texCoordOffset]));
							}



#if 0
							for (unsigned iF = 0; iF < f->vertex_count; iF++){
								cout << "INDEX: " << lFace[(nArrays*iF)]  << endl;
								cout << f->texCoords.at(iF)->s << "  " << f->texCoords.at(iF)->t << endl;
							}

#endif
						}



						// Calculate Normals POLY
						Vertex v1 = *(pMesh->getVertex(f->vertexes[0]));
						Vertex v2 = *(pMesh->getVertex(f->vertexes[1]));
						Vertex v3 = *(pMesh->getVertex(f->vertexes[2]));

						Vector3D v = v2 - v1;    Vector3D w = v3 - v1;
						Vector3D normalTemp = Vector3D::crossProduct(v, w);
						normalTemp.normalize();

						f->normal[0] = normalTemp.x;
						f->normal[1] = normalTemp.y;
						f->normal[2] = normalTemp.z;

						pMesh->addFace(f);

					} // end for faces

					//->>>


				} // end if flPosArray.
			} // end PolyGroups -------------------------------------

			// Get the initial rotation and location of this polymesh
			std::string tempString;
			for (unsigned nNode=0; nNode < nodes.size(); nNode++){
				domNode* tempNode = (domNode*) nodes.at(nNode);
				daeElement* iGeom = tempNode->getChild("instance_geometry");
				// Let's see if this node has the initial properties of our mesh
				if (iGeom != NULL){
					tempString = iGeom->getAttribute("url");
					// Delete the first char "#"
					tempString = tempString.substr(1,tempString.size()-1);
#ifndef NDEBUG
					cout << "[DEBUG]: ---------------------------------------------------------------\n";
					cout << "[DEBUG]: Object Scene:: " << tempString << endl;
#endif
					if (tempString.compare(pMesh->getName().c_str())==0){
#ifndef NDEBUG
						cout << "[DEBUG]: Initial Settings:" << endl;
#endif
						// Get the initial rotation angles.
						domTranslate* dT = (domTranslate*) tempNode->getChild("translate");
						if (dT != NULL){
							domFloat3 transFloat = dT->getValue();
							pMesh->setInitialTranslation(transFloat[0], transFloat[1], transFloat[2]);
						}
						domFloat4 rotateXFloat;
						domFloat4 rotateYFloat;
						domFloat4 rotateZFloat;

						// Now let's go for the rotations
						unsigned nRots =  tempNode->getRotate_array().getCount();
#ifndef NDEBUG
						cout << "[DEBUG]: ··Number of Initial rotations: " << nRots << endl;
#endif
						for (unsigned nRot=0; nRot < nRots; nRot++){
							Rotation* tempRot = new Rotation();
							daeSmartRef<domRotate> rotation = tempNode->getRotate_array().get(nRot);
							if (rotation->getValue().getCount() != 4 ){
								Logger::getLogger()->warning("Bad Angle inside dae file");
							} else {
								tempRot->x = (float) rotation->getValue().get(0);
								tempRot->y = (float) rotation->getValue().get(1);
								tempRot->z = (float) rotation->getValue().get(2);
								tempRot->r = (float) rotation->getValue().get(3);
								pMesh->addRotation(tempRot);
							}
						}

						// Now ... the scale
						unsigned nScales = tempNode->getScale_array().getCount();
#ifndef NDEBUG
						cout << "[DEBUG]: ··Number of initial scales: " << nScales << endl;
#endif
						for (unsigned nScale=0; nScale < nScales; nScale++){
							Scale* tempScale = new Scale();
							daeSmartRef<domScale> scale = tempNode->getScale_array().get(nScale);
							if (scale->getValue().getCount() != 3){
								stringstream msg;
								msg << "Renderable::" << __name << ":: Bad scale transforms inside dae file";
								Logger::getLogger()->warning(msg.str());

							} else {
								tempScale->x = (float) scale->getValue().get(0);
								tempScale->y = (float) scale->getValue().get(1);
								tempScale->z = (float) scale->getValue().get(2);
								pMesh->addScale(tempScale);
							}
						}
#ifndef NDEBUG
						cout << "[DEBUG]: ---------------------------------------------------------------\n";
#endif
					}
				}
			}
			//-------------------------------------------------------
			//-------------------------------------------------------
			__meshes.push_back(pMesh);

		} // end of polymesh

		else // triangles?

//-------------------------------------------------------------------------------
//		Triangle Meshes
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------

		// Check for triangle mesh
		if ( m->getTriangles_array().getCount() > 0 ){
			Logger::getLogger()->note("Renderable:: " + __name + ":: Triangle mesh.");


			domTriangles_Array triangles = m->getTriangles_array();
			unsigned nTriGroups = triangles.getCount(); // How many tris groups?

			// Our new trimesh
			TriangleMesh* tMesh = new TriangleMesh();
			tMesh->setName(d->getAttribute("id").c_str());

			for (unsigned nTris=0; nTris < nTriGroups; nTris++){
				domTrianglesRef triRef = triangles[nTris];

				//Now let's check for input arrays, where the real data is.
				int nArrays = 0;
				bool hasTexCoords = false;
				int vertexOffset=0;
				int texCoordOffset=0;
				for (unsigned nIArray=0; nIArray < triRef->getInput_array().getCount(); nIArray++){

					string semantic = triRef->getInput_array()[nIArray]->getSemantic();

#ifndef NDEBUG
					cout << "[DEBUG]: Semantic: " << semantic << endl;
#endif
					// We want to keep the offsets of the data.
					if (semantic.compare("VERTEX") == 0){
						vertexOffset = nIArray;
					}
					else
						if (semantic.compare("TEXCOORD") == 0){
							texCoordOffset = nIArray;
							hasTexCoords = true;
						}

#ifndef NDEBUG
						else{
							// Note we don't need normals, we'll calculate it
							cout << "[DEBUG]: " << semantic << " : Will not be used." << endl;
						}
#endif
					nArrays++; // One more array of data.

				} // end for offsets (nIArray)

				const domVerticesRef vertsRef = m->getVertices();
				int nInputs = vertsRef->getInput_array().getCount();

				domInputLocalRef fPosRef;
				domInputLocalRef fTexRef;

				for (int nInput=0; nInput < nInputs; nInput++){
					domInputLocalRef localRef = vertsRef->getInput_array()[nInput];
					string semanticI = localRef->getSemantic();
					if (semanticI.compare("POSITION")==0){
						fPosRef = localRef; // That's what we wanted
					}
				} // end for input arrays.


				domFloat_arrayRef flPosArray = m->getSource_array()[vertexOffset]->getFloat_array();
				if(flPosArray){ // We have the vertex array finally!
					// Get them as a list of floats
					const domListOfFloats& vertexList = m->getSource_array()[vertexOffset]->getFloat_array()->getValue();
					int stride = 3; // 3 floats per vertex
					unsigned nVertexes = flPosArray->getCount() / stride;
					unsigned nFaces = triRef->getCount();


					// Reserve space for our vertexes and faces
					tMesh->reserveMemForVectors(nVertexes, nFaces);
#ifndef NDEBUG
					cout << "[DEBUG]: ·Number of vertexes: " << nVertexes << endl;
					cout << "[DEBUG]: ·Number of faces: " << nFaces << endl;
#endif

					// Add the vertexes to our mesh
					for (unsigned nVert=0; nVert < nVertexes; nVert++){
#if 0
						cout << __name << " X:::: " << vertexList[nVert*3] << endl;
						cout << __name << " Y:::: " << vertexList[nVert*3+1] << endl;
						cout << __name << " Z:::: " << vertexList[nVert*3+2] << endl;
#endif
						tMesh->addVertex(vertexList[nVert*3], vertexList[nVert*3+1], vertexList[nVert*3+2]);
					}

					// We get the list of integer representing our list of faces ...
					// Please read collada doc, cause it has an offset ...
					domListOfUInts lFaces=triRef->getP()->getValue();

					//Texture Coords
					vector<TexCoord*> texCoords;
					if (hasTexCoords){
						domFloat_arrayRef flTexArray = m->getSource_array()[texCoordOffset]->getFloat_array();
						if (flTexArray){
							const domListOfFloats& coordsList = flTexArray->getValue();

							// div 2 'cause of the S,P values
							for (unsigned t = 0; t < flTexArray->getCount(); t+=2){ //
								TexCoord* tc;
								tc = new TexCoord();
								tc->s = coordsList[t];
								tc->t = coordsList[t+1];
								texCoords.push_back(tc);
								tMesh->setHasTexCoords();

							}

						} else {
							hasTexCoords = false;
							tMesh->setHasTexCoords();
						}
					}

					// Add the faces to our mesh
					for(unsigned nFace=0; nFace < nFaces; nFace++){
						Face* f = new Face();
						f->vertex_count = 3; //Triangles

						int index = nFace*nArrays*stride + vertexOffset;

						f->vertexes.push_back( lFaces[index+(nArrays*0)] );
						f->vertexes.push_back( lFaces[index+(nArrays*1)] );
						f->vertexes.push_back( lFaces[index+(nArrays*2)] );



						if (tMesh->hasTexCoords()){
							index = nFace*nArrays*3 + texCoordOffset;

							f->texCoords.push_back(texCoords.at(lFaces[index+(nArrays*0)]));
							f->texCoords.push_back(texCoords.at(lFaces[index+(nArrays*1)]));
							f->texCoords.push_back(texCoords.at(lFaces[index+(nArrays*2)]));


#if 0
							cout << "INDEX: " << lFaces[index+(nArrays*0)] << " / " << texCoords.size() << endl;
							cout << "INDEX: " << lFaces[index+(nArrays*1)] << " / " << texCoords.size() << endl;
							cout << "INDEX: " << lFaces[index+(nArrays*2)] << " / " << texCoords.size() << endl;

							cout << f->texCoords.at(0)->s << "  " << f->texCoords.at(0)->t << endl;
							cout << f->texCoords.at(1)->s << "  " << f->texCoords.at(1)->t << endl;
							cout << f->texCoords.at(2)->s << "  " << f->texCoords.at(2)->t << endl;
#endif
						}

						// Calculate Normals
						Vertex v1 = *(tMesh->getVertex(f->vertexes[0]));
						Vertex v2 = *(tMesh->getVertex(f->vertexes[1]));
						Vertex v3 = *(tMesh->getVertex(f->vertexes[2]));

						Vector3D v = v2 - v1;    Vector3D w = v3 - v1;
						Vector3D normalTemp = Vector3D::crossProduct(v, w);
						normalTemp.normalize();

						f->normal[0] = normalTemp.x;
						f->normal[1] = normalTemp.y;
						f->normal[2] = normalTemp.z;

						tMesh->addFace(f);

					} // end for faces

				} //end if flArray
			} // end for triangle groups

			// Get the initial rotation and location of this mesh
			std::string tempString;
			for (unsigned nNode=0; nNode < nodes.size(); nNode++){
				domNode* tempNode = (domNode*) nodes.at(nNode);
				daeElement* iGeom = tempNode->getChild("instance_geometry");
				// Let's see if this node has the initial properties of our mesh
				if (iGeom != NULL){
					tempString = iGeom->getAttribute("url");
					// Delete the first char "#"
					tempString = tempString.substr(1,tempString.size()-1);
#ifndef NDEBUG
					cout << "[DEBUG]: ---------------------------------------------------------------\n";
					cout << "[DEBUG]: Object Scene:: " << tempString << endl;
#endif
					if (tempString.compare(tMesh->getName().c_str())==0){
#ifndef NDEBUG
						cout << "[DEBUG]: Initial Settings:" << endl;
#endif
						// Get the initial rotation angles.
						domTranslate* dT = (domTranslate*) tempNode->getChild("translate");
						if (dT != NULL){
							domFloat3 transFloat = dT->getValue();
							tMesh->setInitialTranslation(transFloat[0], transFloat[1], transFloat[2]);
						}
						domFloat4 rotateXFloat;
						domFloat4 rotateYFloat;
						domFloat4 rotateZFloat;

						// Now let's go for the rotations
						unsigned nRots =  tempNode->getRotate_array().getCount();
#ifndef NDEBUG
						cout << "[DEBUG]: ··Number of Initial rotations: " << nRots << endl;
#endif
						for (unsigned nRot=0; nRot < nRots; nRot++){
							Rotation* tempRot = new Rotation();
							daeSmartRef<domRotate> rotation = tempNode->getRotate_array().get(nRot);
							if (rotation->getValue().getCount() != 4 ){
								cout << "[Warning]: Bad Angle inside dae file" << endl;
							} else {
								tempRot->x = (float) rotation->getValue().get(0);
								tempRot->y = (float) rotation->getValue().get(1);
								tempRot->z = (float) rotation->getValue().get(2);
								tempRot->r = (float) rotation->getValue().get(3);
								tMesh->addRotation(tempRot);
							}
						}

						// Now ... the scale
						unsigned nScales = tempNode->getScale_array().getCount();
#ifndef NDEBUG
						cout << "[DEBUG]: ··Number of initial scales: " << nScales << endl;
#endif
						for (unsigned nScale=0; nScale < nScales; nScale++){
							Scale* tempScale = new Scale();
							daeSmartRef<domScale> scale = tempNode->getScale_array().get(nScale);
							if (scale->getValue().getCount() != 3){
								stringstream msg;
								msg << "Renderable::" << __name << ":: Bad scale transforms inside dae file";
								Logger::getLogger()->warning(msg.str());

							} else {
								tempScale->x = (float) scale->getValue().get(0);
								tempScale->y = (float) scale->getValue().get(1);
								tempScale->z = (float) scale->getValue().get(2);
								tMesh->addScale(tempScale);
							}
						}
#ifndef NDEBUG
						cout << "[DEBUG]: ---------------------------------------------------------------\n";
#endif
					}
				}
			}

			// Add the new mesh
			__meshes.push_back(tMesh);
		}// End of triangle mesh
		else { // Not a PolyMesh, not a TriMesh.
			Logger::getLogger()->warning("Renderable3D:: " + gName + ":: Not supported geometry.");
		}
	} //end for geometries


	vector<string*> textureFileNames;
	vector<daeElement*> images = dae->getDatabase()->typeLookup(domImage::ID());


	for (size_t i = 0; i < images.size(); i++){
#ifndef NDEBUG
		cout << "[DEBUG]: ·· Images " << i << " id: "
			 << images[i]->getChild("init_from")->getCharData() << endl;
#endif
		textureFileNames.push_back(new string(images[i]->getChild("init_from")->getCharData()));
	}
#ifndef NDEBUG
cout << "[DEBUG]: ---------------------------------------------------------------\n";
#endif


	// Free the dae memory
	delete dae;

	if (__meshes.size()==0){
		Logger::getLogger()->warning("Renderable3D::" + __name + " has no meshes to draw!");
		__wMeshes = false;
		return;
	}


	string TexturePath = Configuration::getConfiguration()->defaultTexturePath();

	for (uint i=0; i < textureFileNames.size(); i++){
		__textures.push_back(TextureFactory::getInstance()->getTexture(TexturePath + *textureFileNames.at(i))); //new Texture(TexturePath + *textureFileNames.at(i)));

	}

	// Create the callLists
	createCallLists();



}

vector<Texture*> RenderableModel::getTextures(){
	return __textures;
}

void RenderableModel::setTextures(vector<Texture*> textures){
	textures = textures;
}

void RenderableModel::createCallLists(){

	  for (unsigned n=0; n< __meshes.size() ;n++){

		  __callLists.push_back(glGenLists(1));
		  glNewList(__callLists.at(n), GL_COMPILE);
		  if (__meshes.at(n)->hasTexCoords()){

			  if ( ( n < __textures.size() ) && ( __textures.at(n)->isLoaded() ) )
				  glBindTexture(GL_TEXTURE_2D, __textures[n]->getGLTexture());
		  }

		  // Initial Translation
		  float* iTr = __meshes.at(n)->getInitialTranslation();
		  glPushMatrix();

		  glTranslatef(iTr[0], iTr[1], iTr[2]);

		  // Initial Rotations
		  for (unsigned nRot=0; nRot < __meshes.at(n)->getCountOfRotations(); nRot++){
		  			  Rotation* rTemp = __meshes.at(n)->getRotation(nRot);
		  			  glRotatef(rTemp->r, rTemp->x, rTemp->y, rTemp->z);
		  }

		  // Initial Scales
		  for (unsigned nScale=0; nScale < __meshes.at(n)->getCountOfScales(); nScale++){
		  			  Scale* sTemp = __meshes.at(n)->getScale(nScale);
		  			  glScalef(sTemp->x, sTemp->y, sTemp->z);
		  }

		  if (__meshes[n]->getType() == TRI){
			  // TriMesh!
#ifndef NDEBUG
			  cout << "[DEBUG]: TRI CallList" << endl;
#endif
			  glBegin(GL_TRIANGLES);
			  for (unsigned i = 0; i < __meshes[n]->getCountOfFaces(); i++){
				  Face* tFace;
				  tFace = __meshes[n]->getFace(i);
				  glNormal3fv((GLfloat*) &(tFace->normal));
				  for (int j=0; j<3;j++){
#if 0
					  cout << "S: " <<   tFace->texCoords[j]->s << endl;
					  cout << "T: " <<   tFace->texCoords[j]->t << endl;
#endif
					  if (__meshes.at(n)->hasTexCoords()){
#ifndef NOT_ROTATED_TEXTURES // Usually, we'll want this (PNG, JPG ...)
						  glTexCoord2f((tFace->texCoords[j]->s), 1-(tFace->texCoords[j]->t));
#else
						  glTexCoord2f((tFace->texCoords[j]->s), (tFace->texCoords[j]->t));
#endif
					  }
					  glVertex3fv((GLfloat*)  &(__meshes[n]->getVertexOfFace(i,j)->x ));
				  }
			  }
			  glEnd();

		  }
		  else // PolyMesh
		  {
#ifndef NDEBUG
			  cout << "[DEBUG]: POLY CallList" << endl;
#endif
			  for (unsigned i = 0; i < __meshes[n]->getCountOfFaces(); i++){
				  Face* tFace;
				  tFace = __meshes[n]->getFace(i);
				  // get the number of vertex of the face ...
				  switch(tFace->vertex_count){
					  case 3: glBegin(GL_TRIANGLES); break;
					  case 4: glBegin(GL_QUADS); break;
					  default: glBegin(GL_POLYGON); break;
				  }
#if 0
				  cout << "[DEBUG]: vertex count ::" << __name << " :: " <<  tFace->vertex_count << endl;
#endif
				  glNormal3fv((GLfloat*) &(tFace->normal));
				  for (unsigned j=0; j< tFace->vertex_count;j++){
#if 0
					  cout << "S: " <<   tFace->texCoords[j]->s << endl;
					  cout << "T: " <<   tFace->texCoords[j]->t << endl;
#endif
					  if (__meshes.at(n)->hasTexCoords()){
#ifndef NOT_ROTATED_TEXTURES // Usually, we'll want this (PNG, JPG ...)
						  glTexCoord2f((tFace->texCoords[j]->s), 1-(tFace->texCoords[j]->t));
#else
						  glTexCoord2f((tFace->texCoords[j]->s), (tFace->texCoords[j]->t));
#endif
					  }
#if 0
					  cout << __name << "  X:: " << __meshes[n]->getVertexOfFace(i,j)->x << endl;
					  cout << __name << "  Y:: " << __meshes[n]->getVertexOfFace(i,j)->y << endl;
					  cout << __name << "  Z:: " << __meshes[n]->getVertexOfFace(i,j)->z << endl;
#endif
					  glVertex3fv((GLfloat*)  &(__meshes[n]->getVertexOfFace(i,j)->x ));
				  }
				  glEnd();
			  }


		  } // end PolyMesh.
	  }


	  		glPopMatrix();

	  		glEndList();

}


void RenderableModel::drawWire(){

	glEnable(GL_LINE_SMOOTH);


	bool lEnabled = glIsEnabled(GL_LIGHTING);

	if (lEnabled) glDisable(GL_LIGHTING);

	if (__allTransparentWhileWire)
		glDisable(GL_DEPTH_TEST);
	else
		glEnable(GL_DEPTH_TEST);

	if (__cull)
		glEnable(GL_CULL_FACE);

	glEnable (GL_BLEND);

	if (__wireTextured)
		glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_COLOR);
	else
		glBlendFunc(GL_SRC_COLOR,GL_ONE_MINUS_SRC_ALPHA);


	this->setWireFillColor(__wireFillColor[0], __wireFillColor[2], __wireFillColor[2]);
	this->setWireFillTrans(0.3);

	if (__wireTextured){
		glEnable(GL_TEXTURE_2D);
	}
	else{
		glColor4fv(__wireFillColor);
	}

	for (uint i=0; i< __callLists.size(); i++){
			glCallList(__callLists.at(i));
		}

	if (__wireTextured){
		glDisable(GL_TEXTURE_2D);
	}


	glPolygonMode (GL_FRONT, GL_LINE);

	if (__wireBackEnabled){
		glLineWidth(__wireNormalWidth*__wireAnimStep+__wireMinWidth + __wireMaxWidth);
		glColor4f(0,0,0,1);
		for (uint i=0; i< __callLists.size(); i++){
			glCallList(__callLists.at(i));
		}
	}

	if (!__wireXor)
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_COLOR);
	else
		glBlendFunc(GL_ONE_MINUS_DST_COLOR, GL_ONE_MINUS_SRC_COLOR);

	if (__wireAnimated)
		animateWire();
	else
		__wireAnimStep = 1;

	glLineWidth(__wireNormalWidth*__wireAnimStep+__wireMinWidth);
	glColor4f(__wireColor[0]*__wireAnimStep, __wireColor[1] / __wireAnimStep, __wireColor[2] / __wireAnimStep,  __wireColor[3]*__wireAnimStep);


	for (uint i=0; i< __callLists.size(); i++){
		glCallList(__callLists.at(i));
	}

	glDisable (GL_BLEND);

	glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);

	if (__cull)
		glDisable(GL_CULL_FACE);


	if (__allTransparentWhileWire)
		glEnable(GL_DEPTH_TEST);

	if (lEnabled) glEnable(GL_LIGHTING);

	glDisable(GL_LINE_SMOOTH);

}

void RenderableModel::draw(){

	if (!__wMeshes){
		return;
	}
#if 0
	cout << "POS: " << __position.x << " " <<  __position.y << " " << __position.z << endl;
    cout << "ROT: " << __rotX << " " << __rotY << " " << __rotZ << endl;
#endif


	glPushMatrix();


	computePositionRotation();

//	glTranslatef(__position.x, __position.y, __position.z);
//
//	glRotatef(__rotX,1.0f,0.0f,0.0f);
//	glRotatef(__rotY,0.0f,1.0f,0.0f);
//	glRotatef(__rotZ,0.0f,0.0f,1.0f);

#if 0
	Matrix16 m;

	glGetFloatv(GL_MODELVIEW_MATRIX, m.getData());

	cout << ":: " << __name << " :: \n" << m << endl;
#endif



	if (__wire){
		glDisable(GL_TEXTURE_2D);
		drawWire();
		//renderOutLine();
		glEnable(GL_TEXTURE_2D);
	} else {

		glEnable(GL_TEXTURE_2D);
		glDepthFunc(GL_LEQUAL);
		for (uint i=0; i< __callLists.size(); i++){
			glCallList(__callLists.at(i));
		}
		glDisable(GL_TEXTURE_2D);
	}

	glPopMatrix();

}



void RenderableModel::enableWireXor(){
	__wireXor = true;
}
void RenderableModel::disableWireXor(){
	__wireXor = false;
}

void RenderableModel::enableWireTextured(){
	__wireTextured = true;
}

void RenderableModel::disbaleWireTextured(){
	__wireTextured = false;
}

GLfloat* RenderableModel::getWireColor(){
	return __wireColor;
}

void RenderableModel::setWireColor(const float& r, const float& g, const float& b){
	__wireColor[0] = r;
	__wireColor[1] = g;
	__wireColor[2] = b;

}

GLfloat* RenderableModel::getWireFillColor(){
	return __wireFillColor;
}

void RenderableModel::setWireFillColor(const float& r, const float& g, const float& b){
	__wireFillColor[0] = r;
	__wireFillColor[1] = g;
	__wireFillColor[2] = b;
}

GLfloat RenderableModel::getWireTrans(){
	return __wireColor[3];
}
void RenderableModel::setWireTrans(const float& a){
	__wireColor[3] = a;
}

GLfloat RenderableModel::getWireFillTrans(){
	return __wireFillColor[3];
}
void RenderableModel::setWireFillTrans(const float& a){
	__wireFillColor[3] = a;
}

void RenderableModel::enableWireAnim(){
	__wireAnimated = true;
}

void RenderableModel::disableWireAnim(){
	__wireAnimated = false;
}

void RenderableModel::enableAllTransparentWhileWire(){
	__allTransparentWhileWire = true;
}

void RenderableModel::disableAllTransparentWhileWire(){
	__allTransparentWhileWire = false;
}

void RenderableModel::setWireNormalWidth(const GLfloat& w){
	__wireNormalWidth = w;
}

void RenderableModel::setWireMaxWidth(const GLfloat& w){
	__wireMaxWidth = w;
}

void RenderableModel::setWireMinWidth(const GLfloat& w){
	__wireMinWidth = w;
}

void RenderableModel::animateWire(){
	if ((SDL_GetTicks() - __wireAnimLastTime) > 20){
		__wireAnimLastTime = SDL_GetTicks();
		__wireAnimStep++;
		if (__wireAnimStep > 13 ){
			__wireAnimStep=1;
		}
	}
}

bool RenderableModel::is3D(){
	return true;
}

void RenderableModel::enableBackWire(){
	__wireBackEnabled = true;
}

void RenderableModel::disableBackWire(){
	__wireBackEnabled = false;
}

RenderableModel::~RenderableModel() {
#ifndef NDEBUG
	std::cout << "[DEBUG]: Renderable3d:: Destructor - Cleaning: [" << __name << "]" << endl;
#endif
	for (uint i = 0; i < __meshes.size(); i++){
			delete __meshes.at(i);
	}


}

}
