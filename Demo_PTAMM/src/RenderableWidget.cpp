/*
 * RenderableWidget.cpp
 *
 */

#include "RenderableWidget.h"

namespace mars {



RenderableWidget::RenderableWidget(const unsigned & w, const unsigned & h):
		__height(h), __width(w), __selected(false)  {
	init();
}

void RenderableWidget::setHeight(const unsigned & h){

	if ((h % 2) != 0)
		__height = h + 1;
	else
		__height = h;

}


const unsigned& RenderableWidget::getHeight() const{
	return __height;
}

void RenderableWidget::setWidth(const unsigned& w){
	if ((w % 2) != 0)
		__width = w + 1;
	else
		__width = w;
}

void RenderableWidget::init(){
	for (int i = 0; i < MAX_BUTTONS; ++i)
		__mouseButtonClicked[i] = 0;

	__x1 = 0; __x2 = 0; __y1 = 0; __y2 = 0;
}

const unsigned& RenderableWidget::getWidth() const {
	return __width;
}

void RenderableWidget::handleEvent(SDL_Event event){

	if (!__shown) return;

	switch (event.type){
		case SDL_MOUSEMOTION:
			handleMouseMotion(event.motion.x, event.motion.y);
			break;

		case SDL_MOUSEBUTTONDOWN:
			handleMouseClick(event.button.button, event.button.x, event.button.y);
			break;

		case SDL_MOUSEBUTTONUP:
			handleMouseRelease(event.button.button, event.button.x, event.button.y);
			break;


		case SDL_KEYDOWN:
			handleKey(event.key.keysym.sym);
			break;
	}

}

void RenderableWidget::executeClickEvents(){
	vector<WidgetFunction>::iterator it;

	for (it=__callBacksClick.begin(); it!=__callBacksClick.end(); ++it){
		void (*f)(void*) = (*it).f;
		void* data = (*it).data;
		f(data);
	}
}

void RenderableWidget::executeSelectEvents(){
	vector<WidgetFunction>::iterator it;

	for (it=__callBacksSelect.begin(); it!=__callBacksSelect.end(); ++it){
		void (*f)(void*) = (*it).f;
		void* data = (*it).data;
		f(data);
	}
}

void RenderableWidget::executeKeyEvent(SDLKey k){
	std::map<SDLKey, WidgetFunction>::iterator it;

	it = __callBacksKey.find(k);
	if (it != 	__callBacksKey.end() )    // found
		it->second.f( it->second.data );  // execute it.
}

bool RenderableWidget::isSelected() const {
	return __selected;
}

void RenderableWidget::select(){
	__selected = true;
	executeSelectEvents();
}

void RenderableWidget::unSelect(){
	__selected = false;
}

bool RenderableWidget::anyButtonClicked() const{
	for (int i = 0; i < MAX_BUTTONS; ++i){
			if (__mouseButtonClicked[i]){
				return true;
			}
		}
	return false;
}

void RenderableWidget::publishHandledEvent(wEventType eType,
		void (*fptr) (void*), void* data, SDLKey k){

	switch (eType) {
		case CLICK:
			__callBacksClick.push_back(WidgetFunction(fptr, data));
			break;
		case SELECT:
			__callBacksSelect.push_back(WidgetFunction(fptr, data));
			break;
		case KEY:
			if (k == SDLK_LAST) {
				Logger::getInstance()->warning("RenderableWidget::"
						"publishHandledEvent :: cannot publish a key event w/o"
						" a key");
				return;
			}
			__callBacksKey.insert( pair<SDLKey, WidgetFunction>(k, WidgetFunction(fptr, data)));
			break;
	}
}

void RenderableWidget::publishHandledEventLUA(std::string type, std::string func){
#ifndef NDEBUG
	cout << "[DEBUG]: LUA event publishing: ";
#endif

	if (type.compare("CLICK") == 0){
#ifndef NDEBUG
		cout << "CLICK" << endl;
#endif
		__callBacksClick.push_back(WidgetFunction(func));
	} else
	if (type.compare("SELECT") == 0){
#ifndef NDEBUG
		cout << "SELECT" << endl;
#endif
		__callBacksSelect.push_back(WidgetFunction(func));
	} else {
		Logger::getInstance()->warning("Trying to publish a non-existent type of callback");
	}
}

RenderableWidget::~RenderableWidget() {
	__callBacksClick.reserve(20);
	__callBacksSelect.reserve(20);

}

}
