/*
 * RenderableLinePath.cpp
 *
 */

#include "RenderableLinePath.h"


namespace mars {




RenderableLinePath::RenderableLinePath() {

	__poi.reserve(100);

	__color[0] = 1.0;
	__color[1] = 0.0;
	__color[2] = 0.0;

	__color[3] = 0.7; // Alpha

	__width = 5.0;

	__updateUserPos = false;


}



void RenderableLinePath::addPoint(const Point3D& p){

#ifndef NDEBUG
	cout << "[DEBUG]: Adding point " << p << endl;
#endif

	__poi.push_back(p);

}


void RenderableLinePath::reset(){

#ifndef NDEBUG
	cout << "[DEBUG]: Reseting RenderableLinePath" << endl;
#endif

	__poi.clear();

}

void RenderableLinePath::setColor(const float& r, const float& g, const float& b, const float& a){

	__color[0] = r;
	__color[1] = g;
	__color[2] = b;
	__color[3] = a;

}


void RenderableLinePath::setColor(const float& r, const float& g, const float& b){

	__color[0] = r;
	__color[1] = g;
	__color[2] = b;

}


float* RenderableLinePath::getColor(){
	return __color;
}

float RenderableLinePath::getWidth(){
	return __width;
}

void RenderableLinePath::setWidth(const float& w){
	__width = w;
}


void RenderableLinePath::addPointLUA(const float& x, const float& y, const float& z){
	this->addPoint(Point3D(x,y,z));
}

void RenderableLinePath::draw(){

	if ( __poi.empty() ){
#if 0
		Logger::getInstance()->warning("RenderableLinePath:: empty path");
#endif
		return;
	}

	glPushMatrix();

	poi_it initPoint;
	initPoint = __poi.begin();

	int current, total;
	current = total = 0;

	//Find the nearest point
	if(__updateUserPos){
	  float minLength = 999999;
	  for(poi_it i=__poi.begin();i!=__poi.end();i++){
	    total++;
	    float l = sqrt(pow((*i).x-__userPos.x,2)+pow((*i).y-__userPos.y,2)+pow((*i).z-__userPos.z,2));
	    //float l = sqrt(pow((*i).x-__userPos.x,2)+pow((*i).y-__userPos.y,2));
	    if(l<minLength){
	      minLength = l;
	      initPoint = i;
	      current = total;
	    }
	  }
	}

	poi_it it;
	it = __poi.begin();
	Point3D temp = *it;



	bool drawing = false;
	int recorrido = 0;

	current = total = 0;
	while ( (++it) != __poi.end() ) {
	  if(__updateUserPos){
	    if(!drawing){
	      if(it==initPoint) {
		drawing = true;
		temp = *initPoint;
		drawLine(__userPos,temp,false);
		current = total;
	      }
	    }
	    if(drawing){
	      recorrido+=sqrt(pow((*it).x-temp.x,2)+pow((*it).z-temp.z,2));
	      if(recorrido>=SEPARACION_FLECHAS){
		drawLine(temp, *it,true);
		recorrido = 0;
	      }else{
		drawLine(temp, *it,false);
	      }
	      temp = *it;
		total++;
	    }
	  }else{ //Just draw the whole path
	    recorrido+=sqrt(pow((*it).x-temp.x,2)+pow((*it).z-temp.z,2));
	    if(recorrido>=SEPARACION_FLECHAS){
	      drawLine(temp, *it,true);
	      recorrido = 0;
	    }else{
	      drawLine(temp, *it,false);
	    }
	    temp = *it;
	  }
	}
	
		
	glPopMatrix();
}

void RenderableLinePath::setAlpha(const float& a){
	__color[3] = a;
}

  void RenderableLinePath::drawLine(const Point3D& b, const Point3D& e,bool drawTriangle){
  
	glPushMatrix();


	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);



	glLineWidth(__width);

	glDisable(GL_TEXTURE_2D);
	glDisable(GL_LIGHTING);

	glColor4f(__color[0], __color[2], __color[2], __color[3]);

	glBegin(GL_LINES);
		glVertex3f( b.x, b.y, b.z );
		glVertex3f( e.x, e.y, e.z );
	glEnd();

	if(drawTriangle){
	  float v1[2];
	  v1[0] = e.x - b.x;
	  v1[1] = e.z - b.z;
	  float mod_v1 = sqrt(pow(v1[0],2)+pow(v1[1],2)); 
	  float u1[2];
	  u1[0] = v1[0]/mod_v1;
	  u1[1] = v1[1]/mod_v1;

	  float v2[2];
	  v2[0] = 1;
	  v2[1] = -u1[0]/u1[1];
	  float mod_v2 = sqrt(pow(v2[0],2)+pow(v2[1],2));
	  float u2[2];
	  u2[0] = v2[0]/mod_v2;
	  u2[1] = v2[1]/mod_v2;
 

	  glBegin(GL_TRIANGLES);
	  glVertex3f(u2[0]*0.35+b.x,b.y,u2[1]*0.35+b.z);
	  glVertex3f(-u2[0]*.35+b.x,b.y,-u2[1]*.35+b.z);
	  glVertex3f(u1[0]*.5+b.x,b.y,u1[1]*.5+b.z);
	  glEnd();
	}

	glDisable(GL_BLEND);

	if (World::getInstance()->isLightingEnabled())
		glEnable(GL_LIGHTING);

	glPopMatrix();

	glColor4f(1, 1, 1, 1);
}

void RenderableLinePath::drawPOI(const Point3D& p){
	glPushMatrix();

	// Waiting for boss' decision :)

	glPopMatrix();
}

  void RenderableLinePath::updateUserPos(float ux, float uy, float uz){
  if(!__updateUserPos) __updateUserPos = true;
  __userPos.x = ux;
  __userPos.y = uy;
  __userPos.z = uz;
}


RenderableLinePath::~RenderableLinePath() { }
}
