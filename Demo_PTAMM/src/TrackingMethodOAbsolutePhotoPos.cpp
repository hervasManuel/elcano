/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * TrackingMethodOAbsolutePhotoPos.cpp
 *
 */

#include "TrackingMethodOAbsolutePhotoPos.h"

namespace mars {

  /* vS_0 is supposed to be the main camera */
  TrackingMethodOAbsolutePhotoPos::TrackingMethodOAbsolutePhotoPos(VideoSource* vS_0){
    __methodName="PHOTOPOS";
    
    
    if(vS_0==NULL){
      Logger::getLogger()->error("TrackingMethodOAbsolutePhotoPos: Error in PHOTO POS tracking method: video source 0 is NULL");
      exit(0);
    }
    
    addVideoSource(vS_0);
    __uCam=vS_0;

    /*Params to save the frame to jpeg*/
    __params.push_back(CV_IMWRITE_JPEG_QUALITY);
    __params.push_back(95);

    /*Test this!*/
    //char argv[][1]={{"./build/demo1010"}};
    //__riki=NULL;
    __riki = new mars::photoPos();
    

    __ARTKPosition.x = 0;
    __ARTKPosition.y = 0;


    __posicion.x=0;
    __posicion.y=0;

    __rot=0;
  }



  void TrackingMethodOAbsolutePhotoPos::loopThread(){


    //Obtenemos la posición de ricki
    cv::Mat* frame=__uCam->getLastFrame();
    if(frame->rows==0)
      return;

    cv::imwrite("frame.jpg",*frame,__params);
    delete frame;

    // __riki->invocar(__ARTKPosition);
    
    __posicion = __riki->getPos();
    __rot = __riki->getRot();
    sleep(1);
    
  }


  Elcano::pos TrackingMethodOAbsolutePhotoPos::getPosition(){
    return __posicion;
  }


  float TrackingMethodOAbsolutePhotoPos::getRotation(){
    return __rot;
  }


  void TrackingMethodOAbsolutePhotoPos::updatePosition(float ARTKX, float ARTKY)
  {

    __ARTKPosition.x = ARTKX;
    __ARTKPosition.y = ARTKY;


  }


  TrackingMethodOAbsolutePhotoPos::~TrackingMethodOAbsolutePhotoPos(){
    Logger::getLogger()->note("TrackingMethodOAbsolutePhotoPos: deleting tracking method. Bye!");
  }
}

