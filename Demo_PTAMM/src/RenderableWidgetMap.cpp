/*
 * RenderableWidgetMap.cpp
 *
 */

#include "RenderableWidgetMap.h"

namespace mars {

RenderableWidgetMap::RenderableWidgetMap() : RenderableWidget() , __scale(1),
		__realStartX(0.0), __realStartY(0.0){
	cout << "ERROR: this should not be called." << endl;
}

RenderableWidgetMap::RenderableWidgetMap(const std::string& fileNameBack, const int& width, const int& height, const float& scale) :
	RenderableWidget(width, height), __scale(scale),
	__realStartX(0.0), __realStartY(0.0){

	__background = new RenderableWidgetImage2D(fileNameBack, width, height);

	__thePath = new RenderableLinePath();
}

void RenderableWidgetMap::setPosition(const int& x, const int& y){

	int of_x = x - __position.x;
	int of_y = y - __position.y;

	__position.x = x;
	__position.y = y;
	__position.z = 0;

	met_it it;

	it = __methodPos.begin();

	while(it != __methodPos.end()){
		int nx = it->second->getPosition().x + of_x;
		int ny = it->second->getPosition().y + of_y;
		it->second->setPosition(nx, ny);
		++it;
	}

//	int nxP = __thePath->getPosition().x + of_x;
//	int nyP = __thePath->getPosition().y + of_y;
//	__thePath->setPosition(nxP, nyP);

	__background->setPosition(x, y);
}


void RenderableWidgetMap::addMethod(const std::string& name,
		const std::string& fileName) {

	// FIXME SIZE map
	//RenderableWidgetImage2D* temp = new RenderableWidgetImage2D(fileName, 64, 64);
  RenderableWidgetImage2D* temp = new RenderableWidgetImage2D(fileName, 16, 16);
	__methodPos[name] = temp;

	this->setMethodPosition(name, 0,0);
}

void RenderableWidgetMap::setPathLineWidth(const float& w){
	__thePath->setWidth(w);
}

void RenderableWidgetMap::setMethodAngle(const std::string& name, const float& angle){
	met_it it;

	it = __methodPos.find(name);

	if (it == __methodPos.end() ){
		Logger::getInstance()->warning("RenderableWidgetMap:: cannot set the"
				"angle of a non existent method.");
		return;
	}
	else {
		it->second->setAngle2D(angle);
	}
}

void RenderableWidgetMap::setMethodPosition(const std::string& name,
		const float& x, const float& y) {

	// Convert with scale
	// ----------------------------
	int __pixelX = int ( x  * __scale) + __realStartX;
	int __pixelY = int ( y  * __scale) + __realStartY;
	// ----------------------------

#if 1
	//cout << "Scale: " << __scale << endl;
	//cout << "RealX: " << x << " --> in pixels ---> " << __pixelX << endl;
	//cout << "RealY: " << y << " --> in pixels ---> " << __pixelY << endl;
#endif

	// Check ranges.
	if ( (__pixelX < 0) || (__pixelY < 0) || (__pixelX > int(__width-1) ) || (__pixelY > int(__height-1) ) ){
		Logger::getInstance()->error("RendrableMap:: Out of bounds.");
		return;
	}

	met_it it;

	it = __methodPos.find(name);

	if (it == __methodPos.end() ){
		Logger::getInstance()->warning("RenderableWidgetMap:: cannot set the"
				" position of a non existent method.");
		return;
	}
	else {

		// Calculate the positions.
		float w2 = float(__width) / 2;
		float h2 = float(__height) / 2;

		int newX = int ( __position.x - w2  ) + __pixelX;
		int newY = int ( __position.y - h2  ) + __pixelY;

#if 1
		//cout << "MapX: " << __position.x << " - screenMX ---> " << newX << endl;
		//cout << "MapY: " << __position.y << " - screenMY ---> " << newY << endl;
#endif

		it->second->setPosition(newX, newY);

	}

}

void RenderableWidgetMap::setStartPoint(const int& x, const int& y){
	if (x < 0 || y < 0){
		Logger::getInstance()->warning("RenderableWidgetMap:: Starting point has to be positive!");
		return;
	}
	__realStartX = x;
	__realStartY = y;
}


void RenderableWidgetMap::draw(){

	__background->draw();

	float w2 = float(__width) / 2;
	float h2 = float(__height) / 2;

	VideoOutputSDLOpenGL::getInstance()->setOrtho();
	glPushMatrix();
	glLoadIdentity();
	glTranslatef(__position.x -w2 , h2-__position.y, 0);
	__thePath->setWidth(3);
	__thePath->draw();
	glPopMatrix();
	VideoOutputSDLOpenGL::getInstance()->setView();

	met_it it;

	for (it = __methodPos.begin(); it != __methodPos.end(); it++ ){
		it->second->draw();
	}


}

void RenderableWidgetMap::resetPath(){
	__thePath->reset();
}


void RenderableWidgetMap::addPOI(const float& x, const float& y){
	// Convert with scale
	// ----------------------------
	int __pixelX = int ( x  * __scale) + __realStartX;
	int __pixelY = int ( y  * __scale) + __realStartY;
	// ----------------------------

#if 1
	// cout << "Adding POI to map" << endl;
	// cout << "Scale: " << __scale << endl;
	// cout << "RealX: " << x << " --> in pixels ---> " << __pixelX << endl;
	// cout << "RealY: " << y << " --> in pixels ---> " << __pixelY << endl;
#endif

	// Check ranges.
	if ( (__pixelX < 0) || (__pixelY < 0) || (__pixelX > int(__width-1) ) || (__pixelY > int(__height-1) ) ){
		Logger::getInstance()->error("RendrableMap:: POI Out of bounds.");
		return;
	}

	__thePath->addPoint(Point3D(__pixelX, Configuration::getConfiguration()->openGLScreenHeight() - __pixelY , 0));

}

void RenderableWidgetMap::updateUserPos(float ux, float uy, float uz){
    	int __pixelX = int ( ux  * __scale) + __realStartX;
	int __pixelY = int ( uz  * __scale) + __realStartY;

	
	if ( (__pixelX < 0) || (__pixelY < 0) || (__pixelX > int(__width-1) ) || (__pixelY > int(__height-1) ) ){
	  Logger::getInstance()->error("RendrableMap:: User Pos of bounds.");
	  return;
	}

	__thePath->updateUserPos(__pixelX,Configuration::getConfiguration()->openGLScreenHeight() -  __pixelY, 0);
  }

RenderableWidgetMap::~RenderableWidgetMap() {
	met_it it;

	for (it = __methodPos.begin(); it != __methodPos.end(); it++ )
		delete it->second;

	delete __background;

	delete __thePath;
}

}
