// -*- mode:c++; coding:utf-8 -*-
#include <LocationAdminI.h>
#include <MLP/Location.h>

LocationAdminI::LocationAdminI(IceStorm::TopicPrx topic)
{
  _topic = topic;
}
void LocationAdminI::addListener(const MLP::LocationListenerPrx &listener, const Ice::Current&)
{
  IceStorm::QoS qos;

  _topic->subscribe(qos,listener);
}
void LocationAdminI::removeListener(const MLP::LocationListenerPrx &listener, const Ice::Current&)
{
  _topic->unsubscribe(listener);
}
MLP::LocationDataInfo LocationAdminI::getLocationDataInfo(const Ice::Current&) const{}

