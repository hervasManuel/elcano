/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * Configuration.cpp
 *
 */

#include "Configuration.h"

namespace mars {


Configuration::Configuration(){


	_defaultImage =  "./images/defaultImage.png";
	_defaultVideoTexture =  "./textures/defaultVideoTexture.png";
	_defaultTexturePath = "./textures/";
	_defaultVideoPath = "./videos/";
	_defaultFontPath = "./fonts/";
	_defaultSoundPath = "./sounds/";
	_defaultFont = "LiberationSans-Regular.ttf";
	_buttonsFont = "Buttons.ttf";
	_consoleFont = "Console.ttf";
	_defaultButtonTexture = "ButtonTexture.png";
	_openGLFullScreen = false;
	_openGLScreenWidth = 640;
	_openGLScreenHeight = 480;
	_openGLScreenDepth = 32;
	_ARTKDataPath = "./ARTKData";
	_ARTKPatternsFile = "patterns.artk";

	try{
		__configFile.open(CONFIG_FILE, ios::in);
		if (__configFile.is_open())
			parseConfig();
		else
			Logger::getLogger()->warning("Config:: No config file: "  CONFIG_FILE);
	} catch (std::exception e){
		Logger::getLogger()->error("Config:: Processig "  CONFIG_FILE );
	}

}

void Configuration::parseConfig(){
	//
	string line;
	string tempP;
	string tempV;

	int nLine=0;

	if (__configFile.is_open())
	  {
	    while (!__configFile.eof() )
	    {
	    	nLine++;
	    	getline(__configFile, line);
	    	// Remove comments
	    	string tline;
	    	size_t sharpPos = line.find("#");
	    	if ( sharpPos!= string::npos)
	    		tline = line.substr(0, sharpPos);
	    	else
	    		tline = line;
	    	// Look for [ and ]
	        size_t lBPos = tline.find("[");
	        size_t rBPos = tline.find("]");
	        size_t eqPos = tline.find(":=");
	    	bool existLB = (lBPos != string::npos);
	    	bool existRB = (rBPos != string::npos);
	        bool existEq = (eqPos != string::npos);

	        bool goodLine = (existLB && existRB && existEq && (rBPos > lBPos + 1));

	        if (goodLine){
	        	tempP = tline.substr(tline.find("[")+1, tline.find("]")-tline.find("[")-1);
	        	tempV = tline.substr(eqPos+3, tline.length()-eqPos);
#ifndef NDEBUG
	        	cout << "--" << endl;
				cout << "Line: " << nLine << endl;
				cout << "property: " << tempP << endl;
	        	cout << "Value: " << tempV << endl;
				cout << "--" << endl;
#endif
				std::transform(tempP.begin(), tempP.end(),
							tempP.begin(), ::toupper);
				addproperty(tempP, tempV, nLine);
	        }

	    }
	  }

}

void Configuration::addproperty(string property, string value, int nLine){

#ifndef NDEBUG
		cout << "[DEBUG]: Property:: " << property << " ==> " <<  value << endl;
#endif

	if (property.compare("DEFAULTIMAGE")==0){

//		if (value.at(value.length()-1) != '/')
//			value = value.append("/");

		_defaultImage = value;

	} else
	if (property.compare("DEFAULTVIDEOTEXTURE")==0){

		_defaultVideoTexture = value;

		} else
	if (property.compare("DEFAULTTEXTUREPATH")==0){

		if (value.at(value.length()-1) != '/')
			value = value.append("/");

		_defaultTexturePath = value;

	} else
	if (property.compare("DEFAULTVIDEOPATH")==0){
		if (value.at(value.length()-1) != '/')
			value = value.append("/");

		_defaultVideoPath = value;

	} else
	if (property.compare("ARTKDATAPATH")==0){

			if (value.at(value.length()-1) != '/')
				value = value.append("/");

			_ARTKDataPath = value;
#ifndef NDEBUG
		cout << "[DEBUG]: Property:: " << property << " is now " <<   _ARTKDataPath << endl;
#endif

	}else
	if (property.compare("DEFAULTFONTPATH")==0){

		if (value.at(value.length()-1) != '/')
			value = value.append("/");

		_defaultFontPath = value;

	} 	else
	if (property.compare("DEFAULTSOUNDPATH")==0){

		if (value.at(value.length()-1) != '/')
			value = value.append("/");

		_defaultSoundPath = value;
#ifndef NDEBUG
		cout << "[DEBUG]: property:: " << property << " is now " <<  value << endl;
#endif

	}else
	if (property.compare("DEFAULTFONT")==0){

		_defaultFont = value;

	} else
	if (property.compare("BUTTONSFONT")==0){

		_buttonsFont = value;

	} else
	if (property.compare("CONSOLEFONT")==0){

		_consoleFont = value;

	} else
	if (property.compare("OPENGLFULLSCREEN")==0){
		string tValue = value;
		std::transform(tValue.begin(), tValue.end(),
									tValue.begin(), ::toupper);
		if ( tValue.find("YES") != string::npos )
			_openGLFullScreen = true;
		else if (tValue.find("NO") != string::npos)
			_openGLFullScreen = false;
		else{
			stringstream msg;
			msg << "Bad openGLFullScreen value: " << value;
			Logger::getLogger()->warning(msg.str());
			_openGLFullScreen = false;
			return;
		}
#ifndef NDEBUG
		if (_openGLFullScreen)
		cout << "[DEBUG]: Property:: " << property << " is now YES" << endl;
		else
    	cout << "[DEBUG]: Property:: " << property << " is now NO" << endl;
#endif
	} else
	if (property.compare("OPENGLSCREENWIDTH")==0){
		stringstream ss(value);
		ss >> _openGLScreenWidth;
#ifndef NDEBUG
		cout << "[DEBUG]: Property:: " << property << " is now " <<   _openGLScreenWidth << endl;
#endif
	} else
	if (property.compare("OPENGLSCREENHEIGHT")==0){
		stringstream ss(value);
		ss >> _openGLScreenHeight;
#ifndef NDEBUG
		cout << "[DEBUG]: Property:: " << property << " is now " <<   _openGLScreenHeight << endl;
#endif
	} else
	if (property.compare("OPENGLSCREENDEPTH")==0){
		stringstream ss(value);
		ss >> _openGLScreenDepth;
#ifndef NDEBUG
		cout << "[DEBUG]: property:: " << property << " is now " <<   _openGLScreenDepth << endl;
#endif
	} else
	if (property.compare("DEFAULTBUTTONTEXTURE")==0){
			_defaultButtonTexture = value;
#ifndef NDEBUG
		cout << "[DEBUG]: property:: " << property << " is now " <<  value << endl;
#endif
	} else
	if (property.compare("ARTKPATTERNSFILE")==0){
					_ARTKPatternsFile = value;
#ifndef NDEBUG
		cout << "[DEBUG]: property:: " << property << " is now " <<  value << endl;
#endif
	}else
	if (property.compare("ADDSTREAMURL")==0){
		addStreamURL(value, nLine);
	}



}

void Configuration::addStreamURL(string csv, int nLine){
	// This should be a csv : comma separated value
	// 3 values = 2 commas
	string orig = csv;
	urlStreamConfig* tempUSC = new urlStreamConfig();
	size_t firstNonSpace = csv.find_first_not_of(" ");

	size_t fComma = csv.find_first_of(",");
	if (fComma == string::npos){
		cout << endl;
		stringstream msg;
		msg << "Config::  At [" << nLine  << "] Bad CVS [addStreamURL]" << orig;
		Logger::getLogger()->warning(msg.str());
		cout << "  Example: [addStreamURL] :=" <<
			 	" http://www.vid.com/file.mp4,name,stream description" << endl;
		cout << endl;
		delete tempUSC;
		return;
	}

	tempUSC->URL = csv.substr( firstNonSpace, fComma);
	csv = csv.substr(fComma+1, csv.length()-fComma-1);


	size_t sComma = csv.find_first_of(",");
	if (sComma == string::npos){
		cout << endl;
		stringstream msg;
		msg << "Config::  At [" << nLine  << "] Bad CVS [addStreamURL]" << orig;
		Logger::getLogger()->warning(msg.str());
		cout << "**Example: [addStreamURL] :=" <<
				" http://www.exmaple.com/video.mp4,name,little description" << endl;
		cout << endl;
		delete tempUSC;
		return;
	}

	firstNonSpace = csv.find_first_not_of(" ");
	tempUSC->name = csv.substr( firstNonSpace, sComma);

	csv = csv.substr(sComma+1, csv.length()-sComma-1);
	tempUSC->description = csv;

	__streamsURL.push_back(tempUSC);

#ifndef NDEBUG
	cout << "[DEBUG]: New url stream added" << endl;
	cout << "[DEBUG]: ------------------------------" << endl;
	cout << "[DEBUG]: ·URL        : " << tempUSC->URL << endl;
	cout << "[DEBUG]: ·Name       : " << tempUSC->name << endl;
	cout << "[DEBUG]: ·Description: " << tempUSC->description << endl;
	cout << "[DEBUG]: ------------------------------" << endl;
#endif


}

Configuration* Configuration::getConfiguration(){
	return getInstance();
}



  void Configuration::parseARTKPatternsFile(std::vector<ARTKMark>& marks){
  try{
    _ARTKPatternsFile = _ARTKDataPath + _ARTKPatternsFile;
    __patternsFile.open(_ARTKPatternsFile.c_str() , ios::in);
  } catch (std::exception e){
    Logger::getLogger()->error("Config:: Processig "  + _ARTKPatternsFile );
  }
  
  if (!__patternsFile.is_open()){
    Logger::getLogger()->warning("Config:: No config file: " + _ARTKPatternsFile);
  }
  else {
    int nLine=0;
    string line;
    char tempName[255];
    float d[16] = {1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1};
    
    while (!__patternsFile.eof() )
      {
	nLine++;
	getline(__patternsFile, line);
	
	if (line.compare("") == 0) continue;
	
	setlocale (LC_NUMERIC, "C"); // ... so we have an uniform config file...
	
	float size = 0;
	int nElem;
	char type[255];
	
	int type_flag = 0;
	
	sscanf(line.c_str(),"%s ",type);
	
	if(type[0]=='a'){
	  /*ACTUATION MARK*/
	  type_flag|=ACTUATION_MARK;
	  nElem = sscanf(line.c_str(), "%s %s %f",
			 type, tempName, &size);
	  
	  if (nElem != 3){
	    stringstream msgW;
	    msgW << _ARTKPatternsFile << ":: Line(" << nLine << ") is corrupt. ::: " << line;
	    Logger::getLogger()->warning(msgW.str());
	  }
	}else if(type[0]=='b' || type[0]=='p' || type[0]=='u'){
	 
	  if(type[0]=='b'){/*POSITION AND ACTUATION MARK*/
	    type_flag|=ACTUATION_MARK;
	    type_flag|=POSITION_MARK;
	  }else if(type[0]=='p'){/*POSITION MARK*/
	    type_flag|=POSITION_MARK;
	  }else if(type[0]=='u'){/*USER MARK*/
	    type_flag|=USER_MARK;
	  }

	  nElem = sscanf(line.c_str(), "%s %s %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f",
			 type, tempName, &d[0], &d[1], &d[2], &d[3], &d[4], &d[5], &d[6], &d[7],
			 &d[8], &d[9], &d[10], &d[11], &d[12], &d[13], &d[14], &d[15], &size);
	  
	  if (nElem != 19){
	    stringstream msgW;
	    msgW << _ARTKPatternsFile << ":: Line(" << nLine << ") is corrupt. ::: " << line;
	    Logger::getLogger()->warning(msgW.str());
	  }			  
	}else{
	  Logger::getLogger()->warning(":: Corrupt line in patterns.artk. :::");
	  return;
	}
	
	// -------------- To string ;)
	stringstream ss;
	ss << "Adding ARTK Mark: " << tempName;
	for (int i=0;i<16;i++) ss << " " << d[i];
	ss << " Size(" <<  size << ")";
	// --------------
	Logger::getLogger()->note(ss.str());
	Matrix16 m((GLfloat*) d);
#ifndef NDEBUG
	cout << "[DEBUG]: Adding Mark: " << tempName << endl;
	cout << m << endl;
#endif
	ARTKMark mark(m, string(this->_ARTKDataPath + tempName), size, type_flag);
	marks.push_back(mark);
      }
  
  }
}
  

Configuration::~Configuration(){
#ifndef NDEBUG
	std::cout << "[DEBUG]: Configuration:: Cleaning up." << endl;
#endif
	for (unsigned i=0; i < __streamsURL.size(); i++){
		delete __streamsURL.at(i);
	}
}

}
