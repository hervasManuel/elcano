/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/* This class provides an abstract factory for the VideoCapture */

// This is a singleton class

/*
 *
 * VideoCaptureFactory.cpp
 *
 */



#include "VideoCaptureFactory.h"


namespace mars {

//VideoCaptureFactory* VideoCaptureFactory::__instance = NULL;



VideoCaptureFactory::VideoCaptureFactory() {
	Logger::getLogger()->note("Creating VideoCapture abstract factory");
#ifdef WUEYE
	__wInit = false;
#endif
}

VideoCaptureFactory* VideoCaptureFactory::getFactory(){
	return getInstance();
}

VideoCapture* VideoCaptureFactory::createVideoCapture(VIDEO_CAPTURE_TYPE vCaptureType){
	switch(vCaptureType){

	case OPENCV:  // An OpenCV video capture mode is required
	{
			#ifndef NDEBUG
			std::cout << "[DEBUG]: Returning an OpenCV capturer" << std::endl;
			#endif
			VideoCaptureOpenCV* tempVC;
			tempVC = new VideoCaptureOpenCV();
			__vCapturers.push_back(tempVC);
			return (VideoCapture*) tempVC;
		}

#ifdef WUEYE
	case UEYE: // An UEYE video capture ... with Ueye Video devices ...
	{

		if (!__wInit){
			__wInit = true;
			__uEyeNext = 0;
			__pCam = VideoDeviceUEye::getUEyeCameraList();
			__uEyeNDevices = __pCam->dwCount;
		}

		#ifndef NDEBUG
			std::cout << "[DEBUG]: Returning an UEYE capturer" << std::endl;
			#endif
			if (__uEyeNext + 1 > __uEyeNDevices){
				Logger::getInstance()->error("VideoCaptureFactory:: You have only " +
						toString(__uEyeNDevices) + " available. You asked for " +
						toString(__uEyeNext+1));
				return NULL;
			}

			VideoCaptureUEye* tempVC = new VideoCaptureUEye();
			tempVC->addNewVideoSource(__pCam->uci[__uEyeNext], ("uEye" +
					toString(__pCam->uci[__uEyeNext].SerNo)) );
			__uEyeNext += 1;

			__vCapturers.push_back(tempVC);
			return (VideoCapture*) tempVC;
	}
#endif

	//
	// ... Here if more VideoCapturers are needed to be added.
	//
	default:
		VideoCaptureOpenCV* tempVC;
		tempVC = new VideoCaptureOpenCV();
		__vCapturers.push_back(tempVC);
		return (VideoCapture*) tempVC;
	}
}


VideoCaptureFactory::~VideoCaptureFactory() {
	Logger::getLogger()->note("Ending VideoCapture abstract factory");


	for (unsigned i = 0; i < __vCapturers.size(); ++i){
		if (__vCapturers.at(i) != NULL){
			delete __vCapturers.at(i);
		}
	}

}

}
