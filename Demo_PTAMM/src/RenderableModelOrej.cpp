/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * RenderableModelOrej.cpp
 *
 */

#include "RenderableModelOrej.h"

namespace mars {


RenderableModelOrej::RenderableModelOrej() {
	__name = "unknown";

	//Initialize
	__position.x = 0;
	__position.y = 0;
	__position.z = 0;

	__wire = false;

	__type= r3D;

	setRenderMode(2);

}

  RenderableModelOrej::RenderableModelOrej(std::string pathOrej, std::string pathTex, float sx, float sy, float sz){

	__name = "unknown";

	//Initialize
	__position.x = 0;
	__position.y = 0;
	__position.z = 0;

	__scaleX = sx;
	__scaleY = sy;
	__scaleZ = sz;

	__wire = false;

	__type= r3D;

	setRenderMode(2);

	__pathOrej = pathOrej;
	__pathTex = pathTex;
	__playingAnim = false;
	__currentFrame = 0;
	__animDirection = 1;


	// Let's load the file
	loadTexture();
	loadModel();
}


void RenderableModelOrej::loadTexture() {
	SDL_Surface* img = NULL;
	GLenum textureFormat;

	img = IMG_Load(__pathTex.c_str());

	if (img == NULL) {
	  Logger::getInstance()->error("Error Loading the texture: " + __pathTex);
	  //	throw "Error Loading the texture: " + __pathTex;
	  exit(0);
	}

	__texHeight = img->h;


	switch (img->format->BytesPerPixel) {
	case 4: //With alpha channel
		if (img->format->Rmask == 0x000000ff) {
			textureFormat = GL_RGBA;
		} else {
			textureFormat = GL_BGRA;
		}
		break;

	case 3: // Without alpha channel
		if (img->format->Rmask == 0x000000ff) {
			textureFormat = GL_RGB;
		} else {
			textureFormat = GL_BGR;
		}
		break;
	default:
	  {}
	  //Logger::getInstance()->error(
	  //		"Error determining the texture format. Bytes per pixel: "
	  //					+ img->format->BitsPerPixel);
	}

	glGenTextures(1, &__textureId);


	if (__textureId == GL_INVALID_OPERATION) {
	  	Logger::getInstance()->error("Error processing an OreJ texture");
		//	throw "Error processing an OreJ texture";
		exit(0);
	}

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glBindTexture(GL_TEXTURE_2D, __textureId);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D, 0, img->format->BytesPerPixel, img->w, img->h,
			0, textureFormat, GL_UNSIGNED_BYTE, img->pixels);

	SDL_FreeSurface(img);
}



  void RenderableModelOrej::loadModel(){
	// Save the path (and the whales!!)
	//__filePath = pathToFile;
	std::ifstream file;
	int nLine = 0;
	char line[256];
	int iVertex = 0;
	int iFaces = 0;
	int iFrames = 0;
	int iUV = 0;
	float a, b, c, d, e, f;

	file.open(__pathOrej.c_str());
	if (file.bad()) {
	  Logger::getInstance()->error("Error loading the OreJ file: "
	  			+ __pathOrej);
	  //	throw "Error loading the OreJ file: "+ __pathOrej;
	  exit(0);
	}

	while (!file.eof()) {
		file.getline(line, 256);
		nLine++;

		switch (line[0]) {
		case '#':
			break;
		case 'f': {
			__faces.push_back(FaceOrej());
			a = b = c = d = -1;
			sscanf(&line[2], "%f %f %f", &a, &b, &c);

			if (a < 0 || b < 0 || c < 0) {
			  Logger::getInstance()->error("Format error in OreJ! Line: "
			  			+ nLine);
			  //	throw "Format error in OreJ! Line: "+ nLine;
				exit(0);
			}

			__faces.at(iFaces).vertex[0] = &__vertex[(int) --a];
			__faces.at(iFaces).vertex[1] = &__vertex[(int) --b];
			__faces.at(iFaces).vertex[2] = &__vertex[(int) --c];

			iFaces++;
			break;
		}
		case 'v': {
			__vertex.push_back(Vector3Orej());
			sscanf(&line[2], "%f %f %f", &__vertex.at(iVertex).x, &__vertex.at(
					iVertex).y, &__vertex.at(iVertex).z);

			__vertex.at(iVertex).x *= __scaleX;
			__vertex.at(iVertex).y *= __scaleY;
			__vertex.at(iVertex).z *= __scaleZ;

			iVertex++;
			break;
		}
		case 't':
			a = b = c = d = e = f = -1;
			if (iUV > iFaces) {
			  Logger::getInstance()->error(
			    			"UV value not associated to any face!");
			  //row "UV value not associated to any face!";
			  exit(0);
			}
			sscanf(&line[2], "%f %f %f %f %f %f", &a, &b, &c, &d, &e, &f);
			__faces[iUV].uv[0].x = a;
			__faces[iUV].uv[0].y = b;
			__faces[iUV].uv[1].x = c;
			__faces[iUV].uv[1].y = d;
			__faces[iUV].uv[2].x = e;
			__faces[iUV].uv[2].y = f;

			if (a < 0 || b < 0 || c < 0 || d < 0 || e < 0 || f < 0) {
			    	Logger::getInstance()->error("Invalid UV value!");
				std::cout<<"Invalid UV value of "<<__pathOrej<<std::endl;
			  //row "Invalid UV value!";
				exit(0);
			}

			iUV++;
			break;

			break;
		case 'm':

		{
			float* m = new float[16];
			sscanf(&line[2], "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f",
					&m[0], &m[1], &m[2], &m[3], &m[4], &m[5], &m[6], &m[7],
					&m[8], &m[9], &m[10], &m[11], &m[12], &m[13], &m[14],
					&m[15]);
			__animMatrix.push_back(m);
		}
			iFrames++;

			break;
		default:
			break;
		}
	}

	__nFaces = iFaces;
	__nVertex = iVertex;
	__nFrames = iFrames;

	file.close();

	__listMesh = glGenLists(1);

	glNewList(__listMesh, GL_COMPILE);

	/* Here is where is fixed the "Origin problem"
	 * OpenGl uses a bottom-left and Blender uses an upper-left origin!
	 */

	for (int i = 0; i < __nFaces; i++) {
		glBegin( GL_TRIANGLES);
		glTexCoord2f(__faces.at(i).uv[2].x, __texHeight - __faces.at(i).uv[2].y);
		glVertex3f(__faces.at(i).vertex[0]->x, __faces.at(i).vertex[0]->y,
				__faces.at(i).vertex[0]->z);

		glTexCoord2f(__faces.at(i).uv[1].x, __texHeight - __faces.at(i).uv[1].y);
		glVertex3f(__faces.at(i).vertex[1]->x, __faces.at(i).vertex[1]->y,
				__faces.at(i).vertex[1]->z);

		glTexCoord2f(__faces.at(i).uv[0].x, __texHeight - __faces.at(i).uv[0].y);
		glVertex3f(__faces.at(i).vertex[2]->x, __faces.at(i).vertex[2]->y,
				__faces.at(i).vertex[2]->z);

		glEnd();
	}
	glEndList();

	if (__nFrames == 0) {
	  //Logger::getInstance()->warning(
	  //			"There is 0 frames for the OreJ. This may incquiry problems!");
	}

    

	__timeLastFrame = clock();


}

vector<Texture*> RenderableModelOrej::getTextures(){
  return vector<Texture*>();
    //  return __textures;
}

void RenderableModelOrej::setTextures(vector<Texture*> textures){
	textures = textures;
}


void RenderableModelOrej::drawWire(){
  std::cout<<"DRAW WIRE NOT IMPLEMENTED FOR RENDERABLE MODEL OREJ!"<<std::endl;

}

void RenderableModelOrej::draw(){
  glPushMatrix();
  //glLoadIdentity();

  // if(__useMatrix){
  //   glLoadMatrixf(__internalMat.getData());
  // }else{
  //   computePositionRotation();
    
  //   glTranslatef(__position.x, __position.y, __position.z);
    
  //   glRotatef(__rotX,1.0f,0.0f,0.0f);
  //   glRotatef(__rotY,0.0f,1.0f,0.0f);
  //   glRotatef(__rotZ,0.0f,0.0f,1.0f);
    
  // }
  computePositionRotation();

  glEnable(GL_TEXTURE_2D);
  
  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
  glBindTexture(GL_TEXTURE_2D, __textureId);
  glPolygonMode(GL_FRONT, GL_FILL);
  

  //Refresh anim!
  glMultMatrixf(__animMatrix.at(__currentFrame));
  
  if (__playingAnim) {
    
    clock_t currentTime = clock();
    int difTime = currentTime - __timeLastFrame; 
    
    if(difTime>1.0/FRAMES_PER_SEC*CLOCKS_PER_SEC){
      __currentFrame += __animDirection;
      __timeLastFrame = clock();
    }
    
    
    if (__currentFrame == (__nFrames - 1) && __animDirection==1) {
      switch (__animType) {
      case SIMPLE:
	stopAnim();
	break;
      case LOOP:
	__currentFrame = 0;
	break;
      case PINGPONG:
	
	//		  std::cout<<"Cambio de dirección!"<<std::endl;
	__animDirection *= -1;
	break;
      }
    }
    
    //For Ping-Pong mode!
    if (__currentFrame == -1) {
      __animDirection *= -1;
      __currentFrame = 0;
    }
  }
  
  glCallList(__listMesh);
  glPopMatrix();
  
  
  glDisable(GL_TEXTURE_2D);
  
  
  

  /*------------------------------------------------------------------------------------------*/
  /*------------------------------------------------------------------------------------------*/
  /*------------------------------------------------------------------------------------------*/
}



void RenderableModelOrej::enableWireXor(){
	__wireXor = true;
}
void RenderableModelOrej::disableWireXor(){
	__wireXor = false;
}

void RenderableModelOrej::enableWireTextured(){
	__wireTextured = true;
}

void RenderableModelOrej::disbaleWireTextured(){
	__wireTextured = false;
}

GLfloat* RenderableModelOrej::getWireColor(){
	return __wireColor;
}

void RenderableModelOrej::setWireColor(const float& r, const float& g, const float& b){
	__wireColor[0] = r;
	__wireColor[1] = g;
	__wireColor[2] = b;

}

GLfloat* RenderableModelOrej::getWireFillColor(){
	return __wireFillColor;
}

void RenderableModelOrej::setWireFillColor(const float& r, const float& g, const float& b){
	__wireFillColor[0] = r;
	__wireFillColor[1] = g;
	__wireFillColor[2] = b;
}

GLfloat RenderableModelOrej::getWireTrans(){
	return __wireColor[3];
}
void RenderableModelOrej::setWireTrans(const float& a){
	__wireColor[3] = a;
}

GLfloat RenderableModelOrej::getWireFillTrans(){
	return __wireFillColor[3];
}
void RenderableModelOrej::setWireFillTrans(const float& a){
	__wireFillColor[3] = a;
}

void RenderableModelOrej::enableWireAnim(){
	__wireAnimated = true;
}

void RenderableModelOrej::disableWireAnim(){
	__wireAnimated = false;
}

void RenderableModelOrej::enableAllTransparentWhileWire(){
	__allTransparentWhileWire = true;
}

void RenderableModelOrej::disableAllTransparentWhileWire(){
	__allTransparentWhileWire = false;
}

void RenderableModelOrej::setWireNormalWidth(const GLfloat& w){
	__wireNormalWidth = w;
}

void RenderableModelOrej::setWireMaxWidth(const GLfloat& w){
	__wireMaxWidth = w;
}

void RenderableModelOrej::setWireMinWidth(const GLfloat& w){
	__wireMinWidth = w;
}

void RenderableModelOrej::animateWire(){
	if ((SDL_GetTicks() - __wireAnimLastTime) > 20){
		__wireAnimLastTime = SDL_GetTicks();
		__wireAnimStep++;
		if (__wireAnimStep > 13 ){
			__wireAnimStep=1;
		}
	}
}

bool RenderableModelOrej::is3D(){
	return true;
}

void RenderableModelOrej::enableBackWire(){
	__wireBackEnabled = true;
}

void RenderableModelOrej::disableBackWire(){
	__wireBackEnabled = false;
}

RenderableModelOrej::~RenderableModelOrej() {
  /* :S */
}

/* Animation stuff */
  void RenderableModelOrej::playAnim(int animType){
    if(__animMatrix.size()<=1) return;
    
    if (!__playingAnim) {
		__playingAnim = true;
	}
    __animType = animType;
  }
  void  RenderableModelOrej::stopAnim(){
	if (__playingAnim) {
		__playingAnim = false;
	}
	__currentFrame = 0;
	__animDirection = 1;

  }
  void  RenderableModelOrej::pauseAnim(){
	if (__playingAnim) {
		__playingAnim = false;
	}

  }


}
