#include <Communication.h>

Communication::Communication(int argc, char **argv)
{
  ic = Ice::initialize(argc, argv);
  baseMS = ic->stringToProxy("MobileService@MobileService.elcanosql1.Adapter");
  baseU = ic->stringToProxy("UserManagerAdmin@UserManager.elcanosql1.Adapter");

  UmOK = true;
  MsOk = true;
  MdOk = true;

  ident = new Elcano::DevProfile("00:0c:f1:5d:70:e0", "b", "c");

  adapter
    = ic->createObjectAdapterWithEndpoints(
					   "MDAdapter", "default -p 10000");
  dev = new MobileDeviceI;
  object = dev;
  baseMD = adapter->add(object,
		       ic->stringToIdentity("MD"));
  adapter->activate();

  pthread_create( &__thread_id, NULL, &Communication::exec, this );

  //Proxy a MobileService
  try{
    MSPrx = Elcano::MobileServicePrx::checkedCast(baseMS);
  }catch(const Ice::ConnectionRefusedException){
    MsOk = false;
  }catch(const Ice::DNSException){
    MsOk = false;
  }

  //Proxy a UserManager
  try{
    um = Elcano::UserManagerPrx::checkedCast(baseU);
  }catch(const Ice::ConnectionRefusedException){
    UmOK = false;
  }catch(const Ice::DNSException){
    UmOK = false;
  }

  //Proxy a MobileDevice
  try{
    MDPrx = Elcano::MobileDevicePrx::checkedCast(baseMD);
  }catch(const Ice::ConnectionRefusedException){
    MdOk = false;
  }catch(const Ice::DNSException){
    MdOk = false;
  }
}
Communication::~Communication() {}

void* Communication::exec(void* thr)
{
  ((Communication*)thr)->sirviente();
}
void Communication::sirviente()
{
  ic->waitForShutdown();
}
TaskSeq Communication::getTasks()
{
  TaskSeq lista;
  try{
    lista = dev->getTasks();
  }catch(const IceUtil::NullHandleException){}
  return lista;
}
OpenLS::Route Communication::getRoute()
{
  OpenLS::Route ruta;
  try{
  ruta = dev->getRoute();
  }catch(const IceUtil::NullHandleException){
  }
  return ruta;
}
void Communication::login()
{
  if(UmOK)
    um->login(ident, MDPrx);
}
void Communication::exit()
{
  if(UmOK)
    um->exit(ident);
}

void Communication::selectTask(std::string name)
{
  if(MsOk)
    MSPrx->beginTask(name, ident);
}
