/*
 * RenderableVideoPlane.cpp
 *
 */

#include "RenderableVideoPlane.h"

namespace mars {

RenderableVideoPlane::RenderableVideoPlane() {
	init();

	__heigh = 1;
	__width = 2;
}




RenderableVideoPlane::RenderableVideoPlane(std::string name, std::string fileName, unsigned w, unsigned h, bool isURL) : Renderable(){

	init();

	__isURL = isURL;

	__heigh = h;
	__width = w;

	__name = name;

	__currentFile = fileName;

	VLCPlayer::getInstance()->addMedia(name, fileName, isURL);
	__sMedia = VLCPlayer::getInstance()->getMediaFromName(fileName); // NULL if failed
	if (__sMedia==NULL) {cout << "NULL" << endl;}

}


void RenderableVideoPlane::playURL(const string& URL){
	if (__sMedia->playing ) stop();
	__isURL = true;
	VLCPlayer::getInstance()->addMedia(URL, URL, true);
	__sMedia = VLCPlayer::getInstance()->getMediaFromName(URL); // NULL if failed
	//VLCPlayer::getInstance()->playMedia(URL);
	__currentFile = URL;
}

void RenderableVideoPlane::playFile(const string& fileName){
	if (__sMedia->playing ) stop();
	__isURL = false;
	VLCPlayer::getInstance()->addMedia(fileName, fileName, false);
	__sMedia = VLCPlayer::getInstance()->getMediaFromName(fileName); // NULL if failed
	//VLCPlayer::getInstance()->playMedia(fileName);
	__currentFile = fileName;
}

void RenderableVideoPlane::draw(){

	__sMedia->update();

	if ( (__sMedia->playing) && (__sMedia!=NULL) ){
		// Get the texture ...
        // -----------------

		__currentTexture = Texture::convertSurfaceToOpenGLTexture(__sMedia->ctx.surface);


		//----------------------
	}

	drawPlane();

	if ((__sMedia->playing) && (__sMedia!=NULL)){
		// Free the texture
		//----------------------
		Texture::freeOpenGLTexture(__currentTexture);
		// -----------------
	}
}

void RenderableVideoPlane::drawPlane(){
	glPushMatrix();

	int h2 = __heigh / 2;
	int w2 = __width / 2;

#if 0
	cout << "POS: " << __position.x << " " <<  __position.y << " " << __position.z << endl;
    cout << "ROT: " << __rotX << " " << __rotY << " " << __rotZ << endl;
#endif

    computePositionRotation();

//	glTranslatef(__position.x , __position.y, __position.z);
//
//	glRotatef(__rotX,1.0f,0.0f,0.0f);
//	glRotatef(__rotY,0.0f,1.0f,0.0f);
//	glRotatef(__rotZ,0.0f,0.0f,1.0f);

#if 0
	Matrix16::printModelView();
#endif

	glDisable(GL_LIGHTING);

	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

	glDisable(GL_BLEND);
//	glEnable(GL_BLEND);
//	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_TEXTURE_2D);

	if ( (__sMedia->playing) && (__sMedia!=NULL) )
		glBindTexture(GL_TEXTURE_2D, __currentTexture);
	else
		glBindTexture(GL_TEXTURE_2D, __defaultTexture->getGLTexture());

	float x1, x2, y1, y2;

	x1 = -w2 ;
	x2 = +w2 ;

	y1 = -h2;
	y2 = +h2 ;

//	Matrix16::printModelView();


#if 0
	std::cout  << x1 << " : " << x2 << " : "<< y1 << " : " << y2 << endl;
#endif

	glBegin(GL_QUADS);
		glTexCoord3f(0, 0, 0); glVertex3f(x1, y2, 0);
		glTexCoord3f(1, 0, 0); glVertex3f(x2, y2, 0);
		glTexCoord3f(1, 1, 0); glVertex3f(x2, y1, 0);
		glTexCoord3f(0, 1, 0); glVertex3f(x1, y1, 0);
	glEnd();


	if (World::getInstance()->isLightingEnabled())
		glEnable(GL_LIGHTING);

//	glDisable(GL_BLEND);

	glPopMatrix();
}

void RenderableVideoPlane::init(){

	__isURL = false;

	__currentFile = "";

	__defaultTexture = TextureFactory::getInstance()->getTexture(Configuration::getInstance()->defaultVideoTexture() );
	__currentTexture = TextureFactory::getInstance()->getGLTexture(Configuration::getInstance()->defaultVideoTexture() );
}

void RenderableVideoPlane::play(){
	VLCPlayer::getInstance()->playMedia(__currentFile);
}

void RenderableVideoPlane::stop(){
	VLCPlayer::getInstance()->stopMedia(__currentFile, __isURL);
}

RenderableVideoPlane::~RenderableVideoPlane() {

}


}
