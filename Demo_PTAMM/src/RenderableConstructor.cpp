/*
 * RenderableConstructor.cpp
 *
 */

#include "RenderableConstructor.h"


namespace mars {

RenderableConstructor::RenderableConstructor() {
	Logger::getLogger()->note("Creating Renderable Factory");
	__defaultModel = new RenderableModel();
}

RenderableConstructor::~RenderableConstructor() {

	Logger::getLogger()->note("Ending Renderable Factory");

	std::map<std::string,RenderableVideoPlane*>::iterator itv;
	std::map<std::string,RenderableWidgetButton*>::iterator itb;
	std::map<std::string,RenderableWidgetImage2D*>::iterator iti;
	std::map<std::string,RenderableModel*>::iterator itm;
	std::map<std::string,RenderableEmpty*>::iterator ite;
	std::map<std::string,Light*>::iterator itl;
	std::map<std::string,RenderableWIFICell*>::iterator itw;
	std::map<std::string,RenderableLinePath*>::iterator itp;
	std::map<std::string,RenderableWidgetMap*>::iterator itmaps;
	std::map<std::string,RenderableWidgetList*>::iterator itlists;
	std::map<std::string,RenderableText*>::iterator ittexts;

	for(ittexts = __texts.begin(); ittexts != __texts.end(); ++ittexts)
				delete ittexts->second;

	for(itlists = __lists.begin(); itlists != __lists.end(); ++itlists)
				delete itlists->second;

	for(itp = __paths.begin(); itp != __paths.end(); ++itp)
			delete itp->second;

	for(itmaps = __maps.begin(); itmaps != __maps.end(); ++itmaps)
			delete itmaps->second;

	for(iti = __images2D.begin(); iti != __images2D.end(); ++iti)
			delete iti->second;

	for(itm = __models.begin(); itm != __models.end(); ++itm)
		delete itm->second;

	for(itl = __lights.begin(); itl != __lights.end(); ++itl)
			delete itl->second;

	for(ite = __emptys.begin(); ite != __emptys.end(); ++ite)
				delete ite->second;

	for(itv = __videoPlanes.begin(); itv != __videoPlanes.end(); ++itv)
					delete itv->second;
}

Light* RenderableConstructor::createLight(const string & name){
	return getLight(name);
}

Light* RenderableConstructor::getLight(const string & name){
	std::map<std::string,Light*>::iterator it;

	it = __lights.find(name);
	if (it != __lights.end())
			return it->second;

	Light* temp = new Light();
	__lights[name]= temp;
	World::getInstance()->addLight(temp);
	return temp;
}

RenderableWidgetButton* RenderableConstructor::createWButton(const string & name, const string & text, int x, int y, int w, int h){

	RenderableWidgetButton* temp = getWButton(name);


	temp->setPosition( x, y );
	temp->setWidth(w);
	temp->setHeight(h);
	temp->setText(text);
	return temp;
}

RenderableModel* RenderableConstructor::createModel(const string & name, const std::string & file){

	RenderableModel* temp = new RenderableModel();
	__models[name] = temp;
	temp->loadWithColladaDOM(file);
	World::getInstance()->addRenderable(temp);
	return temp;

}

  RenderableModelOrej* RenderableConstructor::createModelOrej(const string & name, const std::string & fileOrej, const std::string & fileTex, float sx, float sy, float sz){
    RenderableModelOrej* temp = new RenderableModelOrej(fileOrej,fileTex,sx,sy,sz);
	__models[name] = temp;
	//temp->loadWithColladaDOM(file);
	World::getInstance()->addRenderable(temp);
	return temp;
}

RenderableWidgetButton* RenderableConstructor::getWButton(const string & name){
	std::map<std::string,RenderableWidgetButton*>::iterator it;

	it = __buttons.find(name);

	if (it != __buttons.end()){
		return it->second;
	}

	RenderableWidgetButton* temp = new RenderableWidgetButton();
	__buttons[name]= temp;
	temp->useAutoScale(false);
	World::getInstance()->addWidget(temp);
	return temp;
}

RenderableModel* RenderableConstructor::getModel(const string & name){
	std::map<std::string,RenderableModel*>::iterator it;

	it = __models.find(name);

	if (it != __models.end())
		return it->second;

	Logger::getInstance()->warning("RenderableConstructor:: Model " + name + " not found!");
	return __defaultModel;
}

RenderableEmpty* RenderableConstructor::createEmpty(const string & name){
	return getEmpty(name);
}

RenderableEmpty* RenderableConstructor::getEmpty(const string & name){
	std::map<std::string,RenderableEmpty*>::iterator it;

	it = __emptys.find(name);

	if (it != __emptys.end())
		return it->second;

	RenderableEmpty* temp = new RenderableEmpty();
	__emptys[name] = temp;
	World::getInstance()->addRenderable(temp);
	return temp;
}

RenderableVideoPlane* RenderableConstructor::createVideoPlane(const string & name, const string & fileName,
                                                              const unsigned& w, const unsigned& h,
                                                              bool isURL){

	RenderableVideoPlane* temp;

	std::map<std::string,RenderableVideoPlane*>::iterator it;

	it = __videoPlanes.find(name);

	if (it != __videoPlanes.end())
		return it->second;


	temp = new mars::RenderableVideoPlane(name, fileName, w, h, isURL);
	World::getInstance()->addRenderable(temp);
	__videoPlanes[name] = temp;
	return temp;
}

RenderableVideoPlane* RenderableConstructor::getVideoPlane(const string & name){
	std::map<std::string,RenderableVideoPlane*>::iterator it;

	it = __videoPlanes.find(name);

	if (it != __videoPlanes.end())
		return it->second;

	//Logger::getInstance()->warning("RenderableConstructor :: VideoPlane does not exist : " + name);

	return NULL;

}

RenderableWIFICell* RenderableConstructor::createWIFICell(const string& name, const float& radius){

	std::map<std::string,RenderableWIFICell*>::iterator it;
	it = __wifis.find(name);

	if (it == __wifis.end() ){
		RenderableWIFICell* temp = new RenderableWIFICell(radius);
		__wifis[name] = temp;
		World::getInstance()->addRenderable(temp);
		return temp;
	}
	else {
		Logger::getInstance()->warning("RenderableConstructor:: wifiCell named "
				+ name + " already exists");
		return it->second;
	}

}

RenderableWIFICell* RenderableConstructor::getWIFICell(const string& name){
	std::map<std::string,RenderableWIFICell*>::iterator it;

	it = __wifis.find(name);

	if (it != __wifis.end() ){
		return it->second;
	}
	else {
		Logger::getInstance()->warning("RenderableConstructor:: wifiCell named "
				+ name + " does not exist, creating new one with radius =  1.0...");
		RenderableWIFICell* temp = new RenderableWIFICell(1.0);
		__wifis[name] = temp;
		World::getInstance()->addRenderable(temp);
		return temp;
	}
}


RenderableLinePath* RenderableConstructor::createLinePath(const string& name){
	return getLinePath(name);
}


RenderableLinePath* RenderableConstructor::getLinePath(const string& name){

	std::map<std::string,RenderableLinePath*>::iterator it;

	it = __paths.find(name);


	if (it == __paths.end() ){
		RenderableLinePath* temp = new RenderableLinePath();
		__paths[name] = temp;
		World::getInstance()->addRenderable(temp);
		return temp;
	}
	else {
		return it->second;
	}


}

RenderableWidgetImage2D* RenderableConstructor::createImage2D(const string& name,
		const string& textureFile, const unsigned& w, const unsigned& h){

	std::map<std::string,RenderableWidgetImage2D*>::iterator it;

	it = __images2D.find(name);

	if (it != __images2D.end()){
		Logger::getInstance()->warning("RenderableConstructor:: image2d already exists, returning an already create instance");
			return it->second;
	}

	mars::RenderableWidgetImage2D* temp = new mars::RenderableWidgetImage2D(textureFile, w, h);
	World::getInstance()->addWidget(temp);
	__images2D[name] = temp;
	return temp;

}

RenderableWidgetImage2D* RenderableConstructor::getImage2D(const string& name){
	std::map<std::string,RenderableWidgetImage2D*>::iterator it;

	it = __images2D.find(name);
	if (it != __images2D.end()){
				return it->second;
	}
	Logger::getInstance()->error("RenderableConstructor :: image2D does not exist : " + name+ " :: Returning NULL!!");
	return NULL;
}


RenderableWidgetMap* RenderableConstructor::createWMap(const std::string& name,
			const std::string& fileNameBack,
			const int& width, const int& height,
			const float& scale){

	std::map<std::string,RenderableWidgetMap*>::iterator itmaps;

	itmaps = __maps.find(name);

	if (itmaps != __maps.end()){
		Logger::getInstance()->warning("RenderableConstructor:: map already exists, returning an already create instance");
		return itmaps->second;
	}

	RenderableWidgetMap* temp = new RenderableWidgetMap(fileNameBack, width, height, scale);
	World::getInstance()->addWidget(temp);
	__maps[name] = temp;

	return temp;
}


RenderableWidgetMap* RenderableConstructor::getWMap(const std::string& name){

	std::map<std::string,RenderableWidgetMap*>::iterator itmaps;

	itmaps = __maps.find(name);
	if (itmaps == __maps.end()){
		Logger::getInstance()->error("RenderableConstructor :: map does not exist : " + name + " :: Returning NULL!!");
		return NULL;
	}

	return itmaps->second;

}

RenderableWidgetList* RenderableConstructor::createWList(const std::string& name,
                                                     const unsigned& w,
                                                     const unsigned& h,
                                                     const float& widthPortionUpDown,
                                                     const unsigned& nVisibleOptions){

	std::map<std::string,RenderableWidgetList*>::iterator itlists;

	itlists = __lists.find(name);
	if (itlists != __lists.end()){
		Logger::getInstance()->warning("RenderableConstructor:: list already exists, returning an already create instance");
		return itlists->second;
	}

	RenderableWidgetList* temp = new RenderableWidgetList(w, h,
			widthPortionUpDown, nVisibleOptions);

	World::getInstance()->addWidget(temp);
	__lists[name] = temp;

	return temp;
}


RenderableWidgetList* RenderableConstructor::getWList(const std::string& name){

	std::map<std::string,RenderableWidgetList*>::iterator itlists;

	itlists = __lists.find(name);
	if (itlists == __lists.end()){
		Logger::getInstance()->error("RenderableConstructor:: list DOES NOT"
				" exists, returning a NULL pointer");
		return NULL;
	}
	return itlists->second;
}


RenderableText* RenderableConstructor::createText(const std::string& name,
                                              const std::string& font,
                                              char* str,
                                              FontType fType){

	std::map<std::string,RenderableText*>::iterator it;

	it = __texts.find(name);

	if (it != __texts.end()){
		Logger::getInstance()->warning("RenderableConstructor:: Text Object already"
				" exists. Returning that instance");
		return it->second;
	}

	RenderableText* temp;
	temp = new RenderableText(font, str, fType);
	temp->setSize(6);
	__texts[name] = temp;
	World::getInstance()->addRenderable(temp);
	return temp;


}


RenderableText* RenderableConstructor::createTextLUA(const std::string& name, const string& str){
	return createText(name, Configuration::getConfiguration()->defaultFont(), const_cast<char*>(str.c_str()), EXTRUDED);
}

RenderableText* RenderableConstructor::getText(const std::string& name){
	std::map<std::string,RenderableText*>::iterator it;

	it = __texts.find(name);
	if (it == __texts.end()){
		Logger::getInstance()->warning("RenderableConstructor:: Text Object ( "
				+ name + ") not found. Returning a new instance");
		RenderableText* temp;
		temp = new RenderableText();
		__texts[name] = temp;
		return temp;
	}

	return it->second;
}


}
