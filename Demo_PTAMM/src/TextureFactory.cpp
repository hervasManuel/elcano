/*
 * TextureFactory.cpp
 *
 */

#include "TextureFactory.h"

namespace mars {


TextureFactory::TextureFactory() {
	Logger::getLogger()->note("Initializing Texture Factory");
}

TextureFactory::~TextureFactory() {

	Logger::getLogger()->note("Ending Texture Factory");

	map<string, Texture*>::iterator it;

		for (it = __textures.begin(); it != __textures.end(); ++it)
			delete it->second;

}
const GLuint TextureFactory::getGLTexture(const string& name) {

	map<string, Texture*>::iterator it;

	it = __textures.find(name);

	if (it != __textures.end())
		return __textures[name]->getGLTexture();

	// Does not exist yet.
	Texture* tempTexture = new Texture(name);
	__textures[name] = tempTexture;
	return __textures[name]->getGLTexture();
}

Texture* TextureFactory::getTexture(const string& name) {
	map<string, Texture*>::iterator it;

	it = __textures.find(name);

	if (it != __textures.end())
		return __textures[name];

	// Does not exist yet.
	Texture* tempTexture = new Texture(name);
	__textures[name] = tempTexture;
	return __textures[name];
}


}
