/*
 * VideoDeviceUEye.cpp
 *
 */

#include "VideoDeviceUEye.h"

namespace mars {

VideoDeviceUEye::VideoDeviceUEye(UEYE_CAMERA_INFO cameraInfo): VideoDevice("default_UEye") {

	__cameraInfo = cameraInfo;
	init();
}

VideoDeviceUEye::VideoDeviceUEye(UEYE_CAMERA_INFO cameraInfo, std::string name) : VideoDevice(name){
	__cameraInfo = cameraInfo;
	init();
}



void VideoDeviceUEye::ThreadCaptureFrame(){
	Mat* frame;

	while(!__done){

		if(!__done){
//			if (is_FreezeVideo(__hCam, IS_WAIT) == IS_SUCCESS){
			if (is_WaitEvent(__hCam, IS_SET_EVENT_FRAME, 1) == IS_SUCCESS) {
				IplImage i;
				IplImage* img = new IplImage;
                                
				img->nSize=112;
				img->ID=0;
				img->nChannels=3;
				img->alphaChannel=0;
				img->depth=8;
				img->dataOrder=0;
				img->origin=0;
				img->align=4;
				img->width=__maxW;
				img->height=__maxH;
				img->roi=NULL;
				img->maskROI=NULL;
				img->imageId=NULL;
				img->tileInfo=NULL;
				img->imageSize=3*__maxW*__maxH;
				img->imageData=(char*)__imageMem;  //the pointer to imagaData
				img->widthStep=3*__maxW;
				img->imageDataOrigin=(char*)__imageMem; //and again
                                
                                
				frame = new Mat(img, false);
                                
                                
				fBuffer->addFrame(frame);
			}
		}


	}

	// Free Memory and Camera Exit :)

	is_FreeImageMem( __hCam, __imageMem, __memId);

	if (__hCam)
		is_ExitCamera( __hCam); // FIXME EXIT CAMERA


}

void VideoDeviceUEye::init(){


	Logger::getInstance()->note("VideoDeviceUEye:: Trying device " + toString(__cameraInfo.dwDeviceID));
	__hCam = (HIDS) __cameraInfo.dwDeviceID;

#ifndef NDEBUG
	cout << "uEye hCam = " << __hCam << endl;
#endif


	int ret = is_InitCamera(&__hCam, NULL);

	is_EnableAutoExit(__hCam, IS_ENABLE_AUTO_EXIT);

	if (ret == IS_SUCCESS){

		SENSORINFO sInfo;                 // sensor information
		is_GetSensorInfo(__hCam, &sInfo); // query

		is_SetExternalTrigger (__hCam, IS_SET_TRIGGER_OFF);

		__maxW = sInfo.nMaxWidth;
		__maxH = sInfo.nMaxHeight;

		cout << "maxW:: " << __maxW << endl;
		cout << "maxH:: " << __maxH << endl;

		__maxW = Configuration::getInstance()->openGLScreenWidth();  // FIXME --- UNSUPPORTED RESOLUTION?
		__maxH = Configuration::getInstance()->openGLScreenHeight();

		__bitsPerPixel = 24;

		__colorMode = IS_CM_BGR8_PACKED;

		is_SetColorMode(__hCam, __colorMode);

#ifndef NDEBUG
		cout << "colorMode:: " << __colorMode << endl;
#endif

		// Allocate memory for the image
		is_AllocImageMem(__hCam, __maxW, __maxH, __bitsPerPixel, &__imageMem, &__memId);

		is_SetImageMem(__hCam, __imageMem, __memId);

		int xPos, yPos = 0;
                
                //is_AOI
                //
                IS_RECT rectAOI;
 
                rectAOI.s32X     = 0;
                rectAOI.s32Y     = 0;
                rectAOI.s32Width = __maxW;
                rectAOI.s32Height = __maxH;
                
                is_AOI(__hCam, IS_AOI_IMAGE_SET_AOI ,(void*)&rectAOI, sizeof(rectAOI));
                
                
                
		//Deprecated!!! 
                
                //is_SetAOI(__hCam, IS_SET_IMAGE_AOI, &xPos, &yPos, &__maxW, &__maxH);

		//is_SetImagePos(__hCam, 0, 0);

		//is_SetImageSize(__hCam, __maxW, __maxH);
                is_SetPixelClock(__hCam, 16);
                
		double nFPS;
		is_SetFrameRate(__hCam, 30.0, &nFPS);


//		double pVal1=NULL;
//		double pVal2=NULL;

		//is_SetAutoParameter(__hCam, IS_MAX_AUTO_SPEED, NULL, NULL);
		//is_SetAutoParameter(__hCam, , pVal1, pVal2);

		is_EnableEvent(__hCam, IS_SET_EVENT_FRAME);

		is_CaptureVideo(__hCam, IS_DONT_WAIT);

	}

		fps = 15;

		width = __maxW;
		height =__maxH;

}

PUEYE_CAMERA_LIST VideoDeviceUEye::getUEyeCameraList(){

//	PUEYE_CAMERA_LIST cList = new UEYE_CAMERA_LIST;
//	cList->dwCount = 0;
//
//	 if (is_GetCameraList (cList) != IS_SUCCESS)
//	 {
//		 Logger::getInstance()->error("VideoDeviceUEye:: no uEye camera found.");
//	 } else {
//		 Logger::getInstance()->note("VideoDeviceUEye:: " +
//				 toString(cList->dwCount) +
//				 " uEye camera/s found.");
//	 }

	PUEYE_CAMERA_LIST cList;
	cList = new UEYE_CAMERA_LIST;
	cList->dwCount = 0;
	DWORD nCams_uEYE = 0;
	if (is_GetCameraList (cList) == IS_SUCCESS){

		nCams_uEYE = cList->dwCount;

		cout << "[*] Number of Cameras:: " << nCams_uEYE << endl;

		delete cList;

		cList = (PUEYE_CAMERA_LIST) new char[sizeof(DWORD) + nCams_uEYE * sizeof(UEYE_CAMERA_INFO)];
		cList->dwCount = nCams_uEYE;

		cout << sizeof(DWORD) + nCams_uEYE * sizeof(UEYE_CAMERA_INFO) << endl;
		cout << cList->dwCount << endl;

		if (is_GetCameraList(cList) == IS_SUCCESS)	{
			cout << "[*] OK :)" << endl;
			for (int c=0; c < (int) nCams_uEYE; ++c){
				cout << "-- UEYE CAMERA --" << endl;
				cout << "CameraID :" << cList->uci[c].dwCameraID << endl;
				cout << "Ser No.  :" << cList->uci[c].SerNo << endl;
				cout << "DeviceID :" << cList->uci[c].dwDeviceID << endl;
				cout << "In use   :" << cList->uci[c].dwInUse << endl;
				cout << "SensorID :" << cList->uci[c].dwSensorID << endl;
			}
			cout << "--" << endl;
		}
	  }


	return cList;
}

VideoDeviceUEye::~VideoDeviceUEye(){

	is_StopLiveVideo(__hCam, IS_DONT_WAIT);

	Logger::getInstance()->note("VideoDeviceUEye:: " + toString(__hCam) + " exiting::" + __name);


}


}
