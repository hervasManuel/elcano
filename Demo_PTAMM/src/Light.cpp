/*  This file is part of M.A.R.S.

    Copyright (C) 2013  Oreto Research Lab (Universidad de Castilla-La Mancha)

    M.A.R.S. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    M.A.R.S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with M.A.R.S.   If not, see <http://www.gnu.org/licenses/>.
*/


/*
 * Light3D.cpp
 *
 */

#include "Light.h"

namespace mars {

Light::Light() {
	__ambient[0]=0;
	__ambient[1]=0;
	__ambient[2]=0;
	__ambient[3]=0;

	__diffuse[0]=0;
	__diffuse[1]=0;
	__diffuse[2]=0;
	__diffuse[3]=0;

	__specular[0]=0;
	__specular[1]=0;
	__specular[2]=0;
	__specular[3]=0;

	__position[0]=0;
	__position[1]=0;
	__position[2]=0;
	__position[3]=0;
}

void Light::setAmbient(const GLfloat& r, const GLfloat& g, const GLfloat& b,
		const GLfloat& a){
	__ambient[0]=r;
	__ambient[1]=g;
	__ambient[2]=b;
	__ambient[3]=a;
}

void Light::setDiffuse(const GLfloat& r, const GLfloat& g, const GLfloat& b, const GLfloat& a){
	__diffuse[0]=r;
	__diffuse[1]=g;
	__diffuse[2]=b;
	__diffuse[3]=a;
}

void Light::setSpecular(const GLfloat& r, const GLfloat& g, const GLfloat& b, const GLfloat& a){
	__specular[0]=r;
	__specular[1]=g;
	__specular[2]=b;
	__specular[3]=a;
}

void Light::setPosition(const GLfloat& x, const GLfloat& y, const GLfloat& z, const GLfloat& w){
	__position[0]=x;
	__position[1]=y;
	__position[2]=z;
	__position[3]=w;
}

const GLfloat* Light::getAmbient(){
	return __ambient;
}

const GLfloat* Light::getDiffuse(){
	return __diffuse;
}

const GLfloat* Light::getSpecular(){
	return __specular;
}

const GLfloat* Light::getPosition(){
	return __position;
}

Light::~Light() {
#ifndef NDEBUG
	std::cout <<  "[DEBUG]: Deleting light" << std::endl;
#endif

}

}
