#include "mars.h"

using namespace std;

cv::Mat markPlaneMat(4,4,CV_32F);
float markPlaneSize;
int mapId=-1;
Fine * fTunning;
mars::RenderableEmpty* empty;
mars::RenderableModel* mainR;

cv::Mat calculateOffset(){
  std::cout<<"Calculating Offset Matrix for map "<<mapId<<"..."<<std::endl;
  cv::Mat renderableMat(4,4,CV_32F);
  cv::Mat offsetMat(4,4,CV_32F);


  //Building the renderableMat (Renderable relative-to-ptamm-origin matrix!)
  // renderableScale/markPlaneSize * pX, Py, Pz
  renderableMat=mainR->getRelativePosition();

  //float ratio = markPlane->getScaleX()/markPlaneSize;
  //  float ratio = markPlaneSize/mainR->getScaleX();
  //*aspectRatio = ratio;

  //std::cout<<mainR->getScaleX()<<std::endl;
  //std::cout<<"Aspect Ratio: "<<*aspectRatio<<std::endl;
  
  //The position times ratio 
  //renderableMat.at<float>(3,0)*=ratio;
  //renderableMat.at<float>(3,1)*=ratio;
  //renderableMat.at<float>(3,2)*=ratio;


  //Getting the OffsetMat (theoretically, MOffset = MMArkPlaneMat^-1 * MRenderable)
  //Now, IS an actual Offset Matrix
  offsetMat =(markPlaneMat.inv()  * renderableMat).inv();

  
  //offsetMat.at<float>(3,0)/=ratio;
  //offsetMat.at<float>(3,1)/=ratio;
  //offsetMat.at<float>(3,2)/=ratio;


  return offsetMat;
}



void saveConf(void* arg){

  cv::Mat offsetMat = calculateOffset();

  std::cout<<"Writing Offset Matrix for map "<<mapId<<"..."<<std::endl;
  ofstream file; 
  file.open("PTAMMOffset.mat",ios::out);
  if(!file.is_open()){
    std::cout<<"Error opening PTAMMOffset.mat file to write"<<std::endl;
    exit(-1);
  }

  // file<<"# #MapId #AspectRatio #OffsetMat..."<<std::endl;
  // file<<"# The smaller the Aspect Ratio, the \"bigger\" the object seems :)"<<std::endl;
  // file<<"# The Matrix Offset is \"normalized\", has not been applied the Aspect Ratio"<<std::endl;
  
  file<<"# #MapId #OffsetMat..."<<std::endl;

  file<<"["<<mapId<<"] ";  
  file<<offsetMat.at<float>(0,0)<<" ";  file<<offsetMat.at<float>(0,1)<<" ";  file<<offsetMat.at<float>(0,2)<<" ";  file<<offsetMat.at<float>(0,3)<<" ";
  file<<offsetMat.at<float>(1,0)<<" ";  file<<offsetMat.at<float>(1,1)<<" ";  file<<offsetMat.at<float>(1,2)<<" ";  file<<offsetMat.at<float>(1,3)<<" ";
  file<<offsetMat.at<float>(2,0)<<" ";  file<<offsetMat.at<float>(2,1)<<" ";  file<<offsetMat.at<float>(2,2)<<" ";  file<<offsetMat.at<float>(2,3)<<" ";
  file<<offsetMat.at<float>(3,0)<<" ";  file<<offsetMat.at<float>(3,1)<<" ";  file<<offsetMat.at<float>(3,2)<<" ";  file<<offsetMat.at<float>(3,3)<<" ";
  
  file<<std::endl;
 
  /*Antother*/
  file.close();
  file.open("PTAMMTemp.mat",ios::out);
  if(!file.is_open()){
    std::cout<<"Error opening PTAMMTemp.mat file to write"<<std::endl;
    exit(-1);
  }

  file<<"["<<mapId<<"] ";
  file<<mainR->getPosition().x<<" "<<mainR->getPosition().y<<" "<<mainR->getPosition().z<<" ";
  file<<mainR->getAngles()[0]<<" "<<mainR->getAngles()[1]<<" "<<mainR->getAngles()[2]<<std::endl;


  std::cout<<"Wrote succesfully!"<<std::endl;

}

void readPTAMMOffset(){
  int mi;
  float posx,posy,posz;
  float rotx,roty,rotz;

  ifstream file; 
  file.open("PTAMMTemp.mat",ios::in);
  if(file.is_open()){

    string line;
    while(!file.eof()){
      getline(file,line);
      if(line.size()==0) continue;
      if(line[0]!='#'){
	setlocale (LC_NUMERIC, "C"); // ... so we have an uniform config file...
	sscanf(line.c_str(),"[%d] %f %f %f %f %f %f",
	       &mi,
	       &posx,&posy,&posz,
	       &rotx,&roty,&rotz);
	
	if(mi==mapId){
	  empty->setPosition(posx,posy,posz);
	  empty->addRotationX(rotx);
	  empty->addRotationY(roty);
	  empty->addRotationZ(rotz);
	  
	  mainR->setPosition(posx,posy,posz);
	  mainR->addRotationX(rotx);
	  mainR->addRotationY(roty);
	  mainR->addRotationZ(rotz);
	}
      }
    }
    
  }
  
}

void readMarkPlaneConf(){
  ifstream file; 
  file.open("MarkPlane.mat",ios::in);
  if(!file.is_open()){
    std::cout<<"Error opening MarkPlane.mat"<<std::endl;
    std::cout<<"The format must be:"<<std::endl;
    std::cout<<"#posMatrix #size"<<std::endl;
    exit(-1);
  }

  string line;
  while(!file.eof()){
    getline(file,line);
    if(line.size()==0) continue;
    if(line[0]!='#'){
      setlocale (LC_NUMERIC, "C"); // ... so we have an uniform config file...
      sscanf(line.c_str(),"%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f",
	     &(markPlaneMat.at<float>(0,0)), &(markPlaneMat.at<float>(0,1)),&(markPlaneMat.at<float>(0,2)),&(markPlaneMat.at<float>(0,3)),
	     &(markPlaneMat.at<float>(1,0)), &(markPlaneMat.at<float>(1,1)),&(markPlaneMat.at<float>(1,2)),&(markPlaneMat.at<float>(1,3)),
	     &(markPlaneMat.at<float>(2,0)), &(markPlaneMat.at<float>(2,1)),&(markPlaneMat.at<float>(2,2)),&(markPlaneMat.at<float>(2,3)),
	     &(markPlaneMat.at<float>(3,0)), &(markPlaneMat.at<float>(3,1)),&(markPlaneMat.at<float>(3,2)),&(markPlaneMat.at<float>(3,3)),
	     &markPlaneSize);
    }
  }

}



int main(int argc, char **argv){

  mars::Logger::getLogger()->note("Beginning Execution");

  /* ---- Getting the Map Id ------ */
  if(argc!=2){
    std::cout<<"Usage: ./build/PTAMMLocator #mapId"<<std::endl;
    return 0;
  }

  mapId = atoi(argv[1]);
  if(mapId<0){
    std::cout<<"Invalid Map Id :( FUCK  YOU!"<<std::endl;
    return 0;
  }

  /*----- Getting Mark Size and Position Matrix -----*/
  readMarkPlaneConf();
 
  
  /* ------------- MARS Stuff --------------*/
  mars::VideoCapture* vC_opencv = mars::VideoCaptureFactory::getFactory()->createVideoCapture(OPENCV);;
  VIDEO_CAPTURE_TYPE cameraType;

  /*Indicamos el tipo de cámara a usar*/
  cameraType=OPENCV;

  mars::VideoSource* vD_user=NULL;

  /*Añadiendo los dispositivos de vídeo (VideoDevices) */
  if(cameraType==OPENCV){
    vector<string> deviceNames = mars::getDevVideoCaptureNames();
    if (deviceNames.size() < 1){
      mars::Logger::getInstance()->error("Se necesita al menos una cámara para esta demo");
      return 0;
    } else {

      unsigned firstVideoDevice = atoi(& deviceNames.at(0)[mars::getDevVideoCaptureNames().at(0).size()-1]);

      cout << "--------------------------------" << endl;
      cout << "Device number " << firstVideoDevice << " being used." << endl;
      cout << "--------------------------------" << endl;

      /*OpenCv Camera*/
      vC_opencv->addNewVideoSource(firstVideoDevice, "user"); // Let's add a videosource
      vD_user = vC_opencv->getVideoDevice("user");
    }
  }else{
    mars::VideoCapture* vC = mars::VideoCaptureFactory::getFactory()->createVideoCapture(UEYE);
    if (vC ==NULL) {
      exit(-1);
    } else{
      cameraType=UEYE;
    }

    vD_user = vC->getVideoDevice(0);
  }
  if (vD_user == NULL) {
    mars::Logger::getLogger()->error("No device or video loaded. HALTING.");
    return 0;
  }

  mars::VideoOutputSDLOpenGL* vOGL;
  vOGL = mars::VideoOutputSDLOpenGL::getInstance();
  mars::World* theWorld = mars::World::getInstance();
  mars::Light* l = mars::RenderableConstructor::getInstance()->createLight("Light zero");
  l->setAmbient(0.5f, 0.5f, 0.5f, 1.0f);
  l->setDiffuse(1.0f, 1.0f, 1.0f, 10.f);
  l->setPosition(0.0f, 0.0f, 10.0f, 1.0f);
  theWorld->addLight(l);
  mars::Camera3D* c = vD_user->getOpenGLCamera();
  c->setEye(0,0,0);
  c->setLookTo(0,0,1);
  theWorld->addCamera(c);

  mars::TrackingMethodFactory::getInstance()->createTrackingMethod("PTAMM",mars::PTAMM,vD_user);
  mars::TrackingMethodOAbsolutePTAMM* tPTAMM;
  tPTAMM=(mars::TrackingMethodOAbsolutePTAMM*) mars::TrackingMethodFactory::getInstance()->__tMethods.find("PTAMM")->second;
  mars::TrackingController* tC=mars::TrackingController::getController();
  tC->setCamera(c);

  /*Tracking method*/
  theWorld->setBackgroundSource(vD_user);
  if (cameraType == UEYE)
    theWorld->setFrameResizing(true);




 /*--------------------------------------------------------*/
  /* RENDERABLES! COME ON!! UOH UOH */
  /*--------------------------------------------------------*/

  /*Renderable Empty*/
  empty = mars::RenderableConstructor::getInstance()->createEmpty("EMPTY");
  empty->useMatrix(false);
  empty->setPosition(0,0,0);
  empty->setScale(markPlaneSize,markPlaneSize,markPlaneSize);

  /*Renderable Mark Plane*/
  // mainR = mars::RenderableConstructor::getInstance()->createModel("MARKPLANE","./daes/MarkPlane.dae");
  // mainR->useMatrix(false);
  // mainR->setPosition(0,0,0);
  // mainR->setScale(markPlaneSize,markPlaneSize,markPlaneSize);

  /*Renderable Plano! */
  mainR = mars::RenderableConstructor::getInstance()->createModel("PLANO","./daes/Plano.dae");
  mainR->useMatrix(false);
  mainR->setPosition(0,0,0);
  mainR->setScale(markPlaneSize,markPlaneSize,markPlaneSize);
  mainR->setRenderMode(1);

  readPTAMMOffset();
  /*--------------------------------------------------------*/
  /* FINE TUNNING STUFF! */
  /*--------------------------------------------------------*/

 fTunning = new Fine(tPTAMM,mainR,empty);

  
  mars::RenderableWidgetButton* myButtonX = mars::RenderableConstructor::getInstance()->createWButton("Axis x", "Axis x", 100, 50, 50, 30);
  myButtonX->useAutoScale();
  myButtonX->publishHandledEvent(mars::CLICK, Fine::actX,NULL);
  
  mars::RenderableWidgetButton* myButtonY = mars::RenderableConstructor::getInstance()->createWButton("Axis y", "Axis y", 200, 50, 50, 30);
  myButtonY->useAutoScale();
  myButtonY->publishHandledEvent(mars::CLICK, Fine::actY,NULL);
  
  mars::RenderableWidgetButton* myButtonZ = mars::RenderableConstructor::getInstance()->createWButton("Axis z", "Axis z", 300, 50, 50, 30);
  myButtonZ->useAutoScale();
  myButtonZ->publishHandledEvent(mars::CLICK, Fine::actZ,NULL);
  
  mars::RenderableWidgetButton* myButtonRot = mars::RenderableConstructor::getInstance()->createWButton("Rotate", "Rotate", 400, 50, 50, 30);
  myButtonRot->useAutoScale();
  myButtonRot->publishHandledEvent(mars::CLICK, Fine::rotateBut,NULL);
  
  mars::RenderableWidgetButton* myButtonDesp = mars::RenderableConstructor::getInstance()->createWButton("Displace", "Displace", 500, 50, 50, 30);
  myButtonDesp->useAutoScale();
  myButtonDesp->publishHandledEvent(mars::CLICK, Fine::dispBut,NULL);
  
  mars::RenderableWidgetButton* myButtonPlus = mars::RenderableConstructor::getInstance()->createWButton("+   ", "+   ", 200, 100, 50, 30);
  myButtonPlus->useAutoScale();
  myButtonPlus->publishHandledEvent(mars::CLICK, Fine::increment,NULL);

  mars::RenderableWidgetButton* myButtonPlusPlus = mars::RenderableConstructor::getInstance()->createWButton("+++", "+++", 250, 100, 50, 30);
  myButtonPlusPlus->useAutoScale();
  myButtonPlusPlus->publishHandledEvent(mars::CLICK, Fine::incrementPlus,NULL);
  
  mars::RenderableWidgetButton* myButtonMinus = mars::RenderableConstructor::getInstance()->createWButton("-   ", "-   ", 300, 100, 50, 30);
  myButtonMinus->useAutoScale();
  myButtonMinus->publishHandledEvent(mars::CLICK, Fine::decrement,NULL);

  mars::RenderableWidgetButton* myButtonMinusMinus = mars::RenderableConstructor::getInstance()->createWButton("---", "---", 350, 100, 50, 30);
  myButtonMinusMinus->useAutoScale();
  myButtonMinusMinus->publishHandledEvent(mars::CLICK, Fine::decrementMinus,NULL);

  
  mars::RenderableWidgetButton* myButtonSave = mars::RenderableConstructor::getInstance()->createWButton("Save", "Save", 400, 100, 50, 30);
  myButtonSave->useAutoScale();
  myButtonSave->publishHandledEvent(mars::CLICK, saveConf,NULL);
  
  
 


  
  /*--------------------------------------------------------*/
  /* Main Loop */
  /*--------------------------------------------------------*/
  while(theWorld->step()){
    tC->tempFixDeleteMe();
  }
  std::cout << "Exiting..." << std::endl;
  return 0;
}

