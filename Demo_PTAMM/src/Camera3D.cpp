/*
 * Camera3D.cpp
 *
 */

#include "Camera3D.h"

namespace mars {

Camera3D::Camera3D(std::string name) {
	__up.x = 0.0;
	__up.y = 1.0;
	__up.z = 0.0;

	__eye.x = 0.0;
	__eye.y = 1.0;
	__eye.z = 0.0;

	__lookTo.x = 0.0;
	__lookTo.y = 1.0;
	__lookTo.z = 0.0;

	__name = name;

}

Camera3D::~Camera3D() {}

Camera3D::Camera3D(Matrix16 m, std::string name){
	__name = name;
	__up.x = 0.0;
	__up.y = 1.0;
	__up.z = 0.0;

	__eye.x = 0.0;
	__eye.y = 1.0;
	__eye.z = 0.0;

	__lookTo.x = 0.0;
	__lookTo.y = 1.0;
	__lookTo.z = 0.0;

	__projectionMatrix = m;

}

void Camera3D::setName(std::string name){
	__name = name;
}

std::string Camera3D::getName(){
	return __name;
}

void Camera3D::use(){
	gluLookAt(__eye.x, __eye.y, __eye.z, __lookTo.x, __lookTo.y, __lookTo.z, __up.x, __up.y, __up.z);
}

void Camera3D::setEye(const float& x, const float& y, const float& z){
	__eye.x = x;
	__eye.y = y;
	__eye.z = z;
}
void Camera3D::setLookTo(const float& x, const float& y, const float& z){
	__lookTo.x = x;
	__lookTo.y = y;
	__lookTo.z = z;
}

Vector3D Camera3D::getEye() const{
	return __eye;
}

Vector3D Camera3D::getLookTo() const{
	return __lookTo;
}

void Camera3D::setUp(const float& x, const float& y, const float& z){
	__up.x = x;
	__up.y = y;
	__up.z = z;
}



Vector3D Camera3D::getUp() const{
	return __up;
}

void Camera3D::setProjectionMatrix(Matrix16 m){
	__projectionMatrix = m;
}

void Camera3D::setProjectionMatrix(GLfloat* m){
	__projectionMatrix.setData(m);
}

Matrix16 Camera3D::getProjectionMatrix(){
	return __projectionMatrix;
}


}
