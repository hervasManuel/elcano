#include <WifiPos.h>

WifiPos::WifiPos(int argc, char **argv)
{
  ic = Ice::initialize(argc, argv);
  baseLA = ic->stringToProxy("LocationAdminI@WifiLocation.elcanosql1.Adapter");

  LAOk = false;

  adapter
    = ic->createObjectAdapterWithEndpoints(
					   "LLI", "default -p 10002");
  LL = new LocationListenerI();
  object = LL;
  baseLL = adapter->add(object,
		       ic->stringToIdentity("LLI"));
  adapter->activate();
  LLPrx = MLP::LocationListenerPrx::checkedCast(baseLL);
  pthread_create( &__thread_id, NULL, &WifiPos::exec, this );
}
WifiPos::~WifiPos(){}


bool WifiPos::connect()
{
  if(!LAOk){
    //Proxy a LocationAdmin
    try{
      LAPrx = MLP::LocationAdminPrx::checkedCast(baseLA);
      LAOk = true;
      LAPrx->addListener(LLPrx);
    }catch(const Ice::ConnectionRefusedException){
      LAOk = false;
    }
  }
  return LAOk;
}

void* WifiPos::exec(void* thr)
{
  ((WifiPos*)thr)->sirviente();
}
void WifiPos::sirviente()
{
  ic->waitForShutdown();
}
MLP::ShapePtr WifiPos::getWifiPos()
{
  return LL->getPos();
}
