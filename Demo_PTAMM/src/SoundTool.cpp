/*
 * SoundTool.cpp
 *
 */

#include "SoundTool.h"

namespace mars {


SoundTool::SoundTool() : __audioAvailable(false) {

	Logger::getLogger()->note("Initializing SDL audio");

	if (SDL_Init(SDL_INIT_AUDIO) != 0) {
		Logger::getLogger()->error("Unable initialize audio: " + string(SDL_GetError()));
		__audioAvailable = false;
		return;
	}

	int audioRate = 22050;
	Uint16 audioFormat = AUDIO_S16SYS;
	int audioChannels = 2;
	int audioBuffers = 4096;

	if ( Mix_OpenAudio(audioRate, audioFormat, audioChannels, audioBuffers ) != 0) {
	        Logger::getLogger()->error("Unable to open audio: " + string(SDL_GetError()));
	        __audioAvailable = false;
	        return;
	}
	__audioAvailable = true;
	int heap_size = 210000;
	int load_init_files = 1;

	festival_initialize(load_init_files, heap_size );

	if (!festival_eval_command((const char*) "(voice_el_diphone)"))
		Logger::getLogger()->warning("SoundTool :: Festival : cannot set Spanish language. Is it installed?");


}

void SoundTool::createWAVEfromText(std::string name, std::string text, bool wfs){

	SoundTool::getInstance(); // Just in case ...
	EST_Wave tempWave;
	if (!festival_text_to_wave(text.c_str() , tempWave))
		Logger::getLogger()->warning("SoundTool:: cannot convert to wave this string : " + text);
	tempWave.save(("./sounds/"+name+".wav").c_str(),(const char*) "riff");
	if (wfs)
		festival_wait_for_spooler();
}

SoundTool::~SoundTool() {
	Logger::getLogger()->note("Closing audio factory [SDL]");
	Mix_CloseAudio();
}

void SoundTool::playSoundLUA(std::string s){
	this->playSound(this->getSound(s));
}

void SoundTool::playTextLUA(std::string s){
	this->createWAVEfromText(s, s);
	this->playSoundLUA(s + ".wav");
	cout << "...." << endl;
}

Mix_Music* SoundTool::getSound(const std::string& name) {
	map<string, Mix_Music*>::iterator it;

	it = __sounds.find(name);

	if (it != __sounds.end())
		return it->second;

	// Does not exist yet.
	Mix_Music* tempSound = NULL;
	tempSound =  Mix_LoadMUS((Configuration::getConfiguration()->defaultSoundPath() + name).c_str());
	__sounds[name] = tempSound;
	return __sounds[name];
}

bool SoundTool::audioAvailable(){
	return __audioAvailable;
}

void SoundTool::playSound(Mix_Music* m){
	if (SoundTool::getInstance()->audioAvailable())
		Mix_PlayMusic(m, 0);
}


}
