/*
 * RenderableWidgetConsole.cpp
 *
 */

#include "RenderableWidgetConsole.h"

namespace mars {

RenderableWidgetConsole::RenderableWidgetConsole() : RenderableWidget() {
	init();
}

RenderableWidgetConsole::RenderableWidgetConsole(const unsigned & w , const unsigned & h, const unsigned nLines ) :  RenderableWidget (w, h), __nLines(nLines){
	init();
}

void RenderableWidgetConsole::draw(){
	glPushMatrix();

	VideoOutputSDLOpenGL::getInstance()->setOrtho();

	drawBackground();

	drawLines();

	VideoOutputSDLOpenGL::getInstance()->setView();

	glPopMatrix();

}

void RenderableWidgetConsole::drawBackground(){
	glPushMatrix();

	int h2 = getHeight() / 2;
	int w2 = getWidth() / 2;

	glLoadIdentity();

	glTranslatef(__position.x , Configuration::getConfiguration()->openGLScreenHeight()-__position.y, 0);

	glDisable(GL_LIGHTING);

	glColor4f(0.0f, 0.0f, 0.0f, 0.7f);

	glDisable(GL_TEXTURE_2D);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	float x1, x2, y1, y2;
	int dx, dy;

	x1 = -w2 ;
	x2 = +w2 ;

	y1 = -h2;
	y2 = +h2 ;

	dx = (x2 - x1) / 2;
	dy = (y2 - y1) / 2;

	__x1 = __position.x + x1;
	__x2 = __position.x + x2;

	__y1 = __position.y + y1;
	__y2 = __position.y + y2;

#if 0
	cout << "console ---" << endl;
	cout << "bounds :: " << __x1 << " :: " << __x2 << " :: " << __y1 << " :: " << __y2 << endl;
#endif

	glBegin(GL_QUADS);
		glVertex3f(x1, y2, 0);
		glVertex3f(x2, y2, 0);
		glVertex3f(x2, y1, 0);
		glVertex3f(x1, y1, 0);
	glEnd();


	if (World::getInstance()->isLightingEnabled())
		glEnable(GL_LIGHTING);

	glDisable(GL_BLEND);

	glPopMatrix();
}

void RenderableWidgetConsole::drawLines(){

	// Calculate visible lines here
	__lineSeparation = (__fontH)*0.40;

	__nVisibleLines = (__y2-__y1) / (float) (__lineSeparation+__fontH);

	__nVisibleLines = __textLines.size()<__nVisibleLines?__textLines.size():__nVisibleLines;

	// Draw loop
	std::list<Line>::iterator it;

	it = __textLines.begin();

	for (unsigned i=0; i < __nVisibleLines; ++i){
		drawSingleLine(*it, i);
		++it;
	}

	glColor3f(1.0, 1.0, 1.0); // Restore the original color,
	                          // because we can use the no-lighting option

}

void RenderableWidgetConsole::drawSingleLine(Line theLine, unsigned nLine){

	// We draw for down to up.

	std::string tempText = theLine.getText();

	glPushMatrix();

	glDisable(GL_LIGHTING);

	glEnable(GL_TEXTURE_2D);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glNormal3f(0.0, 0.0, 1.0);

	glLoadIdentity();

	unsigned h = Configuration::getInstance()->getInstance()->openGLScreenHeight();

	glTranslatef(__x1 + __xMargin , __yMargin + h- __y2 + (__lineSeparation+__fontH)*nLine, 0);

	glColor4f(theLine.getColor()[0], theLine.getColor()[1], theLine.getColor()[2], theLine.getColor()[3]);

	__font->Render(theLine.getText().c_str());


	if (World::getInstance()->isLightingEnabled())
		glEnable(GL_LIGHTING);

	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);
	glPopMatrix();

}


void RenderableWidgetConsole::addNote(std::string msg){
	Line tempLine;
	tempLine.setText(msg);

	tempLine.setColor(GREEN);

	__textLines.push_front(tempLine);

}

void RenderableWidgetConsole::addWarning(std::string msg){
	Line tempLine;
	tempLine.setText(msg);

	tempLine.setColor(YELLOW);

	__textLines.push_front(tempLine);
}

void RenderableWidgetConsole::addError(std::string msg){
	Line tempLine;
	tempLine.setText(msg);

	tempLine.setColor(RED);

	__textLines.push_front(tempLine);
}




void RenderableWidgetConsole::init(){

//	cout << "RendeableWidgetConsole init" << endl;
	__font = FontFactory::getFontFactory()->getConsoleFont();

	__nVisibleLines = 0;
	__nLines = 0;
	__xMargin = 5;
	__yMargin = 10;

	__fontSize = FONT_SIZE;

	__font->FaceSize(__fontSize);

	// Calculate the height of the font
	FTBBox bbox  = __font->BBox((char*) "AbcpqRs");
	float x1, x2, y1, y2, z1, z2, cx, cy, cz;

	x1 = bbox.Lower().Xf(); y1 = bbox.Lower().Yf(); z1 = bbox.Lower().Zf();
	x2 = bbox.Upper().Xf(); y2 = bbox.Upper().Yf(); z2 = bbox.Upper().Zf();
	cx = - ((x2 - x1) /2 );
	cy = - ((y2 - y1) /2 );
	cz = - ((z2 - z1) /2 );

	__fontH= y2 - y1;


}

RenderableWidgetConsole::~RenderableWidgetConsole(){

}


}
