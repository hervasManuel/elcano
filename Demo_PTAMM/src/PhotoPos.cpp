#include <PhotoPos.h>

namespace mars{
  photoPos::photoPos()
  {
    iC = new Indexer_controler();
    pthread_create( &__thread_id, NULL, &photoPos::exec, this );
    rotation = 0;
    position.x = position.y = position.z = 0;
    pathPhotoData = "/home/fran/fotos/coordenadasFotos.txt";
    tree = NULL;
    
  }
  

  photoPos::~photoPos()
  {
    delete iC;
  }


  int
  photoPos::rotationToInt(std::string orientation)
  {
    if(orientation == "N")
      return 0;
    else if (orientation == "NO")
      return 45;
    else if (orientation == "O")
      return 90;
    else if (orientation == "SO")
      return 135;
    else if (orientation == "S")
      return 180;
    else if (orientation == "SE")
      return 225;
    else if (orientation == "E")
      return 270;
    else if (orientation == "NE")
      return 315;
    else
      return 0;
    
  }


  Image* 
  photoPos::obtainImage(std::string filePath, std::string fileName)
  {
    std::string fullName = filePath + fileName;
    std::ifstream fileStream(fullName.c_str());

    if(!fileStream.is_open()){
      std::cout << "Error opening the image" << std::endl;
      fileStream.close();
      return NULL;
    }
    else{
      Image* img = new Image(fileName,filePath);
      fileStream.close();
      return img;
    }
  }
 

  
  Image_comp* 
  photoPos::search(std::string imagePath, std::string imageName){

    Indexer_controler* Ind_Cont = new Indexer_controler();
    std::vector<Image*> searched_images;
    std::vector<Image*> images = Ind_Cont->getIndex();
    Comparator *c = new Comparator();

    
    //Obtaining the image to be compared
    Image* im = NULL;
    im = obtainImage(imagePath, imageName);

    
    Image* img = NULL;
    img = new Image(imageName,imagePath);

    if (im == NULL || img == NULL)
      {
        std::cout << "Algo pasa aquí" << std::endl;
      }
    
    if (im != NULL){
    
      if(tree == NULL){
        //std::cout << "Building Voronoi tree ... " << std::endl;
        tree = new VoronoiTree((int)sqrt(images.size()),images);
        //std::cout << "Voronoi tree builded" << std::endl;
      }
      
      int attempts = 0;
      
      do{
        searched_images = tree->searchVTree(1.0, im);
        if(searched_images.size() == 0){
          tree = new VoronoiTree((int)sqrt(tree->getRoot()->getImagesPlaneP().size()), tree->getRoot()->getImagesPlaneP());
          attempts++;
        }
      }while (attempts < 4 && searched_images.size() == 0 );

      if(searched_images.size() == 0){
        std::cout << "Images not found" << std::endl;
        return NULL;
      }


      else{
        std::vector<Image_comp*> listImComp;
        Comparator *_comparator = new Comparator();
        for(int i=0; i < searched_images.size();i++){
          Image_comp *_imgComp = new Image_comp(searched_images.at(i), _comparator->compareImages(searched_images.at(i),im));
          listImComp.push_back(_imgComp);
        }

        quicksort(listImComp, 0, listImComp.size()-1);

        Image_comp *im_comp_searched = listImComp.at(0);

        for(int i = 1; i < listImComp.size(); i++)
          if(listImComp.at(i)->getComp() < im_comp_searched->getComp())
            im_comp_searched = listImComp.at(i);

        delete im;
        delete img;
        return im_comp_searched;
      }
    }
    
    else{
      return NULL;
    }
  }

 
  void 
  photoPos::searchPos(std::string name, int* rot, Elcano::pos *position)
  {
    
    bool searchedPhoto = false;

    char* namePhotoArray = (char*)name.c_str();
    std::ifstream fileStream((char*)pathPhotoData.c_str() , std::ifstream::in);

    if(!fileStream.is_open()){
      std::cout << "Error opening the file" << std::endl;
    }
    else{
      char c;
      int i = 0;
      while((fileStream.good()) && !searchedPhoto) {
        c = (char) fileStream.get();
        if(namePhotoArray[i] == (char) c) {
          //if c is 11, it coincides the name of the photo
          if(i == name.size() - 1)
            searchedPhoto = true;
          i++;
        }
        else i = 0;
      }

      //When the photo is known, the data of position and rotation are extracted

      std::string posX = "";

      c = (char) fileStream.get();

      while((c = (char)fileStream.get()) != ' ' ){
        posX = posX + (char)c;
      }

      std::string posY = "";

      while((c = (char)fileStream.get()) != ' ' ){
        posY = posY + (char)c;
      }

      position->x = atof(posX.c_str());
      position->y = atof(posY.c_str());
      position->z = 0.0;

      String rotation = "";

      while((c = (char)fileStream.get()) != '\n' && (char)c != ' '){
        rotation = rotation + (char)c;
      }

      *rot = rotationToInt(rotation);

      fileStream.close();
    }
   
  }

  void 
  photoPos::quicksort(vector<Image_comp*> _imgComp, int begin, int end)
  {
    int pivot;

    if(begin < end){
      pivot = divide(_imgComp,begin,end);
      quicksort(_imgComp,begin,pivot - 1 );
      quicksort(_imgComp,pivot + 1, end);
    }
  }



  int
  photoPos::divide(std::vector<Image_comp*> _imgComp, int begin, int end)
  {
    
    int left, right;
    float pivot;
    Image_comp *temp;
    pivot = _imgComp.at(begin)->getComp();

    left = begin;
    right = end;

    while (left < right){

      while (_imgComp.at(right)->getComp() > pivot){
        right--;
      }

      while ((left < right) && (_imgComp.at(left)->getComp() <= pivot)){
        left++;
      }

      if(left < right){
        std::swap(_imgComp[left],_imgComp[right]);
      }
    }

    std::swap(_imgComp[left],_imgComp[right]);

    return right; 
  }
  
  
  void* 
  photoPos::exec(void* thr)
  {
    ((photoPos*)thr)->execute();
  }
 

  void photoPos::execute()
  {
    while(1)
      {
        if((searched_img = search("/home/fran/elcano/DemoPTAMM/","frame.jpg"))!= NULL)
          {
          //Searching the rotation and position of the image
            
          searchPos(searched_img->getImage()->getName(), &rotation, &position);
          }
        
        
        sleep(2);
      }
  }


  Elcano::pos photoPos::getPos()
  {
    return position;
  }


  int photoPos::getRot()
  {
    return rotation;
  }

}
