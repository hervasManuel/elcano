/*
 * TrackingMethod.cpp
 *
 */

#include "TrackingMethod.h"




namespace mars {



TrackingMethod::TrackingMethod() {
	__maxHistoricalRecords=20; // 20 by default.
	__historicalRecordOfPerceptions.reserve(20); // Reserve 20 slots.

	__done = false;

	__ready = false;
	__slowMode = false;

	__threadError = -1;

	__detecting=false;
#ifndef NDEBUG
	std::cout << "[DEBUG]: Creating tracking method: " ;
#endif

	pthread_mutex_init(&mutex_tracking, 0);

#ifndef NDEBUG
	std::cout << "TrackingMethod:: Creating thread... ";
#endif


}

TrackingMethod::TrackingMethod(const unsigned int& nRecords) {
	__maxHistoricalRecords= nRecords;
	__historicalRecordOfPerceptions.reserve(nRecords); // Reserve nRecords slots.
}

vector<tUserL*>* TrackingMethod::getHistoricalRecord(){
  return &__historicalRecordOfPerceptions;
}

void* TrackingMethod::exec(void* thr){

#ifndef NDEBUG
	std::cout << "Waiting for a valid Tracking Source." << endl;
#endif

	while(!((TrackingMethod*) thr)->isReady()){}

	if(!((TrackingMethod*) thr)->isDone())
		((TrackingMethod*) thr)->Thread();
	return NULL;
}


bool TrackingMethod::isReady(){
	return __ready;
}

void TrackingMethod::startMethod(){
	__threadError = pthread_create( &__thread_id, NULL, &TrackingMethod::exec, this );

	Logger::getInstance()->note("TrackingMethod " + __methodName + " :: starting...");

	if (!__threadError){
#ifndef NDEBUG
		std::cout << "[DEBUG] : ... created! (" << __thread_id << ")" << endl << flush;
#endif
	}
	else{
		stringstream msg;
		msg << "TrackingMethod:: creating Thread - " << __threadError;
		Logger::getLogger()->error(msg.str());
	}
}

string TrackingMethod::getMethodName(){
	return __methodName;
}

bool TrackingMethod::isDone(){
	return __done;
}




void TrackingMethod::addRecord(const tUserL& userL){
//	lock();
	__userL = userL;
#ifndef NDEBUG
	cout << "----" << endl;
	cout << "Adding an User Location/perception" << endl;
	cout << "\t· Eye: " << __userL.__Eye << endl;
	cout << "\t· LookTo: " << __userL.__LookTo << endl;
	cout << "\t· Up: " << __userL.__Up << endl;
	cout << "----" << endl << flush;

#endif

	if (__historicalRecordOfPerceptions.size()==__maxHistoricalRecords){

		delete __historicalRecordOfPerceptions.at(0);
		__historicalRecordOfPerceptions.erase( __historicalRecordOfPerceptions.begin() );
	} // If it's full, we get rid of the first record


	//Copy constructor ...
	__historicalRecordOfPerceptions.push_back(new tUserL(__userL));

//	unlock();

}

tUserL* TrackingMethod::getLastUserL(){
	return &__userL;
}

void TrackingMethod::lock(){
	pthread_mutex_lock(&mutex_tracking);
}

void TrackingMethod::unlock(){
	pthread_mutex_unlock(&mutex_tracking);
}

const tUserL TrackingMethod::Mat2TUserL(Matrix16& m, const float& weight){
	tUserL temp;
	GLfloat* data = m.getData();
	temp.__Eye.setComponets(data[12], data[13], data[14]);
	temp.__LookTo.setComponets(data[8]+data[12], data[9]+data[13], data[10] + data[14]);
	temp.__Up.setComponets(data[4], data[5], data[6]);
	temp.__Weight = weight;
	return temp;
}


void TrackingMethod::stopMethod(){
	__done = true;
	lock();
}


bool TrackingMethod::isDetecting(){
  return __detecting;
}

TrackingMethod::~TrackingMethod() {

		if (!__threadError){
#ifndef NDEBUG
	std::cout << "[DEBUG]: TrackingMethod:: " << __methodName << ":: Waiting for the thread (" << __thread_id
			<< ") to finish and join..." << endl << flush;
#endif

	pthread_join(__thread_id, NULL);
		}

#ifndef NDEBUG
	std::cout << "done!" << std::endl << std::flush;
#endif

	pthread_mutex_destroy(&mutex_tracking);


}

  void TrackingMethod::pause(){
    __paused = true;
  }
  void TrackingMethod::resume(){
    __paused = false;
  }

  void TrackingMethod::slowMode(){
    __slowMode = true;
    
}
  void TrackingMethod::fastMode(){
    __slowMode = false;
  }

}
