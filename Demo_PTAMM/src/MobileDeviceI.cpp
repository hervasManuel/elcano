#include "MobileDeviceI.h"

void MobileDeviceI::notifyRoute(const OpenLS::Route &theRoute, const Ice::Current&)
{
  std::cout << "Ruta" << std::endl;
  _ruta = theRoute;
}

void MobileDeviceI::notifyTasks(const TaskSeq &tasks, const Ice::Current&)
{
  std::cout << "Tarea recibida" << std::endl;
  _tareas = tasks;

}

OpenLS::Route MobileDeviceI::getRoute()
{
  return _ruta;
}
TaskSeq MobileDeviceI::getTasks()
{
  return _tareas;
}
