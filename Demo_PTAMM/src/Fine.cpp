#include "Fine.h"

int Fine::_operation;
bool Fine::_axisX;
bool Fine::_axisY;
bool Fine::_axisZ;
float Fine::_posX;
float Fine::_posY;
float Fine::_posZ;
float Fine::_rotX;
float Fine::_rotY;
float Fine::_rotZ;
mars::Renderable* Fine::_r1;
mars::Renderable* Fine::_r2;
mars::TrackingMethodOAbsolutePTAMM* Fine::_tM;


void Fine::updateRenderable(){
   _r1->setPosition(_posX, _posY, _posZ);
  _r1->setAngles(_rotX,_rotY,_rotZ);

  _r2->setPosition(_posX, _posY, _posZ);
  _r2->setAngles(_rotX,_rotY,_rotZ);

}

Fine::Fine(mars::TrackingMethodOAbsolutePTAMM* t, mars::Renderable* r1,mars::Renderable* r2)
{
  _tM = t;
  _r1 = r1;
  _r2 = r2;
  
  _axisX = true;
  _axisY = false;
  _axisZ = false;
  _operation = ROTATE;
  _posX = r1->getPosition().x;
  _posY = r1->getPosition().y;
  _posZ = r1->getPosition().z;
  _rotX = r1->getAngles()[0];
  _rotY = r1->getAngles()[1];
  _rotZ = r1->getAngles()[2];

  std::cout<<"Renderable's initial pose"<<std::endl;
  std::cout<<"Pos: "<<_posX<<", "<<_posY<<", "<<_posZ<<std::endl;
  std::cout<<"Rot: "<<_rotX<<", "<<_rotY<<", "<<_rotY<<std::endl;
  
}

Fine::~Fine(){}

void Fine::rotateBut(void* arg){ _operation = ROTATE;}
void Fine::dispBut(void* arg) { _operation = DISPLACE;}

void Fine::actX(void* arg){
  _axisX = true;
  _axisY = false;
  _axisZ = false;
}

void Fine::actY(void* arg)
{
  _axisX = false;
  _axisY = true;
  _axisZ = false;
}

void Fine::actZ(void* arg)
{
  _axisX = false;
  _axisY = false;
  _axisZ = true;
}

void Fine::increment(void* arg)
{
  switch (_operation){
  case ROTATE:
    if(_axisX){
      _rotX += ROT;
    }else if(_axisY){
      _rotY += ROT;
    }else{
      _rotZ += ROT;
    }
    break;
  case DISPLACE:
    if(_axisX){
      _posX += ADD;
    }else if(_axisY){
      _posY += ADD;
    }else{
      _posZ += ADD;
    }
    break;
  }
  updateRenderable();
}

void Fine::decrement(void* arg)
{
  switch (_operation){
  case ROTATE:
    if(_axisX){
      _rotX -= ROT;
    }else if(_axisY){
      _rotY -= ROT;
    }else{
      _rotZ -= ROT;
    }
    break;
  case DISPLACE:
    if(_axisX){
      _posX -= ADD;
    }else if(_axisY){
      _posY -= ADD;
    }else{
      _posZ -= ADD;
    }
    break;
  }
  updateRenderable();
}




void Fine::incrementPlus(void* arg)
{
  switch (_operation){
  case ROTATE:
    if(_axisX){
      _rotX += ROT*30;
    }else if(_axisY){
      _rotY += ROT*30;
    }else{
      _rotZ += ROT*30;
    }
    break;
  case DISPLACE:
    if(_axisX){
      _posX += ADD*30;
    }else if(_axisY){
      _posY += ADD*30;
    }else{
      _posZ += ADD*30;
    }
    break;
  }
  updateRenderable();
}

void Fine::decrementMinus(void* arg)
{
  std::cout<<"Decrement MINUS"<<std::endl;
  switch (_operation){
  case ROTATE:
    if(_axisX){
      _rotX -= ROT*30;
    }else if(_axisY){
      _rotY -= ROT*30;
    }else{
      _rotZ -= ROT*30;
    }
    break;
  case DISPLACE:
    if(_axisX){
      _posX -= ADD*30;
    }else if(_axisY){
      _posY -= ADD*30;
    }else{
      _posZ -= ADD*30;
    }
    break;
  }
  updateRenderable();
}




