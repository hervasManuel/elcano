#include <LocationListenerI.h>

LocationListenerI::LocationListenerI()
{
  //_pos = NULL;
}
void LocationListenerI::locateReport(const MLP::Position& pos, const Ice::Current&) const
{
  MLP::PointPtr p;
  std::string iface = pos.theShape->ice_id();
  if(iface == "::MLP::Point"){
    p = MLP::PointPtr::dynamicCast(pos.theShape);
  }
  _pos->center = p->center;
}
LocationListenerI::~LocationListenerI(){}

MLP::PointPtr LocationListenerI::getPos()
{
  return _pos;
}
void LocationListenerI::locateSeqReport(const MLP::PositionSeq& a, const Ice::Current&)const {}
void LocationListenerI::locateRangeReport(const MLP::PositionSeq& a, const Ice::Current&)const {}

