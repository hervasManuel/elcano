/*
 * RenderableLineDemo.cpp
 *
 */

#include "RenderableLineDemo.h"

namespace mars {

void RenderableLineDemo::setP(Point3D b, Point3D e){
	_b = b;
	_e = e;
}


void RenderableLineDemo::draw(){
	glPushMatrix();


	glLoadIdentity();

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glLineWidth(10);

	glDisable(GL_TEXTURE_2D);
	glDisable(GL_LIGHTING);

#if 0//ndef NDEBUG
	cout << "." << endl;
//	cout << "[DEBUG]: color -> " << __color[0] << " , "  << __color[1] << " , "
//			 << __color[2] << " , "  << __color[3] << endl;
#endif

	glColor4f(0.5, 1, 0.5, 0.5);

	glBegin(GL_LINES);
		glVertex3f( _b.x, _b.y, _b.z );
		glVertex3f( _e.x, _e.y, _e.z );
	glEnd();

	glDisable(GL_BLEND);

	if (World::getInstance()->isLightingEnabled())
		glEnable(GL_LIGHTING);

	glPopMatrix();

	glColor4f(1, 1, 1, 1);
}

RenderableLineDemo::RenderableLineDemo() {
	// TODO Auto-generated constructor stub

}

RenderableLineDemo::~RenderableLineDemo() {
	// TODO Auto-generated destructor stub
}

}
