/* -*- coding: utf-8; mode: c++ -*- */

#ifndef PROFILE_ICE
#define PROFILE_ICE


#include <Ice/BuiltinSequences.ice>


module Elcano {

  class DevProfile {
	string mac;
	string bdaddr;
	string rfid;
  };

  class UserProfile extends DevProfile {
	string name;
	Ice::IntSeq tasks;
  };

  class HandicapProfile extends UserProfile {
	bool blind;
	bool deaf;
	bool wheelchair;
  };

};


#endif
