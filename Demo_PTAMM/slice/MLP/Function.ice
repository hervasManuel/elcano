/* -*- c++ -*-
   5.2.2.2 Function Element Definitions

*/

#include <MLP/Location.ice>


module MLP {

  enum EmeTrigger {
    EmeOrg,
    EmeRel
  };

  struct EmeEvent {
    EmeTrigger trigger;
    EmePosSeq  pos; // 1..n
  };


  class TargetArea {
  };

  class Shape: TargetArea {
  };

  class Cc: TargetArea {
  };

  class Plmn: TargetArea {
    Mcc theMcc;
    Mnc theMnc;
  };

  sequence<byte> Interval;

  enum LocType {
    Current,
    Last,
    CurrentOrLast,
    CurrentOrLastAsFallback,
    Initial
  };

  enum Prio {
    Normal,
    High
  };

  struct PushAddr {
    string url;
    StringSeq id;   // 0..1
    StringSeq pwd;  // 0..1
  };

  class NameArea: TargetArea {
    string name;
  };

  sequence<byte> ReqId;

  struct DateTime {
    ByteSeq data;
    int utcOff;
  };

  class TlrrEvent {
  };

  enum MsActionType {
    MsAvail
  };

  class MsAction: TlrrEvent {
    MsActionType type;
  };

  enum ChangeAreaType {
    MsEntering,
    MsLeaving,
    MsWithinArea
  };

  class ChangeArea : TlrrEvent {
    TargetArea target;
    int noOfReports;    // 0 means not available
    bool locEstimates;
  };

};
