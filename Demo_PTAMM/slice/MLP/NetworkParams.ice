/* -*- c++ -*-

   5.2.2.7 Network Parameters Element Definitions

*/

#include <Ice/BuiltinSequences.ice>

module MLP {

  struct Cgi {
    string mcc;
    string mnc;
    string lac;
    string cellId;
  };

  sequence<Cgi> CgiSeq;

  struct Vmscid {
    string cc;
    string ndc;
    int vmscNo;
  };

  struct Vlrid {
    string cc;
    string ndc;
    int vlrNo;
  };

  struct Neid {
    Vmscid theVmscid;
    Vlrid  theVlrid;
  };

  sequence<Neid> NeidSeq;

  struct GsmNetParam {
    CgiSeq cgi;
    NeidSeq neid;
    Ice::StringSeq nmr;
    Ice::StringSeq ta;
    Ice::StringSeq lmsi;
    Ice::StringSeq imsi;
  };

};
