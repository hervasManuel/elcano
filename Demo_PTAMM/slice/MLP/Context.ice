/* -*- c++ -*-

   5.2.2.8 Context Element Definitions

*/

module MLP {

  // Context is implicitly handled by Ice

  // Requestor must be made explicit if needed.  Indeed it
  // should be avoided in order to allow easier deployment of
  // IceStorm channels.

};
