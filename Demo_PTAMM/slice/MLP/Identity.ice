/* -*- c++ -*-

   5.2.2.1 Identity Element Definitions

*/


module MLP {

  enum MsIdType {
    MsIsdn,
    Imsi,
    Imei,
    Min,
    Mdn,
    EmeMsId,
    AsId,
    OpeId,
    IPv4,
    IPv6,
    SessId,
    SipUri,
    TelUrl
  };

  sequence<MsIdType> MsIdTypeSeq;

};

