/* -*- c++ -*-

   5.2.2.4 Location Element Definitions
   5.2.3.2 Standard Location Inmediate Service

   There are two competing alternatives here.  The SLI Service is
   described at the message passing level.  Nonetheless the "SYNC"
   res_type is a way to ask for synchronous responses.  That is, SYNC
   is equivalent to a non-void remote method invocation.  Ice provides
   transparent async invocations but these are not meant to be used in
   event channels.

   At this moment we cannot anticipate the needs of Hesperia partners.
   Therefore we provide a dual set of interfaces.

   Sessions and passwords are handled at the Glacier Session level.
   Therefore no authentication data is required.  Very specific
   implementations may use the invocation context to pass any required
   token.

   Similar considerations may be done with respect to msid. A location
   service is likely to be specific to a given MsIdType.  If not, you
   may always provide MsIdType information either in the msId string
   (URI) or in the per-method context.

   Note that the efficiency penalty due to disallowing mixed
   ranges/sequences of msids would be negligible if you use batched
   invocations.

   It is not clear whether correlation between requests and responses
   in the asynchronous interface is still useful.  However you may
   always provide per-method context to find correlated
   requests/reports.

   All OMA interfaces are defined as a single set of synchronous and
   asynchronous interfaces. Synchronous interfaces are meant to be used
   for StandardLocationInmediate and EmergencyLocationInmediate.
   Triggered locations and Reporting services may be implemented
   through the asynchronous API using the Admin API (see below).
   EmergencyLocation is handled with privileged event channels (no
   need for a new interface).

   Information on CoordinateSystems and units is provided at the
   service provider level.

   Implied attributes are passed through per-method context.

*/

#ifndef MLP_LOCATION_ICE
#define MLP_LOCATION_ICE


#include <MLP/Shape.ice>
#include <Ice/BuiltinSequences.ice>

module MLP {

  struct LocationDataInfo {
    string srsName;  // "www.epsg.org/#4326" by default
    string distanceUnit; // "degree" by default
    string angularUnit;  // "meter" by default
  };

  /*
    MsIds use an URI notation: 'type:id'

    Standard types: msisdn, imsi, imei, min, mdn, eme_msid,
    asid, ope_id, ipv4, ipv6, sessid, sip_uri, tel_url
  */
  struct Position {
    string msid;
    long time;
    Shape theShape;
  };

  sequence<Position> PositionSeq;

  exception NotSupported {
    string reason;
  };

  exception NotFound {
    string msid;
  };

  exception NotFoundMultiple {
    Ice::StringSeq msids;
  };

  interface LocationDataProvider {
    ["nonmutating", "cpp:const"]
      idempotent LocationDataInfo getLocationDataInfo();
  };

  // Synchronous interface
  interface Location extends LocationDataProvider {
    ["nonmutating", "cpp:const"]
      idempotent string getDefaultMsidType();

    ["nonmutating", "cpp:const"]
      idempotent Ice::StringSeq getAllowedMsidTypes();

    ["nonmutating", "cpp:const"]
      idempotent Position locate(string msid)
      throws NotSupported, NotFound;

    ["nonmutating", "cpp:const"]
      idempotent PositionSeq locateSeq(Ice::StringSeq msids)
      throws NotSupported, NotFoundMultiple;

    ["nonmutating", "cpp:const"]
      idempotent PositionSeq locateRange(string firstMsid, string lastMsid)
      throws NotSupported, NotFoundMultiple;

  };

  // Asynchronous Request
  interface LocationRequest extends LocationDataProvider  {
    ["nonmutating", "cpp:const"]
      idempotent void locateRequest(string msid);

    ["nonmutating", "cpp:const"]
      idempotent void locateSeqRequest(Ice::StringSeq msids);

    ["nonmutating", "cpp:const"]
      idempotent void locateRangeRequest(string firstMsid, string lastMsid);
  };

  // Asynchronous Report
  interface LocationListener {
    ["nonmutating", "cpp:const"]
      idempotent void locateReport(Position pos);

    ["nonmutating", "cpp:const"]
      idempotent void locateSeqReport(PositionSeq pos);

    ["nonmutating", "cpp:const"]
      idempotent void locateRangeReport(PositionSeq pos);

  };

  // Asynchronous Admin service
  interface LocationAdmin extends LocationDataProvider {
    void addListener(LocationListener* listener);
    void removeListener(LocationListener* listener);
  };

};


#endif

