/* -*- c++ -*-

   5.2.2.6 Quality of Position Element Definitions

*/


module MLP {

  enum DelayReq {
    NoDelay,
    LowDelay,
    DelayTol
  };

  enum QoSGuarantee {
    Assured,
    BestEffort
  };


  struct QoP {
    QoSGuarantee qosClass;
    // FIXME: what is ll_acc and hor_acc
  };

  struct EQoP {
    QoP      qop;
    DelayReq respReq;
    long     respTimer;
    long     maxLocAge;
  };


};
