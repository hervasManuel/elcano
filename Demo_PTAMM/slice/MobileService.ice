/* -*- coding: utf-8; mode: c++ -*- */

#ifndef MOBILESERVICE_ICE
#define MOBILESERVICE_ICE


#include <MLP/Shape.ice>
#include <OpenLS/RouteService.ice>

#include <Profile.ice>
#include <Task.ice>


module Elcano {

  interface MobileDevice {

	void notifyRoute(OpenLS::Route theRoute);
	void notifyTasks(TaskSeq tasks);

  };


  struct AccessPoint {
	string mac;
	int power;
  };

  sequence<AccessPoint> AccessPoints;


  interface MobileService {

	void beginTask(string taskName, DevProfile mobile);
	void endTask(string taskName, DevProfile mobile);

	["ami"] MLP::Shape getPosition(DevProfile mobile);

	//void notifyAPList(AccessPoints theAPs, DevProfile mobile);

  };


};

#endif
