/* -*- coding: utf-8; mode: c++ -*- */

#ifndef WIFIESTIMATOR_ICE
#define WIFIESTIMATOR_ICE


#include <MobileService.ice>


module Elcano {

  interface WifiEstimator {
    //"amd"
    ["ami"] void notifyAPList(AccessPoints theAPs, DevProfile mobile);

  };

};


#endif
