/* -*- coding: utf-8; mode: c++ -*- */

#ifndef USERMANAGER_ICE
#define USERMANAGER_ICE


#include <MobileService.ice>

module Elcano {

  interface UserManager {
	void login(DevProfile profile, MobileDevice* mobileProxy);
	void exit(DevProfile profile);
  };

};


#endif
