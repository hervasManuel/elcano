/* -*- c++ -*-

   Based on OpenLS specification
   8.3 Directory Service
   Annex A.1: ADT.xsd, LocationUtilityService.xsd

*/

#include <OpenLS/LocationTypes.ice>


module OpenLS {

  struct GeocodedAddress {
    MLP::Point point;
    string address;
  };

  sequence<GeocodedAddress> GeocodedAddressSeq;

  interface GeocodingService {
    ["nonmutating", "cpp:const", "amd"]
      idempotent GeocodedAddressSeq geocode(AddressSeq addresses);

    ["nonmutating", "cpp:const", "amd"]
      idempotent GeocodedAddressSeq reverseGeocode(MLP::Position pos);
  };

};

