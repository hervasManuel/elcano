/* -*- c++ -*-

   Based on OpenLS specification
   8.1 Directory Service
   Annex A.1: ADT.xsd, DirectoryService.xsd

   Distances assumed to be in meters.
*/

#include <OpenLS/LocationTypes.ice>


module OpenLS {

  enum RequestType {
    SimpleReq,
    AddressReq,
    NearestReq,
    WithinDistanceReq,
    WithinBoundaryReq
  };

  dictionary<string, string> SelectionCriteriaDict;

  // Synchronous interface, offers client syn + asyn calls
  interface DirectoryService extends MLP::LocationListener {
    ["nonmutating", "cpp:const", "ami", "amd"]
      idempotent POISeq findLocation(SelectionCriteriaDict selCriteria)
      throws MissingData;

    ["nonmutating", "cpp:const", "ami", "amd"]
      idempotent POISeq findLocationFromAddress(string address,
                                                SelectionCriteriaDict selCriteria)
      throws MissingData;

    // Defaults criterion to Proximity, therefore only requires a POI
    ["nonmutating", "cpp:const", "ami", "amd"]
      idempotent POISeq findLocationNearest(Location loc,
                                            SelectionCriteriaDict selCriteria)
      throws MissingData;

    ["nonmutating", "cpp:const", "ami", "amd"]
      idempotent POISeq findLocationWithinDistance(Location loc,
                                                   double maxDistance,
                                                   double minDistance,
                                                   SelectionCriteriaDict selCriteria)
      throws MissingData;

    ["nonmutating", "cpp:const", "ami", "amd"]
      idempotent POISeq findLocationWithinBoundary(MLP::Shape aoi,
                                                   SelectionCriteriaDict selCriteria)
      throws MissingData;
  };

};
