/* -*- c++ -*-

   Based on OpenLS specification
   8.5 Route Service
   Annex A.1: ADT.xsd, RouteService.xsd

   Distances assumed to be in meters.
*/

#include <OpenLS/LocationTypes.ice>

module OpenLS {

  sequence<PointOfInterest> AvoidSeq;

  struct RoutePlan {
    string preference;
    ["uml:multi:2..n"] LocationSeq waypoints; //Including start and end points (in order: start->WP1->...->WPn->end)
    ["uml:multi:0..1"] AvoidSeq avoidList;
  };

  dictionary<string, string> ParameterDict;

  struct RouteSummary {
    // For duration see http://www.w3.org/TR/xmlschema-2/#duration
    string Duration; // Represented as PnYn MnDTnH nMnS
    double distance;
    MLP::Polygon boundingBox;
  };

  dictionary<string, string> RouteDetails;
  sequence<RouteDetails> RouteDetailsSeq;

  struct Route {
    RouteSummary summary;
    ["uml:multi:0..1"] MLP::LineStringSeq geometry;
    ["uml:multi:0..1"] RouteDetailsSeq details;
  };

  interface RouteService {
    ["nonmutating", "cpp:const", "amd"]
      idempotent Route findRoute(RoutePlan plan, ParameterDict params)
      throws MissingData;
  };

};

