/* -*- c++ -*-

   Based on OpenLS specification
   Annex A.1: ADT.xsd

   Distances assumed to be in meters.
*/

#include <MLP/Location.ice>


module OpenLS {

  exception MissingData {
    string reason;
  };

  dictionary<string, string> AttributeDict;
  sequence<AttributeDict> AttributeSeq;
  sequence<string> AddressSeq;

  sequence<MLP::Shape> ShapeSeq;

  // A PoI is a "location with a fixed position"
  struct PointOfInterest {
    ["uml:multi:0..1"] AttributeSeq attributes; // eg ID, name, type, ...
    ["uml:multi:0..1"] MLP::PointSeq point;
    ["uml:multi:0..1"] AddressSeq address;
  };

  sequence<PointOfInterest> POISeq;

  enum LocationType {
    Address,
    Position,
    PoI
  };

  struct Location {
    LocationType ltype;
    MLP::Position position;
    PointOfInterest poi;
    string address;
  };

  sequence<Location> LocationSeq;

};
