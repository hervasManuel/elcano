/* -*- coding: utf-8; mode: c++ -*- */

#ifndef PERSISTENTPROFILE_ICE
#define PERSISTENTPROFILE_ICE


#include <Profile.ice>
#include <Synchronization.ice>
#include <MobileService.ice>


module Elcano {

  interface DevProfileRW {
	["freeze:write"] void setDevProfile(DevProfile profile);
	DevProfile getDevProfile();
  };

  interface UserProfileRW extends DevProfileRW {
	["freeze:write"] void setUserProfile(UserProfile profile);
	UserProfile getUserProfile();
  };

  interface HandicapProfileRW extends UserProfileRW {
	["freeze:write"] void setHandicapProfile(HandicapProfile profile);
	HandicapProfile getHandicapProfile();
  };

  class PersistentDevProfile extends DevProfile
	implements DevProfileRW {

	MobileDevice* mobile;
	MobileDevice* getMobileProxy();
  };

  class PersistentUserProfile extends UserProfile
	implements UserProfileRW{};

  class PersistentHandicapProfile extends HandicapProfile
	implements HandicapProfileRW{};


  exception UserStatusException {
	string msg;
  };

  exception UnknownProfile extends UserStatusException{};
  exception InvalidProfile extends UserStatusException{};

  interface UserStatusAdmin {
	PersistentDevProfile* add(DevProfile profile, MobileDevice* mobile)
	  throws InvalidProfile;
	PersistentDevProfile* get(DevProfile profile) throws UnknownProfile;
	void remove(DevProfile profile) throws UnknownProfile;
	bool exist(DevProfile profile);
  };


  // Syncronization classes

  class EventDevProfile extends Synchronization::Event {
	DevProfile profile;
  };

  sequence<EventDevProfile> EventDevProfileSeq;

  interface SynchronizableProfile extends
	Synchronization::SynchronizableAbstract {

	/**
	 * This method provide the last event of new server and receive a
	 * list from old server with the necessary events for its
	 * synchronization
	 **/
	EventDevProfileSeq synchronize(EventDevProfile lastEvent);

	/**
	 * Notify the event to others servers
	 **/
    void notify(EventDevProfile newEvent);

  };


};


#endif
