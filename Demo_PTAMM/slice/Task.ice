/* -*- coding: utf-8; mode: c++ -*- */

#ifndef TASK_ICE
#define TASK_ICE


#include <MLP/Shape.ice>


module Elcano {

  struct Task {
	string name;
	string description;
	MLP::Shape site;
  };

  sequence<Task> TaskSeq;

  class TaskNode {
	string topic;
	TaskSeq tasks;
  };
  sequence<TaskNode> TaskNodeSeq;

  class TaskHierarchy extends TaskNode {
	TaskNodeSeq childs;
  };


  exception TaskException {
	string msg;
  };

  exception UnknownTopic extends TaskException{};
  exception UnknownTask extends TaskException{};
  exception UncoveredArea extends TaskException{};
  exception InvalidHierarchy extends TaskException{};


  interface TaskManagement {

	string getBuilding();

	TaskHierarchy getHierarchy();

	TaskHierarchy getHierarchyToDepth(short depth);
	TaskHierarchy getHierarchyFromTopic(string topic);

	Task getTask(string name)
	  throws UnknownTask;
	TaskSeq getTasksByTopic(string topic)
	  throws UnknownTopic;
	TaskSeq getTasksByArea(MLP::Shape area)
	  throws UncoveredArea;

  };


  interface TaskManagementAdmin {

	void setHierarchy(TaskHierarchy hierarchy)
	  throws InvalidHierarchy;

	void addTopic(string tpcFather, string newTopic)
	  throws UnknownTopic;

	void addTaskToTopic(string topic, Task theTask)
	  throws UnknownTopic;

	void removeTaskToTopic(string topic, Task theTask)
	  throws UnknownTopic;

	void removeTopic(string topic)
	  throws UnknownTopic;

  };


};


#endif
