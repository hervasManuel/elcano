/* -*- coding: utf-8; mode: c++ -*- */

#ifndef BUILDING_ICE
#define BUILDING_ICE


#include <MLP/Shape.ice>


module Elcano {

  enum Technology {Wifi, Bluetooth};

  class BuildingPoint {
	string name;
	Technology tech;
	MLP::Shape area;
  };

  class WifiPoint extends BuildingPoint {
	int channel;
	double frecuency;
  };

  class BluetoothPoint extends BuildingPoint {
	int radius;
  };

  sequence<BuildingPoint> BuildingPointSeq;


  exception BuildingException {
	string msg;
  };

  exception UnknownTech extends BuildingException{};
  exception UncoveredArea extends BuildingException{};


  interface BuildingManager {

	string getName(); //To identifier the building
	MLP::Shape getArea();

	BuildingPointSeq getPoints(); // Get points

	BuildingPointSeq getPointsByTech(string tech)
	  throws UnknownTech;

	BuildingPointSeq getPointsInArea(MLP::Shape area)
	  throws UncoveredArea;

	BuildingPointSeq getPointsInAreaByTech(MLP::Shape area, string tech)
	  throws UnknownTech, UncoveredArea;

  };


  interface BuildingManagerAdmin {

	void addPoints(BuildingPointSeq points); // Add points

	void removePoints(BuildingPointSeq points); // Remove points

	void removePointsByTech(string tech)
	  throws UnknownTech;

  };


};


#endif
