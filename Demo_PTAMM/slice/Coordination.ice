/* -*- c++ -*- ; -*- tab-width:4 -*- */

#ifndef COORDINATION_ICE
#define COORDINATION_ICE


module Coordination {

  enum Status {Stable, Election};

  interface Bully {
    void coordinator(int pid);
    void election(int pid);
    void response(int pid);
  };

};


#endif
