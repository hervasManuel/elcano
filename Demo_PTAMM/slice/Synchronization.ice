/* -*- c++ -*- ; -*- tab-width:4 -*- */

#ifndef SYNCHRONIZATION_ICE
#define SYNCHRONIZATION_ICE


module Synchronization {

  interface LogicClock {
	void cycle(int timeStamp);
  };

  enum EventType {add, remove, update};

  class Event{
	int logicTimeStamp;
	string id;
	EventType type;
  };

  interface SynchronizableAbstract;


  interface SynchronizableAbstract {

	/**
	 * To detect the server type
	 **/
	bool isType(string type);

	/**
	 * Subscribe the new server into the old
	 **/
    void subscribe(SynchronizableAbstract* prx);

	/**
	 * Unsubscribe the new server from the old
	 **/
    void unsubscribe(SynchronizableAbstract* prx);

  };

};


#endif
