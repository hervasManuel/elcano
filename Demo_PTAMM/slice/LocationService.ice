/* -*- coding: utf-8; mode: c++ -*- */

#ifndef LOCATIONSERVICE_ICE
#define LOCATIONSERVICE_ICE

#include <MLP/Location.ice>
#include <Profile.ice>


module Elcano {

  sequence<MLP::Shape> ShapeSeq;
  sequence<DevProfile> DevProfileSeq;


  exception LocationServiceException {
	string msg;
  };

  exception UnknownIdentifier extends LocationServiceException{};
  exception InvalidProxy extends LocationServiceException{};
  exception UncoveredArea extends LocationServiceException{};


  interface LocationService;

  interface LocationService {

	MLP::Shape getArea(); // Federation

	void federate(MLP::Shape area, Elcano::LocationService* locationService)
	  throws UncoveredArea;

	["ami", "amd"] MLP::Position getPosition(string id)
	  throws UnknownIdentifier;

	void trackingDevice(string id, MLP::LocationListener* listener)
	  throws UnknownIdentifier, InvalidProxy;

	["ami", "amd"] DevProfileSeq usersIntoArea(MLP::Shape area)
	  throws UncoveredArea;

	void watchArea(MLP::Shape area, MLP::LocationListener* listener)
	  throws UncoveredArea;

  };


};


#endif
